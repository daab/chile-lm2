var ciudadesvw = [];

ciudadesvw.push("Armenia")
ciudadesvw.push("Barranquilla")
ciudadesvw.push("Bogotá")
ciudadesvw.push("Bucaramanga")
ciudadesvw.push("Cali")
ciudadesvw.push("Cartagena")
ciudadesvw.push("Chía")
ciudadesvw.push("Cúcuta")
ciudadesvw.push("Ibague")
ciudadesvw.push("Manizales")
ciudadesvw.push("Medellín")
ciudadesvw.push("Monteria")
ciudadesvw.push("Neiva")
ciudadesvw.push("Pasto")
ciudadesvw.push("Pereira")
ciudadesvw.push("Tunja")
ciudadesvw.push("Villavicencio")

var concesionariosvw = {};

for (var i = 0;  i < ciudadesvw.length ; i++) {
	concesionariosvw[ciudadesvw[i]] = []
}

concesionariosvw.Armenia.push([])
concesionariosvw.Armenia[0]["val"] = "Armenia, Las Máquinas del Café, Cra. 14 No. 49 Norte 71";
concesionariosvw.Armenia[0]["name"] = "Armenia, Las Máquinas del Café, Cra. 14 No. 49 Norte 71";

concesionariosvw.Barranquilla.push([])
concesionariosvw.Barranquilla[0]["val"] = "Barranquilla, Auto Berlín, Via 40 No. 67-180";
concesionariosvw.Barranquilla[0]["name"] = "Barranquilla, Auto Berlín, Via 40 No. 67-180";

concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[0]["val"] = "Bogotá, Autonal, Autopista Norte AK 45 No. 127D-52";
concesionariosvw.Bogotá[0]["name"] = "Bogotá, Autonal, Autopista Norte AK 45 No. 127D-52";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[1]["val"] = "Bogotá, Casa Toro 170, Calle 170 No. 72-70";
concesionariosvw.Bogotá[1]["name"] = "Bogotá, Casa Toro 170, Calle 170 No. 72-70";
// concesionariosvw.Bogotá.push([])
// concesionariosvw.Bogotá[5]["val"] = "Bogotá, Casa Toro 222, Autopista Norte No. 222-81 costado occidental";
// concesionariosvw.Bogotá[5]["name"] = "Bogotá, Casa Toro 222, Autopista Norte No. 222-81 costado occidental";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[2]["val"] = "Bogotá, Casa Toro Puente Aranda, Calle 13 No. 53-4";
concesionariosvw.Bogotá[2]["name"] = "Bogotá, Casa Toro Puente Aranda, Calle 13 No. 53-4";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[3]["val"] = "Bogotá, Colwagen 100, Cra. 15 No. 100-58";
concesionariosvw.Bogotá[3]["name"] = "Bogotá, Colwagen 100, Cra. 15 No. 100-58";
// concesionariosvw.Bogotá.push([])
// concesionariosvw.Bogotá[3]["val"] = "Bogotá, Colwagen 127, Av. Calle 127 No. 54A-50";
// concesionariosvw.Bogotá[3]["name"] = "Bogotá, Colwagen 127, Av. Calle 127 No. 54A-50";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[4]["val"] = "Bogotá, Colwagen 140, Paralela Autopista Norte No. 140-58";
concesionariosvw.Bogotá[4]["name"] = "Bogotá, Colwagen 140, Paralela Autopista Norte No. 140-58";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[5]["val"] = "Bogotá, Los Coches, Av. Dorado No. 77-04";
concesionariosvw.Bogotá[5]["name"] = "Bogotá, Los Coches, Av. Dorado No. 77-04";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[6]["val"] = "Bogotá, Los Coches Calle 134, Calle 134 con carrera 54";
concesionariosvw.Bogotá[6]["name"] = "Bogotá, Los Coches Calle 134, Calle 134 con carrera 54";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[7]["val"] = "Bogotá, Auto Blitz Morato, Cra 70 No. 95-15";
concesionariosvw.Bogotá[7]["name"] = "Bogotá, Auto Blitz 109, Avenida 19 No. 109-10";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[8]["val"] = "Bogotá, Auto Blitz Centro mayor,  Autopista Sur No. 38a Sur-20 Local 1-120";
concesionariosvw.Bogotá[8]["name"] = "Bogotá , Auto Blitz Centro mayor,  Autopista Sur No. 38a Sur-20 Local 1-120";
concesionariosvw.Bogotá.push([])
concesionariosvw.Bogotá[9]["val"] = "Bogotá, Auto Blitz Morato, Cra 70 No. 95-15";
concesionariosvw.Bogotá[9]["name"] = "Bogotá, Auto Blitz Morato, Cra 70 No. 95-15";

concesionariosvw.Bucaramanga.push([])
concesionariosvw.Bucaramanga[0]["val"] = "Bucaramanga, Promotores del Oriente, Cra. 27 No. 45-38";
concesionariosvw.Bucaramanga[0]["name"] = "Bucaramanga, Promotores del Oriente, Cra. 27 No. 45-38";

concesionariosvw.Cali.push([])
concesionariosvw.Cali[0]["val"] = "Cali, Calima Motor - Sede Norte, Carrera 1 No. 32-35";
concesionariosvw.Cali[0]["name"] = "Cali, Calima Motor - Sede Norte, Carrera 1 No. 32-35";
concesionariosvw.Cali.push([])
concesionariosvw.Cali[1]["val"] = "Cali, motorwagen, Av cañasgordas con cra 127";
concesionariosvw.Cali[1]["name"] = "Cali, motorwagen, Av cañasgordas con cra 127";
concesionariosvw.Cali.push([])
concesionariosvw.Cali[2]["val"] = "Cali, motorwagen, Calle 9 # 42- 81 Cambulos";
concesionariosvw.Cali[2]["name"] = "Cali, motorwagen, Calle 9 # 42- 81 Cambulos";

concesionariosvw.Cartagena.push([])
concesionariosvw.Cartagena[0]["val"] = "Cartagena, Auto Berlín, Calle 26 (Av. Jimenez) No. 21-146";
concesionariosvw.Cartagena[0]["name"] = "Cartagena, Auto Berlín, Calle 26 (Av. Jimenez) No. 21-146";

concesionariosvw.Chía.push([])
concesionariosvw.Chía[0]["val"] = "Chía, Colwagen, Autopista Norte costado oriental KM 20 a 700 metros";
concesionariosvw.Chía[0]["name"] = "Chía, Colwagen, Autopista Norte costado oriental KM 20, a 700 metros";

concesionariosvw.Cúcuta.push([])
concesionariosvw.Cúcuta[0]["val"] = "Cúcuta, Promotores del Oriente, Av.0 # 1N-35 Quinta Bosh";
concesionariosvw.Cúcuta[0]["name"] = "Cúcuta, Promotores del Oriente, Av.0 # 1N-35 Quinta Bosh";

concesionariosvw.Ibague.push([])
concesionariosvw.Ibague[0]["val"] = "Ibague, Casa Toro, Zona Industrial Vía Mirolindo";
concesionariosvw.Ibague[0]["name"] = "Ibague, Casa Toro, Zona Industrial Vía Mirolindo";

concesionariosvw.Manizales.push([])
concesionariosvw.Manizales[0]["val"] = "Manizales, Las Máquinas del Café, Av. Kevin Ángel No. 60-271";
concesionariosvw.Manizales[0]["name"] = "Manizales, Las Máquinas del Café, Av. Kevin Ángel No. 60-271";

concesionariosvw.Medellín.push([])
concesionariosvw.Medellín[0]["val"] = "Medellín, Automotora, Av. Las Vegas Calle 25 Sur No. 48-11";
concesionariosvw.Medellín[0]["name"] = "Medellín, Automotora, Av. Las Vegas Calle 25 Sur No. 48-11";
concesionariosvw.Medellín.push([])
concesionariosvw.Medellín[1]["val"] = "Medellín, Germania Motors, Calle 29 No. 43A-29";
concesionariosvw.Medellín[1]["name"] = "Medellín, Germania Motors, Calle 29 No. 43A-29";

concesionariosvw.Monteria.push([])
concesionariosvw.Monteria[0]["val"] = "Montería, Camperwagen, Cra. 14 No. 40-30";
concesionariosvw.Monteria[0]["name"] = "Montería, Camperwagen, Cra. 14 No. 40-30";

concesionariosvw.Pasto.push([])
concesionariosvw.Pasto[0]["val"] = "Pasto, Casa Buralgo, Av. Panamericana Calle 2 No. 26-36";
concesionariosvw.Pasto[0]["name"] = "Pasto, Casa Buralgo, Av. Panamericana Calle 2 No. 26-36";

concesionariosvw.Pereira.push([])
concesionariosvw.Pereira[0]["val"] = "Pereira, Las Máquinas del Café, Av. 30 de Agosto Calle 94";
concesionariosvw.Pereira[0]["name"] = "Pereira, Las Máquinas del Café, Av. 30 de Agosto Calle 94";

concesionariosvw.Neiva.push([])
concesionariosvw.Neiva[0]["val"] = "Neiva, Reindustrias, Cra. 16 No. 20A-35";
concesionariosvw.Neiva[0]["name"] = "Neiva, Reindustrias, Cra. 16 No. 20A-35";

concesionariosvw.Tunja.push([])
concesionariosvw.Tunja[0]["val"] = "Tunja, Carrazos, Calle 53 No. 5-98";
concesionariosvw.Tunja[0]["name"] = "Tunja, Carrazos, Calle 53 No. 5-98";

concesionariosvw.Villavicencio.push([])
concesionariosvw.Villavicencio[0]["val"] = "Villavicencio, Casa Toro, Anillo Vial Vía Acacias";
concesionariosvw.Villavicencio[0]["name"] = "Villavicencio, Casa Toro, Anillo Vial Vía Acacias";

//console.log(concesionariosvw)
//console.log(concesionariosvw.Armenia[0].val)

function setConcesionarios(){
		$('input[name=marca]').val( "Volkswagen PKW" );
		$("#dropCiudad").html("<option value='' disabled selected>Ciudad</option>")

		for (var i = 0;  i < ciudadesvw.length ; i++) {
			$("#dropCiudad").append("<option value='"+ciudadesvw[i]+"'>"+ciudadesvw[i]+"</option>")
		}

		$("#dropCiudad").change(function(){
			var selectcity = $(this).val()
			//console.log()
			var numbC = concesionariosvw[selectcity].length
			$("#dropVitrina").html("")

			if(numbC == 1){
				var val = concesionariosvw[selectcity][0].val
				var name = concesionariosvw[selectcity][0].name
				$("#dropVitrina").html("<option value='"+val+"' selected>"+name+"</option>")
			}else{

				$("#dropVitrina").html("<option value='' disabled selected>Seleccionar concesionario</option>")
				for (var i = 0;  i < (numbC) ; i++) {
					var val = concesionariosvw[selectcity][i].val
					var name = concesionariosvw[selectcity][i].name
					$("#dropVitrina").append("<option value='"+val+"'>"+name+"</option>")
				}
			}
			
		})

}



