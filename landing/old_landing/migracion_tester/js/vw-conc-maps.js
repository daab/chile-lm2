var mapsAll = [];
	mapsAll.push({})
	mapsAll[0]["val"] = "Armenia, Las Máquinas del Café, Cra. 14 No. 49 Norte 71";
	mapsAll[0]["name"] = "Las Máquinas del Café";
	mapsAll[0]["dir"] = "Cra. 14 No. 49 Norte 71";
	mapsAll[0]["coorX"] = 4.555995;
	mapsAll[0]["coorY"] = -75.662579;
	mapsAll[0]["City"] = "Armenia";

	mapsAll.push({})
	mapsAll[1]["val"] = "Barranquilla, Colwagen, Via 40 No. 67-180";
	mapsAll[1]["name"] = "Colwagen";
	mapsAll[1]["dir"] = "Via 40 No. 67-180";
	mapsAll[1]["coorX"] = 11.005237;
	mapsAll[1]["coorY"] = -74.788951;
	mapsAll[1]["City"] = "Barranquilla";

	mapsAll.push({})
	mapsAll[2]["val"] = "Bogotá, Auto Blitz Morato, Cra 70 No. 95-15";
	mapsAll[2]["name"] = "Auto Blitz 109";
	mapsAll[2]["dir"] = "Avenida 19 No. 109-10";
	mapsAll[2]["coorX"] = 4.695203;
	mapsAll[2]["coorY"] = -74.050363;
	mapsAll[2]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[3]["val"] = "Bogotá, Auto Blitz Morato, Cra 70 No. 95-15";
	mapsAll[3]["name"] = "Auto Blitz Morato";
	mapsAll[3]["dir"] = "Cra 70 No. 95-15";
	mapsAll[3]["coorX"] = 4.691432;
	mapsAll[3]["coorY"] = -74.079269;
	mapsAll[3]["City"] = "Bogotá";
	// mapsAll.push({})
	// mapsAll[4]["val"] = "Bogotá, Auto Blitz Centro mayor,  Autopista Sur No. 38a Sur-20 Local 1-120";
	// mapsAll[4]["name"] = "Auto Blitz Centro mayor";
	// mapsAll[4]["dir"] = "Autopista Sur No. 38a Sur-20 Local 1-120";
	// mapsAll[4]["coorX"] = 4.5915301;
	// mapsAll[4]["coorY"] = -74.1263875;
	// mapsAll[4]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[4]["val"] = "Bogotá, Autonal, Autopista Norte AK 45 No. 127D-52";
	mapsAll[4]["name"] = "Autonal";
	mapsAll[4]["dir"] = "Autopista Norte AK 45 No. 127D-52";
	mapsAll[4]["coorX"] = 4.712713;
	mapsAll[4]["coorY"] = -74.052263;
	mapsAll[4]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[5]["val"] = "Bogotá, Casa Toro 170, Calle 170 No. 72-70";
	mapsAll[5]["name"] = "Casa Toro 170";
	mapsAll[5]["dir"] = "Calle 170 No. 72-70";
	mapsAll[5]["coorX"] = 4.761521;
	mapsAll[5]["coorY"] = -74.066885;
	mapsAll[5]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[6]["val"] = "Bogotá, Casa Toro 222, Autopista Norte No. 222-81 costado occidental";
	mapsAll[6]["name"] = "Casa Toro 222";
	mapsAll[6]["dir"] = "Autopista Norte No. 222-81 costado occidental";
	mapsAll[6]["coorX"] = 4.800577;
	mapsAll[6]["coorY"] = -74.038811;
	mapsAll[6]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[7]["val"] = "Bogotá, Casa Toro Puente Aranda, Calle 13 No. 53-4";
	mapsAll[7]["name"] = "Casa Toro Puente Aranda";
	mapsAll[7]["dir"] = "Calle 13 No. 53-4";
	mapsAll[7]["coorX"] = 4.628539;
	mapsAll[7]["coorY"] = -74.109420;
	mapsAll[7]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[8]["val"] = "Bogotá, Colwagen 100, Cra. 15 No. 100-58";
	mapsAll[8]["name"] = "Colwagen 100";
	mapsAll[8]["dir"] = "Cra. 15 No. 100-58";
	mapsAll[8]["coorX"] = 4.686534;
	mapsAll[8]["coorY"] = -74.047672;
	mapsAll[8]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[9]["val"] = "Bogotá, Colwagen 127, Av. Calle 127 No. 54A-50";
	mapsAll[9]["name"] = "Colwagen 127";
	mapsAll[9]["dir"] = "Av. Calle 127 No. 54A-50";
	mapsAll[9]["coorX"] = 4.709101;
	mapsAll[9]["coorY"] = -74.063742;
	mapsAll[9]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[10]["val"] = "Bogotá, Colwagen 140, Paralela Autopista Norte No. 140-58";
	mapsAll[10]["name"] = "Colwagen 140";
	mapsAll[10]["dir"] = "Paralela Autopista Norte No. 140-58";
	mapsAll[10]["coorX"] = 4.724255;
	mapsAll[10]["coorY"] = -74.050424;
	mapsAll[10]["City"] = "Bogotá";
	mapsAll.push({})
	mapsAll[11]["val"] = "Bogotá, Los Coches, Av. Dorado No. 77-04";
	mapsAll[11]["name"] = "Los Coches";
	mapsAll[11]["dir"] = "Av. Dorado No. 77-04";
	mapsAll[11]["coorX"] = 4.672146;
	mapsAll[11]["coorY"] = -74.114828;
	mapsAll[11]["City"] = "Bogotá";

	mapsAll.push({})
	mapsAll[12]["val"] = "Bucaramanga, Promotores del Oriente, Cra. 27 No. 45-38";
	mapsAll[12]["name"] = "Promotores del Oriente";
	mapsAll[12]["dir"] = "Cra. 27 No. 45-38";
	mapsAll[12]["coorX"] = 7.117185;
	mapsAll[12]["coorY"] = -73.115444;
	mapsAll[12]["City"] = "Bucaramanga";

	mapsAll.push({})
	mapsAll[13]["val"] = "Cali, Calima Motor - Sede Norte, Carrera 1 No. 32-35";
	mapsAll[13]["name"] = "Calima Motor - Sede Norte";
	mapsAll[13]["dir"] = "Carrera 1 No. 32-35";
	mapsAll[13]["coorX"] = 3.464135;
	mapsAll[13]["coorY"] = -76.516117;
	mapsAll[13]["City"] = "Cali";

	mapsAll.push({})
	mapsAll[14]["val"] = "Cali, Calima Motor - Sede Norte, Carrera 1 No. 32-35";
	mapsAll[14]["name"] = "Calima Motor";
	mapsAll[14]["dir"] = "Calle 5 No. 66B-10";
	mapsAll[14]["coorX"] = 3.396908;
	mapsAll[14]["coorY"] = -76.5467;
	mapsAll[14]["City"] = "Cali";

	mapsAll.push({})
	mapsAll[15]["val"] = "Cali, motorwagen, Av cañasgordas con cra 127";
	mapsAll[15]["name"] = "Motorwagen";
	mapsAll[15]["dir"] = "Av cañasgordas con cra 127";
	mapsAll[15]["coorX"] = 3.3330901;
	mapsAll[15]["coorY"] = -76.5327384;
	mapsAll[15]["City"] = "Cali";

	mapsAll.push({})
	mapsAll[16]["val"] = "Cartagena, Auto Berlín, Calle 26 (Av. Jimenez) No. 21-146";
	mapsAll[16]["name"] = "Auto Berlín";
	mapsAll[16]["dir"] = "Calle 26 (Av. Jimenez) No. 21-146";
	mapsAll[16]["coorX"] = 10.412251;
	mapsAll[16]["coorY"] = -75.536859;
	mapsAll[16]["City"] = "Cartagena";

	mapsAll.push({})
	mapsAll[17]["val"] = "Chía, Colwagen, Autopista Norte costado oriental KM 20 a 700 metros";
	mapsAll[17]["name"] = "Colwagen";
	mapsAll[17]["dir"] = "Autopista Norte costado oriental KM 20 a 700 metros";
	mapsAll[17]["coorX"] = 4.8833135;
	mapsAll[17]["coorY"] = -74.0355165;
	mapsAll[17]["City"] = "Chía";

	mapsAll.push({})
	mapsAll[18]["val"] = "Cúcuta, Promotores del Oriente, Av.0 # 1N-35 Quinta Bosh";
	mapsAll[18]["name"] = "Promotores del Oriente";
	mapsAll[18]["dir"] = "Av.0 # 1N-35 Quinta Bosh";
	mapsAll[18]["coorX"] = 7.899116;
	mapsAll[18]["coorY"] = -72.500641;
	mapsAll[18]["City"] = "Cúcuta";

	mapsAll.push({})
	mapsAll[19]["val"] = "Ibague, Casa Toro, Zona Industrial Vía Mirolindo";
	mapsAll[19]["name"] = "Casa Toro";
	mapsAll[19]["dir"] = "Zona Industrial Vía Mirolindo";
	mapsAll[19]["coorX"] = 4.422425;
	mapsAll[19]["coorY"] = -75.186318;
	mapsAll[19]["City"] = "Ibague";

	mapsAll.push({})
	mapsAll[20]["val"] = "Manizales, Las Máquinas del Café, Av. Kevin Ángel No. 60-271";
	mapsAll[20]["name"] = "Las Máquinas del Café";
	mapsAll[20]["dir"] = "Av. Kevin Ángel No. 60-271";
	mapsAll[20]["coorX"] = 5.060855;
	mapsAll[20]["coorY"] = -75.484639;
	mapsAll[20]["City"] = "Manizales";

	mapsAll.push({})
	mapsAll[21]["val"] = "Medellín, Germania Motors, Calle 29 No. 43A-29";
	mapsAll[21]["name"] = "Germania Motors";
	mapsAll[21]["dir"] = "Calle 29 No. 43A-29";
	mapsAll[21]["coorX"] = 6.227995;
	mapsAll[21]["coorY"] = -75.570362;
	mapsAll[21]["City"] = "Medellín";


	mapsAll.push({})
	mapsAll[22]["val"] = "Medellín, Automotora, Av. Las Vegas Calle 25 Sur No. 48-11";
	mapsAll[22]["name"] = "Automotora";
	mapsAll[22]["dir"] = "Av. Las Vegas Calle 25 Sur No. 48-11";
	mapsAll[22]["coorX"] = 6.18526;
	mapsAll[22]["coorY"] = -75.583423;
	mapsAll[22]["City"] = "Medellín";

	mapsAll.push({})
	mapsAll[23]["val"] = "Montería, Camperwagen, Cra. 14 No. 40-30";
	mapsAll[23]["name"] = "Camperwagen";
	mapsAll[23]["dir"] = "Cra. 14 No. 40-30";
	mapsAll[23]["coorX"] = 8.758857;
	mapsAll[23]["coorY"] = -75.874549;
	mapsAll[23]["City"] = "Montería";

	mapsAll.push({})
	mapsAll[24]["val"] = "Pereira, Las Máquinas del Café, Av. 30 de Agosto Calle 94";
	mapsAll[24]["name"] = "Las Máquinas del Café";
	mapsAll[24]["dir"] = "Av. 30 de Agosto Calle 94";
	mapsAll[24]["coorX"] = 4.810962;
	mapsAll[24]["coorY"] = -75.754243;
	mapsAll[24]["City"] = "Pereira";

	mapsAll.push({})
	mapsAll[25]["val"] = "Neiva, Reindustrias, Cra. 16 No. 20A-35";
	mapsAll[25]["name"] = "Reindustrias";
	mapsAll[25]["dir"] = "Cra. 16 No. 20A-35";
	mapsAll[25]["coorX"] = 2.939651;
	mapsAll[25]["coorY"] = -75.282213;
	mapsAll[25]["City"] = "Neiva";

	mapsAll.push({})
	mapsAll[26]["val"] = "Tunja, Carrazos, Calle 53 No. 5-98";
	mapsAll[26]["name"] = "Carrazos";
	mapsAll[26]["dir"] = "Calle 53 No. 5-98";
	mapsAll[26]["coorX"] = 5.560343;
	mapsAll[26]["coorY"] = -73.348124;
	mapsAll[26]["City"] = "Tunja";

	mapsAll.push({})
	mapsAll[27]["val"] = "Villavicencio, Casa Toro, Anillo Vial Vía Acacias";
	mapsAll[27]["name"] = "Casa Toro";
	mapsAll[27]["dir"] = "Anillo Vial Vía Acacias";
	mapsAll[27]["coorX"] = 4.133138;
	mapsAll[27]["coorY"] = -73.625217;
	mapsAll[27]["City"] = "Villavicencio";


var markers = [];
var currentMarker = -1;
var contactTopPosition =0;
var iconVW = "M34.4-74c0-19-15.4-34.4-34.4-34.4S-34.4-93-34.4-74S0,0,0,0S34.4-55,34.4-74zM0-47.4c-15.3,0-27.8-12.4-27.8-27.8c0-15.3,12.4-27.8,27.8-27.8c10.4,0,19.5,5.8,24.3,14.3	c2.2,4,3.5,8.6,3.5,13.5c0,7.2-2.7,13.8-7.2,18.7C15.5-50.9,8.1-47.4,0-47.4zM21.9-82.2l-12.5,28c8-3.6,13.5-11.6,13.5-21C23-77.6,22.6-80,21.9-82.2zM-18.7-88.6l10.8,24.3l4.5-10.1h6.7l4.5,10.1l10.8-24.3c-1.8-2.5-4.2-4.7-6.9-6.3l-8.5,19h-6.7l-8.5-19C-14.5-93.3-16.9-91.1-18.7-88.6zM-6.8-97.2L0-82l6.8-15.2c-2.1-0.7-4.4-1-6.8-1C-2.3-98.2-4.6-97.8-6.8-97.2zM6.8-53.2L0-68.3l-6.8,15.2c2.1,0.7,4.4,1,6.8,1C2.4-52.2,4.6-52.5,6.8-53.2zM-23-75.2c0,9.3,5.6,17.4,13.5,21l-12.5-28C-22.6-80-23-77.6-23-75.2z"


function initMap() {

	var iconvw = {
	    path: iconVW,
	    fillColor: '#0099dc',
	    fillOpacity: 1,
	    anchor: new google.maps.Point(0,0),
	    strokeWeight: 0.7,
	    strokeColor: '#FFFFFF',
	    scale: .4
	}

	var mapVw = new google.maps.Map(document.getElementById('map-vw'), {
		zoom: 5,
		center: {lat: 6.2169227, lng: -75.6186163}
	});

	if (navigator.geolocation) {
	 	navigator.geolocation.getCurrentPosition(function(position) {
            $(".result-geopos").show();
            var pos = {
              "lat": position.coords.latitude,
              "lng": position.coords.longitude
            }

            console.log(pos.lat +" - "+ pos.lng)
            NearestCity(pos.lat, pos.lng)
        }, function(error){

        	 $(".result-geopos").hide();
        })
	 }else{
	 	$(".result-geopos").hide();
	 }

	for (i = 0; i < mapsAll.length; i++) {
			markers[i] = new google.maps.Marker({
	        position: new google.maps.LatLng(mapsAll[i].coorX, mapsAll[i].coorY),
	        map: mapVw,
	        icon: iconvw
      	});

      	google.maps.event.addListener(markers[i], 'click', (function(marker, i) {
        return function() {
          //infowindow.setContent(locations[i][0]);
          //infowindow.open(map, marker);
          mapVw.setCenter(markers[i].getPosition());
    	  mapVw.setZoom(15);
    	  animating = false;
    	  if (currentMarker == i) return;
    	  Bval = true;
    	  currentMarker = i;
    	  if(validBttn == 0){
    	  	 $(".maps-placer-bttn").scrollTop($(".map-bttn-"+i).position().top)
    	  }
    	 	validBttn = 0;
    	  $(".map-bttn-container").children().removeClass("active-marker")
    	  $(".map-bttn-"+i).addClass("active-marker")
    	  $(".bttn-continuar").removeClass("bttn-null")
        }
     	})(markers[i], i));

	}


	

}

var validBttn = 0;
var currentCity;
var citiesInnerData = {};

var arrayhelper = [];

function setMAPS(){
	for (i = 0; i < mapsAll.length; i++) {
		$(".map-bttn-container").append("<div class='map-bttn map-bttn-"+i+" cc-"+mapsAll[i].City+"'><h4>"+mapsAll[i].City+", "+mapsAll[i].name+"</h4><p>"+mapsAll[i].dir+"</p></div>")
		$(".map-bttn-"+i).data("foo",i)
		var curcity = mapsAll[i].City
		if( citiesInnerData.hasOwnProperty(mapsAll[i].City) ){
			var property = mapsAll[i].City;
			citiesInnerData[property].numb += 1;
			//console.log(citiesInnerData[property].numb) 
		}else{
			
			var object = mapsAll[i].City;
			citiesInnerData[mapsAll[i].City] = {"numb" : 1, "firstDealer": i};
			
		}
		//console.log(citiesInnerData)
		$(".map-bttn-"+i).click(function(){
			var cur = $(this).data("foo")
			animating = true;
			validBttn = 1;
			google.maps.event.trigger(markers[cur], 'click');
			
            
        });
        
        $(".map-bttn-"+i).dblclick(function(){
			var cur = $(this).data("foo")
			animating = true;
			validBttn = 1;
			google.maps.event.trigger(markers[cur], 'click');
			OpenC()
            
        });

        var ciudades;
        if(currentCity !== mapsAll[i].City){
        	currentCity = mapsAll[i].City
        	$("#filtro-ciudad").append("<option value='"+currentCity+"''>"+currentCity+"</option>")
        }
        
	}

	$(".map-bttn").addClass("filtro-cc")

	$("#filtro-ciudad").change( function(){
		if( $("#filtro-ciudad").val() == "todos" ){
			console.log("todos dude")
			$(".map-bttn").addClass("filtro-cc")
			// ---> function center map colombia 
		}else{
			//console.log("selected- " + $("#filtro-ciudad").val())
			$(".map-bttn").removeClass("filtro-cc")
			$( ".cc-"+ $("#filtro-ciudad").val() ).addClass("filtro-cc")

			// ---> center map city
			var property = $("#filtro-ciudad").val()
			//console.log(citiesInnerData[property].numb + " %%%%")
			if(citiesInnerData[property].numb == 1){
				$( ".map-bttn-"+ citiesInnerData[property].firstDealer ).trigger( "click" );
			}else{
				console.log("no geolocation")
			}
		}
	})
}



function setMAPSGOOGLE(){
	$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyAa3Q-M-6193bhNE9ALV70safePlL8SMek&callback=initMap");
}

(function ($) {
    $.fn.isOnScreen = function () {
        var topView = $(window).scrollTop();
        var botView = topView + $(window).height();
        var topElement = this.offset().top;
        var botElement = topElement + this.height();
        return ((botElement <= botView) && (topElement >= topView));
    }
})(jQuery);

function Deg2Rad(deg) {
  return deg * Math.PI / 180;
}

function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
  lat1 = Deg2Rad(lat1);
  lat2 = Deg2Rad(lat2);
  lon1 = Deg2Rad(lon1);
  lon2 = Deg2Rad(lon2);
  var R = 6371; // km
  var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
  var y = (lat2 - lat1);
  var d = Math.sqrt(x * x + y * y) * R;
  return d;
}

function NearestCity(latitude, longitude) {
  var mindif = 99999;
  var closest;

  for (index = 0; index < mapsAll.length; ++index) {
    var dif = PythagorasEquirectangular(latitude, longitude, mapsAll[index].coorX, mapsAll[index].coorY);
    if (dif < mindif) {
      closest = index;
      mindif = dif;
    }
  }

  // echo the nearest city
  console.log("the nearest " + mapsAll[closest].name);
  $(".result-geopos").html("<h5>Concesionairo más cercano:</h5><div class='bttn-submit rounded-corners bttn-atajo' style='width:90%; margin:0'><p>"+mapsAll[closest].name+"</p></div>")
  $(".bttn-atajo").data("foo", closest)
  $(".bttn-atajo").click(function(){
  	var numb = $(this).data("foo")
  	$( ".map-bttn-"+ numb ).trigger( "dblclick" );
  })
  
}



