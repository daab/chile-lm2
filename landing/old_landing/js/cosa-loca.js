var currentStep = 0;
var Bval = false;

function OpenB(n){
	
	if(currentStep == 3){
		/*CLOSE C*/
		$(".title-bttn-datos").addClass("stNull")
		$(".title-bttn-datos").removeClass("stNormal")
		$(".db-datos").slideToggle(1000)
	}
	if(currentStep == 0 || currentStep == 1){
		/*CLOSE A*/
		var model = n;
		$('input[name=modelo]').val( vwmodelos[model].val );
		$(".title-bttn-models .title-models").html("<img src='"+vwmodelos[model].car_img+"' alt='"+vwmodelos[model].name+"'>")
		$(".title-bttn-models .title-text").html(vwmodelos[model].name);
		if (currentStep == 0 ) setMAPSGOOGLE();
		$(".title-bttn-models").removeClass("stNormal")
		$(".title-bttn-models").addClass("stBttn")
		$(".db-models").slideToggle(1000)
		if(Bval){OpenC(); return;}
	}

	if(Bval) $(".maps-placer-bttn").scrollTop($(".map-bttn-"+currentMarker).position().top)
	$(".title-bttn-conce").removeClass("stNull");
	$(".title-bttn-conce").removeClass("stBttn");
	$(".title-bttn-conce").addClass("stNormal");
	$(".maps-conc").slideToggle(1200, function(){
		$(".title-bttn-conce .title-text").html("<span>Segundo paso</span><br>Escoge un concesionario")
			currentStep =2;
			animating = false;
		})
}


function OpenC(){
	selectedConce ();
	$(".title-bttn-conce").removeClass("stNormal stNull")
	$(".title-bttn-conce").addClass("stBttn")
	if (currentStep == 2) $(".maps-conc").slideToggle(1000)
	$(".title-bttn-datos").removeClass("stNull")
	$(".title-bttn-datos").addClass("stNormal")
	$(".db-datos").slideToggle(1200, function(){
			currentStep = 3;
			animating = false;
		})
}

function OpenA(){
	if(Bval) selectedConce ();
	if(currentStep == 2) $(".maps-conc").slideToggle(1000)
	if(currentStep == 3) $(".db-datos").slideToggle(1000)

	$(".title-bttn-conce").removeClass("stNormal stBttn")
	$(".title-bttn-conce").addClass("stNull")
	$(".title-bttn-datos").removeClass("stNormal stBttn")
	$(".title-bttn-datos").addClass("stNull")

	$(".title-bttn-models").removeClass("stNull stBttn")
	$(".title-bttn-models").addClass("stNormal")
	$('input[name=modelo]').val("");
	$(".title-bttn-models .title-text").html("<span>Primer paso</span><br>Escoge un modelo")
	$(".db-models").slideToggle(1200, function(){
					currentStep = 1;
					animating = false;
			})

}

function selectedConce (){
	$(".title-bttn-conce .title-text").html(mapsAll[currentMarker].City+" - "+mapsAll[currentMarker].name)
	$('input[name=concesionario]').val( mapsAll[currentMarker].val);
}