var vwmodelos = [{
	"name":"Nueva Amarok",
	"val":"nueva_amarok",
	"car_img":"images/models_pkw/model_pkw_0.png",
	"id" : 1
}, {
	"name":"Beetle",
	 "val":"beetle",
	 "car_img":"images/models_pkw/model_pkw_1.png",
	"id" : 2
}, {
	"name":"Beetle Cabriolet",
	   "val":"beetle_cabriolet",
	   "car_img":"images/models_pkw/model_pkw_2.png",
	"id" : 3
}, {
	"name":"Bora",
	"val":"bora",
	"car_img":"images/models_pkw/model_pkw_3.png",
	"id" : 4
}, {
	"name":"Crafter",
	   "val":"crafter",
	   "car_img":"images/models_pkw/model_pkw_4.png",
	"id" : 5
}, {
	"name":"Nueva California",
	   "val":"nueva_california",
	   "car_img":"images/models_pkw/model_pkw_5.png",
	"id" : 6
}, {
	"name":"Nuevo Golf",
	   "val":"nuevo_golf",
	   "car_img":"images/models_pkw/model_pkw_6.png",
	"id" : 8
}, {
	"name":"Nuevo Golf GTI",
	   "val":"nuevo_golf_gti",
	   "car_img":"images/models_pkw/model_pkw_7.png",
	"id" : 9
}, {
	"name":"Gol",
	   "val":"gol",
	   "car_img":"images/models_pkw/model_pkw_8.png",
	"id" : 10
}, {
	"name":"Saveiro",
	   "val":"saveiro",
	   "car_img":"images/models_pkw/model_pkw_9.png",
	"id" : 11
}, {
	"name":"Scirocco",
	   "val":"scirocco",
	   "car_img":"images/models_pkw/model_pkw_10.png",
	"id" : 12
}, {
	"name":"Voyage",
	   "val":"voyage",
	   "car_img":"images/models_pkw/model_pkw_11.png",
	"id" : 13
}, {
	"name":"Nuevo Polo",
	   "val":"nuevo_polo",
	   "car_img":"images/models_pkw/model_pkw_12.png",
	"id" : 14
}, {
	"name":"Nuevo Atlas",
	   "val":"nuevo_atlas",
	   "car_img":"images/models_pkw/model_pkw_13.png",
	"id" : 15
}, {
	"name":"Nuevo Tiguan",
	   "val":"nuevo_tiguan",
	   "car_img":"images/models_pkw/model_pkw_14.png",
	"id" : 15
}, {
	"name":"Nuevo Virtus",
	   "val":"nuevo_virtus",
	   "car_img":"images/models_pkw/model_pkw_15.png",
	"id" : 15
}, {
	"name":"Touareg",
	   "val":"touareg",
	   "car_img":"images/models_pkw/model_pkw_16.png",
	"id" : 15
}, {
	"name":"Transporter",
	   "val":"transporter",
	   "car_img":"images/models_pkw/model_pkw_17.png",
	"id" : 15
}, {
	"name":"Nuevo Multivan",
	   "val":"nuevo_multivan",
	   "car_img":"images/models_pkw/model_pkw_18.png",
	"id" : 15
}];

var vwdealers = [	
	{  
	   "name":'Preubas',
	   "val":'Pruebas, santiago, concesionario de pruebas',
	   "tel":'(+56 55) 2477250',
	   "dir":'Av. Pedro Aguirre Cerda 7903, Antofagasta',
	   "lat":-23.595999,
	   "lon":-70.389890,
	   "city":'Antofagasta',
	   "city_id": 1,
	   "id":1,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Marubeni',
	   "val":'Antofagasta, Marubeni, Av. Pedro Aguirre Cerda 7903 - Antofagasta',
	   "tel":'(+56 55) 2477250',
	   "dir":'Av. Pedro Aguirre Cerda 7903, Antofagasta',
	   "lat":-23.595999,
	   "lon":-70.389890,
	   "city":'Antofagasta',
	   "city_id": 1,
	   "id":1,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Carmona',
	   "val":'Copiapó, Carmona Y Cia, Av. Rámon Freire 254 - Copiapó',
	   "tel":'(+56 52) 2243141',
	   "dir":'Av. Rámon Freire 254, Copiapó',
	   "lat":-27.366045,
	   "lon":-70.339756,
	   "city":'Copiapó',
	   "city_id": 2,
	   "id":2,
	   "img": "images/dealers/dealer-2.jpg"
	},
	{  
	   "name":'Carmona',
	   "val":'La Serena, Carmona Y Cia, Av. Balmaceda 3812 - La Serena',
	   "tel":'(+56 51) 2200206',
	   "dir":'Av. Balmaceda 3812, La Serena',
	   "lat":-29.935386,
	   "lon":-71.260446,
	   "city":'La Serena',
	   "city_id": 3,
	   "id":3,
	   "img": "images/dealers/dealer-3.jpg"
	},
	{  
	   "name":'Carmona',
	   "val":'Carmona Ovalle, Av. Gobernadora Laura Pizarro 1540',
	   "tel":'(+56 51) 2627090',
	   "dir":'Av. Gobernadora Laura Pizarro 1540',
	   "lat":-30.607814,
	   "lon":-71.213588,
	   "city":'Ovalle',
	   "city_id": 4,
	   "id":4,
	   "img": "images/dealers/dealer-27.jpg"
	},
	{  
	   "name":'Cartoni',
	   "val":'Viña del Mar, Cartoni, Av. Benidorm 946 (ex 15 Norte) - Viña del Mar',
	   "tel":'(+56 32) 2381212',
	   "dir":'Av. Benidorm 946 (ex 15 Norte), Viña del Mar',
	   "lat":-33.008561,
	   "lon":-71.545980,
	   "city":'Viña del Mar',
	   "city_id": 5,
	   "id":5,
	   "img": "images/dealers/dealer-4.jpg"
	},
	{  
	   "name":'Circulo Autos',
	   "val":'Santiago, Circulo Autos, Av. Irarrázaval 401 - Ñuñoa',
	   "tel":'(+56 22) 3527700',
	   "dir":'Av. Irarrázaval 401, Ñuñoa',
	   "lat":-33.452564,
	   "lon":-70.626610,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":6,
	   "img": "images/dealers/dealer-5.jpg"
	},
	{  
	   "name":'Daniel Achondo',
	   "val":'Santiago, Daniel Achondo, Av. Bilbao 5759 - La Reina',
	   "tel":'(+56 22) 8162000',
	   "dir":'Av. Bilbao 5759, La Reina',
	   "lat":-33.431371,
	   "lon":-70.573530,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":7,
	   "img": "images/dealers/dealer-6.jpg"
	},
	{  
	   "name":'Inalcar Mall Plaza Vespucio',
	   "val":'Santiago, Inalcar, Mall Plaza Vespucio - Auto Plaza - La Florida',
	   "tel":'(+56 22) 586291 / 92 / 93',
	   "dir":'Mall Plaza Vespucio, Auto Plaza, La Florida',
	   "lat":-33.5176132,
	   "lon":-70.6002728,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":8,
	   "img": "images/dealers/dealer-8fg.jpg"
	},
	{  
	   "name":'Inalcar Mall Plaza Tobalaba',
	   "val":'Santiago, Inalcar, Mall Plaza Tobalaba - Toabalaba 108 - L107-108-109 - Pte Alto',
	   "tel":'(+56 22) 5807985 / 86 / 87',
	   "dir":'Mall Plaza Tobalaba, Toabalaba 108, L107-108-109, Pte Alto',
	   "lat":-33.5698263,
	   "lon":-70.5584551,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":9,
	   "img": "images/dealers/dealer-7.jpg"
	},
	{  
	   "name":'Inalcar Mall Plaza Alameda',
	   "val":"Santiago, Inalcar, Mall Plaza Alameda - Autoplaza - Avenida Bernardo O-Higgins 3470",
	   "tel":'(+5622) 834 9041',
	   "dir":"Mall Plaza Alameda, Autoplaza - Avenida Bernardo O'Higgins 3470, Estación Central",
	   "lat":-33.4526736,
	   "lon":-70.6844652,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":10,
	   "img": "images/dealers/dealer-9.jpg"
	},
	{  
	   "name":'Klassik Car',
	   "val":'Santiago, Klassik Car, Av. Vitacura 8126 - Vitacura',
	   "tel":'(+56 22) 7310530',
	   "dir":'Av. Vitacura 8126, Vitacura',
	   "lat":-33.384996,
	   "lon":-70.555077,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":11,
	   "img": "images/dealers/dealer-10.jpg"
	},
	{  
	   "name":'Miguel Jacob Helo',
	   "val":'Santiago, Miguel Jacob Helo, Av. Bilbao 2626 - Providencia',
	   "tel":'(+56 22) 5698000',
	   "dir":'Av. Bilbao 2626, Providencia',
	   "lat":-33.435994,
	   "lon":-70.596434,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":12,
	   "img": "images/dealers/dealer-11.jpg"
	},
	// {  
	//    "name":'Miguel Jacob Helo',
	//    "val":'Santiago, Miguel Jacob Helo, San Ignacio 531 - Santiago Centro',
	//    "tel":'(+56 22) 5698000',
	//    "dir":'San Ignacio 531, Santiago Centro',
	//    "lat":-33.453459,
	//    "lon":-70.655912,
	//    "city":'Santiago',
	//    "city_id": 6,
	//    "id":13,
	//    "img": "images/dealers/dealer-12.jpg"
	// },
	{  
	   "name":'PIA Movicenter',
	   "val":'Santiago, Porsche Inter Auto, Movicenter - Av. Américo Vespucio 1155 - local G1 Huechuraba',
	   "tel":'(+56 22) 3224 623 / 4625 / 4650',
	   "dir":'Movicenter, Av. Américo Vespucio 1155, local G1 Huechuraba',
	   "lat":-33.372354,
	   "lon":-70.665148,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":13,
	   "img": "images/dealers/dealer-13.jpg"
	},
	{  
	   "name":'PIA Las Condes',
	   "val":'Santiago, Porsche Inter Auto, Av. Las Condes 12260 - Las Condes',
	   "tel":'(+56 22) 3224600',
	   "dir":'Av. Las Condes 12260, Las Condes',
	   "lat":-33.373089,
	   "lon":-70.518724,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":14,
	   "img": "images/dealers/dealer-14.jpg"
	},
	{  
	   "name":'Salazar Mall Plaza Oeste',
	   "val":'Santiago, Salazar Israel, Mall Plaza Oeste - Av. Américo Vespucio 1501 - local 153 - Cerrillos',
	   "tel":'600 818 6000',
	   "dir":'Mall Plaza Oeste, Av. Américo Vespucio 1501, local 153, Cerrillos',
	   "lat":-33.5034095,
	   "lon":-70.738096,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":15,
	   "img": "images/dealers/dealer-15.jpg"
	},
	{  
	   "name":'Salazar Cerrillos',
	   "val":'Santiago, Salazar Israel, Av. Pedro Aguirre Cerda 6697 - Cerrillos',
	   "tel":'600 818 6000',
	   "dir":'Av. Pedro Aguirre Cerda 6697, Cerrillos',
	   "lat":-33.498793,
	   "lon":-70.708657,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":16,
	   "img": "images/dealers/dealer-16.jpg"
	},
	{  
	   "name":'Marubeni Macul',
	   "val":'Santiago, Vicuña Mackenna, vicuña mackenana 33000 - Macul',
	   "tel":'600 818 6000',
	   "dir":'vicuña mackenana 33000 - Macul',
	   "lat":-33.482046,
	   "lon":-70.620807,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":17,
	   "img": "images/dealers/dealer-16.jpg"
	},
	{  
	   "name":'Marubeni Mall Plaza Sur',
	   "val":'Santiago, Mall Plaza Sur, Avda. Presidente Jorge Alessandri',
	   "tel":'600 818 6000',
	   "dir":'Av. Presidente Jorge Alessandri Nº 20040 AP 201, 203, 205',
	   "lat":-33.6319496,
	   "lon":-70.7124749,
	   "city":'Santiago',
	   "city_id": 6,
	   "id":18,
	   "img": "images/dealers/dealer-16.jpg"
	},
	{  
	   "name":'De Real',
	   "val":'Rancagua, Comercial del Real, Freire 471 - Rancagua',
	   "tel":'(+56 72) 2320370',
	   "dir":'Freire 471, Rancagua',
	   "lat":-34.171875,
	   "lon":-70.735596,
	   "city":'Rancagua',
	   "city_id": 7,
	   "id":19,
	   "img": "images/dealers/dealer-17.jpg"
	},
	{  
	   "name":'Circulos Auto Talca',
	   "val":'Talca, Circulo Autos, San Miguel 3010 - Talca',
	   "tel":'(+56 71) 2243714',
	   "dir":'San Miguel 3010, Talca',
	   "lat":-35.433924,
	   "lon":-71.632001,
	   "city":'Talca',
	   "city_id": 8,
	   "id":20,
	   "img": "images/dealers/dealer-18.jpg"
	},
	{  
	   "name":'Circulo Autos Linares',
	   "val":'Linares, Circulo Autos Linares, Av Anibal Leon Bustos 1300',
	   "tel":'',
	   "dir":'Av Anibal Leon Bustos 1300',
	   "lat":-35.8416734,
	   "lon":-71.620009,
	   "city":'Linares',
	   "city_id": 9,
	   "id":21,
	   "img": "images/dealers/dealer-3.jpg"
	},
	{  
	   "name":'Salazar Israel',
	   "val":'Chillán, Salazar Israel, Av. Ecuador 768 - Chillan',
	   "tel":'600 818 6000',
	   "dir":'Av. Ecuador 768, Chillan',
	   "lat":-36.600786,
	   "lon":-72.097636,
	   "city":'Chillán',
	   "city_id": 10,
	   "id":22,
	   "img": "images/dealers/dealer-19.jpg"
	},
	{  
	   "name":'Salazar Israel Mall Plaza Trebol',
	   "val":'Concepción, Salazar Israel, Mall Plaza Trebol - L13 - Talcahuano',
	   "tel":'600 818 6000',
	   "dir":'Mall Plaza Trebol, L13, Talcahuano',
	   "lat":-36.7921492,
	   "lon":-73.0682781,
	   "city":'Concepción',
	   "city_id": 11,
	   "id":23,
	   "img": "images/dealers/dealer-20.jpg"
	},
	{  
	   "name":'Salazar Israel',
	   "val":'Concepción, Salazar Israel, Chacabuco 330 - Concepción',
	   "tel":'600 818 6000',
	   "dir":'Chacabuco 330, Concepción',
	   "lat":-36.832369,
	   "lon":-73.051912,
	   "city":'Concepción',
	   "city_id": 11,
	   "id":24,
	   "img": "images/dealers/dealer-21.jpg"
	},
	// {  
	//    "name":'Salazar Israel',
	//    "val":'Concepción, Salazar Israel, Av. Prat 1099 - Concepción',
	//    "tel":'600 818 6000',
	//    "dir":'Av. Prat 1099, Concepción',
	//    "lat":-36.823600,
	//    "lon":-73.062985,
	//    "city":'Concepción',
	//    "city_id": 10,
	//    "id":23,
	//    "img": "images/dealers/dealer-22.jpg"
	// },
	{  
	   "name":'Salazar Israel',
	   "val":'Los Angeles, Salazar Israel, Av. Las Industrias (Ex Ruta 5 Sur) Km 508 - Los Angeles',
	   "tel":'600 818 6000',
	   "dir":'Av. Las Industrias (Ex Ruta 5 Sur) Km 508, Los Angeles',
	   "lat":-37.457520,
	   "lon":-72.327826,
	   "city":'Los Angeles',
	   "city_id": 12,
	   "id":25,
	   "img": "images/dealers/dealer-23.jpg"
	},
	{  
	   "name":'Salazar Israel',
	   "val":'Temuco, Salazar Israel, Av. Dinamarca 501 - Temuco',
	   "tel":'600 818 6000',
	   "dir":'Av. Dinamarca 501, Temuco',
	   "lat":-38.736121,
	   "lon":-72.605730,
	   "city":'Temuco',
	   "city_id": 13,
	   "id":26,
	   "img": "images/dealers/dealer-24.jpg"
	},
	{  
	   "name":'Servimaq',
	   "val":'Osorno, Servimaq, Av. Juan Mackenna 1641 - Osorno',
	   "tel":'(+56 64) 2234673',
	   "dir":'Av. Juan Mackenna 1641, Osorno',
	   "lat":-40.575563,
	   "lon":-73.122030,
	   "city":'Osorno',
	   "city_id": 14,
	   "id":27,
	   "img": "images/dealers/dealer-25.jpg"
	},
	{  
	   "name":'Servimaq',
	   "val":'Puerto Montt, Servimaq, Regimiento 801 - Puerto Montt',
	   "tel":'(+56 65) 2277694',
	   "dir":'Regimiento 801, Puerto Montt',
	   "lat":-41.468281,
	   "lon":-72.924336,
	   "city":'Puerto Montt',
	   "city_id": 15,
	   "id":28,
	   "img": "images/dealers/dealer-26.jpg"
	},
	{  
	   "name":'Jordan',
	   "val":'Pta. Arenas, Jordan, Galería Ona (Av. Principal Local 10-11) - Zona Franca',
	   "tel":'(+56 61) 2202772',
	   "dir":'Galería Ona (Av. Principal Local 10-11) Zona Franca',
	   "lat":-53.1633379,
	   "lon":-70.9282473,
	   "city":'Pta. Arenas',
	   "city_id": 16,
	   "id":29,
	   "img": "images/dealers/dealer-27.jpg"
	}
	
]