function getURLParameter(name) {
  	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function Urlcoder(data_array,url){
	var url = String(url);
	var a = data_array;
	var chain = ""
	var checkParam = false
	console.log(url)
	url.indexOf("?") === -1 ? checkParam = true  : true;
	if(!checkParam) { chain += "&" } else{
		chain += "?"
	}
	for (var i in a){
		var val;
		if(a[i].name !== "mail") {
		 val = a[i].val.replace(/\s/g, "_")
		 //val = val.toLowerCase();
		}else{
			val = a[i].val.replace(/\s/g, "");
			//val = val.toLowerCase();
		}
		chain += a[i].name + "=" + val
		if(Number(i) !== a.length - 1) chain += "&"
	}

	var full = url + chain
	return (full.split("?")[1])
}


var aaa = false

var google_key = false;

var data_saves = {
	"va": null,
	"model": null,
	"model2": null,
	"dealer": null,
	"email": false,
	"data": false 
}

var city_info = [];
var second_model = null;

function setcotizador(){

		var utm_s = getURLParameter('utm_source');
		$('input[name=source]').val( utm_s );

		var utm_modelo = getURLParameter('utm_content');

		for (var i in vwmodelos){
			if (utm_modelo == vwmodelos[i]["val"]){
				data_saves.model = i
				$('input[name=modelo]').val( vwmodelos[data_saves.model]["val"] );
			} 
		}

		// $("#dblnkd-form")[0].reset();

		for (var i in vwmodelos) {
			$(".models").append("<div class='model al-a hover model-id-"+i+"'><img src='"+vwmodelos[i].car_img+"'/><h5>"+vwmodelos[i].name+"</h5></div>")
			$(".model-id-"+i).data("car", i)
		}

		for (var i in vwdealers){
			city_info.indexOf(vwdealers[i].city) === -1 ? city_info.push(vwdealers[i].city) : true;
			$(".dealer_menu").append("<div class='menu_bttn dealer_bttn al-a hover bg ccs-id-"+i+" city-id-"+vwdealers[i].city_id+"'><h4>"+vwdealers[i].name+"</h4><p>"+vwdealers[i].dir+"<br>"+vwdealers[i].city+"</p><br><div class='dealer_map'></div><div class='bttnvw'><h1>Confirmar</h1></div></div>")
			$(".ccs-id-"+i).data("ccs", i)
		}

		for (var i in city_info){
			$(".modal-city").append("<div class='bttn_menu bttn_menu_id_"+i+"'>"+city_info[i]+"</div>")
			var datacity = 0;
			$(".bttn_menu_id_"+i).data("city", i)
			$(".bttn_menu_id_"+i).click(function(){

			})
		}

		$(".model").click(function(){
			if(aaa || $(this).hasClass("nuller")) return;
			var car = $(this).data("car");
			data_saves.model = car;
			$('input[name=modelo]').val( vwmodelos[car]["val"] );
			if(data_saves.model2 !== null) $('input[name=modelo2]').val( vwmodelos[data_saves.model2]["val"] );
			printcotizador();
			return false;
		})

		$(".home_url").click(function(){
			window.location.href = "https://www.volkswagen.cl"
		})

		// Concesionario 1er SELECT
		$(".dealer_bttn").click(function(){
			if( aaa || $(this).hasClass("nuller") ) return;
			var ccs = $(this).data("ccs")
			if( $(this).hasClass("check_dealer") ){
				// Concesionario 12do SELECT
				data_saves.dealer = $(this).data("ccs");
				$('input[name=concesionario]').val( vwdealers[data_saves.dealer]["val"] );
				printcotizador()
				return false;
			}
			$(".dealer_bttn").removeClass("check_dealer")
			$(this).addClass("check_dealer")
			$(".dealer_map").html("")
			$(".ccs-id-"+ccs+" .dealer_map" ).html("<img src='"+vwdealers[ccs].img+"' />")
			currentMarker = ccs;
			google.maps.event.trigger(markers[ccs], 'click');
			return false;
		})

		// EDITAR modelo
		$(".nav_1").click(function(){
			if(aaa || $(this).hasClass("nuller") || data_saves.data) return;
			data_saves.model = null
			printcotizador()
			return false;
		})

		// EDITAR dealer
		$(".nav_2").click(function(){
			if(aaa || $(this).hasClass("nuller") || data_saves.data) return;
			data_saves.dealer = null
			printcotizador()
			return false;
		})

		$(".nav_4").click(function(){
			if(aaa || $(this).hasClass("nuller") || data_saves.data) return;
			data_saves.model2 = data_saves.model;
			data_saves.model = null
			second_model = true
 			printcotizador()
 			return false;
 		})

		$(".dealer_select").click(function(){
			console.log("yeap?")
		})

		$(".bttn_header").click(function(){
			var url = $(this).data("url")
			window.location.href = url
		})

		$("#bttlgoogle").click( function(){
			var lat = vwdealers[data_saves.dealer]["lat"]
			var lon = vwdealers[data_saves.dealer]["lon"]
			console.log(lat)
			window.location.href = "https://www.google.com/maps/dir//"+lat+","+lon+"/@"+lat+","+lon+"?dcr=0";
			$("#bttlgoogle").html("Abrir en Google Maps ...")
		})

		$("#bttlwaze").click( function(){
			var lat = vwdealers[data_saves.dealer]["lat"]
			var lon = vwdealers[data_saves.dealer]["lon"]
			//"waze://?ll=4.7211645,-74.0606375&navigate=yes"
			window.location.href = "https://waze.com/ul?ll="+lat+","+lon;
			$("#bttlwaze").html("Abrir en Waze ...")
			
		})

		$(".db-models").append("<div class='clearfix'></div>")


		//CALL MODAL
		$(".call-modal, .menu-hbg, .bttn_menu, .modal_bg").click(function(){
			if(aaa || $(this).hasClass("nuller") || data_saves.data) return;
			aaa = true;
			if( Number( $(".menu-hbg").data("state") ) == 0 ){
				$(".modal_bg").css("display", "block")
				$(".menu-hbg").data("state", 1)
				$(".menu-hbg").removeClass("dsktp_fix")
				$(".menu-hbg").css({"display": "block"})
				var modal = $(this).data("modal")
				var modal_base = $(this).data("modal-box")
				$(".menu_open").css({"display": "none"})
				$(".wrapper").addClass("wrapper_nuller")
				$(".modal_section").css({"display": "none"})
				$(".modal-"+modal).css({"display": "block"})
				$(".modal-box-"+modal_base).addClass("modal_path_active")
				console.log("whate the funk?")
				// $(".modal_path").addClass("modal_path_active")
				$(".top-title").addClass("top-title-modal")
				setTimeout( function(){
					$(".modal-box-"+modal_base).addClass("modal_path_active_extra")
					aaa = false;
				},1000)
			}else{
				if( $(this).hasClass("bttn_menu") ){
					var city = $(this).data("city")
					filterCity(city)
				}
				$(".modal_bg").css("display", "none")
				$(".menu-hbg").data("state", 0)
				$(".menu-hbg").addClass("dsktp_fix")
				$(".menu-hbg").css({"display": "none"})
				$(".menu_open").css({"display": "block"})
				$(".modal_section").css({"display": "block"})
				$(".top-title").removeClass("top-title-modal")
				$(".modal_path").removeClass("modal_path_active")
				$(".modal_path").removeClass("modal_path_active_extra")
				setTimeout( function(){
					$(".wrapper").removeClass("wrapper_nuller")
					aaa = false;
				},300)
			}
		})


		$( "#dblnkd-form" ).validate({
			    messages: {
			        concesionario: "Se requiere Concesionario.",
			        modelo: "Se requiere Modelo.",
			        nombres: "Se requiere Nombre.",
			        apellidos: "Se requiere Apellido.",
			        telefono: "Teléfono no valido. (56 + 9 digitos)",
			        rut: "Rut no valido. (8 a 9 digitos)",
			        acepto: "Confirmar.",
			        correo: {
			            required: "Se requiere email.",
			            email: "Debe proveer un correo valido."
			        }
			    },rules: {
			    telefono: {
			    	required: true,
			    	number: true,
			    	maxlength: 9,
			    	minlength: 9
			    },rut: {
			    	required: true,
			    	minlength: 9,
			    }
			},
		    focusInvalid: false,
		    submitHandler: function() {
		    	//guardarDatos();        
		    }
		});

		$( "#dblnkd-form-prev" ).validate({
			    messages: {
			        prev: {
			            required: "Se requiere email.",
			            email: "Debe proveer un correo valido."
			        }
			    },
		    focusInvalid: false,
		    submitHandler: function() {
		    	//guardarDatos();        
		    }
		});

		$(".bttn-submit").click( function(){
				if ( $("#dblnkd-form").valid() ) {
					$(".bttn-submit").addClass("nuller")
		        	guardarDatos(); 
		        	//return false;
		    	}else{
					$("#dblnkd-form").valid()
				}
		})

		$( "#dblnkd-form" ).validate({
			    messages: {
			        correo: {
			            required: "Se requiere email.",
			            email: "Debe proveer un correo valido."
			        }
			    },
		    focusInvalid: false,
		    submitHandler: function() {
		    	//guardarDatos();        
		    }
		});

		$(".bttn-submit-prev").click( function(){
				if ( $("#dblnkd-form-prev").valid() ) {
					$(".bttn-submit-prev").addClass("nuller")
		        	//guardarDatos(); 
		        	//return false;
		        	var correo = $('input[name=prev').val();
		        	data_saves.email = correo
		        	printcotizador();
		    	}else{
					$("#dblnkd-form-prev").valid()
				}
		})

		printcotizador();

}

function printcotizador(){

	$(".nav, .nav_box, .models, .dealer, .form_path, .form_path_prev, .result_path").addClass("hidder")
	if(data_saves.model == null){
		aaa = true;
		$(".models").addClass("al-0")
		$(".models").removeClass("hidder")
		setTimeout( function(){
			$(".models").removeClass("al-0")
			aaa = false;
		},100)

	}else{
		$(".nav").removeClass("hidder")
		$(".nav_1").removeClass("hidder")
		$(".nav_4").removeClass("hidder")
		if(data_saves.model2 !== null){$(".nav_4").html("<div class='model_nav'><img src='"+vwmodelos[data_saves.model2].car_img+"'/><h1>"+vwmodelos[data_saves.model2].name+"</h1></div><div class='bg edit'></div>")}
		$(".nav_1").html("<div class='model_nav'><img src='"+vwmodelos[data_saves.model].car_img+"'/><h1>"+vwmodelos[data_saves.model].name+"</h1></div><div class='bg edit'></div>")
		//setTimeout( function(){$("#a_dealer")[0].click()},9)	
			
		navCheckout()
		if(data_saves.dealer == null){
			aaa = true;
			$(".dealer").addClass("al-0")
			$(".dealer").removeClass("hidder")
			setTimeout( function(){
				$(".dealer").removeClass("al-0")
				if(!google_key) setMAPSGOOGLE();
				google_key = true
				aaa = false;
			},100)
		}else{



			$(".nav_2").removeClass("hidder")
			$(".nav_2").html("<span>Concesionario:</span><h1>"+vwdealers[data_saves.dealer].name+", "+vwdealers[data_saves.dealer].city+"</h1><div class='bg edit'></div>")
			// setTimeout( function(){$("#a_data")[0].click()},12)	
			navCheckout()
			if(data_saves.email == false){
				aaa = true;
				$(".form_path_prev").addClass("al-0")
				$(".form_path_prev").removeClass("hidder")
				setTimeout( function(){
					$(".form_path_prev").removeClass("al-0")
					aaa = false;
				},100)
			}else{
				$('input[name=correo').val(data_saves.email);
				if(data_saves.data == false){
					console.log("emial guardaddo: ",data_saves.email)
					aaa = true;
					$(".form_path").addClass("al-0")
					$(".form_path").removeClass("hidder")
					setTimeout( function(){
						$(".form_path").removeClass("al-0")
						aaa = false;
				},100)
				}else{
					var name = document.querySelector('[name="nombres"]').value;
					var last_name = document.querySelector('[name="apellidos"]').value;
					$(".result_title h1").html("Hola " + name + " " + last_name +", ")
					$(".result_path").removeClass("hidder");
				}

			}
			
		}
	}

}


function filterCity(a){
	var b = Number(a) + 1
	console.log(b)
	$(".dealer_bttn").removeClass("hidder")
	$(".dealer_bttn").addClass("hidder")
	$(".city-id-"+b).removeClass("hidder")
}

function navCheckout(){
	return false;
}


$( document ).ajaxStart(function() {
	$("#dblnkd-form").hide(0);
	$(".form_msg").css({"display": "block"})
	$(".form_msg").html("Enviando datos...")
	$(".nav").addClass("nuller")
})

function guardarDatos(){

	fullurl = window.location.href.split('#')[0];
	var lm_url = "https://www.cotizadoronline.cl/leads/saveLead";
	lm_url = "http://localhost:8888/chile-lm2/leads/saveLead";
	// lm_url = "#";
	var nombre = window.btoa( $('input[name=nombres]').val() );
	var apellido =  window.btoa( $('input[name=apellidos]').val() );
	var val_array = [
		{"name": "name", "val" : nombre},
		{"name": "lastname", "val" : apellido},
		{"name": "model", "val" : data_saves.model},
		{"name": "dealer", "val" : data_saves.dealer}
	]

	var campoA = $('input[name=campo1a]').val();
	var campoB = $('#campo1b').find(":selected").text();

	$('input[name=campo1]').val(campoA + " " + campoB)
	console.log(val_array)
	param = $('#dblnkd-form').serialize();


	$.ajax ({
		type: "POST",
		data: "submit=&"+param,
		url: lm_url,
		success: function(datos){
			console.log("respuesta:", datos)
			if(datos == -1 || datos == "-1") {
				
				$(".form_msg").html("Error.") 
			}else{
				var json = datos;
				$(".form_msg").html("No es posible enviar los datos en este momento, por favor intente más tarde o intente con otro modelo.") 
				var name = document.querySelector('[name="nombres"]').value;
				data_saves.data = true;
				$(".form_msg").html("<h2>Hola "+name+",</h2><br><p>Tu regístro fue exitoso.</p>")
				
				$(".form_msg").html("<h2>Hola "+name+",</h2><br><p>Tu regístro fue exitoso.</p>")
				var newUrl = ( Urlcoder(val_array,fullurl) )
				finalURL = "https://www.cotizadoronline.cl/volkswagen/register.html?" + newUrl
				//window.location.href = (finalURL,"_self")
				printcotizador();
				setTimeout(function(){
		        	//urlCaller(finalURL);
		        },700)
			}
			
		}
	});
}


setcotizador();

function urlCaller(a){
 		window.open(a,"_self")
}







