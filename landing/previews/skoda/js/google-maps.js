

var markers = [];
var currentMarker = -1;
var contactTopPosition =0;
var iconVW = "M0-108.4c-19,0-34.4,15.4-34.4,34.4S0,0,0,0s34.4-55,34.4-74S19-108.4,0-108.4z M-11.7-99.6h7.5c0.2,0,0.4,0,0.6,0.1c0.2,0.1,0.3,0.2,0.5,0.4l2.5,2.7H0l2.5-2.7c0.2-0.2,0.3-0.3,0.5-0.4s0.3-0.1,0.6-0.1H11c0.2,0,0.3,0,0.3,0.1s0,0.2-0.2,0.3l-6.2,6.6c-0.2,0.2-0.3,0.3-0.5,0.4C4.3-92.1,4.1-92,3.9-92h-8.5c-0.2,0-0.4,0-0.6-0.1c-0.2-0.1-0.3-0.2-0.5-0.4l-6.2-6.6c-0.1-0.1-0.2-0.3-0.2-0.3C-12-99.5-11.9-99.6-11.7-99.6z M15.7-49.9c-0.6,1.7-1.6,3.1-3,4.3c-1.4,1.2-3.4,2-5.8,2.6s-5.6,0.9-9.4,0.9c-2.6,0-5-0.1-7.3-0.4c-2.2-0.3-4.2-0.6-5.9-1.1c-0.4-0.1-0.6-0.4-0.5-0.8l1.4-8.3c0.1-0.4,0.3-0.6,0.7-0.5c1.1,0.2,2.1,0.3,3.1,0.5s1.8,0.2,2.6,0.3c0.8,0.1,1.7,0.1,2.5,0.2s1.7,0.1,2.7,0.1c1.9,0,3.3-0.1,4.5-0.2c1.1-0.1,2-0.3,2.6-0.6s1-0.6,1.2-1.1s0.3-1,0.3-1.6c0-0.4-0.1-0.8-0.3-1.1C4.9-57,4.6-57.4,4-57.8s-1.2-0.8-2.2-1.3c-0.9-0.5-2.1-1.1-3.6-1.8L-7-63.4c-2.1-1-3.8-2-5.1-3.1c-1.3-1.1-2.3-2.1-3.1-3.2s-1.2-2.2-1.5-3.3c-0.3-1.1-0.4-2.2-0.4-3.4c0-2.4,0.4-4.4,1.1-6.1c0.7-1.6,1.9-3,3.5-4c1.6-1,3.5-1.7,5.9-2.2c2.4-0.4,5.2-0.6,8.5-0.6c2.2,0,4.2,0.1,6.2,0.3c2,0.2,3.8,0.6,5.5,1.1c0.4,0.1,0.6,0.4,0.5,0.8l-1.4,8c-0.1,0.4-0.3,0.6-0.7,0.5c-0.8-0.1-1.6-0.2-2.5-0.3C8.6-78.9,7.8-79,7-79.1c-0.8-0.1-1.5-0.1-2.2-0.1c-0.7,0-1.2,0-1.7,0c-1.6,0-3,0-4.2,0.1c-1.2,0.1-2.1,0.2-2.9,0.4c-0.7,0.2-1.3,0.4-1.6,0.8C-5.8-77.6-6-77.1-6-76.6c0,0.5,0.1,0.9,0.3,1.3c0.2,0.4,0.5,0.7,1,1.1s1.2,0.8,2.1,1.2c0.9,0.4,2,1,3.5,1.6l6,2.7c1.9,0.9,3.5,1.7,4.7,2.6c1.2,0.9,2.2,1.8,3,2.8c0.7,1,1.2,2.1,1.5,3.3c0.3,1.2,0.4,2.6,0.4,4.1C16.5-53.5,16.3-51.6,15.7-49.9z"


function initMap() {

	var iconvw = {
	    path: iconVW,
	    fillColor: '#4BA82E',
	    fillOpacity: 1,
	    anchor: new google.maps.Point(0,0),
	    strokeWeight: 0.7,
	    strokeColor: '#FFFFFF',
	    scale: .4
	}

	var mapVw = new google.maps.Map(document.getElementById('map-vw'), {
		zoom: 5,
		center: {lat: -32.7226802, lng: -72.8298594}
	});

	// if (navigator.geolocation) {
	//  	navigator.geolocation.getCurrentPosition(function(position) {
 //            $(".result-geopos").show();
 //            var pos = {
 //              "lat": position.coords.latitude,
 //              "lng": position.coords.longitude
 //            }

 //            console.log(pos.lat +" - "+ pos.lng)
 //            NearestCity(pos.lat, pos.lng)
 //        }, function(error){

 //        	 $(".result-geopos").hide();
 //        })
	//  }else{
	//  	$(".result-geopos").hide();
	//  }

	for (var i in vwdealers) {
			markers[i] = new google.maps.Marker({
	        position: new google.maps.LatLng(vwdealers[i].lat, vwdealers[i].lon),
	        map: mapVw,
	        icon: iconvw
      	});

      	google.maps.event.addListener(markers[i], 'click', (function(marker, i) {
        return function() {
          //infowindow.setContent(locations[i][0]);
          //infowindow.open(map, marker);
          mapVw.setCenter(markers[i].getPosition());
    	  mapVw.setZoom(15);

    	  if (currentMarker !== i){
    	  	console.log("que?")
    	  	$(".ccs-id-"+ i).trigger('click')
    	  	var elem = $(".ccs-id-"+ i)
    	  	$(".dealer_menu").animate({scrollTop: $(".dealer_menu").scrollTop()+elem.position().top})
    	  	//$(".dealer_menu").scrollTop($(".ccs-id-"+ i).position().top)
    	  	console.log($(".ccs-id-"+ i).position().top)
    	  }
    	  // Bval = true;
    	  // currentMarker = i;
    	  // if(validBttn == 0){
    	  // 	 $(".maps-placer-bttn").scrollTop($(".map-bttn-"+i).position().top)
    	  // }
    	 	// validBttn = 0;
    	  $(".map-bttn-container").children().removeClass("active-marker")
    	  $(".map-bttn-"+i).addClass("active-marker")
    	  $(".bttn-continuar").removeClass("bttn-null")
        }
     	})(markers[i], i));

	}


	

}

var validBttn = 0;
var currentCity;
var citiesInnerData = {};


// vwdealers.push({})
// 	vwdealers[27]["val"] = "Villavicencio, Casa Toro, Anillo Vial Vía Acacias";
// 	vwdealers[27]["name"] = "Casa Toro";
// 	vwdealers[27]["dir"] = "Anillo Vial Vía Acacias";
// 	vwdealers[27]["coorX"] = 4.133138;
// 	vwdealers[27]["coorY"] = -73.625217;
// 	vwdealers[27]["City"] = "Villavicencio";

var arrayhelper = ""

console.log(arrayhelper)

// function setMAPS(){
// 	for (i = 0; i < vwdealers.length; i++) {
// 		//arrayhelper += "{'val':'"+ vwdealers[i].val +"', 'name': '"+vwdealers[i].name+"', 'dir': '"+vwdealers[i].dir+"', 'coorX': "+vwdealers[i].coorX+", 'coorY': "+vwdealers[i].coorY+", 'City': '"+vwdealers[i].City+"', 'Tel': 'null'},"
// 		console.log(arrayhelper)
// 		$(".map-bttn-container").append("<div class='map-bttn map-bttn-"+i+" cc-"+vwdealers[i].City+"'><h4>"+vwdealers[i].City+", "+vwdealers[i].name+"</h4><p>"+vwdealers[i].dir+"</p></div>")
// 		$(".map-bttn-"+i).data("foo",i)
// 		var curcity = vwdealers[i].City
// 		if( citiesInnerData.hasOwnProperty(vwdealers[i].City) ){
// 			var property = vwdealers[i].City;
// 			citiesInnerData[property].numb += 1;
// 			//console.log(citiesInnerData[property].numb) 
// 		}else{
			
// 			var object = vwdealers[i].City;
// 			citiesInnerData[vwdealers[i].City] = {"numb" : 1, "firstDealer": i};
			
// 		}
// 		//console.log(citiesInnerData)
// 		$(".map-bttn-"+i).click(function(){
// 			var cur = $(this).data("foo")
// 			animating = true;
// 			validBttn = 1;
// 			google.maps.event.trigger(markers[cur], 'click');
			
            
//         });

//         var ciudades;
//         if(currentCity !== vwdealers[i].City){
//         	currentCity = vwdealers[i].City
//         	$("#filtro-ciudad").append("<option value='"+currentCity+"''>"+currentCity+"</option>")
//         }
        
// 	}

// 	$(".map-bttn").addClass("filtro-cc")

// 	$("#filtro-ciudad").change( function(){
// 		if( $("#filtro-ciudad").val() == "todos" ){
// 			console.log("todos dude")
// 			$(".map-bttn").addClass("filtro-cc")
// 			// ---> function center map colombia 
// 		}else{
// 			//console.log("selected- " + $("#filtro-ciudad").val())
// 			$(".map-bttn").removeClass("filtro-cc")
// 			$( ".cc-"+ $("#filtro-ciudad").val() ).addClass("filtro-cc")

// 			// ---> center map city
// 			var property = $("#filtro-ciudad").val()
// 			//console.log(citiesInnerData[property].numb + " %%%%")
// 			if(citiesInnerData[property].numb == 1){
// 				$( ".map-bttn-"+ citiesInnerData[property].firstDealer ).trigger( "click" );
// 			}else{
// 				console.log("no geolocation")
// 			}
// 		}
// 	})
// }



function setMAPSGOOGLE(){
	$.getScript("https://maps.googleapis.com/maps/api/js?key=AIzaSyA2MBXwDCl0tCPMeW0Yw6M31WJ-l1Vhwsk&callback=initMap");
	
}

// (function ($) {
//     $.fn.isOnScreen = function () {
//         var topView = $(window).scrollTop();
//         var botView = topView + $(window).height();
//         var topElement = this.offset().top;
//         var botElement = topElement + this.height();
//         return ((botElement <= botView) && (topElement >= topView));
//     }
// })(jQuery);

// function Deg2Rad(deg) {
//   return deg * Math.PI / 180;
// }

// function PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
//   lat1 = Deg2Rad(lat1);
//   lat2 = Deg2Rad(lat2);
//   lon1 = Deg2Rad(lon1);
//   lon2 = Deg2Rad(lon2);
//   var R = 6371; // km
//   var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
//   var y = (lat2 - lat1);
//   var d = Math.sqrt(x * x + y * y) * R;
//   return d;
// }

// function NearestCity(latitude, longitude) {
//   var mindif = 99999;
//   var closest;

//   for (index = 0; index < vwdealers.length; ++index) {
//     var dif = PythagorasEquirectangular(latitude, longitude, vwdealers[index].coorX, vwdealers[index].coorY);
//     if (dif < mindif) {
//       closest = index;
//       mindif = dif;
//     }
//   }

//   // echo the nearest city
//   console.log("the nearest " + vwdealers[closest].name);
//   $(".result-geopos").html("<div class='maps-extra-ico'><img src='images/icons/ico-conce.svg' /></div><h5>Aquí tu concesionario más cercano:</h5><div class='bttn-submit2 rounded-corners bttn-atajo' style='margin:0'><p>"+vwdealers[closest].name+"</p></div>")
//   $(".bttn-atajo").data("foo", closest)
//   $(".bttn-atajo").click(function(){
//   	var numb = $(this).data("foo")
//   	$( ".map-bttn-"+ numb ).trigger( "dblclick" );
//   })
  
// }



