var vwmodelos = [{
	"name":"Fabia",
	"val":137,
	"car_img":"images/models_skoda/fabia.jpg",
	"id" : 1
}, {
	"name":"Spaceback",
	 "val":138,
	 "car_img":"images/models_skoda/rapid_spaceback.jpg",
	"id" : 2
}, {
	"name":"Octavia",
	 "val":139,
	 "car_img":"images/models_skoda/octavia.jpg",
	"id" : 3
},{
	"name":"Octavia RS",
	 "val":140,
	 "car_img":"images/models_skoda/octavia_rs.jpg",
	"id" : 4
},{
	"name":"Octavia Scout",
	 "val":141,
	 "car_img":"images/models_skoda/octavia_scout.jpg",
	"id" : 5
},{
	"name":"Superb",
	 "val":142,
	 "car_img":"images/models_skoda/superb.jpg",
	"id" : 6
},{
	"name":"Kodiaq",
	 "val":143,
	 "car_img":"images/models_skoda/kodiaq.jpg",
	"id" : 7
}
];

var vwdealers = [	
	{  
	   "name":'Skoda pruebas',
	   "val":35,
	   "tel":'(+56 22) 3224600',
	   "dir":'Av. Las Condes 12.260, Vitacura, Santiago',
	   "lat":-33.372778,
	   "lon":-70.518639,
	   "city":'Santiago',
	   "city_id": 1,
	   "id":1,
	   "img": "images/dealers/dealer-1.jpg"
	}
]