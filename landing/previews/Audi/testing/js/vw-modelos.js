var vwmodelos = [{
	'name': 'Cross Up',

	'val': 'cross_up!',

	'car_img': 'images/models/crossup.png',

	'head_img': 'images/models/modelo_header/hero-crossup-volkswagen.jpg',

	'title_w': true,

	'model_url': 'https://www.volkswagen.co/cross-up',

	'section_img': 'images/models/modelo_header/section-cross-up-volkswagen.jpg'

}, {
	'name': 'Gol',

	'val': 'gol',

	'car_img': 'images/models/gol.png',

	'head_img': 'images/models/modelo_header/hero-gol-volkswagen.jpg',

	'title_w': true,

	'model_url': 'https://www.volkswagen.co/nuevo-gol/gol',

	'section_img': 'images/models/modelo_header/section-gol-volkswagen.jpg'

}, {
	'name': 'Voyage',

	'val': 'voyage',

	'car_img': 'images/models/voyage.png',

	'head_img': 'images/models/modelo_header/hero-voyage-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/voyage',

	'section_img': 'images/models/modelo_header/section-voyage-volkswagen.jpg'

}, {
	'name': 'CrossFox',

	'val': 'crossfox',

	'car_img': 'images/models/crossfox.png',

	'head_img': 'images/models/modelo_header/hero-crossfox-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/crossfox',

	'section_img': 'images/models/modelo_header/section-crossfox-volkswagen.jpg'

}, {
	'name': 'Beetle',

	'val': 'beetle',

	'car_img': 'images/models/beetle.png',

	'head_img': 'images/models/modelo_header/hero-beetle-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/the-beetle',

	'section_img': 'images/models/modelo_header/section-beetle-volkswagen.jpg'

}, {
	'name': 'Golf',

	'val': 'golf',

	'car_img': 'images/models/golf.png',

	'head_img': 'images/models/modelo_header/hero-golf-volkswagen.jpg',

	'title_w': true,

	'model_url': 'https://www.volkswagen.co/golf',

	'section_img': 'images/models/modelo_header/section-golf-volkswagen.jpg'

}, 
// {
// 	'name': 'Golf GTI',

// 	'val': 'golf_gti',

// 	'car_img': 'images/models/golfgti.png',

// 	'head_img': 'images/models/modelo_header/hero-golf-gti-volkswagen.jpg',

// 	'title_w': true,

// 	'model_url': 'https://www.volkswagen.co/golf-gti',

// 	'section_img': 'images/models/modelo_header/section-golf-gti-volkswagen.jpg'

// }, 
{
	'name': 'Jetta',

	'val': 'jetta',

	'car_img': 'images/models/jetta.png',

	'head_img': 'images/models/modelo_header/hero-jetta-volkswagen.jpg',

	'title_w': true,

	'model_url': 'https://www.volkswagen.co/jetta',

	'section_img': 'images/models/modelo_header/section-jetta-volkswagen.jpg'

}, {
	'name': 'Jetta GLI',

	'val': 'jetta_gli',

	'car_img': 'images/models/jettagli.png',

	'head_img': 'images/models/modelo_header/hero-jetta-gli-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/jetta-gli',

	'section_img': 'images/models/modelo_header/section-jetta-gli-volkswagen.jpg'

}, {
	'name': 'Tiguan',

	'val': 'tiguan',

	'car_img': 'images/models/tiguan.png',

	'head_img': 'images/models/modelo_header/hero-tiguan-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/nuevo-tiguan-allspace',

	'section_img': 'images/models/modelo_header/section-tiguan-volkswagen.jpg'

}, {
	'name': 'Passat',

	'val': 'passat',

	'car_img': 'images/models/passat.png',

	'head_img': 'images/models/modelo_header/hero-passat-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/passat',

	'section_img': 'images/models/modelo_header/section-passat-volkswagen.jpg'

}, {
	'name': 'Saveiro CS',

	'val': 'nuevo_saveiro_cs',

	'car_img': 'images/models/saveiro-cs.png',

	'head_img': 'images/models/modelo_header/hero-saveiro-volkswagen.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/nuevo-saveiro-cabina-sencilla',

	'section_img': 'images/models/modelo_header/section-saveiro-volkswagen.jpg'

}, {
	'name': 'Saveiro Plus',

	'val': 'nuevo_saveiro_plus',

	'car_img': 'images/models/saveiro-plus.png',

	'head_img': 'images/models/modelo_header/hero-saveiro-cross.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/nueva-saveiro',

	'section_img': 'images/models/modelo_header/section-saveiro-cross.jpg'

}, {
	'name': 'Saveiro Cross',

	'val': 'nuevo_saveiro_cross',

	'car_img': 'images/models/saveiro-cross.png',

	'head_img': 'images/models/modelo_header/hero-saveiro-cross.jpg',

	'title_w': false,

	'model_url': 'https://www.volkswagen.co/nuevo-saveiro-cross',

	'section_img': 'images/models/modelo_header/section-saveiro-cross.jpg'

}];

var animating = false;
var current_model_global;
var utm_model_global;

function setModelos(){
		 

		for (var i = 0;  i < vwmodelos.length ; i++) {
			$(".db-models").append("<div class='car-model hover car-id-"+i+"'><img src='"+vwmodelos[i].car_img+"'/><p>"+vwmodelos[i].name+"</p></div>")
			$(".car-id-"+i).data("foo", i)
			$(".car-id-"+i).click(function(){
				if (animating) return;
				animating = true;
				var cur = $(this).data("foo");
				//$(".exchange_img").html("<img src='"+vwmodelos[cur].head_img+"'/>").hide().fadeIn(1000);
				// $(".wrapper-titles").removeClass("w-t-w")
				// if(vwmodelos[cur].title_w) $(".wrapper-titles").addClass("w-t-w")
				current_model_global = cur;
				OpenB(cur)
			})

		}
		var utm_modelo = getURLParameter('utm_content');
		console.log(utm_modelo +  " ??")

		if(utm_modelo !== null){
			for (var i = 0;  i < vwmodelos.length ; i++){
				if(utm_modelo == vwmodelos[i].val){
					animating = true;
					var cur = i
					current_model_global = cur;
					OpenB(cur)
				}
			}
		}else{
			console.log("si null")

		}

		$(".db-models").append("<div class='clearfix'></div>")

		$(".title-bttn-models").click( function(){
			var active = $(this).hasClass("stBttn")
			if (!active || animating) return;
			animating = true;
			OpenA()
			
		})

		$(".title-bttn-conce").click( function(){
			var active = $(this).hasClass("stBttn")
			if (!active || animating) return;
			animating = true;
			console.log(currentMarker)
			OpenB()
			
		})

		$(".bttn-continuar").click( function(){
			var active = $(this).hasClass("bttn-null")
			if (active || animating) return;
			animating = true;
			OpenC()
			
		})

}





