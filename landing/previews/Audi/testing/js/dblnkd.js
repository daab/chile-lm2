function getURLParameter(name) {
  	return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
	}

function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}


var dataObject, retrievedObject;

function dbLaunchPage(){
	//localStorage.clear();
	console.log("launch easy2")
	retrievedObject = localStorage.getItem('dataObject');
	retrievedObject = JSON.parse(retrievedObject)
	console.log(retrievedObject);
	//ga('send', 'event', 'function', 'PaginaCargada', 'carga exitosa');

	var utm_s = getURLParameter('utm_source');
	console.log(utm_s)
	$('input[name=source]').val( utm_s );
	$(".title-bttn-models").removeClass("stNull")
	$(".title-bttn-models").addClass("stNormal")
	setModelos();
	setMAPS();
	$('input[name=source]').val( utm_s );

	$("#dblnkd-form")[0].reset();


	$( "#dblnkd-form" ).validate({
	    messages: {
	        ciudad: "Se requiere Ciudad.",
	        concesionario: "Se requiere Concesionario.",
	        modelo: "Se requiere Modelo.",
	        nombres: "Se requiere Nombre.",
	        apellidos: "Se requiere Apelldio.",
	        telefono: "Teléfono no valido. (7 o 10 digitos)",
	        promociones: "Confirmar.",
	        correo: {
	            required: "Se requiere email.",
	            email: "Debe proveer un correo valido."
	        }
	    },rules: {
	    telefono: {
	    	required: true,
	    	number: true,
	    	maxlength: 10,
	    	minlength: 7
	    }
	},
    focusInvalid: false,
    submitHandler: function() {
    	//guardarDatos();        
    }
	});
	$(".bttn-submit").data("foo", 0)
	$(".bttn-submit").click( function(){
		if ($("#dblnkd-form").valid() && $( this ).data( "foo") == 0) {
			$(".bttn-submit").data("foo", 1)
			$(".bttn-submit").addClass("bttn-null")
        	guardarDatos(); 
        	return false;
    	}else if( $( this ).data( "foo") == 0 ){
			$("#dblnkd-form").valid()
		}
	})
	

	$(".link-vwhome").click(function (){
		////ga('send', 'event', 'Clicks', 'home-bttn', 'home button');
		window.open("http://www.volkswagen.co/","_blank")
	});


    $(".modal-exit").click(function (){
		////ga('send', 'event', 'Clicks', 'modal-exit', 'exit terminos');
		$(".db-modal").fadeOut(300)
		$(".modal-content").hide(0)
		$(".modal-exit").hide(0)
	});

	$(".call-terms").click(function (){
			////ga('send', 'event', 'Clicks', 'terms-bttn', 'visita terminos');
			modalSet($(".modal-terms"));
	});

	$(".modal-exit, .db-modal-bg").click(function (){
			////ga('send', 'event', 'Clicks', 'modal-exit', 'modal exit');
			modalOut();
	});

	$(".so-fb").click(function (){
		window.open("https://www.facebook.com/VolkswagenCO");
		
	})

	$(".so-tw").click(function (){
		window.open("https://twitter.com/VolkswagenCo");
		
	})

	$(".so-in").click(function (){
		window.open("https://www.instagram.com/volkswagen_colombia/");
		
	})

	$(".so-yt").click(function (){
		window.open("https://www.youtube.com/channel/UCmT6DM0HRrWItN00CLncMRg/feed");
		
	})


}

//modal In and out, In and out, In and out, In and out, In and out, In and out, In and out,
	var scrollback = $(window).scrollTop();
	function modalSet(x){
		scrollback = $(window).scrollTop();
		$(".db-modal").show(0)
	    $(x).fadeIn(600)
	    $(".modal-exit").fadeIn(600)
	    $(".wrapper").addClass("wrapper-modal-open");
		$("html").scrollTop(0);
	};

	function modalOut(){
		$(".wrapper").removeClass("wrapper-modal-open");
		$(".db-modal").fadeOut(300)
		$(".modal-content").hide(0)
		$(".modal-exit").hide(0)
		$("html").scrollTop(scrollback);
	}


function anchorMan(){
	$('html, body').animate({
        scrollTop: $(".anchordiv").offset().top - 90
    }, 3000);
    return false;
}

//function tester concesionario David

function supperdupperpageTester(){
	console.log("detonado pruebas")
	$('input[name=concesionario]').val('Pruebas, Concesionario de pruebas, para test de LEADS');
	$('input[name=marca]').val('Volkswagen PKW');
	guardarDatos(); 
}

