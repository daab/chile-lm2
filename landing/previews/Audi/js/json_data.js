var vwmodelos = [{
	"name":"A1",
	"val":"a1",
	"car_img":"images/audi_models/a1/a1sb-size-h236-2016.png",
	"id" : 1
}, {
	"name":"A3",
	 "val":"a3",
	 "car_img":"images/audi_models/a3/a3sb-size-h236.png",
	"id" : 2
}, {
	"name":"A4",
	 "val":"a4",
	 "car_img":"images/audi_models/a4/a4limo-size-h236.png",
	"id" : 3
},{
	"name":"A5",
	 "val":"a5",
	 "car_img":"images/audi_models/a5/a5-sportback-seitenansicht.png",
	"id" : 4
},{
	"name":"A6",
	 "val":"a6",
	 "car_img":"images/audi_models/a6/a6limo-size-h236.png",
	"id" : 5
},{
	"name":"A7",
	 "val":"a7",
	 "car_img":"images/audi_models/a7/a7sb-size-h236-2016.png",
	"id" : 6
},{
	"name":"A8",
	 "val":"a8",
	 "car_img":"images/audi_models/a8/s8plus-size-h236.png",
	"id" : 7
},{
	"name":"Q2",
	 "val":"q2",
	 "car_img":"images/audi_models/q2/q2-size-h236.png",
	"id" : 8
},{
	"name":"Q3",
	 "val":"q3",
	 "car_img":"images/audi_models/q3/q3-size-h236-2016.png",
	"id" : 9
},{
	"name":"Q5",
	 "val":"q5",
	 "car_img":"images/audi_models/q5/q5-2017-size-h236.png",
	"id" : 10
},{
	"name":"Q7",
	 "val":"q7",
	 "car_img":"images/audi_models/q7/q7-size-h236-2016.png",
	"id" : 11
},{
	"name":"TT",
	 "val":"tt",
	 "car_img":"images/audi_models/tt/ttsr.png",
	"id" : 12
},{
	"name":"R8",
	 "val":"r8",
	 "car_img":"images/audi_models/r8/r8-size-h236-2016.png",
	"id" : 13
}
];

var vwdealers = [	
	{  
	   "name":'Audi Zentrum',
	   "val":'Santiago, Audi Zentrum, Av. Raúl Labbé 12509 Lo Barnechea. - Audi',
	   "tel":'Telefono Pendiente',
	   "dir":'Av. Raúl Labbé 12509 Lo Barnechea, Santiago',
	   "lat":-33.3661703,
	   "lon":-70.5176188,
	   "city":'Santiago',
	   "city_id": 1,
	   "id":1,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Klassik Car',
	   "val":'Santiago, Klassik Car, Av Vitacura 8092. Vitacura - Audi',
	   "tel":'Telefono Pendiente',
	   "dir":'Av Vitacura 8092. Vitacura, Santiago',
	   "lat":-33.3850394,
	   "lon":-70.5578307,
	   "city":'Santiago',
	   "city_id": 1,
	   "id":2,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Carmona',
	   "val":'La Serena, Carmona, Balmaceda 3681 La Serena. - Audi',
	   "tel":'Telefono Pendiente',
	   "dir":'Carmona, Balmaceda 3681 La Serena.',
	   "lat":-29.9350822,
	   "lon":-71.2625822,
	   "city":'La Serena',
	   "city_id": 2,
	   "id":3,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Cartoni',
	   "val":'Viña del Mar, Cartoni, Av. Libertad 945 Viña del Mar. - Audi',
	   "tel":'Telefono Pendiente',
	   "dir":'Av. Libertad 945 Viña del Mar, Viña del Mar',
	   "lat":-33.013717,
	   "lon":-71.549853,
	   "city":'Viña del Mar',
	   "city_id": 3,
	   "id":4,
	   "img": "images/dealers/dealer-1.jpg"
	},
	{  
	   "name":'Salazar Israel',
	   "val":'Concepcion, Salazar Israel, Chacabuco 330 Concepcion. - Audi',
	   "tel":'Telefono Pendiente',
	   "dir":'Salazar Israel, Chacabuco 330, Concepcion',
	   "lat":-36.812281, 
	   "lon":-73.059449,
	   "city":'Concepcion',
	   "city_id": 4,
	   "id":5,
	   "img": "images/dealers/dealer-1.jpg"
	}
]