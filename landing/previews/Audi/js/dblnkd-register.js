
function toTitleCase(str) {
    return str.replace(/(?:^|\s)\w/g, function(match) {
        return match.toUpperCase();
    });
}

var dataObject, retrievedObject;

function registerLoader(){
	$(".db-content-vw").append("<div class='dbvw-duo-section dbvw-sec1'><div class='dbvw-secimg'></div><h2></h2><p></p><a class='a-bttn-submit rounded-corners' target='_self' href='#'>...</a></div>")
	$(".db-content-vw").append("<div class='dbvw-duo-section dbvw-sec2'><div class='dbvw-secimg'></div><h2></h2><p></p><a class='a-bttn-submit rounded-corners' target='_self' href='#'>...</a></div>")
	$(".db-content-vw").append("<div class='clearfix'></div>")

	retrievedObject = localStorage.getItem('dataObject');
	retrievedObject = JSON.parse(retrievedObject)
	console.log(retrievedObject);

	$(".db-mainSection").hide(0);
	$(".end-message").fadeIn();
	var curr_nome = toTitleCase(retrievedObject.nome)
	//$(".exchange_img").html("<img src='"+vwmodelos[curr_model].head_img+"'/>").hide().fadeIn(1000);
	if(retrievedObject.datos !== "-1") {
		setTimeout(function() {
			
		    $(".mensaje-form").html("Hola "+curr_nome+", <br /><br /> Muchas gracias por tu inscripción, en poco tiempo nos comunicaremos contigo. Un asesor de <span>" + retrievedObject.conce + "</span> de " +retrievedObject.city+ " se comunicará contigo para cotizar tu Volkswagen "+ retrievedObject.modelo + ".")}, 1000)
		
		
	}else if(retrievedObject.datos == "-1") {
		setTimeout(function() {
		    $(".mensaje-form").html("Hola "+curr_nome+", <br /><br /> El correo <span>"+retrievedObject.invalid+"</span> ya está registrado en el concesionario <span>"+ retrievedObject.conce + "</span> de " +retrievedObject.city+ ", nuestros asesores lo contactaran pronto. Si desea volver a registrarse inténtalo con otro concesionario.")

		    $(".bttn-volver").css({"display": "block"});
		    
		}, 1000)
		
	}

	var linkOnSec1=retrievedObject.model_url

	$(".dbvw-sec1 .dbvw-secimg").html("<img src='"+retrievedObject.section_url+"'/>").hide().fadeIn(1000);
	$(".dbvw-sec1 h2").html(retrievedObject.modelo)
	$(".dbvw-sec1 p").html("Visita la página del Volkswagen "+retrievedObject.modelo)
	$(".dbvw-sec1 a").attr("href", linkOnSec1)
	$(".dbvw-sec1 a").html("Más informacón")
	$(".dbvw-secimg").click( function(){
		window.open(linkOnSec1,"_self")
	})
	

	var linkOnSec2="https://www.volkswagen.co/nuestros-modelos"
	$(".dbvw-sec2 .dbvw-secimg").html("<img src='images/dbvw-1000-modelos.jpg'/>").hide().fadeIn(1000);
	$(".dbvw-sec2 h2").html("Ver todos los modelos")
	$(".dbvw-sec2 p").hide()
	$(".dbvw-sec2 a").attr("href", linkOnSec2)
	$(".dbvw-sec2 a").html("Más informacón")
	$(".dbvw-secimg").click( function(){
		window.open(linkOnSec2,"_self")
	})
	
	//$(".dbvw-sec1 p").remove();

}
