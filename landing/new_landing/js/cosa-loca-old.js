var currentStep = 0;
var Bval = false;

function CloseA(n) {
	animating = true;
	var model = n;
	
	$('input[name=modelo]').val( vwmodelos[model].val );
	$(".title-bttn-models .title-models").html("<img src='"+vwmodelos[model].car_img+"' alt='"+vwmodelos[model].name+"'>")
	$(".title-bttn-models .title-text").html(vwmodelos[model].name);
	$(".title-bttn-models .title-text").css({"display":"table-cell"})
	$(".title-bttn-models .title-models").css({"display":"table-cell"})
	if (currentStep == 0 ) setMAPSGOOGLE();
	$(".title-bttn-models").addClass("change-active")
	$(".title-bttn-models .title-change").css({"display":"table-cell"})
	$(".title-bttn-models .title-ico").css({"display":"table-cell"})

	$(".title-bttn-models .title-models").addClass("active-model")
	$(".db-models").slideToggle(1000)
	if(Bval){ OpenC();  return;}
	OpenB(); ;
}

function CloseB(n){
	animating = true;
	if(!Bval){
		$(".title-bttn-conce .title-text").html("<span>Segundo paso</span><br>")
		$(".title-bttn-conce .title-change").css({"display":"none"})
		$(".title-bttn-conce .title-ico").css({"display":"none"})
		
	}else if(n == 3){
		$(".title-bttn-conce .title-text").css({"display":"table-cell"})
		$(".title-bttn-conce .title-ico").css({"display":"table-cell"})
		$(".title-bttn-conce .title-change").css({"display":"table-cell"})

		$(".title-bttn-conce .title-text").html(mapsAll[currentMarker].City+" - "+mapsAll[currentMarker].name)
		$('input[name=concesionario]').val( mapsAll[currentMarker].val);
		}else{
			$(".title-bttn-conce .title-text").css({"display":"table-cell"})
			$(".title-bttn-conce .title-ico").css({"display":"none"})
			$(".title-bttn-conce .title-change").css({"display":"none"})
			$(".title-bttn-conce .title-text").html(mapsAll[currentMarker].City+" - "+mapsAll[currentMarker].name)
			$('input[name=concesionario]').val( mapsAll[currentMarker].val);
		}
		
	
	$(".maps-conc").slideToggle(1200, function(){
					animating = false;
	})
}

function OpenB(){
	if(currentStep == 3) CloseC();
	currentStep =2;
	$(".maps-placer-bttn").scrollTop($(".map-bttn-"+currentMarker).position().top)
	$(".title-bttn-conce .title-text").html("Escoge un concesionario")
	$(".title-bttn-conce .title-text").css({"display":"block"})
	$(".title-bttn-conce .title-ico").css({"display":"none"})
	$(".title-bttn-conce .title-change").css({"display":"none"})
	$(".title-bttn-conce").removeClass("unavailable")
	$(".title-bttn-conce").removeClass("change-active")
	$(".maps-conc").slideToggle(1200, function(){
					animating = false;
				})

}

function OpenC(){
	$(".title-bttn-conce").addClass("change-active")
	$(".title-bttn-conce").removeClass("unavailable")
	$(".title-bttn-datos").removeClass("unavailable")
	if(currentStep == 2) CloseB(3);
	currentStep = 3;
	$(".title-bttn-conce .title-ico").css({"display":"table-cell"})
	$(".title-bttn-conce .title-change").css({"display":"table-cell"})

	$(".db-datos").slideToggle(1200, function(){
					animating = false;
				})
}

function OpenA(){
	animating = true;
	$(".title-bttn-conce").addClass("unavailable")
	$(".title-bttn-conce").removeClass("change-active")
	if(currentStep == 2) CloseB(1);
	if(currentStep == 3) CloseC();
	$(".title-bttn-models").removeClass("change-active")
	$('input[name=modelo]').val("");
	$(".title-bttn-models .title-text").html("Escoge un modelo")
	$(".title-bttn-models .title-text").css({"display":"block"})
	$(".title-bttn-models .title-models").removeClass("active-model")
	$(".title-bttn-models .title-models").css({"display":"none"})
	$(".title-bttn-models .title-change").css({"display":"none"})
	$(".title-bttn-models .title-ico").css({"display":"none"})

	$(".db-models").slideToggle(1200, function(){
					currentStep = 1;
					animating = false;
			})

}

function CloseC(){
	$(".title-bttn-datos").addClass("unavailable")
	$(".title-bttn-conce .title-ico").css({"display":"none"})
	$(".title-bttn-conce").removeClass("change-active")
	$(".db-datos").slideToggle(1000, function(){
					animating = false;
				})
}


