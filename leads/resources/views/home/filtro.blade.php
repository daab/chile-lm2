<div class="row">
	<div class="col-md-6">
		<select name="ano" id="ano" onChange="filtro(-1)" class="form-control">
				<option @if($ano==2016) selected="selected" @endif value="2016">2016</option>
				<option @if($ano==2017) selected="selected" @endif value="2017">2017</option>
				<option @if($ano==2018) selected="selected" @endif value="2018">2018</option>
		</select>
	</div>
</div>
	
<div class="row">
	<div class="col-md-12">
		<button class="btn btn-default  @if($mes==01) activeF @endif"  onclick="filtro('01')">Ene</button>
		<button class="btn btn-default  @if($mes==02) activeF @endif" onclick="filtro('02')">Feb</button>
		<button class="btn btn-default  @if($mes==03) activeF @endif" onclick="filtro('03')">Mar</button>
		<button class="btn btn-default  @if($mes==04) activeF @endif" onclick="filtro('04')">Abr</button>
		<button class="btn btn-default  @if($mes==05) activeF @endif" onclick="filtro('05')">May</button>
		<button class="btn btn-default  @if($mes==06) activeF @endif" onclick="filtro('06')">Jun</button>
		<button class="btn btn-default  @if($mes==07) activeF @endif" onclick="filtro('07')">Jul</button>
		<button class="btn btn-default  @if($mes==08) activeF @endif" onclick="filtro('08')">Ago</button>
		<button class="btn btn-default  @if($mes==09) activeF @endif" onclick="filtro('09')">Sep</button>
		<button class="btn btn-default  @if($mes==10) activeF @endif" onclick="filtro('10')">Oct</button>
		<button class="btn btn-default  @if($mes==11) activeF @endif" onclick="filtro('11')">Nov</button>
		<button class="btn btn-default  @if($mes==12) activeF @endif" onclick="filtro('12')">Dic</button>
	</div>
</div>

