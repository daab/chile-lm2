
		<div class="col-md-12  mt-30">
			@if($rol == 2)
			<table>
				<thead>
					<th>Estado</th>
					<th>Fecha</th>
					<th>Origen</th>
					<th>Modelo</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th>Teléfono</th>
					<th>Email</th>
					<th>Concesionario</th>
					<th>Acepto</th>
					<th class="transparente"></th>
				</thead>
				<tbody>
					@foreach($leads as $lead)
						<tr>
							<td>
								<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
 								@if($lead->estado2 == "")
									{{$lead->estado}}
								@else
									{{$lead->estado2}}
								@endif
 							</td>
							<td>{{$lead->created_at}}</td>
							<td>{{$lead->origen}}</td>
							<td>{{$lead->modelo}}</td>
							<td>{{$lead->nombres}}</td>
							<td>{{$lead->apellidos}}</td>
							<td>{{$lead->telefono}}</td>
							<td>{{$lead->correo}}</td>
							<td>{{$lead->concesionario}}</td>
							<td>{{$lead->acepto}}</td>
							<td class="transparente no-p"><a class="btn btn-default daaboton" href="{{URL::to('editar', $lead->id)}}">Editar</a></td>
						</tr>
					@endforeach
				</tbody>

			</table>
			@else
				<table>
				<thead>
					<th>Estado</th>
					<th>Fecha</th>
					<th>Origen</th>
					<th>Modelo</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th class="transparente"></th>
				</thead>
				<tbody>
					@foreach($leads as $lead)
						<tr>
							<td>
								<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
 								@if($lead->estado2 == "")
									{{$lead->estado}}
								@else
									{{$lead->estado2}}
								@endif
 							</td>
							<td>{{$lead->created_at}}</td>
							<td>{{$lead->origen}}</td>
							<td>{{$lead->modelo}}</td>
							<td>{{$lead->nombres}}</td>
							<td>{{$lead->apellidos}}</td>
							<td class="transparente no-p"><a class="btn btn-default daaboton" href="{{url('/gestionar', urlencode(encriptar($lead->id)))}}">Gestionar</a></td>
						</tr>
					@endforeach
				</tbody>

			</table>

			@endif

		<div class="row" >
				<div class="col-md-4">
					<div class="pagination-div bg-blanco">
						Mostrar de a <select onChange="enviarFormulario()" name="numPage" id="numPage">
								<option value="10">10</option>
								<option value="25">25</option>
								<option value="50">50</option>
								<option value="100">100</option>
							</select> Leads
					</div>
				</div>


				<div class="col-md-4 pull-right">
					{!! $leads->render() !!}
				</div>
			</div>

		</div>
	