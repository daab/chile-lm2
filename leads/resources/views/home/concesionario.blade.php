@extends('template')

@section('content')

<div class="ml-container">
	<div class="lead-box">
		<div class="lead-head">
			<div class="lead-name">
				<p>Leads</p>
			</div>
			<div class="clearfix"></div>
		</div>
		<form>
			<div class="lead-section db-mes-section">
				
				<div class="db-mes-class c-m-id-1">Enero</div>
				<div class="db-mes-class c-m-id-2">Febreo</div>
				<div class="db-mes-class c-m-id-3">Marzo</div>
				<div class="db-mes-class c-m-id-4">Abril</div>
				<div class="db-mes-class c-m-id-5">Mayo</div>
				<div class="db-mes-class c-m-id-6">Junio</div>
				<div class="db-mes-class c-m-id-7">Julio</div>
				<div class="db-mes-class c-m-id-8">Agosto</div>
				<div class="db-mes-class c-m-id-9">Septiembre</div>
				<div class="db-mes-class c-m-id-10">Octubre</div>
				<div class="db-mes-class c-m-id-11">Noviembre</div>
				<div class="db-mes-class c-m-id-12">Diciembre</div>
				
				<div class="clearfix"></div>
			</div>
			<div class="lead-section">
				<div class="lead-sub-section">
					<h3>Filtro Año</h3>
					<select name="ano" id="ano" onChange="filtro(-1)">
					 	<option @if($ano==2016) selected="selected" @endif value="2016">2016</option>
					 	<option @if($ano==2017) selected="selected" @endif value="2017">2017</option>
					 	<option @if($ano==2018) selected="selected" @endif value="2018">2018</option>
					</select>
				</div>
				<div class="lead-sub-section">
					<h3>Filtro estado</h3>
					<select onChange="filtro(-1)" name="estado" id="estado">
						<option value="undefined">Todos</option>
					 	<option @if($estado=='Sin Gestionar') selected="selected" @endif value="Sin Gestionar">Sin Gestionar</option>
					 	<option @if($estado=='Interesado') selected="selected" @endif  value="Interesado">Interesado</option>
					 	<option @if($estado=='No Interesado') selected="selected" @endif  value="No Interesado">No Interesado</option>
					 	<option @if($estado=='No contesto') selected="selected" @endif  value="No contesto">No contesto</option>
					 	<option @if($estado=='Datos errados') selected="selected" @endif  value="Datos errados">Datos errados</option>
					 	<option @if($estado=='Vendido') selected="selected" @endif  value="Vendido">Vendido</option>
					 	<option @if($estado=='Lead Duplicado') selected="selected" @endif  value="Lead Duplicado">Lead Duplicado</option>
					 	<option @if($estado=='Cerrado') selected="selected" @endif  value="Cerrado">Cerrado</option>
					</select>
				</div>
				@if( Auth::user()->rol == 3 )
				<div class="lead-sub-section">
					<h3>Filtro Asesor</h3>

					<select onChange="filtro(-1)" name="asesor" id="asesor">
						<option value="undefined">Todos</option>
					 	@foreach ($asesores as $con)
					 		<option value="{{$con->id}}">{{$con->nombres}} {{$con->apellidos}}</option>
					 	@endforeach
					</select>
				</div>
				@endif
				<div class="clearfix"></div>
			</div>
			<div class="lead-section">
				<div class="lead-sub-section" id="divPaginador"></div>
				<div class="lead-sub-section" id="divBuscador"></div>
				<div class="clearfix"></div>
			</div>
			
		</form>

		<div class="lead-section lm-lead-section" >


			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			<input type="hidden" name="verificar" id="verificar" value="{{$verificar}}" />

			<table id="leads-table" style="width: 100% !important">
				<thead>
					<th>Estado</th>
					<th>Fecha</th>
					<th>Origen</th>
					<th>Modelo</th>
					<th>Nombres</th>
					<th>Apellidos</th>
					<th class="transparente"></th>
				</thead>
				<tbody>
					@foreach($leads as $l)
					<tr>
						<td>
							<span><i style="color: {{$l->estados->color}}" class="fa fa-circle" aria-hidden="true"></i> </span> 
							{{$l->estados->nombre}}
						</td>
						<td>{{$l->created_at}}</td>
						<td>{{$l->origen}}n</td>
						<td>{{$l->modelo}}</td>
						<td>{{$l->nombres}}</td>
						<td>{{$l->apellidos}}</td>
						<td class="transparente">
							<a href="{{ url('/gestionar', $l->id) }}" class="db-mes-section">Gestionar</a>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>

		</div>

	</div>
</div>

@endsection

@section('scripts')

<script type="text/javascript">
	
	var dataSet = {!! $leads !!};

	var mesPhp = {{ $mes }};
	var url = "http://"+'<?php echo $_SERVER['SERVER_NAME'] ?>';
	var datatableData = "";

	$(document).ready(function(){
		datatableData = $('#leads-table').DataTable({
			order: [[1, 'desc']]
		});
            // datatableData = $('#leads-table').DataTable({
            // 	data: dataSet,
            //     columns: [
            //         { data: 'estado', name: 'estado' },
            //         { data: 'created_at', name: 'fecha' },
            //         { data: 'origen', name: 'origen' },
            //         { data: 'modelo', name: 'modelo' },
            //         { data: 'nombres', name: 'nombres' },
            //         { data: 'apellidos', name: 'apellidos' },
            //         { data: 'apciones', name: 'apciones', orderable: false, searchable: true},
            //     ],
            //     order: [[1, 'desc']]
            // });

            var currentMonth = mesPhp; //Mes actual = Abril//

			$(".c-m-id-"+ currentMonth).addClass("m-active")

			for (var i = 1; i < 13; i++) {
				$(".c-m-id-"+ i).data("fuu", i)
				$(".c-m-id-"+ i).click(function(){
					if ( $(this).hasClass("m-active") ) return;
					currentMonth = $(this).data("fuu")
					$(".db-mes-section").children().removeClass("m-active")
					$(this).addClass("m-active")

					/*Funcion Cambio de mes con loader*/
					filtro(currentMonth);
				})
			}


			setTimeout(function(){
				var element = $('#leads-table_filter').detach();
				$('#divBuscador').append(element);

				var element2 = $('#leads-table_length').detach();
				$('#divPaginador').append(element2);

    			
			}, 0);

			
			
        });

	setInterval(revisarLeads, 60000);

	function revisarLeads(){
		//AJAX
		$.ajax({
			type: "POST",
			data: "verificar="+$('#verificar').val()+"&_token="+$('#token').val(),
			url: "{{url('/verificar')}}",
			success: function(datos){
				var verificar = parseInt(datos);
				if (verificar == 1) {

					//Quitamos la modal anterior y creamos la nueva...
					$('.sweet-alert').remove();
					$('.sweet-overlay').remove();

					swal({
					  title: "Nuevo Lead recibido",
					  text: "Se mostrara el nuevo lead",
					  type: "warning",
					  showCancelButton: false,
					  confirmButtonColor: "#DD6B55",
					  closeOnConfirm: false,
					},
					function(isConfirm){
					  if (isConfirm) {
					    window.location = "{{url('/')}}";
					  } 
					});
					 
				}
			}
		});
	}

	function filtro(mes){
		var ano = $('#ano').val();
		if(mes == -1){
			mes = mesPhp;
		}

		/*url = url + "/leads/home/"+ano+"/"+mes;
		window.location.href = url;*/

		$.ajax({
			type: "POST",
			data: "_token="+$('#token').val()+"&mes="+mes+"&ano="+ano+"&estado="+$('#estado').val()+"&asesor="+$('#asesor').val()+"&concesionario="+$('#concesionario').val(),
			url: "{{url('/home/filtro')}}",
			success: function(datos){

				datos = JSON.parse(datos);
				datatableData.destroy();
				
				datatableData = $('#leads-table').DataTable({
				    data: datos,
	                columns: [
	                    { data: 'estado', name: 'estado' },
	                    { data: 'created_at', name: 'fecha' },
	                    { data: 'origen', name: 'origen' },
	                    { data: 'modelo', name: 'modelo' },
	                    { data: 'nombres', name: 'nombres' },
	                    { data: 'apellidos', name: 'apellidos' },
	                    { data: 'apciones', name: 'apciones', orderable: false, searchable: false},
	                ],
	                order: [[1, 'desc']]
	            });

	            setTimeout(function(){
	            	$('#divBuscador').html("");
	            	$('#divPaginador').html("");
					var element = $('#leads-table_filter').detach();
					$('#divBuscador').append(element);
					var element2 = $('#leads-table_length').detach();
					$('#divPaginador').append(element2);
				}, 0);


			}
		});

	}

</script>

@endsection
