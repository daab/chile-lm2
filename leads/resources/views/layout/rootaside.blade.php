<aside class="col-md-1 main-sidebar">
	<div class="bg-blanco h25"></div>
	<div>
		@if(count(Auth::user()->roles) >0)

			@foreach(Auth::user()->roles->first()->modules as $module)

				<div class="item-menu">
					<div class="item-nav">
						<a href="{{url($module->route)}}" title='{{$module->name}}'>
							<i class="{{$module->icon}}" aria-hidden="true"></i>
						</a>
					</div>
					<div class="bg-blanco h35"></div>
				</div>				

			@endforeach
		@endif
		

		<div class="item-menu">
			<div class="item-nav">
				<a href="{{url('/auth/logout')}}">
					<i class="fa fa-sign-out" aria-hidden="true"></i>
				</a>
			</div>
		</div>

	</div>
	<div class="bg-blanco h35"></div>
</aside>