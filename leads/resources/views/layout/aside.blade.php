<?php 
        switch (Auth::user()->rol) {
            case 1:
                $type = "root";
            break;
            case 2:
                # code...
                $type = "marca";
            break;
            case 3:
                # code...
                $type = "concesionario";
            break;
            case 4:
                # code...
                $type = "conglomerado";
            break;
            case 5:
                # code...
                $type = "asesor";
            break;
            default:
                # code...
                $type = "hacked";
            break;
        }
        
    ?>

<aside class="col-md-1 main-sidebar">

<div class="db-wrapper-menu">
    <div class="db-deco0"></div>
    
    <div class="@if($active == 'lista')db-active @endif db-bttn">
    	<div class="db-bttn-ico"></div>
    	<a href="{{url('/')}}">
    		<img src="{{ asset('/public/images/ico-leads.jpg') }}" />
    	</a>
    </div>

    
    <div class="@if($active == 'export')db-active @endif db-bttn">
    	<div class="db-bttn-ico"></div>
    	<a href="{{url('/exportar', $type)}}">
    		<img src="{{ asset('/public/images/ico-down.jpg') }}" />
    	</a>
    </div>

    @if(Auth::user()->rol == 3)
        <div class="@if($active == 'usuarios')db-active @endif db-bttn">
        	<div class="db-bttn-ico"></div>
        	<a href="{{url('/concesionario/usuarios')}}">
               <img src="{{ asset('/public/images/ico-config.jpg') }}" />
            </a>
        </div>
    @endif


    <div class="@if($active == 'estadistica')db-active @endif db-bttn">
        <div class="db-bttn-ico"></div>
        <a href="{{url('/estados', $type)}}">
            <img src="{{ asset('/public/images/ico-estad.jpg') }}" />
        </a>
    </div>
    
    
    <div class="db-bttn">
    	<div class="db-bttn-ico"></div>
    	<a href="{{url('/auth/logout')}}">
    		<img src="{{ asset('/public/images/ico-logout.jpg') }}" />
    	</a>
    </div>
    

   	<div class="db-deco1"></div>
    <div class="clearfix"></div>
</div>


	
</aside>