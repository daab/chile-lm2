@extends('mobile')

@section('title')
	Leads | Asesores
@endsection

@section('content')
<div class="wrapper">
		<div class="section-box print-leads">
				@foreach($assessors as $assessor)
				<div class='leads-box'>
					<div class='lead-item lead-item-e'>
						@if($assessor->capacidad > 0)
							<div class='eicon' style='background-color:#33cc33;'></div>
							Activo
						@else
							<div class='eicon' style='background-color:#000000;'></div>
							Inactivo
						@endif
					</div>
					<div class='lead-item lead-item-n'>
						<span>ID:</span><br>
						{{$assessor->user_id}}
					</div>
					<div class='lead-item lead-item-n'>
						<span>Nombre:</span><br>
						{{$assessor->user_name}}
					</div>
					<div class='lead-item lead-item-n'>
						<span>Email:</span><br>
						{{$assessor->user_email}}
					</div>
					<div class='lead-item lead-item-n'>
						<span>Ponderacion</span><br>
						{{$assessor->capacidad}}0%
					</div>
					<div class='lead-item lead-item-n'>
						<span>Cupo/Capacidad</span><br>
						{{$assessor->cupo}}/{{$assessor->capacidad}}
					</div>
					<a class="lead-item lead-item-g" href="{{ url('/root/concesionarios/asesores/nuevo', $assessor->user_id) }}">
						Editar
					</a>
				</div>
				@endforeach
			<a href="{{ url('/root/concesionarios/asesores/nuevo/') }}" class="daaboton_extra">Nuevo Asesor</a>	
		</div>
		<div class="btta-modal"></div>
		<div class="dsktp-top"></div>
		<div class="dsktp-left"></div>
</div>
	<div class="menu-board">
		<div class="menu-boad-dsktp">
			<div class="btta-title btta-homemenu">Cotizaciones:</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'today', value:'gestion', boolean:1});">Cotizaciones de hoy</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'sin gestionar', boolean:1});">Sin Gestionar</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'all', value:'gestion', boolean:1});">Todos los leads</div>
			
			<div class="btta-title btta-homemenu">Proceso en vitrina:</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'agendado', boolean:1});">Calendario</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'en proceso', boolean:1});">En proceso / resultados</div>
			<div class="btta-title btta-homemenu">otros:</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/root/concesionarios/asesores') }}">Asesores</div>		
			
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/statistics') }}">Estadistica</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/exportLeads') }}">
				Descarga
			</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/guide') }}">
				Guia
			</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/auth/logout') }}">Salir</div>
		</div>
		<div class="btta-menu btta-fecha" data-fuu="1">Fecha ascendente</div>
		<div class="btta-menu btta-fecha  btta-current" data-fuu="0">Fecha descendente</div>
		<div class="btta-title btta-name ">Buscar por nombre o apellido:<br><input id="searcher" type="text" name="searcher"/></div>
		<div class="btta-menu btta-name " data-fuu="0">Buscar</div>
	</div>

     <div class="db-header">
		<div class="header-logo"><a href="#" target="_blank">&nbsp;</a></div>
		<div class="header-title header-title-null">Asesores</div>
		<div class="header-drop bttn-hover header-drop-dsktp" data-fuu="homemenu">
			<div class="bttn-filler"></div>
			<img id="ico-menu" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-menu.svg')}}"/>
			<img id="ico-exit" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-exit.svg') }}"/>
		</div>
	</div>


	<script type="text/javascript" src="{{ asset('/public/css/mobile/js/jquery-1.7.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/public/css/mobile/js/view-alles.js') }}"></script>


<script type="text/javascript">

	var rol = {{Auth::user()->roles()->first()->id}};
	console.log("rol: ", rol )

	var es_all_blade = false;
	var menu_var =  0; if(rol == 2){ menu_var = 1; $(".rol_2").removeClass("hidder")}
	
	

	var menu_var =  0;

	if(rol == 2){
		menu_var = 1
	}

	//Funciones para conservar filtro el local storage
	
	//viewLeads(local_u);
	launchPage()

	

	function filterLeads(options) {
		
		if (options['boolean'] == 0) return;
//modal(0);

    	var form = document.createElement("form");
    	form.setAttribute("method", "POST");
    	form.setAttribute("action", "{{url('/home/filtro')}}");

    	for(var index in options) { 
	 		var el = document.createElement("input");
	   		el.type = "hidden";
	   		el.name = index;
	   		el.value = options[index];
	   		form.appendChild(el); 		   
		}	

    	document.body.appendChild(form);
    	form.submit();
	}

</script>

@endsection