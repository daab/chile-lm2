@extends('mobile')

@section('title')
	Leads | Asesores
@endsection

@section('content')
<div class="wrapper">
		<div class="section-box print-leads">
			<h2>{{ (!empty($assessor) ? 'Editar' : 'Nuevo') }} Asesor</h2>
			<form class="form-vertical" role="form" method="POST" action="{{ url('root/concesionarios/asesores/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="distributor" value="{{$distributor->id}}">

					<input type="hidden" name="role" value="{{(!empty($role) ? $role->id : '')}}">

					<input type="hidden" name="assessor" value="{{ (!empty($assessor) ? $assessor->id : '') }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Usuario</label>
					
						<input type="text" 
							   class="form-control" 
							   name="name" 
							   value="{{ (!empty($assessor) ? $assessor->name : '') }}" 
							   placeholder="Ej. Roberto Roa Ruiz">
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Correo electrónico</label>
					
						<input type="text" 
							   class="form-control" 
							   name="email" 
							   value="{{ (!empty($assessor) ? $assessor->email : '') }}" 
							   placeholder="Ej. correo@mail.com">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Password</label><br>
					
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Ponderación (actual: {{ (!empty($assessor) ? $assessor->capacidad : '') }})</label><br>
						<select class="form-control" name="capacidad" id="capacidad">
							<option value="10" selected=""> 100%</option>
							<option value="9" > 90%</option>
							<option value="8" > 80%</option>
							<option value="7" > 70%</option>
							<option value="6" > 60%</option>
							<option value="5" > 50%</option>
							<option value="4" > 40%</option>
							<option value="3" > 30%</option>
							<option value="2" > 20%</option>
							<option value="1" > 10%</option>
							<option value="0" > 0% (Inactivo)</option>
						</select>
					</div>

					<div class="form-group" id="s_brand">
						
						<label class="color-blanco control-label">Marca</label>

						<select class="form-control" name="brand" id="brand">
						
							<!-- <option value=""> - Seleccione una opción -</option> -->

							{{--*/ $temp_brand = (!empty($assessor) ? $assessor->brand()->first() : '') /*--}}

							@foreach ($distributor->marcas as $brand)
							
								{{--*/ 

									$selected = (!empty($assessor) && isset($temp_brand) && $temp_brand->id == $brand->id ? 'selected' : '') 
									
								/*--}}

								<option value="{{$brand->id}}" {{$selected}}>{{$brand->name}}</option>
							
							@endforeach
						
						</select>
					</div>

										
					<div class="form-group">
						
						<button type="submit" class="btn daaboton_extra hover">Guardar</button>
						
					</div>
				</form>
			
		</div>
		<div class="btta-modal"></div>
		<div class="dsktp-top"></div>
		<div class="dsktp-left"></div>
</div>
<div class="menu-board">
		<div class="menu-boad-dsktp">
			<div class="btta-title btta-homemenu">Cotizaciones:</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'today', value:'gestion', boolean:1});">Cotizaciones de hoy</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'sin gestionar', boolean:1});">Sin Gestionar</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'all', value:'gestion', boolean:1});">Todos los leads</div>
			
			<div class="btta-title btta-homemenu">Proceso en vitrina:</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'agendado', boolean:1});">Calendario</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'en proceso', boolean:1});">En proceso / resultados</div>
			<div class="btta-title btta-homemenu">otros:</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/root/concesionarios/asesores') }}">Asesores</div>		
			
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/statistics') }}">Estadistica</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/exportLeads') }}">
				Descarga
			</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/guide') }}">
				Guia
			</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/auth/logout') }}">Salir</div>
		</div>
		<div class="btta-menu btta-fecha" data-fuu="1">Fecha ascendente</div>
		<div class="btta-menu btta-fecha  btta-current" data-fuu="0">Fecha descendente</div>
		<div class="btta-title btta-name ">Buscar por nombre o apellido:<br><input id="searcher" type="text" name="searcher"/></div>
		<div class="btta-menu btta-name " data-fuu="0">Buscar</div>
	</div>

     <div class="db-header">
		<div class="header-logo"><a href="#" target="_blank">&nbsp;</a></div>
		<div class="header-title header-title-null">Asesores</div>
		<div class="header-drop bttn-hover header-drop-dsktp" data-fuu="homemenu">
			<div class="bttn-filler"></div>
			<img id="ico-menu" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-menu.svg')}}"/>
			<img id="ico-exit" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-exit.svg') }}"/>
		</div>
	</div>


	<script type="text/javascript" src="{{ asset('/public/css/mobile/js/jquery-1.7.1.min.js') }}"></script>
	<script type="text/javascript" src="{{ asset('/public/css/mobile/js/view-alles.js') }}"></script>


<script type="text/javascript">

	var rol = {{Auth::user()->roles()->first()->id}};
	console.log("rol: ", rol )

	var es_all_blade = false;
	var menu_var =  0; if(rol == 2){ menu_var = 1; $(".rol_2").removeClass("hidder")}
	
	

	var menu_var =  0;

	if(rol == 2){
		menu_var = 1
	}

	//Funciones para conservar filtro el local storage
	
	//viewLeads(local_u);
	launchPage()

	

	function filterLeads(options) {
		
		if (options['boolean'] == 0) return;
//modal(0);

    	var form = document.createElement("form");
    	form.setAttribute("method", "POST");
    	form.setAttribute("action", "{{url('/home/filtro')}}");

    	for(var index in options) { 
	 		var el = document.createElement("input");
	   		el.type = "hidden";
	   		el.name = index;
	   		el.value = options[index];
	   		form.appendChild(el); 		   
		}	

    	document.body.appendChild(form);
    	form.submit();
	}

</script>


@endsection
