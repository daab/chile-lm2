@extends('root')

@section('title')
	Leads | Metas concesionario
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
			<h2>Registro de metas</h2>
			<br/>
			<h1>Concesionario {{$distributor->name}}</h1>
			<h3>{{$distributor->city->name}}</h3>

			<br/><br/>

			<form class="form-vertical" role="form" method="POST" action="{{ url('root/concesionario/metas/guardar') }}">
			<input type="hidden" id="distributor_id" name="distributor_id" value="{{ $distributor->id }}">
			<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
			<table width="100%">

				<tr>
					<th width="30%">Año</th>
					<th width="30%">Mes</th>					
					<th width="30%">Meta</th>
				</tr>

				{{--*/$count = 1 /*--}}

				@foreach($goals as $goal)
				<tr>
					<td class="text-center">{{$goal['year']}}</td>
					<td>{{$goal['month']}}</td>
					<td>
						<input type="text" class="form-control text-center" name="value_{{$count}}" value="{{$goal['value']}}">
					</td>
				</tr>

				{{--*/$count = $count + 1 /*--}}
				@endforeach
				
			</table>

			<br />
			<div class="form-group">
				
				<button type="submit" class="btn btn-primary daaboton">Guardar</button>
				
			</div>

			</form>			
			
		</div>


	</div>
</div>
@endsection


@section('scripts')

	<script type="text/javascript">

	</script>

@endsection
