@extends('root')

@section('title')
	Leads | Concesionarios
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('root/concesionario/nuevo') }}" class="btn btn-default daaboton">Nuevo Concesionario</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 20%">Nombre</th>
				<th style="width: 20%">Ciudad</th>
				<th style="width: 20%">Direccion</th>
				<th style="width: 20%">Correo</th>
				<th style="width: 5%">Telefono</th>
				<th style="width: 10%">Facebook</th>
                                <th style="width: 5%">Metas</th>
				<th class="transparente" style="width: 5%"></th>
			</thead>
			
			<tbody>
				@foreach($concesionarios as $concesionario)

				<tr>
					<td>{{ $concesionario->name }}</td>
					<td>{{ $concesionario->city['name']}}</td>
					<td>{{ $concesionario->address }}</td>
					<td>{{ $concesionario->email }}</td>
					<td>{{ $concesionario->phone }}</td>
					<td>{{ $concesionario->facebook }}</td>
					<td class="text-center">
						<a href="{{ url('root/concesionario/metas', $concesionario->id) }}">Metas</a>
					</td>
					<td class="text-center"><a href="{{ url('root/concesionario/nuevo', $concesionario->id) }}">Editar</a> | <a href="{{ url('root/concesionario/eliminar', $concesionario->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				

			</tbody>

		</table>			
			
		
	</div>
	
</div>
@endsection