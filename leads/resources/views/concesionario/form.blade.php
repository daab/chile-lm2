@extends('root')

@section('title')
	Leads | Concesionarios
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/concesionario/guardar') }}">
					<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="id" name="id" value="{{ $concesionario->id }}">

					<div class="form-group">
						<label class="color-blanco control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" value="{{ $concesionario->name }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Direccion</label>
						<input type="text" class="form-control" name="direccion" value="{{ $concesionario->address }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Correo</label>
						<input type="text" class="form-control" name="correo" value="{{ $concesionario->email }}">
					</div>


					<div class="form-group">
						<label class="color-blanco control-label">Telefono</label>
						<input type="text" class="form-control" name="telefono" value="{{ $concesionario->phone }}">
					</div>
					
					<div class="form-group">
						<label class="color-blanco control-label">Campo en Facebook</label>
						<input type="text" class="form-control" name="facebook" value="{{ $concesionario->facebook }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Ciudad</label>

						<select class="form-control" name="city" id="city">
							<option value=""></option>
							@foreach ($cities as $city)
							
							{{--*/ 
								$selected = (!empty($concesionario) && $concesionario->city_id == $city->id ? 'selected' : '')
							/*--}}

								<option value="{{$city->id}}" {{$selected}}>{{$city->name}}</option>
							@endforeach
						</select>
					</div>					
					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>

		@if(count($marcas) > 0)
			<div class="col-md-6">

				<h3>Marcas</h3>
				<?php $i = 0 ?>
				
				@foreach($marcas as $con)
					<?php 
						$checked = "";
						if (in_array($con->id, $cons)) {
							$checked = "checked";
						}
					?>
					<input type="checkbox" id="con{{$i}}" {{$checked}} onChange="update({{$con->id}}, {{$i}})" /> {{$con->name}}<br />
					<?php $i++ ?>
				@endforeach

			</div>
			<br /><br /><br />
		@endif
	</div>
</div>
@endsection


@section('scripts')

	<script type="text/javascript">

		function update(id, con){

			var guardar = 0;
			var idCon = "con"+con;
			
			if ($('#'+idCon).is(':checked')) { 
				guardar = 1;
			}

			//AJAX
			$.ajax({
				type: "POST",
				data: "_token="+$('#token').val()+"&idConcesionario="+$('#id').val()+"&idMarca="+id+"&guardar="+guardar,
				url: "{{url('/root/update')}}",
				success: function(datos){
					console.log("Exito dude...");
				}
			});
		}

	</script>

@endsection
