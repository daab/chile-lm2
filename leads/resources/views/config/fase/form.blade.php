@extends('root')

@section('title')
	Leads | Configuración
@endsection

@section('content')


@if(isset($object))

<div class="row figux-container">

@endif

	<div class="row">

		<div class="col-md-5" >

			<form class="form-vertical" role="form" method="POST" action="{{ url('/configuracion/save') }}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{{ isset($object) ? $object->id : '' }}}">			
			
				@if($form == 'phase')
				<input type="hidden" name="type" value="phase">

					<h2> 
						@if($id == '') 
							Nueva
						@else
							Actualizando 
						@endif 

						Fase
					</h2>

					<div class="form-group">
						<label class="control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ej. Nuevo lead" value="{{{ isset($object) ? $object->nombre : '' }}}">
					</div>


					<div class="form-group">
						<label class="control-label">Orden</label>
						<input type="number" class="form-control" name="orden" id="orden" placeholder="Ej. 1" value="{{{ isset($object) ? $object->orden : '' }}}" min="1">
					</div>

				@elseif ($form == 'state')

					<input type="hidden" name="type" value="state">
					<input type="hidden" name="phase" value="{{{ isset($phase) ? $phase->id : '' }}}">
					
					<h2> 
						@if($id == '') 
							Nuevo
						@else
							Actualizando 
						@endif 

						Estado
					</h2>

					<p>
						Usted está a punto de adicionar un nuevo estado para la fase <span style="font-weight: bold">{{$phase->nombre}}</span>
					</p>
					

					<div class="form-group">
						<label class="control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ej. Datos errados" value="{{{ isset($object) ? $object->nombre : '' }}}">
					</div>

					<div class="form-group">
						<label class="control-label">Color</label>
						<input type="text" class="form-control" name="color" id="color" placeholder="Ej. #000000" value="{{{ isset($object) ? $object->color : '' }}}">
					</div>	

					<div class="form-group">
						<label class="control-label">Titulo seccion HTML</label>
						<input type="text" class="form-control" name="titulo" id="titulo" placeholder="Ej. ¿No contactado?" value="{{{ isset($object) ? $object->titulo : '' }}}">
					</div>	

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="es_final" 
								   id="es_final" {{{ isset($object) && $object->es_final == 1 ? 'checked' : '' }}}> ¿Es un estado final?
						</label>
					</div>													

				@elseif ($form == 'list')

					<input type="hidden" name="type" value="list">
					<input type="hidden" name="list" value="{{{ isset($object) ? $object->id : '' }}}">
					
					<h2> 
						@if($id == '') 
							Nueva
						@else
							Actualizando 
						@endif 

						Lista
					</h2>			

					<div class="form-group">
						<label class="control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ej. No visito vitrina - No interesado" value="{{{ isset($object) ? $object->nombre : '' }}}">
					</div>										

					<div class="form-group">
						<label class="control-label">Valores <small>Ingrese los valores separados por comas(,).</small></label>
						<textarea  class="form-control" rows="5" name="valores" id="valores" placeholder="Ej. opcion 1, opcion 2, etc.">@if(isset($object))@foreach($object->valores as $v){{trim($v->valor).', '}} @endforeach @endif</textarea>
					</div>

				@else 

				<!-- Caso Reason -->

					<input type="hidden" name="type" value="reason"> 
					<input type="hidden" name="state" value="{{{ isset($state) ? $state->id : '' }}}">					

					<h2> 
						@if($id == '') 
							Nuevo
						@else
							Actualizando 
						@endif 

						Motivo
					</h2>

					<p>
						Usted está a punto de adicionar un nuevo motivo para el estado <span style="font-weight: bold">{{$state->nombre}}</span>
					</p>
					

					<div class="form-group">
						<label class="control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" id="nombre" placeholder="Ej. No contactado" value="{{{ isset($object) ? $object->nombre : '' }}}">
					</div>

					<div class="form-group">
						<label class="control-label">Medio de contacto</label>
						<input type="text" class="form-control" name="medio" id="medio" placeholder="Ej. Teléfono" value="{{{ isset($object) ? $object->medio_contacto : '' }}}">
					</div>					

					<br/>
					<label>Opciones especiales</label>

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="recordatorio" 
								   id="recordatorio" {{{ isset($object) && $object->mostrar_recordatorio == 1 ? 'checked' : '' }}}> Mostrar campo de recordatorio
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="comision" 
								   id="comision" {{{ isset($object) && $object->mostrar_comision == 1 ? 'checked' : '' }}}> Mostrar campo de comisión
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="transicion" 
								   id="transicion" {{{ isset($object) && $object->genera_transicion == 1 ? 'checked' : '' }}}> Transición de estados
						</label>
					</div>

					<div id="section-transitions" style="display: none">

						<br/>
						
						<div class="form-group">
							<label class="control-label">Fase</label>
							<select name="fase_siguiente" id="fase_siguiente" class="form-control">
								
								<option value=""></option>

								@if(count($list_phases) > 0)
									@foreach($list_phases as $f)										
										<option value="{{$f->id}}" {{{ isset($object) && $object->fase_siguiente == $f->id ? 'selected' : '' }}}>
											{{$f->nombre}}
										</option>
									@endforeach
								@endif
							</select>
						</div>

						<div class="form-group">
							<label class="control-label">Estado</label>
							<div id="section-state">{{{ isset($state_next) && $state_next!="" ? $state_next->nombre : '' }}}</div>
						</div>

					</div>

				@endif


				<div class="form-group">				
					
					<button type="submit" name="btn-action" id="btn-action" value="save" class="btn btn-primary">Guardar</button>

					<button type="submit" name="btn-action" id="btn-action" value="cancel" class="btn btn-secundary">Cancelar</button>
				</div>


			</form>

		</div>

	</div>

@if(isset($object))

</div>

@endif

<script type="text/javascript">

	if ($("#transicion").prop('checked')) {
		
		$("#section-transitions").show();

	}
		
	$("#transicion").click(function(){

		if ($("#transicion").prop('checked')) {
			
			$("#section-transitions").show();

		} else {
			$("#section-transitions").hide();
		}
	});

	$("#fase_siguiente").change(function(){

		url = '{{ url("/configuracion/state", "param") }}';
		url = url.replace("param", $("#fase_siguiente").val());

		$.ajax({
		  	url: url,
		  	type: "GET",
		  	data: "type=select",
			success:  function (response) {

                $("#section-state").html(response);
            }
		});
	});
</script>

@endsection