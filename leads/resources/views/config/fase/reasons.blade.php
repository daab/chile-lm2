@if($type=='list')

	<ul>
		@if (count($reasons)>0)

			@foreach($reasons as $r)
				<li>
					{{$r->nombre}}
					<span class="texto-edicion-borrado">
						(
							<a href="{{ url('/configuracion/edit', ['type' => 'reason', 'id'=> $r->id]) }}">Editar</a> | 

							<a href="{{ url('/configuracion/delete', ['type' => 'reason', 'id'=> $r->id]) }}" onclick="return confirm_delete();">Eliminar</a> )

							<!-- <a href="#" onclick="toListStates('{{ url("/configuracion/reason", "$r->id") }}');">Motivo</a>   -->
				</span>				
				</li>
			@endforeach
		@else 					
				<li>No existen motivos asociados a este estado.</li>
		@endif

	</ul>

	<a href="#" id="btn_add_reason">

		<i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar motivo
	</a>

@else 

	@if (count($reasons)>0)
		<select name="reason" id="reason">
			<option value=""> - Seleccione una opción - </option>
			@foreach($reasons as $r)
				<option value="{{$r->id}}" data-sr='{{$r->mostrar_recordatorio}}' data-sc='{{$r->mostrar_comision}}'>{{$r->nombre}}</option>
			@endforeach		
		</select>
	@endif

@endif

<script type="text/javascript">

	$("#btn_add_reason").click(function(){

		$("#section-list").hide('1000');
		$("#section-list-2").hide('1000');

		loadForm("{{ url('/configuracion/form', ['reason',$state->id]) }}");

		$("#section-form-panel").show('1000');
	});	



	$("#reason").change(function(){

	    var show_remember = $(this).find(':selected').data('sr');
	    var show_commissions = $(this).find(':selected').data('sc');

		if (show_remember == 1) {

			$('#divRecordatorio').show();	
		} else {
			$('#divRecordatorio').hide();
			$('#agenda').val("");
		}


		if (show_commissions == 1) {
			$('#divVendido').show();
		} else {			
			$('#divVendido').hide();
			$('#comision').val("");
		}		
	});
</script>