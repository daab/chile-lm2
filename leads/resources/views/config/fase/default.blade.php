@extends('root')

@section('title')
	Leads | Configuración
@endsection

@section('content')


<style type="text/css">
	
	ul li {
		display: block; 
		margin-top:15px; 
		margin-left:-30px;
		width:70%;
	}

	.boton-guardar{
		font-size: 20px;
		color:#29a546;
		font-weight: bold;
		margin: 5px;
		border: 0px;
		text-decoration: none;
		background-color: transparent !important;
	}

	.boton-cancelar{
		font-size: 20px;
		color:#a32d10;
		font-weight: bold;
		margin: 5px;
	}	

	.texto-enlace {
		color: #000;
		font-weight: bold;
	}
	.texto-edicion-borrado {
		font-size: 12px;
	}

</style>



<div class="row figux-container">

	<h1>Configuración -  {{ config('app.app_max_day_recontact', '') }}</h1>

	<div id="section-form-panel" class="clearfix"></div>

	<div id="section-list" class="clearfix">

		<div class="row">
			
			<div class="col-md-4">

				<h1 class="btn daaboton">Fases del proceso</h1>

				<ul>
					@if (count($fases)>0)
						@foreach($fases as $fase)
							<li>
								{{$fase->nombre}} 
								<span class="texto-edicion-borrado">
									(
										<a href="{{ url('/configuracion/edit', ['type' => 'phase', 'id'=> $fase->id]) }}">Editar</a> | 

										<a href="{{ url('/configuracion/delete', ['type' => 'phase', 'id'=> $fase->id]) }}" onclick="return confirm_delete();">Eliminar</a> |

										<a href="#" onclick="toListStates('{{ url("/configuracion/state", "$fase->id") }}');">Estados</a>  
									)
							</span>
							</li>
						@endforeach
					@else 					
							<li>No existen fases creadas</li>
					@endif
			
				</ul>


				<a href="#" id="btn_add_fase">

					<i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar fase
				</a>


			</div>

			<div class="col-md-4">

				<h1 class="btn daaboton">Estados de una fase</h1>

				<div id="section-list-state"></div>
			</div>

			<div class="col-md-4">

				<h1 class="btn daaboton">Motivos de un estado</h1>

				<div id="section-list-reasons"></div>				
			</div>
									
		</div>

	</div>

</div>

<div class="row figux-container">

	<div id="section-list-2" class="clearfix">

		<div class="row">


			<div class="col-md-4">

				<h1 class="btn daaboton">Reglas del negocio</h1>				

			</div>

			<div class="col-md-4">

				<h1 class="btn daaboton">Configuración regional</h1>
				<ul>
					<li>
						<a href="{{ url('/configuracion/timezone') }}">Zona horaria</a>
					</li>
				</ul>				
			</div>						

		</div>

	</div>

</div>

<script type="text/javascript">

	$("#section-form-panel").hide();


	$("#btn_add_fase").click(function(){

		$("#section-list").hide('1000');
		$("#section-list-2").hide('1000');

		loadForm("{{ url('/configuracion/form', ['phase',0]) }}");

		$("#section-form-panel").show('1000');
	});

	$("#btn_add_list").click(function(){

		$("#section-list").hide('1000');

		$("#section-list-2").hide('1000');

		loadForm("{{ url('/configuracion/form', ['list',0]) }}");

		$("#section-form-panel").show('1000');
	});


	function loadForm(url) {

		$.ajax({
		  	url: url,
		  	type: "GET",
		  	data: "",
			success:  function (response) {

                $("#section-form-panel").html(response);
            }
		});
	}

	function confirm_delete(){
		if (confirm('Está seguro de eliminar este registro?')) {
			return true;
		}

		return false;
	}

	function toListStates(url) {

		$("#section-list-reasons").html("");

		$.ajax({
		  	url: url,
		  	type: "GET",
		  	data: "",
			success:  function (response) {

                $("#section-list-state").html(response);
            }
		});
	}

	function toListReasons(url) {

		$.ajax({
		  	url: url,
		  	type: "GET",
		  	data: "",
			success:  function (response) {

                $("#section-list-reasons").html(response);
            }
		});
	}
</script>


@endsection