@if($type=='list')

<ul>
	@if (count($state)>0)

		@foreach($state as $s)
			<li>
				{{$s->nombre}}
				<span class="texto-edicion-borrado">
					(
						<a href="{{ url('/configuracion/edit', ['type' => 'state', 'id'=> $s->id]) }}">Editar</a> | 

						<a href="{{ url('/configuracion/delete', ['type' => 'state', 'id'=> $s->id]) }}" onclick="return confirm_delete();">Eliminar</a> |

						<a href="#" onclick="toListReasons('{{ url("/configuracion/reason", "$s->id") }}');">Motivos</a>  
					)
			</span>				
			</li>
		@endforeach
	@else 					
			<li>No existen estados asociados a esta fase.</li>
	@endif

</ul>

<a href="#" id="btn_add_state">

	<i class="fa fa-plus-circle" aria-hidden="true"></i> Agregar estado
</a>

@else 
	@if (count($state)>0)
		<select name="estado_siguiente" id="estado_siguiente" class="form-control">
			<option value=""> - Seleccione una opción - </option>
			@foreach($state as $s)
				<option value="{{$s->id}}">{{$s->nombre}}</option>
			@endforeach		
		</select>
	@endif
@endif

<script type="text/javascript">

	$("#btn_add_state").click(function(){

		$("#section-list").hide('1000');
		$("#section-list-2").hide('1000');

		loadForm("{{ url('/configuracion/form', ['state',$phase->id]) }}");

		$("#section-form-panel").show('1000');
	});	
</script>