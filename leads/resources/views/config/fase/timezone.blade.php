@extends('root')

@section('title')
	Leads | Configuración
@endsection

@section('content')

<div class="row figux-container">


	<div class="row">

		<div class="col-md-5" >

			<form class="form-vertical" role="form" method="POST" action="{{ url('/configuracion/save') }}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="id" value="{{{ isset($object) ? $object->id : '' }}}">
				<input type="hidden" name="type" value="timezone">			
			
					<h2>Configuración regional</h2>


					<div class="form-group">
						<label class="control-label">Zona</label>
						<select name="zone" id="zone" class="form-control">
							<option value="Etc/GMT+7" {{ ($object->zone == 'Etc/GMT+6' ? 'selected':'') }}>GMT-7, 2 Hora menos a la de Colombia</option>							
							<option value="Etc/GMT+6" {{ ($object->zone == 'Etc/GMT+6' ? 'selected':'') }}>GMT-6, 1 Hora menos a la de Colombia</option>							
							<option value="Etc/GMT+5" {{ ($object->zone == 'Etc/GMT+5' ? 'selected':'') }}>GMT-5, Misma hora a la de Colombia</option>
							<option value="Etc/GMT+4" {{ ($object->zone == 'Etc/GMT+4' ? 'selected':'') }}>GMT-4, 1 Hora más a la de Colombia</option>
							<option value="Etc/GMT+3" {{ ($object->zone == 'Etc/GMT+3' ? 'selected':'') }}>GMT-3, 2 Horas más a la de Colombia</option>
							<option value="Etc/GMT+2" {{ ($object->zone == 'Etc/GMT+2' ? 'selected':'') }}>GMT-2, 3 Horas más a la de Colombia</option>
						</select>
					</div>

				<div class="form-group">				
					
					<button type="submit" name="btn-action" id="btn-action" value="save" class="btn btn-primary">Guardar</button>

					<button type="submit" name="btn-action" id="btn-action" value="cancel" class="btn btn-secundary">Cancelar</button>
				</div>


			</form>

		</div>

	</div>

</div>


@endsection