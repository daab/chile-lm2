
<div class="col-xs-12 mt-10">
@if($rol == 2)

	@foreach($leads as $lead)
	<div class="row mb-30">

		<div class="row row-data">

			<div class="col-xs-8 p-10">
				<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
				@if($lead->estado2 == "")
				{{$lead->estado}}
				@else
				{{$lead->estado2}}
				@endif
			</div>
			<div class="col-xs-4 p-10">
				<small class="pull-right">{{$lead->created_at}}</small>
			</div>

		</div>

		<div class="row row-data">
			<div class="col-xs-6 p-30-b-5">
				<small>{{$lead->origen}}</small>
				<h3>{{$lead->nombres}}</h3>
			</div>
			<div class="col-xs-6 p-30-b-5">
				<small>Modelo</small>
				<h3>{{$lead->modelo}}</h3>
			</div>

			<div class="col-xs-12 sinPlr2">

				<a class="btn btn-default big-buton" href="{{url('/editar', $lead->id)}}">Editar</a>

			</div>

		</div>

	</div>
	@endforeach

@else

	@foreach($leads as $lead)
	<div class="row mb-30">

		<div class="row row-data">

			<div class="col-xs-8 p-10">
				<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
				@if($lead->estado2 == "")
				{{$lead->estado}}
				@else
				{{$lead->estado2}}
				@endif
			</div>
			<div class="col-xs-4 p-10">
				<small class="pull-right">{{$lead->created_at}}</small>
			</div>

		</div>

		<div class="row row-data">
			<div class="col-xs-4 p-30-b-5">
				<small>{{$lead->origen}}</small>
				<h3>{{$lead->nombres}}</h3>
			</div>
			<div class="col-xs-4 p-30-b-5">
				<small>Modelo</small>
				<h3>{{$lead->modelo}}</h3>
			</div>
			<div class="col-xs-4 sinPlr">

				<a class="btn btn-default big-buton" href="{{url('/gestionar', $lead->id)}}">Gestionar</a>

			</div>

		</div>

	</div>
	@endforeach


@endif
</div>

	