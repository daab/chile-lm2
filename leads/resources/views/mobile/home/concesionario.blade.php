@extends('mainmobile')

@section('content')
<form class="form-horizontal" name="formGuardar" id="formGuardar" onSubmit="return false;" method="post">

<input type="hidden" name="_token" value="{{ csrf_token() }}">
<input type="hidden" name="orden" id="orden" value="DESC" />
<input type="hidden" name="criteria" id="criteria" value="" />

<div class="container-fluid">

	<!-- Mostrar leads -->
	<div class="row" >
		<div class="col-xs-11 col-sm-8 ml-20">
			<div class="pagination-div bg-blanco">
				Mostrar de a <select onChange="enviarFormulario()" name="numPage" id="numPage">
				<option value="10">10</option>
				<option value="25">25</option>
				<option value="50">50</option>
				<option value="100">100</option>
			</select> Leads
		</div>
	</div>



</div>

	<!-- Paginacion -->
	<div class="row" >
		<div class="col-xs-8 ml-20">
			{!! $leads->render() !!}
		</div>
	</div>

	<!-- Ordenar por -->
	<div class="row"> 
		
		<div class="col-xs-11 col-sm-8 bg-blanco pt-15 ml-20">
			
			<div class="form-group col-xs-5 col-sm-4">
				<label class="control-label">Ordenar por</label>
			</div>
			
			<div class="form-group col-xs-7 col-sm-6">
					<select class="form-control" onChange="enviarFormulario()" name="criteria_order" id="criteria_order">
						<option value="">Seleccione</option>
						<option value="estado">Estado</option>
						<option selected value="created_at">Fecha</option>
						<option value="origen">Origen</option>
						<option value="modelo">Modelo</option>
						<option value="nombres">Nombre</option>
						<option value="apellidos">Apellido</option>

					</select>
				</div>
				<div class="form-group col-xs-1" style="margin-left: 15px">
					<a href="#" onClick="orderby('asc')">
						<i class="fa fa-arrow-up" aria-hidden="true"></i>
					</a>
					<a href="#" onClick="orderby('desc')">
						<i class="fa fa-arrow-down" aria-hidden="true"></i>
					</a>
				</div>

			
		</div>

	</div>


	<div class="row" id="tabla-resultados">
		<div class="col-xs-12 mt-10">

			
			@foreach($leads as $lead)

			<div class="row mb-30">

				<div class="row row-data">

					<div class="col-xs-8 p-10">
						<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
						@if($lead->estado2 == "")
						{{$lead->estado}}
						@else
						{{$lead->estado2}}
						@endif
					</div>
					<div class="col-xs-4 p-10">
						<small class="pull-right">{{$lead->created_at}}</small>
					</div>

				</div>

				<div class="row row-data">
					<div class="col-xs-4 p-30-b-5">
						<small>{{$lead->origen}}</small>
						<h3>{{$lead->nombres}}</h3>
					</div>
					<div class="col-xs-4 p-30-b-5">
						<small>Modelo</small>
						<h3>{{$lead->modelo}}</h3>
					</div>
					<div class="col-xs-4 sinPlr">

						<a class="btn btn-default big-buton" href="{{url('/gestionar', $lead->id)}}">Gestionar</a>

					</div>

				</div>

			</div>

			
			
				
				
			
			@endforeach
				

			


		</div>
	</div>
	
</div>

</form>

@endsection

@section('scripts')

<script type="text/javascript">

$( "#criteria" ).keypress(function() {
		var buscar = $( "#criteria" ).val();
		if (buscar.length >= 3 || buscar.length == 0) {
			param = $('#formGuardar').serialize();
			
			//AJAX
			$.ajax({
				type: "POST",
				data: "submit=&opcion=guardar&"+param,
				url: "{{url('/buscar/leads')}}",
				success: function(datos){
					$('#tabla-resultados').html(datos);
				}
			});
		}
		
	});


	function enviarFormulario(){

		param = $('#formGuardar').serialize();
			
			//AJAX
			$.ajax({
				type: "POST",
				data: "submit=&opcion=guardar&"+param,
				url: "{{url('/buscar/leads')}}",
				success: function(datos){
					$('#tabla-resultados').html(datos);
				}
			});

	}

	function orderby(orden){
		$('#orden').val(orden);

		enviarFormulario();
	}
</script>

@endsection