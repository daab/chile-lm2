@extends('mobile')

@section('content')
	<div class="formdiv"></div>
	<div class="wrapper">
		<div class="section-box print-leads"></div>
		<div class="btta-modal"></div>
		<div class="dsktp-top"></div>
		<div class="dsktp-left"></div>
	</div>
	
	<div class="menu-board">
		<div class="menu-boad-dsktp">
			<div class="btta-title btta-homemenu">Cotizaciones:</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'today', value:'gestion', boolean:1});">Cotizaciones de hoy</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'sin gestionar', boolean:1});">Sin Gestionar</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'all', value:'gestion', boolean:1});">Todos los leads</div>
			
			<div class="btta-title btta-homemenu">Proceso en vitrina:</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'agendado', boolean:1});">Calendario</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'en proceso', boolean:1});">En proceso / resultados</div>
			<div class="btta-title btta-homemenu">otros:</div>
			<div class="btta-menu btta-homemenu hidder rol_3" data-url="{{ url('/root/concesionarios/asesores') }}">Asesores</div>		
			
			<div class="btta-menu btta-homemenu" data-url="{{ url('/statistics') }}">Estadistica</div>
			<div class="btta-menu btta-homemenu rol_2" data-url="{{ url('/exportLeads') }}">
				Descarga
			</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/guide') }}">
				Guia
			</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/auth/logout') }}">Salir</div>
		</div>
		<div class="btta-menu btta-fecha" data-fuu="1">Fecha ascendente</div>
		<div class="btta-menu btta-fecha  btta-current" data-fuu="0">Fecha descendente</div>
		<div class="btta-title btta-set3 ">Agregar comisi�n:<br><input id="the_comision" type="text" name="the_comision"/></div>
		<div class="btta-menu btta-set3 " data-fuu="comision">Guardar</div>
		<div class="btta-title btta-set2 "><br><br>Recontactar en: 
			<div class="btta-menu btta-set2 " data-fuu="30">Un mes</div>
			<div class="btta-menu btta-set2 " data-fuu="60">Dos meses</div>
			<div class="btta-menu btta-set2 " data-fuu="90">Tres meses</div>
			<br><br>Por fecha: 
			<div class="calendar_lm"></div><br><input id="the_date" type="hidden" name="the_date"/>
		</div>
		<div class="btta-title btta-set2 ">
			<br><br>Hora:<br>
			<select id="the_hour" type="text" name="time" value="08:00">
				<option selected="" value="08:00">8:00</option>
				<option value="09:00">9:00</option>
				<option value="10:00">10:00</option>
				<option value="11:00">11:00</option>
				<option value="12:00">12:00</option>
				<option value="13:00">13:00</option>
				<option value="14:00">14:00</option>
				<option value="15:00">15:00</option>
				<option value="16:00">16:00</option>
				<option value="17:00">17:00</option>
				<option value="18:00">18:00</option>
				<option value="19:00">19:00</option>
				<option value="20:00">20:00</option>
			</select>
		</div>
		<div class="btta-menu btta-set2 btta-current id-guardaragenda" data-fuu="0">Siguiente</div>
		<div class="btta-title btta-purshace-time "><br><br>Tiempo de compra: 
			<div class="btta-menu btta-purshace-time " data-fuu="0">Sin definir</div>
			<div class="btta-menu btta-purshace-time " data-fuu="1">Un mes</div>
			<div class="btta-menu btta-purshace-time " data-fuu="3">3 meses</div>
			<div class="btta-menu btta-purshace-time " data-fuu="6">6 meses</div>
		</div>
	</div>

	<div class="db-header">
		<div class="header-logo"><a href="#" target="_blank">&nbsp;</a></div>
		<div class="header-title header-title-null" data-fuu="mes">Gestion</div>
		<div class="header-drop bttn-hover" data-fuu="homemenu">
			<div class="bttn-filler"></div>
			<img id="ico-menu" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-menu.svg') }}"/>
			<img id="ico-exit" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-exit.svg') }}"/>
		</div>
	</div>

	<div class="modal"></div>

@endsection

@section('script')

	<script type="text/javascript" src="{{ asset('/public/css/mobile/js/view-gestion.js') }}"></script>

	<script type="text/javascript">
		var vitrinas = {!! $concesionarios !!};	 //20180508:ajuste leer el modelo JSON concesionarios para uso jscript
		var modelos = {!! $modelos !!};		
		var gestion = {!! $lead !!};
		//alert('Concesionarios: ' + concesionarios);
		console.log("Concesionarios: " + vitrinas);
		console.log("Modelos: " + modelos);
		var fase = {!! $fase !!} 
		var prospectacion = {!! $prospectacion !!};
		var visita = {!! $agenda_visita !!};
		var resultado_visita = {!! $resultado_visita !!};		
		var proceso = {!! $resultado_proceso !!};	
		var agenda = {!! $agenda !!};
		console.log("fase: ", fase);
		console.log("p 1: ",prospectacion);
		console.log("v 2 ",visita);
		console.log("rv 3 ",resultado_visita);
		console.log("p 4 ",proceso);
		console.log("agenda 5 ",agenda);
		var rol = {{Auth::user()->roles()->first()->id}};
		rol = Number(rol);
		if(rol == 2){ $(".rol_2").removeClass("hidder")}
		if(rol == 3){ $(".rol_3").removeClass("hidder")}
		
		var calendar = false;
		var source = "{{$source}}";
		
		printGestion();

		function guardarDatos(){
		      		modal(0);
		      param = $('#lm-form').serialize();

		      $.ajax ( {
		        type: "POST",
		        data: "submit=&"+param+"&_token={{ csrf_token() }}",
		        url:  "{{url('/actualizar')}}",
		        success: function(datos){
		        	window.location.reload()
		        }
		    });
		}


		function filterLeads(options){
				//modal(0);
			if (options['boolean'] == 0) return;

			modal(0);			
	    	var form = document.createElement("form");
	    	form.setAttribute("method", "POST");
	    	form.setAttribute("action", "{{url('/home/filtro')}}");

	    	for(var index in options) { 
		 		var el = document.createElement("input");
		   		el.type = "hidden";
		   		el.name = index;
		   		el.value = options[index];
		   		form.appendChild(el); 		   
			}

	    	document.body.appendChild(form);
	    	form.submit();
		}

	</script>

@endsection