@extends('mobile')

@section('content')

	<div class="formdiv"></div>
	<div class="wrapper">
		<div class="section-box print-filtros"></div>
		<div class="section-box reportes">
		</div>
		<div class="btta-modal"></div>
		<div class="dsktp-top"></div>
		<div class="dsktp-left"></div>
	</div>
	
	<div class="menu-board">
		<div class="menu-boad-dsktp">
			<div class="btta-title btta-homemenu">Cotizaciones:</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'today', value:'gestion', boolean:1});">Cotizaciones de hoy</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'sin gestionar', boolean:1});">Sin Gestionar</div>
			<div class="btta-menu btta-homemenu " data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'all', value:'gestion', boolean:1});">Todos los leads</div>
			
			<div class="btta-title btta-homemenu">Proceso en vitrina:</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'agendado', boolean:1});">Calendario</div>
			<div class="btta-menu btta-homemenu" data-url="#" onclick="filterLeads({_token:'{{ csrf_token() }}', case:'status', value:'en proceso', boolean:1});">En proceso / resultados</div>
			<div class="btta-title btta-homemenu">otros:</div>
			<div class="btta-menu btta-homemenu hidder rol_3" data-url="{{ url('/root/concesionarios/asesores') }}">Asesores</div>		
			
			<div class="btta-menu btta-homemenu" data-url="{{ url('/statistics') }}">Estadistica</div>
			<div class="btta-menu btta-homemenu rol_2" data-url="{{ url('/exportLeads') }}">
				Descarga
			</div>
			<div class="btta-menu btta-homemenu hidder" data-url="{{ url('/guide') }}">
				Guia
			</div>
			<div class="btta-menu btta-homemenu" data-url="{{ url('/auth/logout') }}">Salir</div>
		</div>
		<div class="btta-menu btta-fecha" data-fuu="1">Fecha ascendente</div>
		<div class="btta-menu btta-fecha  btta-current" data-fuu="0">Fecha descendente</div>
		<div class="btta-title btta-name ">Buscar por nombre o apellido:<br><input id="searcher" type="text" name="searcher"/></div>
		<div class="btta-title btta-clm ">Guardar Fecha:<div class="calendar_lm"></div><br><input id="the_date" type="hidden" name="the_date"/></div> 
    	<div class="btta-menu btta-clm btta-current id-guardaragenda trigger-clm" data-fuu="agenda">Confirmar</div> 
		<div class="btta-menu btta-name " data-fuu="0">Buscar</div>
	</div>

	<div class="db-header">
		<div class="header-logo"><a href="#" target="_blank">&nbsp;</a></div>
		<div class="header-title header-title-null" data-fuu="mes">Estadística</div>
		<div class="header-drop bttn-hover" data-fuu="homemenu">
			<div class="bttn-filler"></div>
			<img id="ico-menu" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-menu.svg') }}"/>
			<img id="ico-exit" class="icon-menu-button" src="{{ asset('/public/css/mobile/images/header/ico-exit.svg') }}"/>
		</div>
	</div>

@endsection


@section('script')

<script type="text/javascript" src="{{ asset('/public/css/mobile/js/view-pie.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/css/mobile/js/json-estadistica.js') }}"></script>
<script type="text/javascript" src="{{ asset('/public/css/mobile/js/view-estadistica.js') }}"></script>

<script type="text/javascript">
	var estadistica = {};
	estadistica.datos = {!! $statistics !!};		
	estadistica.vitrinas = {!! $distributors !!};
	// estadistica.asesores = json_asesores;
	estadistica.modelos = {!! $models !!};
	estadistica.ciudades = {!! $cities !!};
	estadistica.concesionarios = {!! $conglomerates !!};
	var filtros = {!! $filtros !!};

	var rol = {{Auth::user()->roles()->first()->id}};
	rol = Number(rol);
	if(rol == 2){ $(".rol_2").removeClass("hidder")}
	if(rol == 3){ $(".rol_3").removeClass("hidder")}
			
	collectData();

	function filterLeads(options) {
		
		if (options['boolean'] == 0) return;
                 //modal(0);

		if($(this).hasClass("nuller")) return; 
    	var form = document.createElement("form");
    	form.setAttribute("method", "POST");
    	form.setAttribute("action", "{{url('/home/filtro')}}");

    	for(var index in options) { 
	 		var el = document.createElement("input");
	   		el.type = "hidden";
	   		el.name = index;
	   		el.value = options[index];
	   		form.appendChild(el); 		   
		}

    	document.body.appendChild(form);
    	form.submit();
	}


	function toFilter() {

		modal(0);
		param = $('#lm-form').serialize();
		var element = '<input type="hidden" name="_token" value="{{ csrf_token() }}"/>';		

		$('#lm-form').attr("action", "{{url('/statistics/filters')}}");
		$('#lm-form').attr('method', 'POST');
		$('#lm-form').append(element);
		$('#lm-form').append(param);
		$('#lm-form').submit();
	}

</script>

@endsection