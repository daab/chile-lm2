@extends('mainmobile')

@section('content')

<form class="form-horizontal" name="formGuardar" id="formGuardar" onSubmit="return false;" method="post">
<div class="container-fluid">
	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-6"> 
					<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
					@if($lead->estado2 !="")
						{{$lead->estado2}}
					@else
						{{$lead->estado}}
					@endif
				</div>

				<div class="col-xs-6">
					<span class="pull-right">{{$lead->created_at}}</span>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-6"> 
					<small>{{$lead->origen}}</small>
					<h3  class="titulo"><strong>{{$lead->nombres}} {{$lead->apellidos}}</strong></h3>
				</div>

				<div class="col-xs-6">
					<small>Modelo</small>
					<select name="modelo" class="form-control" id="modelo">
						@foreach ($modelos as $modelo)
							<option value="{{$modelo->nombre}}" @if($modelo->nombre==$lead->modelo)selected @endif >{{$modelo->nombre}}</option>
						@endforeach
					</select>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Teléfono</small>
					<h3 class="titulo"><strong>{{$lead->telefono}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Correo</small>
					<h3 class="titulo"><strong>{{$lead->correo}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Estado</small>
					<input type="hidden" id="estadoTemp" />
					<div class="col-xs-12">
						<select class="form-control" name="estado" id="estado" onChange="guardarEstado('estado', 1)">
								<option @if($lead->estado=='Sin Gestionar')selected @endif value="Sin Gestionar">Sin Gestionar</option>
								<option @if($lead->estado=='Interesado')selected @endif value="Interesado">Interesado</option>
								<option @if($lead->estado=='No Interesado')selected @endif value="No Interesado">No Interesado</option>
								<option @if($lead->estado=='No contesto')selected @endif value="No contesto">No contesto</option>
								<option @if($lead->estado=='Vendido')selected @endif value="Vendido">Vendido</option>
								<option @if($lead->estado=='Datos errados')selected @endif value="Datos errados">Datos errados</option>
								<option @if($lead->estado=='Lead Duplicado')selected @endif value="Lead Duplicado">Lead Duplicado</option>
						</select>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15" id="divInteresado" style="display: none">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Motivo</small>
					<div class="col-xs-12">
						<select class="form-control" name="estadoInteresado" id="estadoInteresado" onChange="guardarEstado('estadoInteresado', 1)">
									<option value="1">Seleccione...</option>
									<option @if($lead->estado2=='Agendo Cita')selected @endif value="Agendo Cita">Agendo Cita</option>
									<option @if($lead->estado2=='Test Drive')selected @endif value="Test Drive">Test Drive</option>
									<option @if($lead->estado2=='Prospecto futuro')selected @endif value="Prospecto futuro">Prospecto futuro</option>
								</select>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15" id="divNoInteresado" style="display: none">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Motivo</small>
					<div class="col-xs-12">
						<select class="form-control" name="estadoNoInteresado" id="estadoNoInteresado" onChange="guardarEstado('estadoNoInteresado', 1)">
									<option value="1">Seleccione...</option>
									<option @if($lead->estado2=='Ya compró')selected @endif value="Ya compró">Ya compró</option>
									<option @if($lead->estado2=='Fuera de presupuesto')selected @endif value="Fuera de presupuesto">Fuera de presupuesto</option>
									<option @if($lead->estado2=='Ingreso por curiosidad')selected @endif value="Ingreso por curiosidad">Ingreso por curiosidad</option>
								</select>
					</div>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15" id="divVendido" style="display: none">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Comisión</small>
					<div class="col-xs-12">
						<input value="{{ $lead->comision }}" type="text" class="form-control" name="comision" id="comision" /><br />
						<button class="btn btn-default daaboton col-md-4" onClick="guardarEstado('Vendido',  1)">Guardar</button>
					</div>
				</div>
			</div>

		</div>
	</div>


	
								
	

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco">
			<div class="row">
				<div class="col-xs-12 pt-5">
					<div class="row">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						<div class="col-xs-7">
							<input type="text" class="form-control" name="anotacion" id="anotacion" />
						</div>
						<div class="col-xs-5">
							<button class="btn btn-default daaboton col-md-4" onClick="guardarAnotacion()">Anotar</button>
						</div>
					</div>

					<div class="row mt-30">
						<div class="col-md-11" style="padding: 20px;background: #EEEEEE;margin-bottom: 20px;margin-left:15px;margin-right:15px" id="anotaciones">
							@foreach($anotaciones as $anotacion)
								<div>
									<small>{{$anotacion->created_at}}</small>
									<p>{{$anotacion->anotacion}}</p>
								</div>
							@endforeach
						</div>
						<br /><br />
					</div>
				</div>
			</div>
		</div>
	</div>





@endsection

@section('scripts')
<script type="text/javascript">

	var leadId = {{$lead->id}};
	guardarEstado('estado', 0);
	
	function guardarEstado(estado, bandera){

		if (estado == 'estado') {

			switch($('#estado').val()){
				case 'Interesado':
					$('#divInteresado').show();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
				case 'No Interesado':
					$('#divInteresado').hide();
					$('#divNoInteresado').show();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
				case 'No contesto':
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').show();
					$('#divVendido').hide();
				break;
				case 'Vendido':
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').show();
				break;
				default:
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
			}

		}


		var estadosUnicos = ['Datos errados', 'Lead Duplicado'];
		var validarEstado = jQuery.inArray($('#'+estado).val(), estadosUnicos);

		var segundoEstado = ['estadoNoInteresado','estadoInteresado','estadoNoContesto', 'Vendido'];
		var validarEstado2 = jQuery.inArray(estado, segundoEstado);
		

		if (bandera == 1) {
		

			if( validarEstado >= 0  || validarEstado2 >= 0 ){
				$('#estadoTemp').val(estado);
				
				swal({
					  title: "¿Esta seguro?",
					  text: "Va a realizar un cambio de estado en el lead",
					  type: "warning",
					  confirmButtonColor: "#DD6B55",
					  confirmButtonText: "Si, Estoy seguro",
					  showCancelButton: true,
					  closeOnConfirm: false
					},
					function(){

						var anotacion = "Cambio de estado de {{$lead->estado}} a "+$('#estado').val();
						var comision = "";
						var estado2 = "";
						
						switch($('#estadoTemp').val()){
							case 'estadoNoInteresado':
								 estado2 = $('#estadoNoInteresado1').val();
							break; 
							case 'estadoInteresado':
								 estado2 = $('#estadoInteresado').val();
							break; 
							case 'estadoNoContesto':
								 estado2 = $('#estadoNoContesto').val();
							break; 
							case 'Vendido':
								comision = $('#comision').val();
								estado2 = '';
							break;
						}
						
						
						if(estado2 == '1' || validarEstado > 0  ){
							estado2 = '';
						}
						
						
						var estado = $('#estado').val();

						//AJAX
						$.ajax({
							type: "POST",
							data: "_token="+$('#token').val()+"&anotacion="+anotacion+"&id="+leadId+"&estado="+estado+"&estado2="+estado2+"&comision="+comision+"&modelo="+$('#modelo').val(),
							url: "{{url('/anotacion')}}",
							success: function(datos){
								
								swal("Exito!", "Cambio realizado", "success");
								$('#anotaciones').html(datos);
							}
						});



					  
					});
			}//Guardar formulario
		}
	}

	function guardarAnotacion(){
		//AJAX
		$.ajax({
			type: "POST",
			data: "_token="+$('#token').val()+"&anotacion="+$('#anotacion').val()+"&id="+leadId+"&modelo="+$('#modelo').val(),
			url: "{{url('/anotacion')}}",
			success: function(datos){
				$('#anotacion').val('');
				swal("Exito!", "Cambio realizado", "success");
				$('#anotaciones').html(datos);
			}
		});
	}

</script>
@endsection