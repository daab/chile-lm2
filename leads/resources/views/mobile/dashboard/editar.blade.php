@extends('mainmobile')

@section('content')
<form class="form-horizontal" name="formGuardar" id="formGuardar" onSubmit="return false;" method="post">

<div class="container-fluid">
	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-6"> 
					<i style="color: {{$colores[$lead->estado]}}" class="fa fa-circle" aria-hidden="true"></i>
					{{$lead->estado}}
				</div>

				<div class="col-xs-6">
					<span class="pull-right">{{$lead->created_at}}</span>
				</div>
			</div>

		</div>
	</div>


	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-6"> 
					<small>{{$lead->origen}}</small>
					<h3  class="titulo"><strong>{{$lead->nombres}} {{$lead->apellidos}}</strong></h3>
				</div>

				<div class="col-xs-6">
					<small>Modelo</small>
					<h3 class="titulo"><strong>{{$lead->modelo}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Teléfono</small>
					<h3 class="titulo"><strong>{{$lead->telefono}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Correo</small>
					<h3 class="titulo"><strong>{{$lead->correo}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco pt-5">

			<div class="row pb-5">
				<div class="col-xs-12"> 
					<small>Estado</small>
					<h3 class="titulo"><strong>{{$lead->estado}}</strong></h3>
				</div>
			</div>

		</div>
	</div>

	


	<div class="row mb-15">
		<div class="col-xs-12 bg-blanco">
			<div class="row">
				<div class="col-xs-12 pt-5">
					<div class="row">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						<div class="col-xs-7">
							<select name="concesionario" id="concesionario" class="form-control">
								@foreach ($concesionarios as $conc)
									<option value="{{$conc->nombre}}" @if($conc->nombre==$lead->concesionario) selected @endif>{{$conc->nombre}}</option>
								@endforeach
							</select>
						</div>
						<div class="col-xs-5">
							<button class="btn btn-default daaboton col-md-4" onClick="guardarConcesionario()">Cambiar</button>
						</div>
					</div>

					<div class="row mt-30">
						<div class="col-md-11" style="padding: 20px;background: #EEEEEE;margin-bottom: 20px;margin-left:15px;margin-right:15px" id="anotaciones">
							@foreach($anotaciones as $anotacion)
								<div>
									<small>{{$anotacion->created_at}}</small>
									<p>{{$anotacion->anotacion}}</p>
								</div>
							@endforeach
						</div>
						<br /><br />
					</div>
				</div>
			</div>
		</div>
	</div>

	
</div>




</form>

@endsection

@section('scripts')
<script type="text/javascript">

	var leadId = {{$lead->id}};
	
	function guardarConcesionario(){
		swal({
			  title: "¿Esta seguro?",
			  text: "Va a realizar un cambio de concesionario",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Si, Estoy seguro",
			  closeOnConfirm: false
			},
			function(){

				
				
				//AJAX
				$.ajax({
					type: "POST",
					data: "_token="+$('#token').val()+"&concesionario="+$('#concesionario').val()+"&id="+leadId,
					url: "{{url('/cambioconcesionario')}}",
					success: function(datos){
						
						swal("Exito!", "Cambio realizado", "success");
						$('#anotaciones').html(datos);
					}
				});



			  
			});
	}



</script>
@endsection