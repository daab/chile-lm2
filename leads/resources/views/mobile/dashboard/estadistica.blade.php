@extends('mainmobile')

@section('content')
<form name="formEstado" id="formEstado" method="post" action="{{URL::to('/estados')}}">
<div class="container-fluid">
	
	<div class="bg-blanco" style="width: 90%; padding: 20px">
		


		<div class="row">

			<div class="col-md-4">

 
				<h1 class="color-rojo">Estados</h1>

			</div>


			<div class="col-md-7">
				@if(count($objs) > 0)
				
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<label>{{$titulo}}</label>
					<input value="{{$titulo}}" type="hidden" name="type" id="type">
					<select onChange="enviar()" class="form-control" name="concesionario" id="concesionario">
						<option value="">Seleccione para filtrar</option>
						@foreach($objs as $obj) 
							<option value="{{$obj->nombre}}" @if($obj->nombre==$concesionario) selected @endif>{{$obj->nombre}}</option>
						@endforeach
					</select>
				
				@endif
			</div>

		</div>


		<div class="row">

			
				
				
			

						@foreach($leads as $lead)
						<div class="col-xs-12" style="border-bottom: 1px solid #CCC">
							
								<div class="col-xs-6">
									<h3 class="est">{{$lead->num}}</h3>
									<p>{{$lead->estado}}</p>
								</div>

								@if($lead->estado == 'Interesado' || $lead->estado == 'No Interesado')
									<div class="col-xs-6">
									@foreach ($leads2 as $lead2)
										@if($lead->estado == 'Interesado' || $lead->estado == 'No Interesado' || $lead->estado == 'No contesto')
											<h4>{{$lead2->num}} <small>{{$lead2->estado2}}</small></h4>
											
										@endif
										
									@endforeach
									</div>
								@endif
							
							</div>
						@endforeach
				

			

		</div>



	</div>
</div>
</form>
@stop


@section('scripts')
	<script type="text/javascript">

		function enviar(){
			document.getElementById('formEstado').submit();
		}
		

	</script>
@stop
