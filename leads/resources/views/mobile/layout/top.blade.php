<div class="db-wrapper-menu">
    <div class="db-deco0"></div>
    <div class="db-bttn">
    	<div class="db-bttn-ico"></div>
        <a href="{{url('/')}}">
            <img src="{{ asset('/public/images/ico-leads.jpg') }}" />
        </a>
    </div>
    <?php 
        if(Auth::user()->rol == 2){
            $type = "marca";
        }elseif(Auth::user()->rol == 3){
            $type = "concesionario";
        }else{
            $type = "root";
        }
    ?>
    
    <div class="db-bttn">
    	<div class="db-bttn-ico"></div>
        <a href="{{url('/exportar', $type)}}">
            <img src="{{ asset('/public/images/ico-down.jpg') }}" />
        </a>
    </div>
    <?php 
        if(Auth::user()->rol == 2){
            $type = "marcas";
        }elseif(Auth::user()->rol == 3){
            $type = "concesionarios";
        }else{
            $type = "root";
        }

    ?>
    
    <div class="db-bttn">
    	<div class="db-bttn-ico"></div>
        <a href="{{url('/estados', $type)}}">
            <img src="{{ asset('/public/images/ico-estad.jpg') }}" />
        </a>
    </div>
    
    <div class="db-bttn">
    	<div class="db-bttn-ico"></div>
        <a href="{{url('/auth/logout')}}">
            <img src="{{ asset('/public/images/ico-logout.jpg') }}" />
        </a>
    </div>
    
    <div class="db-deco1"></div>
    <div class="clearfix"></div>
</div>