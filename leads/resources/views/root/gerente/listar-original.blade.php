@extends('root')

@section('content')
<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('/root/nuevo/gerente') }}" class="btn btn-default daaboton">Nuevo Gerente</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 20%">Correo</th>
				<th style="width: 20%">Rol</th>
				<th style="width: 20%">Producto</th>
				<th style="width: 20%">Concesionario</th>
				<th class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($gerentes as $user)
				<tr>
					<td>{{ $user->correo }}</td>
					@if($user->rol == 1)
						<td>Gerente de producto</td>
						<td>{{ $user->text }}</td>
						<td></td>
					@else
						<td>Gerente de concesionario</td>
						<td></td>
						<td>{{ $user->text }}</td>
					@endif
					<td class="text-center"><a href="{{ url('root/nuevo/gerente', $user->id) }}">Editar</a> | <a href="{{ url('root/eliminar/gerente', $user->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection