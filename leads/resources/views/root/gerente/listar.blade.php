@extends('root')

@section('title')
	Leads | Gerente
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('/root/nuevo/gerente') }}" class="btn btn-default daaboton">Nuevo Gerente</a>
		<br /><br /><br />
		
		<table id="gerentes">
			<thead>
				<th style="width: 20%">Correo</th>
				<th style="width: 20%">Rol</th>
				<th style="width: 20%">Producto</th>
				<th style="width: 20%">Concesionario</th>
				<th>Opciones</th>
			</thead>
			
			<tbody>
				
			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(document).ready(function(){
		$('#gerentes').DataTable({
			processing: true,
			serverSide: true,
			ajax: "{{ route('obtener-gerentes') }}",
			language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ningún dato disponible en esta tabla",
            "sInfo":           "Mostrando del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Ningún dato disponible en esta tabla",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
			columns: [
				{ data: 'correo', name: 'correo' },
				{ data: 'gerenteproducto', name: 'gerenteproducto' },
				{ data: 'gerenteproducto2', name: 'gerenteproducto2' },
				{ data: 'gerenteproducto3', name: 'gerenteproducto3' },
				{ data: 'apciones', name: 'apciones', orderable: false, searchable: false},
			],
        	order: [[1, 'desc']]
		});
	});
</script>
@endsection