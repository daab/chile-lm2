@extends('root')

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/guardar/gerente') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{{ $gerente->id }}">

					<div class="form-group">
						<label class="color-blanco control-label">Corre Electronico</label>
						<input type="text" class="form-control" name="correo" value="{{ $gerente->correo }}">
					</div>

					

					<div class="form-group">
						<label class="color-blanco control-label">Rol</label>
						<select class="form-control" name="rol" onChange="changeRol()" id="rol">
							<option>Seleccion...</option>
							<option @if($gerente->rol==1)selected @endif value="1">Producto</option>
							<option @if($gerente->rol==2)selected @endif value="2">Concesionario</option>
						</select>
					</div>

					<div class="form-group" id="divMarca" style="display: none">
						<label class="color-blanco control-label">Marca</label>

						<select class="form-control" name="marca" id="marca">
							<option value="">Seleccione una opcion</option>
							@foreach ($marcas as $marca)
								<option value="{{$marca->nombre}}" @if($marca->nombre==$gerente->text)selected @endif>{{$marca->nombre}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group" id="divConcesionario" style="display: none">
						<label class="color-blanco control-label">Concesionario</label>
						<select class="form-control" name="concesionario" id="concesionario">
							<option value="">Seleccione una opcion</option>
							@foreach ($concesionarios as $concesionario)
								<option value="{{$concesionario->nombre}}" @if($concesionario->nombre==$gerente->text)selected @endif>{{$concesionario->nombre}}</option>
							@endforeach
						</select>
					</div>

					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	$( document ).ready(function() {
	  // Handler for .ready() called.
	  changeRol();
	});
	
	function changeRol(){
		var rol = parseInt($('#rol').val());

		//Siempre ocultamos los divs
		$('#divMarca').hide();
		$('#divConcesionario').hide();
		switch(rol){
			case 1:
				//Mostramos las marcas
				$('#divMarca').show();
			break;
			case 2:
				//Mostramos los concesionarios
				$('#divConcesionario').show();
			break;
		}
	}

</script>
@endsection
