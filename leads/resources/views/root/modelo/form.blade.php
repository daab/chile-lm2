@extends('root')

@section('title')
	Leads | Modelos
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/modelo/guardar') }}" enctype="multipart/form-data">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{{ $modelo->id }}">

					<div class="form-group">
						<label class="color-blanco control-label">Nombre</label>
						<input type="text" class="form-control" name="nombre" value="{{ $modelo->name }}">
					</div>


					<div class="form-group">
						<label class="color-blanco control-label">Marca</label>
						<select name="marca" id="marca">
							@foreach ($marcas as $marca)
								<option value="{{$marca->id}} @if($marca->id==$modelo->marcas_id)selected @endif ">{{$marca->name}}</option>
							@endforeach
						</select>
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Ficha tecnica</label>
						<input type="file" name="doc" id="doc" />

					</div>
					

					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection
