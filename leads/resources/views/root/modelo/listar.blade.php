@extends('root')

@section('title')
	Leads | Modelos
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('root/modelo/nuevo') }}" class="btn btn-default daaboton">Nuevo Modelo</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 20%">Nombre</th>
				<th style="width: 20%">Marca</th>
				<th class="transparente" style="width: 20%"></th>
			</thead>
			
			<tbody>
				@foreach($modelos as $modelo)
				<tr>
					<td>{{ $modelo->name }}</td>
					<td>{{ $modelo->marcas->name }}</td>
					<td class="text-center"><a href="{{ url('root/modelo/nuevo', $modelo->id) }}">Editar</a> | <a href="{{ url('root/modelo/eliminar', $modelo->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				

			</tbody>

		</table>			
			
		
	</div>
	
</div>
@endsection