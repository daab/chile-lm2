@extends('root')

@section('title')
	Leads | Dominio
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('/root/dominios/guardar/2') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="id" value="{{ $dominio->id }}">


					<div class="form-group">
						<label class="color-blanco control-label">Dominio</label>
						<input type="text" class="form-control" name="dominio" value="{{ $dominio->dominio }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">IP</label>
						<input type="text" class="form-control" name="ip" value="{{ $dominio->ip }}">
					</div>

					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection
