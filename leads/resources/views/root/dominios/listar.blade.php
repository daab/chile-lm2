@extends('root')

@section('title')
	Leads | Dominio
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-8">

		<a href="{{ url('root/dominios/nuevo/2') }}" class="btn btn-default daaboton">Nuevo Dominio</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 5%">Id</th>
				<th style="width: 25%">Dominio</th>
				<th style="width: 10%">IP</th>
				<th style="width: 50%">Token</th>
				<th class="transparente" style="width: 10%"></th>
			</thead>
			
			<tbody>
				@foreach($dominios as $dominio)
				<tr>
					<td>{{ $dominio->id }}</td>
					<td>{{ $dominio->dominio }}</td>
					<td>{{ $dominio->ip }}</td>
					<td>{{ $dominio->token }}</td>
					<td class="text-center"><a href="{{ url('root/dominios/nuevo/2', $dominio->id) }}">Editar</a> | <a href="{{ url('root/dominios/eliminar/2', $dominio->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection