@extends('root')

@section('title')
	Leads | Ciudades
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6" >

			<h1>{{ (empty($city) ? 'Nuevo' : 'Editar') }} Ciudad</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/ciudad/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($city) ? '' : $city->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Ciudad</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($city) ? '' : $city->name) }}" 
						placeholder="Ej. Bogotá D.C.">
					
					</div>
				
										
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	
</script>
@endsection
