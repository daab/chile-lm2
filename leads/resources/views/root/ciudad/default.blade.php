@extends('root')

@section('title')
	Leads | Ciudades
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-6">

		<a href="{{ url('/root/ciudad/nuevo') }}" class="btn btn-default daaboton">Nuevo ciudad</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:16%;">Ciudad</th>
				<th style="width:3%;" class="transparente"></th>
				<th style="width:3%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($cities as $city)
				<tr>
					
					<td>{{ $city->name }}</td>
					
					<td class="text-center">
						<a href="{{ url('root/ciudad/nuevo', $city->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/ciudad/eliminar', $city->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>			
				</tr>
				@endforeach				

			</tbody>

		</table>
			
	</div>
	
</div>
@endsection