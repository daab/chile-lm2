@extends('root')

@section('title')
	Leads | Estados
@endsection

@section('content')

<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('/root/estados/nuevo') }}" class="btn btn-default daaboton">Nuevo Estado</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:20%;">Fase</th>
				<th style="width:20%;">Estado</th>
				<th style="width:18%;">Motivos</th>
				<th style="width:3%;">Inicial</th>
				<th style="width:3%;">Final</th>
				<th style="width:3%;">Color</th>
				<th style="width:3%;" class="transparente"></th>
				<th style="width:3%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($status as $state)
				<tr>
					
					<td>{{ $state->phase['name'] }}</td>
					<td>{{ $state->name }}</td>
					<td>
						<ul>
							@foreach ($state->reasons as $reason)
								<li style="display:block;">{{$reason->name}}</li>
							@endforeach
						</ul>
					</td>
					
					<td class="text-center">
						@if ($state->is_initial == 1) 
							<i class="fa fa-check" aria-hidden="true"></i>
						@endif
					</td>

					<td class="text-center">
						@if ($state->is_final == 1) 
							<i class="fa fa-check" aria-hidden="true"></i>
						@endif						
					</td>

					<td class="text-center">
						<i class="fa fa-circle" style="font-size:15px; color:{{$state->color}};" aria-hidden="true"></i>
					</td>

					<td class="text-center">
						<a href="{{ url('root/estados/nuevo', $state->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/estados/eliminar', $state->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>					
				</tr>
				@endforeach				

			</tbody>

		</table>
			
	</div>
	
</div>
@endsection