@extends('root')

@section('title')
	Leads | Estados
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6" >

			<h1>{{ (empty($status) ? 'Nuevo' : 'Editar') }} estado</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/estados/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($status) ? '' : $status->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Estado</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($status) ? '' : $status->name) }}" 
						placeholder="Ej. Sin Gestionar">
					
					</div>


					<div class="form-group">
					
						<label class="color-blanco control-label">Fase</label>

						<select class="form-control" name="phase" id="phase">
							
							<option value=""></option>
							
							@foreach ($phases as $p)

								{{--*/ 
									
									$selected = (!empty($status) &&  $status->phase->id == $p->id ? 'selected' : '') 

									/*--}}

								<option value="{{$p->id}}" {{$selected}} >

									{{$p->sort}} - {{$p->name}}
								</option>
							@endforeach

						</select>					
					
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Color</label>					
						<input type="text" class="form-control" name="color" value="{{ (empty($status) ? '' : $status->color) }}" 
						placeholder="Ej. #fffff">
					
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Titulo</label>					
						<input type="text" class="form-control" name="title" value="{{ (empty($status) ? '' : $status->title) }}" 
						placeholder="Ej. ¡Contactado!">
					
					</div>					

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="es_inicial" 
								   id="es_inicial" {{(!empty($status) && $status->is_initial == 1 ? 'checked' : '')}}> ¿Es un estado inicial?
						</label>
					</div>

					<div class="checkbox">
						<label>
							<input type="checkbox" 
								   name="es_final" 
								   id="es_final" {{(!empty($status) && $status->is_final == 1 ? 'checked' : '')}}> ¿Es un estado final?
						</label>
					</div>

					<hr/>

					<div class="form-group" style="width: 150%">
						<label>
							<a href="#" onclick="addReasons();">
								<i class="fa fa-plus-circle" style="font-size:18px;" aria-hidden="true"></i>
								Adicionar motivo
							</a>
						</label>

						<table id="t_reasons">

							<tr>
								<th width="25%">Motivo</th>
								<th>Calificación</th>
								<th>Mostrar recordatorio</th>
								<th>Mostrar comisión</th>
								<th>WebServices</th>
								<th>Opción</th>
								<th width="5%" class="transparente"></th>
							</tr>

							@if (!empty($status))

							{{--*/ $count = 1 /*--}}

								@foreach ($status->reasons as $state)
									<input type='hidden' name='reason_id_{{$count}}' value="{{$state->id}}">
									<tr>
										<td>								
											<input type='text' name='reason_{{$count}}' value="{{$state->name}}" placeholder='Ej. Datos errados'>
										</td>
										<td>
											<select name='rate_{{$count}}'>
												<option value =''></option>
												<option value ='Soñador' {{($state->rate == 'remarketing') ? 'selected' : ''}}>
													remarketing
												</option>
												<option value ='Planeador' {{($state->rate == 'Planeador') ? 'selected' : ''}}>
													Planeador
												</option>
												<option value ='Comprador' {{($state->rate == 'Comprador') ? 'selected' : ''}}>
													Comprador
												</option>
											</select>
										</td>
										<td class='text-center'>
											<input type='checkbox' 
												   name='sw_reminder_{{$count}}' 
												   {{($state->show_reminder == 1 ? 'checked' : '')}}>
										</td>

										<td class='text-center'>
											<input type='checkbox' 
												   name='sw_commission_{{$count}}' 
												   {{($state->show_commission == 1 ? 'checked' : '')}}>
										</td>

										<td class='text-center'>
											<input type='checkbox' 
												   name='web_services_{{$count}}' 
												   {{($state->web_services == 1 ? 'checked' : '')}}>
										</td>

										<td>
											<select name='sw_option_{{$count}}'>
												<option value =''></option>
												<option value ='2' {{($state->sw_option == '2') ? 'selected' : ''}}>
													Cerrado sin venta
												</option>
												<option value ='1' {{($state->sw_option == '1') ? 'selected' : ''}}>
													Reagendado
												</option>
											</select>
										</td>

										<td class='text-center' onclick='$(this).parent().remove(); delReason("{{$state->id}}")'>x</td>
									</tr>
									
									{{--*/ $count = $count + 1 /*--}}

								@endforeach

							@endif
							
						</table>						

					</div>				

					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	function addReasons() {

		var rowCount = $('#t_reasons tr').length;

		var add_tr = "";

		add_tr += "<tr>";
			add_tr += "<td><input type='text' name='reason_" + rowCount + "' placeholder='Ej. Datos errados'></td>";
			add_tr += "<td><select name='rate_" + rowCount + "'>";
				add_tr += "<option value =''></option>";
				add_tr += "<option value ='Soñador'>Soñador</option>";
				add_tr += "<option value ='Planeador'>Planeador</option>";
				add_tr += "<option value ='Comprador'>Comprador</option>";
			add_tr += "</select></td>";			
			add_tr += "<td class='text-center'><input type='checkbox' name='sw_reminder_" + rowCount + "'></td>";
			add_tr += "<td class='text-center'><input type='checkbox' name='sw_commission_" + rowCount + "'></td>";
			add_tr += "<td class='text-center'><input type='checkbox' name='web_services_" + rowCount + "'></td>";			
			add_tr += "<td><select name='sw_option_" + rowCount + "'>";
				add_tr += "<option value =''></option>";
				add_tr += "<option value ='2'>Cerrado sin venta</option>";
				add_tr += "<option value ='1'>Reagendado</option>";
			add_tr += "</select></td>";			
			add_tr += "<td class='text-center' onclick='$(this).parent().remove(); delReason(0)'>x</td>";
		add_tr += "</tr>";			

		$('#t_reasons').append(add_tr);
	}

	function delReason(reason) {

		if (reason == 0) return;

        $.ajax({
        	
        	type:"POST",

        	url: "{{url('root/estados/eliminar')}}",

        	data: '&_token={{ csrf_token() }}&id='+reason,

        	success: function(data){

		    }
        });		
	}	
</script>
@endsection
