@extends('root')

@section('title')
	Leads | Marcas
@endsection

@section('content')scripts
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('/root/marcas/guardar/1') }}">
					<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="id" name="id" value="{{ $marca->id }}">

					<div class="form-group">
						<label class="color-blanco control-label">Marca</label>
						<input type="text" class="form-control" name="nombre" value="{{ $marca->name }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Template</label>
						<input type="text" class="form-control" name="template" value="{{ $marca->template }}">
					</div>


					<div class="form-group">
						<label class="color-blanco control-label">URL Logo</label>
						<input type="text" class="form-control" name="logo" value="{{ $marca->logo }}">
					</div>

					

					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>

		@if(count($concesionarios) > 0)
			<div class="col-md-6">

				<h3>Concesionarios</h3>
				<?php $i = 0 ?>
				
				@foreach($concesionarios as $con)
					<?php 
						$checked = "";
						if (in_array($con->id, $cons)) {
							$checked = "checked";
						}
					?>
					<input type="checkbox" id="con{{$i}}" {{$checked}} onChange="update({{$con->id}}, {{$i}})" /> {{$con->name}}<br />
					<?php $i++ ?>
				@endforeach

			</div>
			<br /><br /><br />
		@endif
	</div>
</div>
@endsection



@section('scripts')

	<script type="text/javascript">

		function update(id, con){

			var guardar = 0;
			var idCon = "con"+con;
			
			if ($('#'+idCon).is(':checked')) { 
				guardar = 1;
			}

			//AJAX
			$.ajax({
				type: "POST",
				data: "_token="+$('#token').val()+"&idMarca="+$('#id').val()+"&idConcesionario="+id+"&guardar="+guardar,
				url: "{{url('/root/update')}}",
				success: function(datos){
					console.log("Exito dude...");
				}
			});
		}

	</script>

@endsection


