@extends('root')

@section('title')
	Leads | Marcas
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-6">

		<a href="{{ url('root/marcas/nuevo/1') }}" class="btn btn-default daaboton">Nueva Marca</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 25%">Id</th>
				<th style="width: 50%">Nombre</th>
				<th class="transparente" style="width: 25%"></th>
			</thead>
			
			<tbody>
				@foreach($marcas as $marca)
				<tr>
					<td>{{ $marca->id }}</td>
					<td>{{ $marca->name }}</td>
					<td class="text-center"><a href="{{ url('root/marcas/nuevo/1', $marca->id) }}">Editar</a> | <a href="{{ url('root/marcas/eliminar/1', $marca->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection