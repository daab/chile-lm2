<select class="form-control" name="brand_distributor" id="brand_distributor">

	<option value=""> - Seleccione una opción -</option>

	@foreach ($brands as $brand)
		
			<option value="{{$brand->id}}">{{$brand->name}}</option>

	@endforeach

</select>