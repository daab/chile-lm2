@extends('root')

@section('title')
	Leads | Usuarios
@endsection

@section('content')

<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('/root/usuario/nuevo') }}" class="btn btn-default daaboton">Nuevo Usuario</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:16%;">Usuario</th>				
				<th style="width:16%;">Rol</th>
				<th style="width:16%;">Concesionario</th>
				<th style="width:16%;">Marca</th>
				<th style="width:16%;">Correo</th>
				<th style="width:5%;" class="transparente"></th>
				<th style="width:5%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($users as $user)
				<tr>
					
					
					
					<td>{{ $user->roles->first()['name'] }}</td>

					<td>
						@if (count($user->distributor) > 0)

							{{ $user->distributor->first()['name'] }}

						@endif
					</td>

					<td>

						@if (count($user->brand)>0)

							{{ $user->brand->first()['name'] }}							
						@endif						
					</td>

					<td>{{ $user->email }}</td>
					
					<td class="text-center">
						<a href="{{ url('root/usuario/nuevo', $user->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/usuario/eliminar', $user->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>					
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection