@extends('root')

@section('title')
	Leads | Usuarios
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >

			<h1>{{ (empty($user) ? 'Nuevo' : 'Editar') }} usuario</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/usuario/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($user) ? '' : $user->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Usuario</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($user) ? '' : $user->name) }}" 
						placeholder="Ej. Roberto Roa Ruiz">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Correo electrónico</label>
					
						<input type="text" class="form-control" name="email" value="{{ (empty($user) ? '' : $user->email) }}" placeholder="Ej. correo@mail.com">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Password</label>
					
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>

					<div class="form-group">

						<label class="color-blanco control-label">Rol</label>
						
						<select class="form-control" name="role" id="role">

							<option value=""> - Seleccione una opción -</option>
														 
							{{--*/ 
								$temp_rol = (!empty($user) ? $user->roles()->first() : '') 
							/*--}}

							@foreach ($roles as $rol)

								{{--*/ 
									$selected = ( !empty($user) && $temp_rol->id == $rol->id ? 'selected' : '') 
								/*--}}

								<option value="{{$rol->id}}" {{ $selected }}>{{$rol->name}}</option>

							@endforeach

						</select>
					</div>

					<div class="form-group" id="s_brand">
						
						<label class="color-blanco control-label">Marca</label>

						<select class="form-control" name="brand" id="brand">
						
							<option value=""> - Seleccione una opción -</option>

							{{--*/ $temp_brand = (!empty($user) ? $user->brand()->first() : '') /*--}}

							@foreach ($brands as $brand)

								@if (!empty($temp_brand))

									{{--*/ $selected = (!empty($user) && $temp_brand->id == $brand->id ? 'selected' : '') /*--}}

								@endif
							
								<option value="{{$brand->id}}" {{$selected}}>{{$brand->name}}</option>
							
							@endforeach
						
						</select>
					</div>

					<div class="form-group" id="s_distributor">
						
						<label class="color-blanco control-label">Concesionario</label>

						<select class="form-control" name="distributor" id="distributor">
						
							<option value=""> - Seleccione una opción -</option>

							{{--*/ $temp_distributor = (!empty($user) ? $user->distributor()->first() : '') /*--}}

							@foreach ($distributors as $distributor)

								@if (!empty($temp_distributor))

									{{--*/ 

										$selected = (!empty($user) && $temp_distributor->id == $distributor->id ? 'selected' : '') 

									/*--}}

								@endif							
							
								<option value="{{$distributor->id}}" {{$selected}}>{{$distributor->name}}</option>
							
							@endforeach
						
						</select>
					</div>

					<div class="form-group" id="s_brand_distributor">
						
						<label class="color-blanco control-label">Marca</label>

						<select class="form-control" name="brand_distributor" id="brand_distributor">
						
						</select>
					</div>					

					<div class="form-group" id="s_conglomerate">
						
						<label class="color-blanco control-label">Conglomerado</label>

						<select class="form-control" name="conglomerate" id="conglomerate">
						
							<option value=""> - Seleccione una opción -</option>

							{{--*/ $temp_conglomerate = (!empty($user) ? $user->conglomerate()->first() : '') /*--}}

							@foreach ($conglomerates as $conglomerate)
							
								@if (!empty($temp_conglomerate))

									{{--*/ 

										$selected = (!empty($user) && $temp_conglomerate->id == $conglomerate->id ? 'selected' : '') 

									/*--}}

								@endif

								<option value="{{$conglomerate->id}}" {{$selected}}>{{$conglomerate->name}}</option>
							
							@endforeach
						
						</select>
					</div>															
										
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	$("#s_brand").hide();
	$("#s_distributor").hide();
	$("#s_conglomerate").hide();
	$("#s_brand_distributor").hide();

    if ($('#role option').is(':selected')) {

		var option = 'value = ' + $("#role").val();		

		if ($("#role").val() != "") {

			var tag = $("#role option["+option+"]").text();

			lm_show_hide_section(tag);	
		}	    	
    }	

	function lm_show_hide_section(tag) {

		switch (tag.toLowerCase()){
			case 'marca':
					$("#s_brand").show();
					$("#distributor").val("");
					$("#conglomerate").val("");
					$("#brand_distributor").val("");
				break;

			case 'concesionario':
					$("#s_distributor").show();
					$("#brand").val("");
					$("#conglomerate").val("");
					$("#brand_distributor").val("");
					break;

			case 'asesor':
					$("#s_distributor").show();
					$("#brand").val("");
					$("#conglomerate").val("");
					$("#s_brand_distributor").show();
				break;

			case 'conglomerado':
					$("#s_conglomerate").show();
					$("#brand_distributor").val("");
					$("#distributor").val("");
					$("#brand").val("");					
				break;														
		}
	}	

	$("#role").change(function(){

		if ($("#role").val() != "") {

			$("#s_brand").hide();
			$("#s_distributor").hide();
			$("#s_conglomerate").hide();
			$("#s_brand_distributor").hide();

			var option = 'value = ' + $("#role").val();

			if ($("#role").val() != "") {

				var tag = $("#role option["+option+"]").text();
				lm_show_hide_section(tag);
			}

		} else {
			$("#s_brand").hide();
			$("#s_distributor").hide();
			$("#s_conglomerate").hide();
			$("#s_brand_distributor").hide();
		}
	});

	$("#s_distributor").change(function(){

		var option = 'value = ' + $("#role").val();		

		if ($("#role").val() != "") {

			var tag = $("#role option["+option+"]").text();

			if (tag.toLowerCase() == 'asesor') {

		        $.ajax({
		        	
		        	type:"POST",

		        	url: "{{url('root/usuario/concesionario/marca')}}",

		        	data: 'distributor='+$("#distributor").val()+'&_token={{ csrf_token() }}',

		        	success: function(data){

		        		$("#brand_distributor").html(data);		        		
				    }
		        });

			}
		}

	});
	
</script>
@endsection
