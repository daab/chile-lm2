@extends('root')

@section('title')
	Leads | Fases
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6" >

			<h1>{{ (empty($city) ? 'Nueva' : 'Editar') }} Fase</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/fases/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($phase) ? '' : $phase->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Fase</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($phase) ? '' : $phase->name) }}" 
						placeholder="Ej. ¿Contactado?">					
					</div>
				
					<div class="form-group">
					
						<label class="color-blanco control-label">Orden</label>
					
						<input type="text" class="form-control" name="sort" value="{{ (empty($phase) ? '' : $phase->sort) }}" 
						placeholder="Ej. 1">					
					</div>				
										
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>					
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	
</script>
@endsection
