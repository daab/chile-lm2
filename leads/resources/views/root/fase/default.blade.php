@extends('root')

@section('title')
	Leads | Fases
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-6">

		<a href="{{ url('/root/fases/nuevo') }}" class="btn btn-default daaboton">Nueva fase</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:16%;">Fase</th>
				<th style="width:5%;">Orden</th>
				<th style="width:3%;" class="transparente"></th>
				<th style="width:3%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($phases as $phase)
				<tr>
					
					<td>{{ $phase->name }}</td>
					<td>{{ $phase->sort }}</td>
					
					<td class="text-center">
						<a href="{{ url('root/fases/nuevo', $phase->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/fases/eliminar', $phase->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>			
				</tr>
				@endforeach				

			</tbody>

		</table>
			
	</div>
	
</div>
@endsection