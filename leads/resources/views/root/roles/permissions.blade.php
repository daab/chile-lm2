@extends('root')

@section('title')
	Leads | Roles
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-8">

		<h1>Asignación de permisos</h1>

		<p>			
			<h2>{{$role->name}}</h2>
			<p>{{$role->description}}</p>
		</p>
		
		<table>
			<thead>
				<th style="width:16%;">Módulo</th>
				<th style="width:16%;">Descripción</th>
				<th style="width:5%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($modules as $module)
				<tr>
					
					<td>{{ $module->name }}</td>
					
					<td>{{ $module->description }}</td>

					<td class="text-center">
						<input type="checkbox" name="allow_{{$module->id}}" id="allow_{{$module->id}}" onclick="lm_to_assign('{{$module->id}}', '{{$role->id}}');" {{ (isset($module->assigned) ? 'checked' : '') }}>
					</td>					
				</tr>
				@endforeach				

			</tbody>

		</table>
			
	</div>
</div>

<script type="text/javascript">
	
	function lm_to_assign(module, role) {

		var input = "#allow_" + module;

		var action = "unassign";

        if ($(input).is(':checked')) {
        
        	action = "toassing";
        }

        $.ajax({
        	
        	type:"POST",

        	url: "{{url('root/roles/permisos/save')}}",

        	data: 'action='+action+"&role="+role+"&module="+module+"&_token={{ csrf_token() }}",

        	success: function(data){

		    }
        });
	}

</script>
@endsection