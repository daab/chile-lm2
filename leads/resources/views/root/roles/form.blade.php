@extends('root')

@section('title')
	Leads | Roles
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6" >

			<h1>{{ (empty($role) ? 'Nuevo' : 'Editar') }} Rol</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/roles/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($role) ? '' : $role->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Módulo</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($role) ? '' : $role->name) }}" 
						placeholder="Ej. Concesionario">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Descripción</label>
						
						<textarea class="form-control" rows="5" name="description">{{ (empty($role) ? '' : $role->description) }}</textarea>
					
					</div>					
										
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	
</script>
@endsection
