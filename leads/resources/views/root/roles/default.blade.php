@extends('root')

@section('title')
	Leads | Roles
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-8">

		<a href="{{ url('/root/roles/nuevo') }}" class="btn btn-default daaboton">Nuevo Rol</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:16%;">Rol</th>
				<th style="width:16%;">Descripción</th>
				<th style="width:5%;" class="transparente"></th>
				<th style="width:5%;" class="transparente"></th>
				<th style="width:5%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($roles as $rol)
				<tr>
					
					<td>{{ $rol->name }}</td>
					
					<td>{{ $rol->description }}</td>

					<td class="text-center">
						<a href="{{ url('root/roles/nuevo', $rol->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/roles/eliminar', $rol->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>
					<td class="text-center">						
						<a href="{{ url('root/roles/permisos', $rol->id) }}" title="Asignar permisos">
							<i class="fa fa-random" aria-hidden="true"></i>
						</a>						
					</td>					
				</tr>
				@endforeach				

			</tbody>

		</table>
			
	</div>
	
</div>
@endsection