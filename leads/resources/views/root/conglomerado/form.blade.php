@extends('root')

@section('title')
	Leads | Conglomerado
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6" >
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('/root/conglomerado/guardar') }}">
					<input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" id="id" name="id" value="{{ $obj->id }}">


					<div class="form-group">
						<label class="color-blanco control-label">Conglomerado</label>
						<input type="text" class="form-control" name="nombre" value="{{ $obj->name }}">
					</div>

					<div class="form-group">
						<label class="color-blanco control-label">Distribuidor</label>
						<select name="distributor" id="distributor">
							<option value="-1">NO es distribuidor</option>
							@foreach($distribuidores as $distri)
								<option value="{{$distri->id}}" @if($distri->id == $obj->distributor_id) selected @endif  >{{ $distri->name }}</option>
							@endforeach
						</select>
					</div>



					<div class="form-group">
						<label class="color-blanco control-label">Marca</label>
						<select name="marca" id="marca">
							<option>Seleccione...</option>
							@foreach ($marcas as $marca)
								<option value="{{$marca->id}}" @if($marca->id==$obj->brand_id)selected="selected" @endif >{{$marca->name}}</option>
							@endforeach
						</select>
					</div>

					
					<br /><br /><br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>


		@if(count($concesionarios) > 0)
			<div class="col-md-6">

				<h3>Concesionarios</h3>
				<?php $i = 0 ?>
				
				@foreach($concesionarios as $con)

					<?php 
						$checked = "";
						if (in_array($con->id, $cons)) {
							$checked = "checked";
						}
					?>
					<input type="checkbox" id="con{{$i}}" {{$checked}} onChange="update({{$con->id}}, {{$i}})" /> {{$con->name}}<br />
					<?php $i++ ?>
				@endforeach

			</div>
			<br /><br /><br />
		@endif
	</div>
</div>
@endsection



@section('scripts')

	<script type="text/javascript">

		function update(id, con){

			var guardar = 0;
			var idCon = "con"+con;
			
			if ($('#'+idCon).is(':checked')) { 
				guardar = 1;
			}

			//AJAX
			$.ajax({
				type: "POST",
				data: "_token="+$('#token').val()+"&idConglomerado="+$('#id').val()+"&idConcesionario="+id+"&idMarca="+$('#marca').val()+"&guardar="+guardar,
				url: "{{url('/root/conglomerado/update')}}",
				success: function(datos){
					console.log("Exito dude...");
				}
			});
		}

	</script>

@endsection


