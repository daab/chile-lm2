@extends('root')

@section('title')
	Leads | Conglomerado
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-6">

		<a href="{{ url('root/conglomerado/nuevo/-1') }}" class="btn btn-default daaboton">Nuevo Conglomerado</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width: 10%">Id</th>
				<th style="width: 25%">Nombre</th>
				<th style="width: 25%">Marca</th>
				<th style="width: 15%">Distribuidor</th>
				<th class="transparente" style="width: 25%"></th>
			</thead>
			
			<tbody>
				@foreach($objs as $obj)
				<tr>
					<td>{{ $obj->id }}</td>
					<td>{{ $obj->name }}</td>
					<td>{{ $obj->marca->name }}</td>
					<td class="text-center"><a href="{{ url('root/conglomerado/nuevo', $obj->id) }}">Editar</a> | <a href="{{ url('root/conglomerado/eliminar', $obj->id) }}">Eliminar</a></td>
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection