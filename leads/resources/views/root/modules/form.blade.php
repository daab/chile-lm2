@extends('root')

@section('title')
	Leads | Módulos
@endsection

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
	
		
		<div class="col-md-6 " >

			<h1>{{ (empty($user) ? 'Nuevo' : 'Editar') }} módulo</h1>
			
				@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif

				<form class="form-vertical" role="form" method="POST" action="{{ url('root/modulo/guardar') }}">

					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<input type="hidden" name="id" value="{{ (empty($module) ? '' : $module->id) }}">

					<div class="form-group">
					
						<label class="color-blanco control-label">Módulo</label>
					
						<input type="text" class="form-control" name="name" value="{{ (empty($module) ? '' : $module->name) }}" 
						placeholder="Ej. Usuarios y roles">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Url</label>
					
						<input type="text" class="form-control" name="route" value="{{ (empty($module) ? '' : $module->route) }}" placeholder="Ej. /root/gerente">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Icono</label>
					
						<input type="text" class="form-control" name="icono" value="{{ (empty($module) ? '' : $module->icon) }}" placeholder="Ej. fa fa-database">
					
					</div>

					<div class="form-group">
					
						<label class="color-blanco control-label">Descripción</label>
						
						<textarea class="form-control" rows="5" name="description">{{ (empty($module) ? '' : $module->description) }}</textarea>
					
					</div>					
										
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Guardar</button>
						
					</div>
				</form>
		
			
		</div>
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">

	
</script>
@endsection
