@extends('root')

@section('title')
	Leads | Módulos
@endsection

@section('content')
<div class="row figux-container">
	
	<div class="col-md-10">

		<a href="{{ url('/root/modulo/nuevo') }}" class="btn btn-default daaboton">Nuevo Módulo</a>
		<br /><br /><br />
		
		<table>
			<thead>
				<th style="width:16%;">Módulo</th>
				<th style="width:16%;">Descripción</th>
				<th style="width:16%;">URL</th>
				<th style="width:8%;">Icono</th>
				<th style="width:5%;" class="transparente"></th>
				<th style="width:5%;" class="transparente"></th>
			</thead>
			
			<tbody>
				@foreach($modules as $module)
				<tr>
					
					<td>{{ $module->name }}</td>
					
					<td>{{ $module->description }}</td>

					<td>{{ $module->route }}</td>

					<td class="text-center">
						<i class="{{$module->icon}}" aria-hidden="true"></i>
						<br/>
						{{$module->icon}}
					</td>
					
					<td class="text-center">
						<a href="{{ url('root/modulo/nuevo', $module->id) }}" title="Editar">
							<i class="fa fa-edit" aria-hidden="true"></i>
						</a>
					</td>

					<td class="text-center">
						<a href="{{ url('root/modulo/eliminar', $module->id) }}" onclick="return confirm('Seguro desea borrar este registro?');" title="Eliminar">
							<i class="fa fa-trash" aria-hidden="true"></i>
						</a>
					</td>					
				</tr>
				@endforeach
				
				

			</tbody>

		</table>			
			
			
	</div>
	
</div>
@endsection