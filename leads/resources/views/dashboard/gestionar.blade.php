@extends('template')

@section('content')

<div class="ml-container">
	<div class="lead-box">
		<div class="lead-section">
				<div class="lead-sub-section"><span><i style="color: {{$lead->estados->color}}" class="fa fa-circle" aria-hidden="true"></i> </span>{{$lead->estados->nombre}}</div>
				<div class="lead-sub-section lead-fecha"><h3>Asesor: 
					@if(is_object($lead->user))
						{{$lead->user->nombres}} {{$lead->user->apellidos}} 
					@endif
					/ {{$lead->created_at}}<h3></div>
				<div class="clearfix"></div>
			</div>
		<div class="lead-head">
			<div class="lead-name">
				<p>{{$lead->nombres}} {{$lead->apellidos}}</p>
				<div class="lead-stars">
					<div class="starbox"></div>
					<div class="starbox-hover"></div>
					<div class="lead-star star-0"><img src="{{ asset('/public/images/star.svg') }}"></div>
					<div class="lead-star star-1"><img src="{{ asset('/public/images/star.svg') }}"></div>
					<div class="lead-star star-2"><img src="{{ asset('/public/images/star.svg') }}"></div>

					<div class="clearfix"></div>
				</div>
			</div>
			<div class="lead-sub-head">Lead de: {{$lead->origen}} 
				@if($lead->link != "")
					<a href="{{$lead->link}}" target="_blank">Visitar página</a>
				@endif
			</div>
			<div class="lead-sub-head">{{$lead->field1}}</div>
			<div class="lead-sub-head">{{$lead->field2}}</div>
			<div class="clearfix"></div>
		</div>
		<form name="anotaciones" id="formActualizar" action="{{ url('/actualizar') }}" method="post">

			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			<input type="hidden" name="id" id="id" value="{{ $lead->id }}">

			<div class="lead-section">
				<div class="lead-sub-section"><h3>Modelo de interés:</h3>
					<select onChange="mostrarAdjuntos()" name="modelo" id="modelo">
						@foreach ($modelos as $modelo)
							<option value="{{$modelo->nombre}}" @if($modelo->nombre==$lead->modelo)selected @endif >{{$modelo->nombre}}</option>
						@endforeach
					</select>
				</div>
				<div class="lead-sub-section"><h3>Teléfono:</h3>{{$lead->telefono}}</div>
				<div class="lead-sub-section"><h3>Correo:</h3>{{$lead->correo}}</div>

				<div class="clearfix"></div>
			</div>
			<div class="lead-section">
				<div class="lead-sub-section"><h3>Estado:</h3>
					<input type="hidden" id="estadoTemp" />

					<select name="state_today" id="state_today">
						<option value=""></option>
						@foreach($fase->estados as $f)
							<option value="{{$f->id}}" @if($lead->estados->id == $f->id) selected @endif >{{$f->nombre}}</option>
						@endforeach
					</select>

<!-- 					<select name="estado" id="estado" onChange="guardarEstado('estado', 1)">
						<option @if($lead->estado=='Sin Gestionar')selected @endif value="Sin Gestionar">Sin Gestionar</option>
						<option @if($lead->estado=='Interesado')selected @endif value="Interesado">Interesado</option>
						<option @if($lead->estado=='No Interesado')selected @endif value="No Interesado">No Interesado</option>
						<option @if($lead->estado=='No contesto')selected @endif value="No contesto">No contesto</option>
						<option @if($lead->estado=='Vendido')selected @endif value="Vendido">Vendido</option>
						<option @if($lead->estado=='Datos errados')selected @endif value="Datos errados">Datos errados</option>
						<option @if($lead->estado=='Lead Duplicado')selected @endif value="Lead Duplicado">Lead Duplicado</option>
						<option @if($lead->estado=='Cerrado')selected @endif value="Cerrado">Cerrado</option>
					</select> -->
				</div>

				<div class="lead-sub-section"><h3>Justificación del estado:</h3>

					<div id="section_js">
						@if(count($lead->motivo) > 0)
							{{$lead->motivo->nombre}}
						@else
							{{'Sin Gestionar'}}	
						@endif
					</div>

				<!-- <div class="lead-sub-section"><h3>Sub-estado:</h3> -->
					<div  id="divInteresado" class="validar" style="display: none">
						<select name="estadoInteresado" id="estadoInteresado" onChange="guardarEstado('estadoInteresado', 1)">
							<option value="1">Seleccione...</option>
							<option @if($lead->estado2=='Agendo Cita')selected @endif value="Agendo Cita">Agendo Cita</option>
							<option @if($lead->estado2=='Test Drive')selected @endif value="Test Drive">Test Drive</option>
							<option @if($lead->estado2=='Prospecto futuro')selected @endif value="Prospecto futuro">Prospecto futuro</option>
						</select>
					</div>

					<div  id="divNoInteresado" class="validar" style="display: none">
						<select name="estadoNoInteresado" id="estadoNoInteresado" onChange="guardarEstado('estadoNoInteresado', 1)">
							<option value="1">Seleccione...</option>
							<option @if($lead->estado2=='Ya compró')selected @endif value="Ya compró">Ya compró</option>
							<option @if($lead->estado2=='Fuera de presupuesto')selected @endif value="Fuera de presupuesto">Fuera de presupuesto</option>
							<option @if($lead->estado2=='Ingreso por curiosidad')selected @endif value="Ingreso por curiosidad">Ingreso por curiosidad</option>
						</select>
					</div>

					<div id="divNoContesto" class="validar" style="display: none">
						<select name="estadoNoContesto" id="estadoNoContesto" onChange="guardarEstado('estadoNoContesto', 1)">
							<option value="1">Seleccione...</option>
							<option @if($lead->estado2=='Contacto por whatsapp')selected @endif value="Contacto por whatsapp">Contacto por whatsapp</option>
							<option @if($lead->estado2=='Segunda llamada')selected @endif value="Segunda llamada">Segunda llamada</option>
							<option @if($lead->estado2=='Contacto por correo')selected @endif value="Contacto por correo">Contacto por correo</option>
						</select>
					</div>

					<div  id="divVendido" style="display: none">
						<p style="margin-top: 5px;">Comisión: </p>
						<input value="{{ $lead->comision }}" type="text"  name="comision" id="comision" /><br />
					</div>
				</div>

				<div class="lead-sub-section" id="divRecordatorio" style="display: none">
					<h3>Recordatorio:</h3>
					<input type="text" name="agenda" id="agenda" class="datepicker" />
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="lead-section">
				<button type="button" onClick="verificarEstado()" class="lead-bttn">
					<div class="bttn-effect"></div>
					Guardar cambios
				</button>

				<div class="clearfix"></div>
			</div>
			
			@if($lead->user_id > 0)
			<div id="divFicha" style="diplay: none">
				<div class="lead-box">
					<div class="lead-head">
							<div class="lead-sub-head">
							Envío ficha técnica ( {{ $lead->mailing }} )
							</div>
							<div class="clearfix"></div>
					</div>
					<div class="lead-section lm-time-line">
						
						<div class="lead-sub-section">
							@if($lead->marca == 'Volkswagen PKW' || $lead->marca == 'Volkswagen LCV')
								@include('emails.mensaje.volkswagen')
							@elseif($lead->marca == 'Skoda' || $lead->marca == 'Seat')
								@include('emails.mensaje.euromotors')
							@endif
							<button type="button" class="lead-bttn" onClick="enviarFicha()">
							<div class="bttn-effect"></div>Enviar
						</button>
						<div class="lead-bttn user-img" data-toggle="modal" data-target="#myModal">
							<div class="bttn-effect"></div>
							<img src="{{$lead->user->image}}" />
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
			@endif

			<input name="calificacion" id="calificacion" type="hidden" />
			<input name="estado2" id="estado2" type="hidden" />

		</form>
		<div class="lead-section lm-time-line">
			<div class="lead-sub-section">
				<input type="text" name="anotacion" id="anotacion" />
			</div>
			
			<button onClick="guardarAnotacion()" type="button" class="lead-bttn">
				<div class="bttn-effect"></div>Anotación
			</button>
			
			<div class="clearfix"></div>

			<div id="anotaciones">
				@foreach($anotaciones as $anotacion)
					<div>
						<small>{{$anotacion->created_at}}</small>
						<p>{{$anotacion->anotacion}}</p>
					</div>
				@endforeach
			</div>
		</div>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Imagen de perfil</h4>
      </div>
      <form name="cambiarImagenPerfil" id="cambiarImagenPerfil" action="{{ url('/imagenperfil') }}" method="post" enctype="multipart/form-data">
      <div class="modal-body">
      
      	

			<input type="hidden" name="_token" id="token2" value="{{ csrf_token() }}">
			<input type="hidden" name="id_user" id="id_user2" value="{{$lead->user_id }}">
			<input type="file" name="img" id="img" />


		

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="submit" class="btn btn-primary">Cambiar</button>
      </div>
      </form>
    </div>
  </div>
</div>



@endsection

@section('scripts')
<script type="text/javascript">

	var leadId = {{$lead->id}};
	var modelos = {!! $modelos !!};
	var mensaje = $('#mensaje').val();

	guardarEstado('estado', 0);

	$( function() {
			$( ".datepicker" ).datepicker(
				{ dateFormat: 'yy-mm-dd' }
			);
		});

	function verificarEstado() {

		if ($("#state_today").val() == "") {
			
			swal("Error", "Debe seleccionar un estado", "error");

		} else if ($("#reason").val() == "") {

			swal("Error", "Debe seleccionar la justificación del estado", "error");

		} else {

			document.getElementById('formActualizar').submit();
		}


		
		

		// var elemnt = "";
		// var validar = true;

		// $('body').addClass("loading");
		
		// $(".validar").each(function() {
  //   		if ( $(this).css('display') != 'none' ){
  //   			elemnt = $(this).attr('id').replace("div", "estado"); 
  //   			//alert($(this).attr('id') + " " + $(this).css('display')+ " " +$('#'+elemnt).val());
  //   			if ($('#'+elemnt).val() == 1 || $('#'+elemnt).val() == undefined) {
  //   				swal("Error", "Debe seleccionar un subestado", "error");
  //   				validar = false;
  //   			}
  //   		}
		// });

		// if ($('#divVendido').css('display') != 'none' && $('#comision').val() == "" ) {
		// 	swal("Error", "Debe escribir una comisión", "error");
		// 	validar = false;
		// }

		// var comision = $('#comision').val();
		// if ($('#divVendido').css('display') != 'none' && comision.length != 6 ) {
		// 	swal("Error", "La comisión debe tener 6 digitos", "error");
		// 	validar = false;
		// }

		// if (validar) {
		// 	document.getElementById('formActualizar').submit();
		// }else{
		// 	$('body').removeClass("loading");
		// }

		
	}
	
	
	function guardarEstado(estado, bandera){

		if (estado == 'estado') {

			switch($('#estado').val()){
				case 'Interesado':
					$('#divInteresado').show();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
				case 'No Interesado':
					$('#divInteresado').hide();
					$('#divNoInteresado').show();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
				case 'No contesto':
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').show();
					$('#divVendido').hide();
				break;
				case 'Vendido':
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').show();
				break;
				default:
					$('#divInteresado').hide();
					$('#divNoInteresado').hide();
					$('#divNoContesto').hide();
					$('#divVendido').hide();
				break;
			}

			$('#estado2').val("");

		}

		var estadosUnicos = ['Datos errados', 'Lead Duplicado', 'Cerrado'];
		var validarEstado = jQuery.inArray($('#'+estado).val(), estadosUnicos);

		var segundoEstado = ['estadoNoInteresado','estadoInteresado','estadoNoContesto', 'Vendido'];
		var validarEstado2 = jQuery.inArray(estado, segundoEstado);
		
		if (bandera == 1) {

			if( validarEstado >= 0  || validarEstado2 >= 0 ){
				$('#estadoTemp').val(estado);
				
				var estado2 = "";

				switch($('#estadoTemp').val()){
					case 'estadoNoInteresado':
						estado2 = $('#estadoNoInteresado').val();
					break; 
					case 'estadoInteresado':
						estado2 = $('#estadoInteresado').val();
					break; 
					case 'estadoNoContesto':
						estado2 = $('#estadoNoContesto').val();
					break; 
					case 'Vendido':
						estado2 = '';
					break;
				}

				if(estado2 == '1' || validarEstado > 0  ){
					estado2 = '';
				}

				$('#estado2').val(estado2);

				if(estado2 == 'Prospecto futuro'){
					$('#divRecordatorio').show();
				}else{
					$('#divRecordatorio').hide();
					$('#agenda').val("");
				}

			}
		}
	}

	function guardarAnotacion(){
		//AJAX
		$.ajax({
			type: "POST",
			data: "_token="+$('#token').val()+"&anotacion="+$('#anotacion').val()+"&id="+leadId+"&modelo="+$('#modelo').val(),
			url: "{{url('/anotacion')}}",
			success: function(datos){
				$('#anotacion').val('');
				swal("Exito!", "Cambio realizado", "success");
				$('#anotaciones').html(datos);
			}
		});
	}



	function mostrarAdjuntos(){

		for (var i = 0; i <= modelos.length - 1; i++) {
			model = modelos[i];
			if (model.nombre == $('#modelo').val()) {
				if (model.doc != null) {

					var res = mensaje.replace("@modelo", model.nombre);
					$('#mensaje').val(res);
					$('#divFicha').show();
				}else{
					$('#divFicha').hide();
				}
			}
		}

	}

	function enviarFicha(){

		var str = $('#mensaje').val();
		str = str.replace(/(?:\r\n|\r|\n)/g, '<br />');


		//AJAX
		$.ajax({
			type: "POST",
			data: "_token="+$('#token').val()+"&mensaje="+str+"&idLead="+leadId,
			url: "{{url('/fichatecnica')}}",
			success: function(datos){
				
				swal("Exito!", "Cambio realizado", "success");
				
			}
		});
	}


	$(document).ready(function(){
		var calificacion = {{$lead->calificacion}} - 1;
		$(".starbox").addClass("active-star-"+calificacion);
		
		mostrarAdjuntos();

		$(".datepicker").datepicker(
			{ dateFormat: 'yy-mm-dd' }
		);

		for (var i = 0; i < 3; i++) {
			$(".star-"+i).data("foo", i)
			
			$(".star-"+i).hover( function() {
		    	var ww = $(this).data("foo");
			   $(".starbox-hover").addClass("hover-star-"+ww);
			 }, function() {
			 	var ww = $(this).data("foo");
			   $(".starbox-hover").removeClass("hover-star-"+ww);
			 }
			);
			

			$(".star-"+i).click(function(){
				var ww = $(this).data("foo");
				$(".starbox").removeClass("active-star-0 active-star-1 active-star-2")
				$(".starbox").addClass("active-star-"+ww)

				$('#calificacion').val( ww + 1);
			})
		}

	});



	$("#state_today").change(function(){

		url = '{{ url("/configuracion/reason", "param") }}';
		url = url.replace("param", $("#state_today").val());

		$.ajax({
		  	url: url,
		  	type: "GET",
		  	data: "type=select",
			success:  function (response) {

                $("#section_js").html(response);
            }
		});		
	});

</script>
@endsection