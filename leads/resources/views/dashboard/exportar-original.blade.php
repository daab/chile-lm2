@extends('template')

@section('content')

<div class="container-fluid figux-container">
	
	<div class="bg-blanco" style="width: 90%; padding: 20px">
		


		<div class="row">

			<div class="col-md-4">

 
				<h1 class="color-rojo">Exportar</h1>

			</div>


			<div class="col-md-7">
				
				<form name="formEstado" id="formEstado" method="post" action="{{URL::to('/exportar')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					
					<input value="{{$type}}" type="hidden" name="type" id="type">
					

					<div class="form-group">
		            	<label>Fecha inicio</label>
		            	<div class="col-md-6">    
		                    <input type='text' name="finicio" class="date form-control" />
		                </div>
		            </div>

		            <div class="form-group">
		                <label>Fecha Fin</label>
		            	<div class="col-md-6">    
		                	<input type='text' name='ffin' class="date form-control" />
		                </div>
		            </div>


		            <button type="submit">Exportar</button>
				</form>
				
			</div>

		</div>

	</div>
</div>

@stop

@section('scripts')
	<script type="text/javascript">
	    $(document).ready(function () {
    

	    	$('.date').datepicker({
	        	
	        		dateFormat: 'yy-mm-dd'
	        });
   		});
	</script>
@stop

