@extends('template')

@section('content')

<div class="container-fluid figux-container">
	
	<div class="bg-blanco" style="width: 90%; padding: 20px">
		


		<div class="row">

			<div class="col-md-3">

 
				<h1 class="color-rojo">Estados</h1>

			</div>


			<div class="col-md-9">
				<form name="formEstado" id="formEstado" method="post" action="{{URL::to('/estados')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input value="{{$titulo}}" type="hidden" name="type" id="type">
					@if(count($objs) > 0)
					<div class="col-md-4">
						<label>{{$titulo}}</label>
						<select  class="form-control" name="concesionario" id="concesionario">
							<option value="">Seleccione para filtrar</option>
							@foreach($objs as $obj) 
								<option value="{{$obj->nombre}}" @if($obj->nombre==$concesionario) selected @endif>{{$obj->nombre}}</option>
							@endforeach
						</select>
					</div>
					@endif
					<div class="col-md-3">
						<label>Fecha de inicio</label>
						<input value="{{$finicio}}" name="finicio" id="finicio" class="datepicker" />
					</div>
					<div class="col-md-3">
						<label>Fecha de fin</label>
						<input value="{{$ffin}}" name="ffin" id="ffin" class="datepicker" />
					</div>
					<div class="col-md-2">
						<br />
						<button><i class="fa fa-search"></i> Buscar</button>
					</div>

				</form>


			</div>

		</div>


		<div class="row">

			<div class="col-md-12">
				<?php $totalLeads = 0; ?>
				
				<table>
					<tr>
						@foreach($leads as $lead)
							<?php $totalLeads += $lead->num; ?>
							<td style="text-align: center; border-right: 1px solid #9ACFE1; width: 25%">
								<div class="col-md-12">
									<h3 class="est">{{$lead->num}}</h3>
									<p>{{$lead->estado}}</p>
								</div>

								@if($lead->estado == 'Interesado' || $lead->estado == 'No Interesado' || $lead->estado == 'No contesto')
									<div class="col-md-12">
										@foreach ($leads2 as $lead2)
												@if ($lead2->estado2 != "" && $dependencias[trim($lead2->estado2)] == $lead->estado)
													<h4>{{$lead2->num}} <small>{{$lead2->estado2}}</small></h4>
												@endif
											@endforeach
									</div>
								@endif
							</td>
						@endforeach
					</tr>
				</table>
				
				<h2>Total Leads: <?php echo $totalLeads; ?></h2>

			</div>

		</div>
		
		



	</div>
	
	
	@if(count($objs) > 0)
	
	<div class="bg-blanco" style="width: 90%; padding: 20px;margin-top: 50px">

		<div class="row">
			<div class="col-md-3">
				<h1 class="color-rojo">Revisados</h1>
			</div>
		</div>
		
		<div class="row">

			<div class="col-md-12">
				
				<?php $totalLeads = 0; ?>
				<table>
					<tr>

						@foreach($sinRevisar as $stat)
						<?php $totalLeads += $stat->num; ?>
							<td style="text-align: center; border-right: 1px solid #9ACFE1; width: 25%">
								<div class="col-md-6">
									<h3 class="est">{{$stat->num}}</h3>
									@if($stat->revisado == 1)
										<p>Revisados</p>
									@elseif($stat->revisado == -1)
										<p>Mas de dos días sin gestión</p>
									@else
										<p>Sin Revisar</p>
									@endif 
								</div>
							</td>
						@endforeach
					</tr>
				</table>
				
				<h2>Total Leads: <?php echo $totalLeads; ?></h2>


			</div>

		</div>
		
</div>
@endif

@if(count($modelos) > 0)
	
	<div class="bg-blanco" style="width: 90%; padding: 20px;margin-top: 50px; margin-bottom: 50px">

		<div class="row">
			<div class="col-md-3">
				<h1 class="color-rojo">Modelos</h1>
			</div>
		</div>
		
		<div class="row">

			@foreach($modelos as $modelo)
				<div class="col-md-2">			
					<h3 class="est">{{$modelo->num}}</h3>
					<p>{{$modelo->modelo}}</p>
				</div>			
			@endforeach

		</div>
		
</div>
@endif

@if(Auth::user()->rol == 2)

<div class="bg-blanco" style="width: 90%; padding: 20px;margin-top: 50px">
		
		<div class="row">

			@if(count($graficaConcesionario) > 0)
				<div class="col-md-6">
			@else
				<div class="col-md-12">
			@endif
				

			<h3>Leads vs Origen</h3>
			<div id="chart"></div>
		</div>

			@if(count($graficaConcesionario) > 0)
			<div class="col-md-6">
				<table>
					<thead>
						<tr>
							<th>Concesionario</th>
							<th>Leads</th>
						</tr>
					</thead>
					<tbody>

						@foreach($graficaConcesionario as $data)

							<tr>
								<td>{{$data['concesionario']}}</td>
								<td>{{$data['num']}}</td>
							</tr>


						@endforeach

					</tbody>

				</table>
			</div>
			@endif
		</div>

	</div>
</div>

@endif
@stop


@section('scripts')
	<script type="text/javascript">

		$( function() {
			$( ".datepicker" ).datepicker(
				{ dateFormat: 'yy-mm-dd' }
			);
		});

		function enviar(){
			document.getElementById('formEstado').submit();
		}

		
		tratarDatos();

		
		function tratarDatos(){

			var origen = {!!$graficaOrigen!!};
			dibujarGrafica(origen, "chart");
		}


		function dibujarGrafica(datos, div){
			var obj = {};
			var datos1 = [];
			var datos2 = [];

			for (var i = 0; i < datos.length; i++) {
				obj = datos[i];
				var aux = [];
				aux[0] = obj.nombre;
				aux[1] = obj.num;
				datos1[i] = aux;
			}

			var chart = c3.generate({
				bindto: '#'+div,
			    data: {
			        columns: datos1,
			        type: 'bar'
			    },
			    bar: {
			        width: {
			            ratio: 0.5 // this makes bar width 50% of length between ticks
			        }
			    }
			        
			});
		}


		

	</script>




@stop
