@extends('template')

@section('content')

<div class="container-fluid figux-container">
	
	<div class="bg-blanco" style="width: 90%; padding: 20px">
		
		<div class="row">

			<div class="col-md-4">
				<h1 class="color-rojo">Exportar</h1>
			</div>


			<div class="col-md-7">
				
				<form name="formEstado" id="formEstado" method="post" onSubmit="obtenerLink(); return false;">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input value="{{$type}}" type="hidden" name="type" id="type">
					
					<div class="form-group">
		            	<label>Fecha inicio</label>
		            	<div class="col-md-6">    
		                    <input type='text' name="finicio" class="date form-control" />
		                </div>
		            </div>

		            <div class="form-group">
		                <label>Fecha Fin</label>
		            	<div class="col-md-6">    
		                	<input type='text' name='ffin' class="date form-control" />
		                </div>
		            </div>


		            <button type="submit">Exportar</button>
				</form>
				
			</div>

		</div>
	</div>
</div>


<div class="container-fluid figux-container" id="divDescarga" style="display:none">
	
	<div class="bg-blanco" style="width: 90%; padding: 20px;">

		<div class="row">
			<div class="col-md-10">
				<h1 class="color-rojo"><a id="linkDescarga">Click Aquí para descargar el archivo</a></h1>
			</div>
		</div>

	</div>
</div>

@stop

@section('scripts')
	<script type="text/javascript">
	    $(document).ready(function () {
    		$('.date').datepicker({
	        	dateFormat: 'yy-mm-dd'
	        });
   		});


   		function obtenerLink(){
   			param = $('#formEstado').serialize();
			
			$.ajax ({
				type: "POST",
				data: param,
				url: '{{URL::to('/exportar')}}',
				success: function(datos){
					var url = "http://<?php echo $_SERVER['SERVER_NAME'] ?>/leads/public/reportes/"+datos;
					$("#linkDescarga").attr("href", url);
					$('#divDescarga').show();
				}
			});		
				
			return false;
   		}
	</script>
@stop

