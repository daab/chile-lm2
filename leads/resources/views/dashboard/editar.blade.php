@extends('template')

@section('content')
<div class="container-fluid figux-container">
	<div class="row">
		<div class="col-md-11 bg-blanco pt-15">

			<div class="row pb-15">
				<div class="col-md-6"> 
					{!!$lead->estado!!}
				</div>

				<div class="lead-sub-section lead-fecha"><h3>Asesor: 
					@if(is_object($lead->user))
						{{$lead->user->nombres}} {{$lead->user->apellidos}}  
					@endif
					/ {{$lead->created_at}}<h3></div>
				<div class="clearfix"></div>
			</div>

		</div>
	</div>


	<div class="row">
		<div class="col-md-11 bg-blanco mt-30">
			<div class="row">
				<div class="col-md-6">
					<h1>{{$lead->nombres}} {{$lead->apellidos}}</h1>
					<small>Lead de: <strong>{{$lead->origen}}</strong></small>

					<div class="row">
						<div class="col-md-6">
							<p>Modelo de interes</p>
							<h3>{{$lead->modelo}}</h3>
						</div>
						<div class="col-md-6">
							<p>Teléfono</p>
							<h3>{{$lead->telefono}}</h3>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<p>Estado</p>
							<h3>{!!$lead->estado!!}</h3>
							
						</div>
						<div class="col-md-6">
							<p>Correo</p>
							<h3>{{$lead->correo}}</h3>
						</div>
					</div>
					<br />
				</div>

				<div class="col-md-6 pt-15">
					<div class="row">
						<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
						<div class="col-md-7">
							<select name="concesionario" id="concesionario" class="form-control">
								@foreach ($concesionarios as $conc)
									<option value="{{$conc->nombre}}" @if($conc->nombre==$lead->concesionario) selected @endif>{{$conc->nombre}}</option>
								@endforeach
							</select>
						</div>
						<button class="btn btn-default daaboton col-md-4" onClick="guardarConcesionario()">Cambiar</button>
					</div>

					<div class="row mt-30">
						<div class="col-md-11" style="padding: 20px;background: #EEEEEE;margin-bottom: 20px" id="anotaciones">
							@foreach($anotaciones as $anotacion)
								<div>
									<small>{{$anotacion->created_at}}</small>
									<p>{{$anotacion->anotacion}}</p>
								</div>
							@endforeach
						</div>
						<br /><br />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<br />

<div class="row">
	<div class="col-md-11">
		<div class="pull-right">
			<a href="{{url('/')}}" style="width: 250px" class="btn btn-default daaboton col-md-4">Volver</a>
		</div>
	</div>
</div>


@endsection

@section('scripts')
<script type="text/javascript">

	var leadId = {{$lead->id}};
	
	function guardarConcesionario(){
		swal({
			  title: "¿Esta seguro?",
			  text: "Va a realizar un cambio de concesionario",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Si, Estoy seguro",
			  closeOnConfirm: false
			},
			function(){

				
				
				//AJAX
				$.ajax({
					type: "POST",
					data: "_token="+$('#token').val()+"&concesionario="+$('#concesionario').val()+"&id="+leadId,
					url: "{{url('/cambioconcesionario')}}",
					success: function(datos){
						
						swal("Exito!", "Cambio realizado", "success");
						$('#anotaciones').html(datos);
					}
				});



			  
			});
	}



</script>
@endsection