@extends('template')

@section('content')

<div class="wrapper bg">
	<div class="loglm_logo">
		
	</div>
	<div class="loglm_logbox">
		<h1>Lead Manager 2.0</h1>
		<span>2.2.rev - Porsche Chile</span>
		<br><br>
		<form class="form-vertical" role="form" method="POST" action="{{ url('/auth/login') }}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">

					<div class="form-group">
						
						<input type="email" class="form-control daabinput" name="email" value="{{ old('email') }}">
						<p>Correo</p><br>
					</div>

					<div class="form-group">
						
						<input type="password" class="form-control daabinput" name="password">
						<p>Contraseña</p><br>
					</div>

					<!--<div class="form-group">
						
						<div class="pull-right">
							<label>
								<a class="daab-a" href="{{ url('/password/email') }}">Recuperar contraseña</a>
							</label>
						</div>
						
					</div>-->
					<br />
					<div class="form-group">
						
						<button type="submit" class="btn btn-primary daaboton">Inicio de sesión</button>
						
					</div>
		</form>
		@if (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif
	</div>
	
</div>
@endsection
