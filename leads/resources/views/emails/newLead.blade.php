<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<title>Lead Manager</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width" />



<!--[if gte mso 9]>
<style>
#outlookFix {
  width:602px;
}
</style>
<![endif]-->

<link rel="shortcut icon" type="image/png" href="images/favicons.png"/>
</head>

<body style="margin:0; background-color:#FFFFFF; width:100%">
<table style="background-color:#FFFFFF;padding:0px;width:100%">
<tr><td style="width:100%">
<center>

<table border="0" cellspacing="0" cellpadding="0" id="outlookFix" style="max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF">


<tr><td style="border: 1px solid #EEEEEE">
<table class="contentTable" width="600" cellpadding="0" cellspacing="0" border="0" style="max-width:600px !important; background-color:#9dd1e3">

	<tr><td style="background-color:#FFFFFF; color:#e95550; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height: 20px; text-align:left; padding:12px 30px 12px 30px">LEAD MANAGER</td></tr>

<tr><td style="padding:20px 0px 20px 0px; line-height:0px; max-width:600px; background-color:#9dd1e3; text-align:center;">
<table class="contentTable2" width="540" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important;">
	<tr><td style="font-family:Arial, Helvetica, sans-serif; font-size:17px; line-height:20px; text-align:left; color:#666666;">

	Hola {{ $data['nombre']}},<br><br>

	Has recibido una nueva cotización, por favor gestiona lo antes posible.

	</td></tr>
	<tr><td style="padding:20px 0px 20px 0px; line-height:0px; max-width:600px; background-color:#9dd1e3;">
		<table class="bttn" width="200" cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:300px !important;">
			<tr><td style="padding:8px 20px 8px 10px; max-width:600px; background-color:#ea5650; ">
				<a href="{{ $data['link'] }}" style="text-decoration:none; font-family:Arial, Helvetica, sans-serif; font-size:17px; line-height:20px; text-align:center; color:#FFFFFF; font-weight: bold">Gestionar cotización</a>
			</td></tr>
		</table>
	</td></tr>

</table>
</td></tr>


<tr><td style="padding:10px 20px 10px 20px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:13px; text-align:left; color:#888888; max-width:600px; background-color:#F9f9f9;">

<table class="legal" cellpadding="0" cellspacing="0" border="0" align="right" width="300">
<tr><td style="padding:5px 0px 5px 7px; font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:18px; text-align:left; color:#cccccc; max-width:600px; text-align:right">&#169; 2018 PORSCHE CHILE. Todos los derechos reservados </td></tr>
</table>

</td></tr>

</table>
</td></tr>

</table>

</center>
</td></tr>
</table>
</body>
</html>