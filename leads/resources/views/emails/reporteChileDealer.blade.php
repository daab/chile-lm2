

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
   <head>
      <title>Audi</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width" />
      <style type="text/css">
         @media only screen and (min-width: 600px) {	
         table[class=bodytable] {width:600px !important;}
         table[class=contentSection] {width:520px !important;}
         table[class=boxleft] {width:172px !important;}
         table[class=boxfunnel] {width:35px !important;}
         table[class=boxleft2] {width:350px !important;}
         table[class=boxright] {width:340px !important;}
         table[class=bodytable3] {width:560px !important;}
         table[class=bttn] {width:150px !important;}
         table[class=legal] {width:300px !important;}
         }
         @media only screen and (max-width: 600px) {
         table[class=bodytable] { width:100% !important;}
         table[class=contentSection] {width:90% !important;}
         table[class=boxleft] {width:100% !important;}
         table[class=boxfunnel] {width:20px !important;}
         table[class=boxleft2] {width:100% !important;}
         table[class=boxright] {width:100% !important;}
         table[class=bodytable3] {width:90% !important;}
         table[class=bttn] { width:100% !important;}
         table[class=legal] {width:100% !important;}
         }
      </style>
      <!--[if gte mso 9]>
      <style>
         #outlookFix {
         width:602px;
         }
      </style>
      <![endif]-->
      <!-- <link rel="shortcut icon" type="image/png" href="https://www.leadman.co/e-fichas/images/favicons.png"/> -->
   </head>
   <body style="margin:0; background-color:#FFFFFF; width:100%">
    	<table style='background-color:#FFFFFF;padding:0px;width:100%'>
      		<tr>
		        <td style='width:100%'>
		            <center>
		               <table border='0' cellspacing='0' cellpadding='0' id='outlookFix' style='max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF'>
		               		<tr>
			                    <td style='border: 1px solid #DADADA'>
			                        <table class='bodytable' width='600' cellpadding='0' cellspacing='0' border='0' style='max-width:600px !important; background-color:#f9f9f9'>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding:30px 0 30px 0'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#EF615D; font-weight: normal; border-right: 1px solid #888888'>
			                        									Reporte LM
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 15px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									
			                        									{{ $data['fecha'] }} 
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['totalLeads'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									leads
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['interesado'] }}/{{ $data['noInteresado'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									interesado/No&nbsp;Interesado
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                      
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td colspan="4" style='padding: 5px 0px 20px 0px; font-family:Arial, Helvetica; font-size:20px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        							Tiempos de gestión
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 140px'>
			                        									--
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Cotizaciones
			                        									Horario&nbsp;laborable
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['horarioLaborable'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 110px'>
			                        									{{ $data['pMenosDosH'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									menos de 2 horas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['menosDosH'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 98px'>
			                        									{{ $data['pMenosUnDia'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Menos de un dia
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['menosUnDia'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 98px'>
			                        									{{ $data['pMasUnDia'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Mas de un día.
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['masUnDia'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="5" style='padding: 0px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        									&nbsp;
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="4" style='padding: 2px 5px 2px 5px; font-family:Arial, Helvetica; font-size:10px; line-height:11px; text-align:left; color:#CCCCCC; font-weight: normal; border-bottom:1px solid #CCCCCC'>
			                        									Acumulado mes con base en el total de leads.
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td colspan="4" style='padding: 5px 0px 20px 0px; font-family:Arial, Helvetica; font-size:20px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        							Tasa Gestion
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 140px'>
			                        									{{ $data['pLeadSinGestionar'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Sin&nbsp;Gestión
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['LeadSinGestionar'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 110px'>
			                        									{{ $data['pLeadGestionados'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Gestionados
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['LeadGestionados'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 98px'>
			                        									{{ $data['pLeadNoGestionados'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									No&nbsp;gestionables
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['LeadNoGestionados'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="5" style='padding: 0px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        									&nbsp;
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="4" style='padding: 2px 5px 2px 5px; font-family:Arial, Helvetica; font-size:10px; line-height:11px; text-align:left; color:#CCCCCC; font-weight: normal; border-bottom:1px solid #CCCCCC'>
			                        									Acumulado mes con base en el total de leads.
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['cerradosEnGestion'] }}/{{ $data['cerradosdosEnCita'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Cerrados en Gestión / Cerrados en Vitrina
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<!-- <tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['ventaPriCita'] }}/{{ $data['leadsVenta'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Venta&nbsp;primera&nbsp;cita / Ventas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['ventaReagendad'] }}/{{ $data['leadsVenta'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Venta&nbsp;con&nbsp;reagendamientos / Ventas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['asistenciaSinVenta'] }}/{{ $data['leadsCitaEfectiva'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Asistencia&nbsp;sin&nbsp;venta / Citas&nbsp;Efectivas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['agendadosSinAsistencia'] }}/{{ $data['leadsCita'] }}
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Citas&nbsp;sin&nbsp;asistencia / Citas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr> -->
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td colspan="4" style='padding: 5px 0px 20px 0px; font-family:Arial, Helvetica; font-size:20px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        							Funnel
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; height:200px'>
			                        									--
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Total&nbsp;leads
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['totalLeads'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 170px'>
			                        									{{ $data['pleadsContactado'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Contactado
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['leadsContactado'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 140px'>
			                        									{{ $data['pleadsCita'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Citas&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['leadsCita'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 110px'>
			                        									{{ $data['pleadsCitaEfectiva'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Efectivas&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['leadsCitaEfectiva'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 98px'>
			                        									{{ $data['pleadsVenta'] }}%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Ventas&nbsp;&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									{{ $data['leadsVenta'] }}
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="5" style='padding: 0px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        									&nbsp;
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="4" style='padding: 2px 5px 2px 5px; font-family:Arial, Helvetica; font-size:10px; line-height:11px; text-align:left; color:#CCCCCC; font-weight: normal; border-bottom:1px solid #CCCCCC'>
			                        									Acumulado mes con base en el total de leads.
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE; width: 30%'>
			                        						Concesionario 
			                        					</td>
			                        					
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Leads/<br>Ayer
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Gestion/<br>Ayer
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Leads/<br>mes
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Gestion/<br>mes
			                        					</td>
			                        				</tr>
			                        				
			                        				<tr>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						&nbsp;
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						&nbsp;
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Tasa gestion del dia de ayer.
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						&nbsp;
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Tasa gestion acumulado mes.
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        </table>
			                    </td>
		                	</tr>
		                </table>
		            </center>
		        </td>
		    </tr>
   		</table>
   </body>
</html>

