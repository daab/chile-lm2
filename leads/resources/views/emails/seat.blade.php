<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<title>Seat</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width" />

<style type="text/css">
@media only screen and (min-width: 600px) {	

table[class=contentTable] {
	width:600px !important;
	}


table[class=contentTable2] {
	width:540px !important;
	}

table[class=leftColumn] {
	width:133px !important;
	}

table[class=bttn] {
	width:200px !important;
	}

table[class=legal] {
	width:450px !important;
	}
}

@media only screen and (max-width: 600px) {	
table[class=contentTable] {
	width:100% !important;
	}
table[class=contentTable2] {
	width:90% !important;
	}

table[class=bttn] {
	width:100% !important;
	}

table[class=leftColumn] {
	width:100px !important;
	}
	
table[class=legal] {
	width:100% !important;
	}
}

</style>

<!--[if gte mso 9]>
<style>
#outlookFix {
  width:602px;
}
</style>
<![endif]-->

<link rel="shortcut icon" type="image/png" href="https://www.cotizadoronline.com.co/seat/email/images/favicons.png"/>
</head>

<body style="margin:0; background-color:#FFFFFF; width:100%">
<table style="background-color:#FFFFFF;padding:0px;width:100%">
<tr><td style="width:100%">
<center>

<table border="0" cellspacing="0" cellpadding="0" id="outlookFix" style="max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF">


<tr><td style="border: 1px solid #DADADA">
<table class="contentTable" width="600" cellpadding="0" cellspacing="0" border="0" style="max-width:600px !important; background-color:#f9f9f9">
<tr><td style="text-align:left; line-height:0px; background-color:#ffffff"><a href="http://www.seat.co/" target="_blank"><img src="https://www.cotizadoronline.com.co/seat/email/images/seat-mockup-email-130316_02.jpg" width="191" style="display:block;"></a></td></tr>
<tr><td style="text-align:left; line-height:0px; max-width:600px;"><img src="https://www.cotizadoronline.com.co/seat/email/images/email-ibiza-leon-170124.jpg" border="0" alt="Seat" style="max-width:100%; display:block"></td></tr>

<tr><td style="background-color:#f9f9f9; display:block; max-width:600px; padding-bottom:10px">
<table class="contentTable2" width="540" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important; background-color:#f9f9f9">
<tr><td style="padding:20px 0px 20px 0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; font-weight: bold; line-height:22px; text-align:left; color:#666666; max-width:600px">Buen Día Sr(a), {{$data['lead']}}<br/><br/>
Adjunto información y ficha técnica del vehículo de su interés.<br/><br/>

<strong>{{$data['modelo']}}</strong><br><br>

Tenemos planes de financiamiento hasta del 100% del valor del vehículo y todo depende del perfil del cliente, se revisa en centrales de riesgo, miramos capacidad de endeudamiento e ingresos.
<br /><br />
Tenemos financiamiento de varios planes con BBVA Y Occidente que son los aliados de nuestra marca, pero si usted desea podemos pasar los presentar para el crédito con cualquier otro banco de su preferencia.
<br /><br />
Lo importante es que usted se acerque a nuestra vitrina conozca el vehículo hacemos una prueba de test drive y miremos acá toda la financiación con nuestro asesor de crédito ya que nos toca llenar un formulario y tener una copia de cédula suya para saber que taza de interés y que monto podemos prestar. 

</td></tr>
<tr><td style="padding:20px 0px 25px 0px; font-family:Arial, Helvetica, sans-serif; font-size:13px; line-height:20px; text-align:left; color:#666666; max-width:600px; border-top: 1px solid #cccccc"><b>
Cordialmente,<br />
{{$data['asesor']}}
</b> <br><span style="font-family:Arial, Helvetica, sans-serif; font-size:16px; line-height:20px; text-align:left; color:#666666; max-width:600px"><b>NUEVO CONCESIONARIO / AUTOMOTORES EUROPA</b></span><br>
Calle 126 No. 60-05 / Tel&#233;fono 6139608 / Bogot&#225;, D.C.<br> ventas@automotoreseuropa.com</td></tr>

<tr><td style="display:block; max-width:600px; padding-bottom:30px">
	<table cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:540px !important; margin-bottom: 20px">

		<tr><td style="width: 80px">
			<img src="{{$data['image']}}" width="100%" alt="Asesor" style="display:block;" />
		</td></tr>
		

	</table>

	
</td></tr>

</table></td></tr>



<tr><td style="background-color:#151515; display:block; max-width:600px; padding:20px 0px 0px 0px; padding-bottom:0px">
<table class="contentTable2" width="540" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important; background-color:#151515"><tr><td>
<table class="bttn" cellpadding="0" cellspacing="0" border="0" align="left" width="200">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="24"><img src="https://www.cotizadoronline.com.co/seat/email/images/seat-mockup-email-130316_07.jpg" width="24" style="display:block;"/></td><td style="padding:0px 0px 30px 0px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:18px; text-align:left; color:#FFFFFF; max-width:600px;"><a href="http://www.seat.co/" style="font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:18px; text-align:left; color:#FFFFFF; text-decoration:none" target="_blank">VISITE WWW.SEAT.CO</a></td></tr>
</table>
</td></tr>
</table>
</td></tr>

<tr><td style="background-color:#151515; display:block; max-width:600px; padding:0px 0px 0px 0px; padding-bottom:0px">
<table class="contentTable2" width="540" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important; background-color:#151515"><tr><td>
<table cellpadding="0" cellspacing="0" border="0" align="left">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="30"><a href="https://www.facebook.com/SEATcolombia" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/seat/email/images/social-fb.jpg" width="23" style="display:block;"/></a></td><td valign="top" style="padding:0px 0px 0px 0px;" width="30"><a href="https://twitter.com/SEATColombia" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/seat/email/images/social-tw.jpg" width="23" style="display:block;"/></a></td><td valign="top" style="padding:0px 0px 0px 0px;" width="30"><a href="https://www.youtube.com/user/SEATCOLOMBIA" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/seat/email/images/social-yt.jpg" width="23" style="display:block;"/></a></td></tr>
</table>
<table  class="legal" cellpadding="0" cellspacing="0" border="0" align="right" width="450">
<tr><td style="padding:2px 0px 20px 7px; font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:18px; text-align:left; color:#666666; max-width:600px; text-align:right">&#169; 2017 PORSCHE COLOMBIA. Todos los derechos reservados &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a style="font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:18px; text-decoration:underline; color:#666666;"  target="_blank" href="*|UNSUB|**"></a></td></tr>
</table>
</td></tr>
<tr><td style="padding:2px 0px 20px 7px; font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:10px; text-align:left; color:#666666; max-width:600px; text-align:left; background-color: #000000">

 </td></tr>
</table>
</td></tr>

</table>
</td></tr>

</table>

</center>
</td></tr>
</table>
</body>
</html>