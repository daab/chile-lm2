

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
   <head>
      <title>Audi</title>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width" />
      <style type="text/css">
         @media only screen and (min-width: 600px) {	
         table[class=bodytable] {width:600px !important;}
         table[class=contentSection] {width:520px !important;}
         table[class=boxleft] {width:172px !important;}
         table[class=boxfunnel] {width:35px !important;}
         table[class=boxleft2] {width:350px !important;}
         table[class=boxright] {width:340px !important;}
         table[class=bodytable3] {width:560px !important;}
         table[class=bttn] {width:150px !important;}
         table[class=legal] {width:300px !important;}
         }
         @media only screen and (max-width: 600px) {
         table[class=bodytable] { width:100% !important;}
         table[class=contentSection] {width:90% !important;}
         table[class=boxleft] {width:100% !important;}
         table[class=boxfunnel] {width:20px !important;}
         table[class=boxleft2] {width:100% !important;}
         table[class=boxright] {width:100% !important;}
         table[class=bodytable3] {width:90% !important;}
         table[class=bttn] { width:100% !important;}
         table[class=legal] {width:100% !important;}
         }
      </style>
      <!--[if gte mso 9]>
      <style>
         #outlookFix {
         width:602px;
         }
      </style>
      <![endif]-->
      <!-- <link rel="shortcut icon" type="image/png" href="https://www.leadman.co/e-fichas/images/favicons.png"/> -->
   </head>
   <body style="margin:0; background-color:#FFFFFF; width:100%">
    	<table style='background-color:#FFFFFF;padding:0px;width:100%'>
      		<tr>
		        <td style='width:100%'>
		            <center>
		               <table border='0' cellspacing='0' cellpadding='0' id='outlookFix' style='max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF'>
		               		<tr>
			                    <td style='border: 1px solid #DADADA'>
			                        <table class='bodytable' width='600' cellpadding='0' cellspacing='0' border='0' style='max-width:600px !important; background-color:#f9f9f9'>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding:30px 0 30px 0'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									Audi Report
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 15px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									
			                        									{{ $data['fecha'] }} 
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['totalLeads_brand'] }}
			                        									
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									leads
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado dia.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['totalLeadsAgendas_pendientes'] }} 
			                        									
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Agendas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Agendas del dia.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#BB0A30; font-weight: normal'>
			                        									{{ $data['totalLeadsAgendas_pendientes'] }} 
			                        									
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold'>
			                        									Pendientes
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal'>
			                        									Agendas sin gestionar del dia.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding-top: 0px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 0px 0 0 0; font-family:Arial, Helvetica; font-size:12px; line-height:16px; text-align:left; color:#000000; font-weight: normal;'>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['totalLeads_brand'] }}
			                        									
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									leads
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado del mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        						<table class="boxleft" width="170" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:170px !important; padding: 25px 0 25px 0'>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:64px; line-height:64px; text-align:left; color:#333333; font-weight: normal; border-right: 1px solid #888888'>
			                        									{{ $data['totalLeadsAgendas_pendientes'] }} 
			                        									
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 0px 25px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-right: 1px solid #888888'>
			                        									Agendas
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 0px 25px 5px 25px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-right: 1px solid #888888'>
			                        									Acumulado del mes.
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; height: 200px'>
			                        									&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									leads&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									300
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 100px'>
			                        									50%
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Contactado
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									150
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 100px'>
			                        									100
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Agendas&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									150
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 10px'>
			                        									10
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Efectivas&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									150
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        					<td valign="baseline" style='vertical-align: bottom;'>
			                        						<table class="boxfunnel" width="35" cellpadding='0' cellspacing='0' border='0' align='left' style='max-width:35px !important; padding: 0px 5px 0px 2px'>
			                        							<tr>
			                        								<td style='padding: 0px 5px 0px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:center; color:#FFFFFF; font-weight: normal; background-color: #000000; vertical-align: middle; height: 1px'>
			                        									1
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 1px solid #888888'>
			                        									Ventas&nbsp;&nbsp;&nbsp;&nbsp;
			                        								</td>
			                        							</tr>
			                        							<tr>
			                        								<td style='padding: 5px 0px 0px 0px; font-family:Arial, Helvetica; font-size:11px; line-height:12px; text-align:left; color:#888888; font-weight: normal; border-top: 1px solid #888888'>
			                        									150
			                        								</td>
			                        							</tr>
			                        						</table>
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="5" style='padding: 0px 0px 0px 0px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold;'>
			                        									&nbsp;
			                        					</td>
			                        				</tr>
			                        				<tr>
			                        					<td colspan="4" style='padding: 2px 5px 2px 5px; font-family:Arial, Helvetica; font-size:10px; line-height:11px; text-align:left; color:#CCCCCC; font-weight: normal; border-bottom:1px solid #CCCCCC'>
			                        									Acumulado mes
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        	<tr>
			                        		<td style='background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px'>
			                        			<table class='contentSection' width='520' cellpadding='0' cellspacing='0' border='0' align='center' style='max-width:540px !important; padding: 25px 20px 25px 20px; background-color: #EEEEEE'>
			                        				<tr>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Dealer 
			                        					</td>
			                        					
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						En seguimiento
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Citas efectivas
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Ventas
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Agendas dia
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:12px; line-height:14px; text-align:left; color:#888888; font-weight: bold; border-top: 2px solid #888888; border-right: 10px solid #EEEEEE'>
			                        						Pendientes dia
			                        					</td>
			                        				</tr>
			                        					@foreach($data['fichero'] as $resultado)
			                        						<tr>
				                        					@foreach($resultado as $clave)
				               								<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#444444; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>{{$clave}}</td>
				               								@endforeach
				               								<tr>
			               								@endforeach
			                        				<tr>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						&nbsp;
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Clientes en seguimiento acumulado mes.
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Citas efectivas acumulado mes.
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Ventas acumulado mes.
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Agendas asignadas del dia.
			                        					</td>
			                        					<td style='padding: 10px 5px 15px 5px; font-family:Arial, Helvetica; font-size:11px; line-height:14px; text-align:left; color:#CCCCCC; font-weight: normal; border-top: 1px solid #CCCCCC; border-right: 10px solid #EEEEEE'>
			                        						Agendas sin gestionar al final del dia.
			                        					</td>
			                        				</tr>
			                        			</table>
			                        		</td>
			                        	</tr>
			                        </table>
			                    </td>
		                	</tr>
		                </table>
		            </center>
		        </td>
		    </tr>
   		</table>
   </body>
</html>

