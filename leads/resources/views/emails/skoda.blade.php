<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head>
<title>Skoda</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width" />

<style type="text/css">
@media only screen and (min-width: 600px) {	

table[class=contentTable] {
	width:600px !important;
	}

table[class=contentimg] {
	width:173px !important;
}

table[class=contentTable2] {
	width:520px !important;
	}

table[class=contentTable3] {
	width:560px !important;
	}

table[class=leftColumn] {
	width:240px !important;
	}

table[class=rightColumn] {
	width:240px !important;
	}

table[class=bttn] {
	width:150px !important;
	}

table[class=legal] {
	width:300px !important;
	}
}

@media only screen and (max-width: 600px) {	
table[class=contentimg] {
	width:100% !important;
	}

table[class=contentTable] {
	width:100% !important;
	}
table[class=contentTable2] {
	width:100% !important;
	}

table[class=contentTable3] {
	width:90% !important;
	}

table[class=bttn] {
	width:100% !important;
	}

table[class=leftColumn] {
	width:80px;
	}

table[class=rightColumn] {
	width:100% !important;
	}
	
table[class=legal] {
	width:100% !important;
	}
}

</style>

<!--[if gte mso 9]>
<style>
#outlookFix {
  width:602px;
}
</style>
<![endif]-->

<link rel="shortcut icon" type="image/png" href="https://www.cotizadoronline.com.co/skoda/email/images/favicons.png"/>
</head>

<body style="margin:0; background-color:#FFFFFF; width:100%">
<table style="background-color:#FFFFFF;padding:0px;width:100%">
<tr><td style="width:100%">
<center>

<table border="0" cellspacing="0" cellpadding="0" id="outlookFix" style="max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF">


<tr><td style="border: 1px solid #DADADA">
<table class="contentTable" width="600" cellpadding="0" cellspacing="0" border="0" style="max-width:600px !important; background-color:#f9f9f9">

<tr><td style="text-align:left; line-height:0px; max-width:600px;">
	<a href="http://www.skoda.com.co/" style="line-height:0px; text-decoration:none" target="_blank">
		<img src="https://www.cotizadoronline.com.co/skoda/email/images/email-skoda-ficha-170220.jpg" border="0" alt="skoda" style="max-width:100%; display:block">
	</a>
</td></tr>

<tr><td style="background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px">

<table class="contentTable2" width="520" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important; padding-top: 30px">

<tr><td style="padding:0px 0px 30px 0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:22px; text-align:left; color:#000000; max-width:600px">
Buen Día Sr(a), {{$data['lead']}}<br/><br/>
Adjunto información y ficha técnica del vehículo de su interés.<br/><br/>

<strong>{{$data['modelo']}}</strong><br><br>

{!!$data['field1']!!}

Cordialmente,<br />
{{$data['asesor']}}<br />
AUTOMOTORES EUROPA <br />Calle 126 # 60-05 - Avenida Suba con Calle 126 Teléfono (1) 613 96 08 
</td></tr>

<tr><td style="background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px">
	<table cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:540px !important; margin-bottom: 20px">

		<tr><td style="width: 80px">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/foto-asesor.jpg" width="100%" alt="Asesor" style="display:block;" /><br />
		</td></tr>
		

	</table>

	
</td></tr>

<tr><td style="padding:30px 0px 20px 0px; font-family:Arial, Helvetica, sans-serif; font-size:20px; line-height:24px; text-align:left; color:#555555; max-width:600px; border-top: 1px solid #CCCCCC"><b>&#191;Sab&#237;as qu&#233;?</b></td></tr>

<tr><td style="background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px">
	<table class="leftColumn" width="240" cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:600px !important; border: 1px solid #555555!important; margin-bottom: 20px">

		<tr><td width="50" style="width: 50px; line-height: 0">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/ico-0.jpg" width="50" height="50" alt="icon"/>
		</td><td style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px; text-align:left; color:#555555"><b>
			120 A&#209;OS</b>
		</td></tr>
		<tr><td colspan="2" style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color:#FFFFFF; background-color: #555555;">
		&#352;KODA tiene m&#225;s de 120 a&#241;os de historia y es uno de los fabricantes de veh&#237;culos m&#225;s antiguos del mundo.
		</td></tr>
		

	</table>

	<table class="rightColumn" width="240" cellpadding="0" cellspacing="0" border="0" align="right" style="max-width:600px !important; border: 1px solid #555555!important; margin-bottom: 20px">

		<tr><td width="50" style="width: 50px; line-height: 0">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/ico-0.jpg" width="50" height="50" alt="icon"/>
		</td><td style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px; text-align:left; color:#555555"><b>
			Marca No. 1</b>
		</td></tr>
		<tr><td colspan="2" style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color:#FFFFFF; background-color: #555555;">
		Desde hace siete a&#241;os consecutivos, &#352;KODA es la marca importada No. 1 en Alemania. 
		</td></tr>
		

	</table>
</td></tr>

<tr><td style="background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px">
	<table class="leftColumn" width="240" cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:600px !important; border: 1px solid #555555!important; margin-bottom: 20px">

		<tr><td width="50" style="width: 50px; line-height: 0">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/ico-0.jpg" width="50" height="50" alt="icon"/>
		</td><td style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px; text-align:left; color:#555555"><b>
			Dise&#241;o</b>
		</td></tr>
		<tr><td colspan="2" style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color:#FFFFFF; background-color: #555555;">
		El jefe de dise&#241;o de &#352;KODA, Jozef Kaba&#328;, tambi&#233;n dise&#241;&#243; el emblem&#225;tico Bugatti Veyron.   
		</td></tr>
		

	</table>

	<table class="rightColumn" width="240" cellpadding="0" cellspacing="0" border="0" align="right" style="max-width:600px !important; border: 1px solid #555555!important; margin-bottom: 20px">

		<tr><td width="50" style="width: 50px; line-height: 0">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/ico-0.jpg" width="50" height="50" alt="icon"/>
		</td><td style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px; text-align:left; color:#555555"><b>
			100% europeo</b>
		</td></tr>
		<tr><td colspan="2" style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color:#FFFFFF; background-color: #555555;">
		Los veh&#237;culos &#352;KODA se producen 100% en Europa, en las f&#225;bricas de autos m&#225;s tecnol&#243;gicas.       
		</td></tr>
		

	</table>
</td></tr>

<tr><td style="background-color:#FFFFFF; display:block; max-width:600px; padding-bottom:30px">
	<table class="leftColumn" width="240" cellpadding="0" cellspacing="0" border="0" align="left" style="max-width:600px !important; border: 1px solid #555555!important; margin-bottom: 20px">

		<tr><td width="50" style="width: 50px; line-height: 0">
			<img src="https://www.cotizadoronline.com.co/skoda/email/images/ico-0.jpg" width="50" height="50" alt="icon"/>
		</td><td style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:18px; text-align:left; color:#555555"><b>
			Grupo Volkswagen</b>
		</td></tr>
		<tr><td colspan="2" style="padding:5px 10px 5px 10px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color:#FFFFFF; background-color: #555555;">
		&#352;KODA hace parte del Grupo Volkswagen, uno de los fabricantes de veh&#237;culos m&#225;s importantes del mundo. 
		</td></tr>
		

	</table>
</td></tr>


<tr><td style="padding:20px 0px 0px 0px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:20px; text-align:left; color:#666666; max-width:600px; border-top: 1px solid #CCCCCC"><b>NUEVO IMPORTADOR - PORSCHE COLOMBIA S.A.S UNA EMPRESA DEL GRUPO VOLKSWAGEN</b></td></tr>
<tr><td style="padding:5px 0px 0px 0px; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height:20px; text-align:left; color:#666666; max-width:600px"><b>NUEVO CONCESIONARIO - AUTOMOTORES EUROPA</b></td></tr>

<tr><td style="padding:5px 0px 0px 0px; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:20px; text-align:left; color:#666666; max-width:600px">Calle 126 # 60-05 - Avenida Suba con Calle 126
Tel&#233;fono (1) 613 96 08 <br> ventas@automotoreseuropa.com.co</td></tr>

</table></td></tr>

<tr><td style="background-color:#ececec; display:block; max-width:600px; padding:20px 0px 0px 0px; padding-bottom:0px">



<table class="contentTable3" width="560" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important;">

<tr><td>
<table class="bttn" cellpadding="0" cellspacing="0" border="0" align="left" width="150">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="24"><img src="https://www.cotizadoronline.com.co/skoda/email/images/arrow-skoda-130416.jpg" width="24" style="display:block;"/></td><td style="padding:0px 0px 20px 0px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:18px; text-align:left; color:#444444; max-width:600px;"><a href="http://www.skoda.com.co/" style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:18px; text-align:left; color:#444444; text-decoration:none" target="_blank">Visita skoda.com.co</a></td></tr>
</table>
<table class="bttn" cellpadding="0" cellspacing="0" border="0" align="left" width="150">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="24"><img src="https://www.cotizadoronline.com.co/skoda/email/images/arrow-skoda-130416.jpg" width="24" style="display:block;"/></td><td style="padding:0px 0px 20px 0px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:18px; text-align:left; color:#444444; max-width:600px;"><a style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:18px; text-decoration:none; color:#444444;"  target="_blank" href="*|UNSUB|**">Desinscripci&#243;n</a></td></tr>
</table>
</td></tr>

<tr><td style="background-color:#ececec; display:block; padding:20px 0px 0px 0px; padding-bottom:0px; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:18px; text-align:left; color:#444444; border-top:1px solid #CCCCCC"></td></tr>

<tr><td style="background-color:#ececec; display:block; padding:0px 0px 0px 0px; padding-bottom:0px">
<table  class="leftColumn" cellpadding="0" cellspacing="0" border="0" align="left" width="240">
<tr><td style="text-align:left; line-height:0px; max-width:240px;"><img src="https://www.cotizadoronline.com.co/skoda/email/images/120anhos.jpg" border="0" alt="120a&#241;os" style="max-width:100%; display:block"></td></tr>
</table>
</td></tr>


<tr><td style="border-top: 1px solid #CCCCCC">
<table cellpadding="0" cellspacing="0" border="0" align="left">
<tr><td valign="top" style="padding:20px 0px 0px 0px;" width="40"><a href="http://www.facebook.com/SkodaColombia" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/skoda/email/images/so-fb.jpg" width="23" style="display:block;"/></a></td>
	<td valign="top" style="padding:20px 0px 0px 0px;" width="40"><a href="http://twitter.com/SkodaColombia" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/skoda/email/images/so-tw.jpg" width="23" style="display:block;"/></a></td>
	<td valign="top" style="padding:20px 0px 0px 0px;" width="40"><a href="http://www.youtube.com/user/SkodaColombia?sub_confirmation=1" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/skoda/email/images/so-yt.jpg" width="23" style="display:block;"/></a></td></tr>
</table>
<table  class="legal" cellpadding="0" cellspacing="0" border="0" align="right" width="300">
<tr><td style="padding:22px 0px 20px 7px; font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:18px; text-align:left; color:#444444; max-width:600px; text-align:right">&#169; &#352;KODA Colombia, 2017. Todos los derechos reservados</td></tr>
</table>
</td></tr>
<tr><td valing="top" style="padding:0px 0px 25px 0px; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:13px; text-align:left; color:#666666; max-width:600px">
Fotos de referencia, algunos equipamientos son parte de la ambientaci&#243;n fotogr&#225;fica y pueden no corresponder en su totalidad con las versiones disponibles en el pa&#237;s. Le recomendamos acudir a su concesionario &#352;KODA para conocer m&#225;s detalles de nuestros modelos. 
</td></tr>

</table>
</td></tr>

<tr><td valing="top" style="background-color:#ececec; padding:0px 0px 25px 10px; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:13px; text-align:left; color:#666666; max-width:600px">
<table class="bttn" cellpadding="0" cellspacing="0" border="0" align="left" width="150">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="150"><img src="https://www.cotizadoronline.com.co/skoda/email/images/logo-footer-skoda.jpg" width="150" style="display:block;"/></td></tr>
</table>
</td></tr>




</table>

</center>
</td></tr>
</table>
</body>
</html>