<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=gb18030">
<title>Volkswagen</title>

<meta name="viewport" content="width=device-width" />

<style type="text/css">
@media only screen and (min-width: 600px) {	

table[class=contentTable] {
	width:600px !important;
	}


table[class=contentTable2] {
	width:540px !important;
	}

table[class=leftColumn] {
	width:420px !important;
	}

table[class=rightColumn] {
	width:80px !important;
	}

table[class=bttn] {
	width:200px !important;
	}

table[class=legal] {
	width:300px !important;
	}
}

@media only screen and (max-width: 600px) {	
table[class=contentTable] {
	width:100% !important;
	}
table[class=contentTable2] {
	width:90% !important;
	}

table[class=bttn] {
	width:100% !important;
	}

table[class=leftColumn] {
	width:60% !important;
	}

table[class=rightColumn] {
	width:80px !important;
	}
	
table[class=legal] {
	width:100% !important;
	}
}

</style>

<!--[if gte mso 9]>
<style>
#outlookFix {
  width:602px;
}
</style>
<![endif]-->

<link rel="shortcut icon" type="image/png" href="images/favicons.png"/>
</head>

<body style="margin:0; background-color:#FFFFFF; width:100%">
<table style="background-color:#FFFFFF;padding:0px;width:100%">
<tr><td style="width:100%">
<center>

<table border="0" cellspacing="0" cellpadding="0" id="outlookFix" style="max-width:602px !important; display:inline-block; margin:0 auto; background-color:#FFFFFF">


<tr><td style="border: 1px solid #EEEEEE">
<table class="contentTable" width="600" cellpadding="0" cellspacing="0" border="0" style="max-width:600px !important; background-color:#f9f9f9">
<tr><td style="text-align:left; line-height:0px; background-color:#ffffff; padding:15px"><a href="http://www.volkswagen.co/" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/images/vw-logo-m.jpg" width="30" alt="vw" style="display:block;"></a></td></tr>
<tr><td style="text-align:left; line-height:0px; max-width:600px;"><a href="http://volkswagenpromo.co" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/email/images/vw-email-ficha-banner.jpg" border="0" alt="Gol" style="max-width:100%; display:block" /></a></td></tr>

<tr><td style="padding:0px 0px 40px 0px; line-height:0px; max-width:600px; background-color:#FFFFFF; text-align:center;">
<table class="contentTable2" width="540" cellpadding="0" cellspacing="0" border="0" align="center" style="max-width:540px !important; background-color:#FFFFFF">


		
			<tr><td style="color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:14px; line-height: 20px; text-align:left; padding: 15px 0px 0px 0px;">

				{!!$data['field1']!!}
			</td></tr>
		
		
			<tr><td style="text-align: left; padding: 15px 0 30px 0px">
			<table class="contentTable" width="80" cellpadding="0" align="left" cellspacing="0" border="0" style="max-width:80px !important"><tr><td>
			
				<img src="{{$data['image']}}" width="100%" alt="Asesor" style="display:block;" />
				</td></tr>
				</table>
			</td></tr>
		
	

	<tr><td style="color:#cccccc; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height: 14px; text-align:left; padding: 20px 0px 0px 0px; border-top: 1px solid #cccccc">Cualquier duda o inquietud no dudes en comunicarte de nuevo con nosotros. Para información de otros modelo contacta de nuevo con el asesor o visita <a target="_blank" style="text-decoration: none; color: #0072b1; font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height: 14px; text-align:left" href="http://volkswagen.com.co/modelos">volkswagen.com.co/modelos</a></td></tr>

</table>
</td></tr>


<tr><td style="padding:20px 20px 30px 20px; font-family:Arial, Helvetica, sans-serif; font-size:10px; line-height:13px; text-align:left; color:#888888; max-width:600px; background-color:#dee1e3;">

<table cellpadding="0" cellspacing="0" border="0" align="left">
<tr><td valign="top" style="padding:0px 0px 0px 0px;" width="47">
	<a href="https://www.facebook.com/VolkswagenCO" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/email/images/facebook_icon-01.png" width="40" style="display:block;"/></a></td>

	<td valign="top" style="padding:0px 0px 0px 0px;" width="47"><a href="https://twitter.com/VolkswagenCo" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/email/images/twitter_icon-01.png" width="40" style="display:block;"/></a></td>

	<td valign="top" style="padding:0px 0px 0px 0px;" width="47"><a href="https://www.instagram.com/volkswagen_colombia/" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/email/images/instagram_icon-01.png" width="40" style="display:block;"/></a></td>

	<td valign="top" style="padding:0px 0px 0px 0px;" width=47><a href="https://www.youtube.com/channel/UCmT6DM0HRrWItN00CLncMRg/feed" style="line-height:0px; text-decoration:none" target="_blank"><img src="https://www.cotizadoronline.com.co/volkswagen/email/images/youtube_icon-01.png" width="40" style="display:block;"/></a></td>

</tr></table>


<table class="legal" cellpadding="0" cellspacing="0" border="0" align="right" width="300">
<tr><td style="padding:12px 0px 20px 7px; font-family:Arial, Helvetica, sans-serif; font-size:9px; line-height:18px; text-align:left; color:#666666; max-width:600px; text-align:right">&#169; 2017 VOLKSWAGEN. Todos los derechos reservados </td></tr>
</table>

</td></tr>


</table>
</td></tr>

</table>

</center>
</td></tr>
</table>
</body>
</html>