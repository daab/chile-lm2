<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="HandheldFriendly" content="True">
  	<meta name="MobileOptimized" content="540">
	<META HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<meta name="title" content="Lead Manager">
	<title>Lead Manager</title>
	<!-- <link rel="shortcut icon" href="images/favicons.png"> -->
<!-- 	<link rel='stylesheet' href='css/allesgut.css'> -->	
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
	<link href="{{ asset('/public/css/mobile/css/allesgut.css') }}" rel="stylesheet">
</head>
<body>

@yield('content')


<script type="text/javascript" src="{{ asset('/public/css/mobile/js/jquery-1.7.1.min.js') }}"></script>

@yield('script')

</body>
</html>