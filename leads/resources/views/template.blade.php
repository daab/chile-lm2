<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Lead Manager</title>

	<link rel="stylesheet" href="{{ asset('/public/css/dblnkd.css') }}" media="all">
	<link rel="stylesheet" href="{{ asset('/public/css/lm-menu.css') }}" media="all">
	<link rel="stylesheet" href="{{ asset('/public/css/lm-content.css') }}" media="all">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

	<link href="{{ asset('/public/css/app.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/custom.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/menu-desktop.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/css/font-awesome.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/js/sweetalert/sweetalert.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/js/jquery-ui/jquery-ui.min.css') }}" rel="stylesheet">
	<link href="{{ asset('/public/js/c3/c3.min.css') }}" rel="stylesheet">

	

	

	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
</head>
<body class="bg-azul">


	@if (!Auth::guest())
		@include('layout.aside')
	@endif

	@yield('content')



	<div class="modal"><!-- Place at bottom of page --></div>
	<!-- Scripts -->
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
	<script src="{{ asset('/public/js/sweetalert/sweetalert.min.js') }}"></script>
	<script src="{{ asset('/public/js/jquery-ui/jquery-ui.min.js') }}"></script>
	
	
	<script src="https://d3js.org/d3.v3.min.js"></script>
	<script src="{{ asset('/public/js/c3/c3.min.js') }}"></script>

	<script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js">
	</script>



	<script type="text/javascript">

		// JavaScript Document`
		$(document).ajaxStart(function() {
			$('body').addClass("loading");
			
		});

		// JavaScript Document`
		$(document).ajaxStop(function() {
			$('body').removeClass("loading");
			
		});

	</script>
	
	@yield('scripts')
</body>
</html>
