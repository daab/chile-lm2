<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Maatwebsite\Excel\Facades\Excel;

use App\Models\Distributor;
use App\Models\Conglomerate;
use App\Models\TimeZone;
use App\Models\Brand;
use App\Models\Lead;
use App\Models\Goal;
use App\Models\Rol;

use App\User;
use App\helpers;
use App\Security;

class CalculateReports extends Model {

	public static $data_report = array();

	public static function init($type = 'monthly') {
		$year = date('Y');
		$month = helpers::getMonth('value_'.date('m'));

		$sql = " SELECT 
					distributors.id as id_leadmanager,  
				    cities.name as ciudad,
				    distributors.name as concesionario,
				";
				
				if ($type == 'annual') {

					$sql .= " sum(goal.value) as Total_mes_en_curso, ";

				} else {
					$sql .= " goal.value as Total_mes_en_curso, ";
				}
				
				$sql.= "    				    
					(
				        SELECT 
				            sum(goal.value) as Total
				        FROM goal
				        inner join distributors on distributors.id = goal.distributor_id
				        inner join cities on cities.id = distributors.city_id
				        where 
			   ";

				if ($type == 'monthly') {

					$sql.= " goal.month = '".$month."' and ";
				}

				$sql.= "      goal.year = '".$year."'
						    ) as total
						FROM goal
						inner join distributors on distributors.id = goal.distributor_id
						inner join cities on cities.id = distributors.city_id
						where 
			   		";

				if ($type == 'monthly') {

					$sql.= " goal.month = '".$month."' and ";
				}

				$sql.= " goal.year = '".$year."' ";

				
				if ($type == 'annual') {

					$sql.= " group by distributors.id ";
				}

		// echo "<pre>";

		// echo $sql;

		// die();

		$result = DB::select($sql);

		$result = Goal::hydrate($result);

		foreach ($result as $data) {

			// Totales en Base de datos por concesionario
			$total_leads_dado_baja = self::get_total_status('dado de baja', [$data['id_leadmanager'], 0], ['leads.distributor_id', 'leads.flag_contactado_obc'], $type);		

			$vendidos_entrega = self::get_total_status('Entrega', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$vendidos_pedido = self::get_total_status('Pedido', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$no_compro = self::get_total_status('No compro', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$separacion = self::get_total_status('separacion o proceso de credito', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$credito = self::get_total_status('en seguimiento', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$sin_venta = self::get_total_status('Sin venta', $data['id_leadmanager'], 'leads.distributor_id', $type);
	        $total_leads_agendados = self::get_total_status('agendado', $data['id_leadmanager'], 'leads.distributor_id', $type);
	        $total_leads_no_asistio = self::get_total_status('no asistio', $data['id_leadmanager'], 'leads.distributor_id', $type);

			$total_leads_en_proceso = self::get_total_status('en proceso', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$total_leads_no_compro = self::get_total_status('no compro', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$total_leads_sin_venta = self::get_total_status('sin venta', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$total_leads_rechazado = self::get_total_status('rechazado', $data['id_leadmanager'], 'leads.distributor_id', $type);
			// $total_leads_contacted_obc = self::get_total_leads_contacted_obc();

			$total_ventas = $vendidos_pedido + $vendidos_entrega;
        	$total_efectivas = $total_ventas + $total_leads_en_proceso + $total_leads_no_compro + $total_leads_sin_venta + $total_leads_rechazado;


			$leads_contactados = self::get_leads_contacted($data['id_leadmanager'], $type);
			$leads_logrados_real = self::get_total_leads_of_month($data['id_leadmanager'], $type);
			$total_leads_contactado_obc = self::get_total_leads_contacted_obc($data['id_leadmanager'], $type);

			// $total_leads_contactado = $total_efectivas + $total_leads_agendados +$total_leads_no_asistio;
			/*$total_leads_contactado = $total_efectivas + 
									  $total_leads_agendados + 
									  $total_leads_no_asistio + 
									  $total_leads_contactado_obc;*/

			
			$total_leads_no_atiende = self::get_total_status('no atiende', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$total_leads_sin_gestionar = self::get_total_status('sin gestionar', $data['id_leadmanager'], 'leads.distributor_id', $type);
			$total_leads_datos_errados = self::get_total_status('datos errados', $data['id_leadmanager'], 'leads.distributor_id', $type);
	        $total_leads_no_contactados = $total_leads_no_atiende + $total_leads_datos_errados + $total_leads_dado_baja + $total_leads_sin_gestionar;
			//echo $total_leads_no_atiende." ===> ".$total_leads_datos_errados." ===> ".$total_leads_dado_baja." ===> ".$total_leads_sin_gestionar;die();
			//echo $leads_logrados_real;die();
				

			$data['Citas_efectivas_real'] = $vendidos_entrega + 
											$vendidos_pedido + 
											$no_compro + 
											$separacion + 
											$credito + 
											$sin_venta;
			

			$data['leads_logrados_real'] = $leads_logrados_real;
			
			$total_leads_contactado = $leads_logrados_real - $total_leads_no_contactados;

			$data['leads_contactados_real'] = $total_leads_contactado;
			$data['Ventas_objetivo'] = ($data['Total_mes_en_curso'] * 0.09);
			$data['Citas_efectivas_objetivo'] = ($leads_logrados_real * 0.1);
			$data['Leads_contactados_objetivo'] = ($leads_logrados_real * 0.7);
			$data['leads_objetivo'] = round(($data['Leads_contactados_objetivo']/0.7),2);

			// Calculos para el Excel

			$data['share'] = round((($data['Total_mes_en_curso']/$data['total']) * 100), 2);

			$data['Ventas_real'] = $vendidos_entrega + $vendidos_pedido;

			if ($total_leads_contactado == 0) {

				$data['porcentaje_citas_efectivas'] = 100;

			} else {

				$data['porcentaje_citas_efectivas'] = round((($data['Citas_efectivas_real'] / $total_leads_contactado)*100),2);
			}

			if ($data['leads_logrados_real'] > 0) {

				$data['porcentaje_contactabilidad'] = round((($data['leads_contactados_real'] / $data['leads_logrados_real'])*100),2);

			} else {

				$data['porcentaje_contactabilidad'] = 0;
			}

			


			$array_excel['id_leadmanager'] = $data['id_leadmanager'];
			$array_excel['Ciudad'] = $data['ciudad'];
			$array_excel['Concesionarios'] = $data['concesionario'];

			$array_excel['BP'] = $data['Total_mes_en_curso'];
			$array_excel['Suma_BP_Concesionarios'] = $data['total'];
			$array_excel['Share'] = $data['share'];
			//$array_excel['Leads_Logrados_objetivo'] = $data['leads_objetivo'];
			$array_excel['leads_Logrados_Real_Cumplido'] = $data['leads_logrados_real'];
			$array_excel['Leads_Contactados_objetivo'] = $data['Leads_contactados_objetivo'];
			$array_excel['Leads_Contactados_Real_Cumplido'] = $data['leads_contactados_real'];
			$array_excel['Porcentaje_Contactabilidad'] = $data['porcentaje_contactabilidad'];
			$array_excel['Agendados_Efectivos_Objetivo'] = $data['Citas_efectivas_objetivo'];
			$array_excel['Agendados_Efectivos_Real_Cumplido'] = $data['Citas_efectivas_real'];
			$array_excel['Porcentaje_Citas_Efectivas'] = $data['porcentaje_citas_efectivas'];
			$array_excel['Ventas_objetivo'] = $data['Ventas_objetivo'];
			$array_excel['Ventas_Real_Cumplido'] = $data['Ventas_real'];
			
			array_push(self::$data_report, $array_excel);
		}
	}

    public static function get_total_status($status, $filter, $field, $type = 'monthly') {

        $total = 0;

        $status = trim($status);

        $month = date('m');
        $year = date('Y');
        $day_ini = '01 00:00:00';
        $day_end = '31 23:59:59';

        $date_ini = $year."-".$month."-".$day_ini;
        $date_end = $year."-".$month."-".$day_end;     

        if ($status == 'sin gestionar' || $status == 'agendado' || $status == 'en proceso' || $status=='cerrado') {

              $total = self::get_count_status($status, $date_ini, $date_end, $field, $filter, $type);  

        } else {

              $total = self::get_count_reasons($status, $date_ini, $date_end, $field, $filter, $type);
        }

        return $total;
    }	

    public static function get_count_status($status, $date_ini, $date_end, $field, $filter, $type) {

    	$count = 0;

   		if ($type == 'annual') {

	        $count = Lead::join('reasons', function($join) {
	                       $join->on('reasons.id', '=', 'leads.reason_id');
	                    })
	                    -> join('status', function($join) {
	                       $join->on('status.id', '=', 'reasons.status_id');
	                    })
	                    -> where('status.name', '=',strtolower($status))
	                    -> where($field, '=', $filter) 
	                    -> whereYear('leads.created_at', '=', date('Y'))
	                    -> select('leads.id')->count();

	                    

   		} else {

	        $count = Lead::join('reasons', function($join) {
	                       $join->on('reasons.id', '=', 'leads.reason_id');
	                    })
	                    -> join('status', function($join) {
	                       $join->on('status.id', '=', 'reasons.status_id');
	                    })
	                    -> where('status.name', '=',strtolower($status))
	                    -> where($field, '=', $filter) 
	                    -> where('leads.created_at', '>=', $date_ini)
	                    -> where('leads.created_at', '<=', $date_end)
	                    -> select('leads.id')->count();
   		}

      return $count;

    }

    public static function get_count_reasons($status, $date_ini, $date_end, $field, $filter, $type) {

    	$count = 0;

   		if ($type == 'annual') {

   			if(!is_array($filter)){

		        $count = Lead::join('reasons', function($join) {
		                       $join->on('reasons.id', '=', 'leads.reason_id');
		                   })
		                -> where('reasons.name', '=',$status)
		                -> where($field, '=', $filter)
		                -> whereYear('leads.created_at', '=', date('Y'))
		                -> select('lead.id')->count();

		                
	                    

	        }else{

	        	$count = Lead::join('reasons', function($join) {
	                       $join->on('reasons.id', '=', 'leads.reason_id');
	                   })
	                -> where('reasons.name', '=', $status)
	                -> where($field[0], '=', $filter[0])
	                -> where($field[1], '=', $filter[1])
	                -> whereYear('leads.created_at', '=', date('Y'))
	                -> select('lead.id')->count();

	        }


   		} else {

   			if(!is_array($filter)){

		        $count = Lead::join('reasons', function($join) {
		                       $join->on('reasons.id', '=', 'leads.reason_id');
		                   })
		                -> where('reasons.name', '=',$status)
		                -> where($field, '=', $filter)
		                -> where('leads.created_at', '>=', $date_ini)
		                -> where('leads.created_at', '<=', $date_end)
		                -> select('lead.id')->count();

		    }else{
		    	$count = Lead::join('reasons', function($join) {
		                       $join->on('reasons.id', '=', 'leads.reason_id');
		                   })
		                -> where('reasons.name', '=',$status)
		                -> where($field[0], '=', $filter[0])
		                -> where($field[1], '=', $filter[1])
		                -> where('leads.created_at', '>=', $date_ini)
		                -> where('leads.created_at', '<=', $date_end)
		                -> select('lead.id')->count();
		    }
   		}      

        return $count;    
    }


    public static function get_leads_contacted($distributor_id, $type) {
        
        $month = date('m');
        $year = date('Y');
        $day_ini = '01 00:00:00';
        $day_end = '31 23:59:59';

        $date_ini = $year."-".$month."-".$day_ini;
        $date_end = $year."-".$month."-".$day_end;
        
        $sql = "select count(leads.id) as total from leads 
        		where 
        		leads.medio_contacto <> '' and 
        		leads.distributor_id = ".$distributor_id." 
        	";

        if ($type == 'monthly')	{

	        $sql.= " and leads.created_at >= '".$date_ini."' 
	        		 and leads.created_at <= '".$date_end."' ";
        } else {

        	$sql.= " and year(leads.created_at) = ".$year."";
        }

        $count = DB::select($sql);
        $count = Lead::hydrate($count)->first();
        
        return $count->total;
    }

    public static function get_total_leads_of_month($distributor_id, $type) {
        $month = date('m');
        $year = date('Y');

        $day_ini = '01 00:00:00';
        $day_end = '31 23:59:59';

        $date_ini = $year."-".$month."-".$day_ini;
        $date_end = $year."-".$month."-".$day_end;

        
        $sql = "select count(leads.id) as total from leads 
        		where 
        		leads.distributor_id = ".$distributor_id." 
        		";

        if ($type == 'monthly')	{

	        $sql.= " and leads.created_at >= '".$date_ini."' 
	        		 and leads.created_at <= '".$date_end."' ";
        } else {

        	$sql.= " and year(leads.created_at) = ".$year."";
        }

        //echo $sql;

        $count = DB::select($sql);
        $count = Lead::hydrate($count)->first();
        
        return $count->total;
    }

    public static function get_total_leads_contacted_obc($distributor_id, $type) {

        $month = date('m');
        $year = date('Y');

        $day_ini = '01 00:00:00';
        $day_end = '31 23:59:59';

        $date_ini = $year."-".$month."-".$day_ini;
        $date_end = $year."-".$month."-".$day_end;

        
        $sql = "select count(leads.id) as total from leads 
        		where 
        		leads.distributor_id = ".$distributor_id." 
        		";

        if ($type == 'monthly')	{

	        $sql.= " and leads.created_at >= '".$date_ini."' 
	        		 and leads.created_at <= '".$date_end."' ";
        } else {

        	$sql.= " and year(leads.created_at) = ".$year."";
        }

        $sql.= " and flag_contactado_obc = 1";

        $count = DB::select($sql);
        $count = Lead::hydrate($count)->first();
        
        return $count->total;
    }        
}