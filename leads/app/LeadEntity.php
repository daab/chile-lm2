<?php 

namespace App;

use DB;
use Mail;
use Config;

use App\Security;
use App\User;

use App\Models\TimeZone;
use App\Models\Domain;
use App\Models\Person;
use App\Models\Lead;
use App\Models\Conglomerate;
use App\Models\Distributor;
use App\Models\Brand;
use App\Models\User_rol;
use App\Models\Group_distributor;



class LeadEntity {

	private $security;

	public function __construct(){
		$this->security = new Security();
	}


	public function save($request){

		$mensaje = ['exito' => true, 'mensaje' => 'Lead guardado correctamente'];

		$token = $request->input('token');
		
		//Obtenemos el dominio desde donde nos envian los datos...
		$origin =  $this->getOriginDomain();

		//para verificar datos repetidos

		$emailValidate= $request->input('correo');

		$validar=Person::where('email', '=',  $emailValidate)
						->orderby('id', 'desc')
						->first();
		if (!is_null($validar)) {
			$hoy= new \DateTime(date('Y-m-d H:i:s'));
			$interval = date_diff($hoy, new \DateTime((string)$validar->created_at));
			$minutos = $interval->format('%i');
		}else{
			$minutos= 5;
		}

		//$validar = DB::select($sql);
		//$ultimoregistro=$validar[0]->diff;
			
		
		//Verificamos si tiene las www
		if(strpos($origin, 'www') === false ){
			$aux = explode('//', $origin);
			//Verificamos que sea un dominio y no una IP, en caso de ser una IP la dejamos igual
			if(count($aux) == 2){
				$origin = "http://www.".$aux[1];
			}
		}


		//Validamos que el dominio desde que viene la peticion sea el mismo del token
		$validate1 = Domain::where('dominio', '=', $origin)->orWhere('ip', '=', $origin)->first();
		$all=$request->all();
		if ($minutos >= 4) {
				
			//if((isset($validate1->id) && $validate1->token == $token) || $origin == 'http:\/\/www.audicolombia.com' || $origin == 'http://www.audicolombia.com'){
			if (true) {
				//Recibimos los datos del formulario
				$person = new Person();
					$person->name = $request->input('nombres');
	    			$person->last_name = $request->input('apellidos');
	    			$person->phone = $request->input('telefono');
	    			$person->email = $request->input('correo');
	    			$person->habeas_data = $request->input('acepto');
	    			$person->rut = $request->input('rut');
	    			
				$lead = new Lead();
				$lead->brand_id = 9;//$request->input('marca');
				$lead->model_id = $request->input('modelo');
				$lead->model_alter = $request->input('modelo2');
				//$lead->status_id = 1;  //Nuevo lead
				$lead->reason_id = 1;  //Sin Gestionar
				$lead->origen = $request->input('origen');
				$lead->link = $request->input('link');
				$lead->field1 = $request->input('campo1');
				$lead->field2 =$request->input('campo2');
				$lead->source = $request->input('source');
			
				if (is_null($request->input('purchase')) || $request->input('purchase') == "") {
					$lead->purchase_time = 0;
				}else{
					$lead->purchase_time = $request->input('purchase');
				}
				
				//Verificamos si el concesionario es un distribuidor...
				$aux = $this->isDistributor($request->input('concesionario'));
				
				//$lead->distributor_id = $aux->distributor_id;
				$lead->distributor_id = 35;
				//Es distribuidor debemos marcar en field2 para buscar el proximo
				if($aux->success){
					$lead->field2 = $aux->field2;
				}
				
				//Verificamos que el lead no se haya registrado mas de una vez...
				//if($this->validarLead($person->email, $lead->distributor_id)){

					//Asignar Asesor...
				
					$lead->assessor_id = $this->getAsesor($lead->distributor_id, $lead->brand_id);
					
					//Guardamos el lead
					$person->save(); 
					$lead->person_id = $person->id;
					//error_log("Guarda la persona");
					
					$lead->save(); 

					if ($lead->id != 0 && $person->id != 0) {
						$this->webservice($lead, $person);
						//$this->WebserviceCron();
					}
					//error_log("Intenta guardar el lead");
					
					//WS call Center...
					/*$this->WSCallCenter($lead->distributor_id, $lead, $person);*/

					//Envio de correo al asesor

					#QUITAR PARA SUBIR A PRODUCCION $this->sendContactAsesor($lead);
				//}
				
				

			}else{
				
				$mensaje = ['error' => false, 'mensaje' => 'Dominio no autorizado para guardar leads '.$origin];
				//var_dump($mensaje);
				error_log($mensaje['mensaje']);
			}
			//error_log('Proceso finalizado Lead creado correctamente');
			
		}else{
			$mensaje = ['exito' => false, 'mensaje' => 'Leads Repetido en muy corto tiempo'.$origin];
				//var_dump($mensaje);
				error_log($mensaje['mensaje']);
		}
		
		return json_encode($mensaje);

	}

	public static function Webservice($lead = "", $person=""){

		$datos = Lead::find($lead->id);
		$Ciudad = Distributor::find($lead->distributor_id);
		//var_dump($datos->marca['name'],$datos->model['name'],$datos->distributor['name'],$datos->distributor['city_id'], $Ciudad->city['name'] ); 
		if (empty($lead->field1)) {
			$vacio = "NULL NULL";
			$modeloFecha=explode(' ', $vacio);
		}else{
			$ModeloActual = (string)$lead->field1;
			$modeloFecha=explode(' ', $ModeloActual);
		}

		error_log('Incio envio del lead por Webservice');
		require 'vendor/nusoap/nusoap.php';
		$cliente = new \nusoap_client('http://localhost:8888/chile-lm2/leads/index.php/web');
		$error = $cliente->getError();
		if ($error) {
			var_dump("error!!");
		}
	
		/*$datosPrueba = array('datos_persona_lead' => 
						   array(
						   	'ID' => '1',
						   	'Marca'=>'stazPrueba',
						   	'Modelo'=>'stazPrueba',
						   	'Ciudad'=>'stazPrueba',
						   	'Concesionario'=>'stazPrueba',
						   	'Nombre'=>'stazPrueba',
						   	'Apellido'=>'stazPrueba',
						   	'Correo'=>'stazPrueba',
						   	'Telfono'=>'stazPrueba',
						   	'ModeloActual'=>'stazPrueba',
						   	'AgnoModeloActual'=>'stazPrueba',
						   	'Rut'=>'stazPrueba',
						   	'FechaCotizacion'=>'stazPrueba',
						   	'ViaContacto'=>'stazPrueba'
						   	 )
					    );*/
		$datosPrueba = array('datos_leads' => 
						   array(
						   	'ID' =>  $lead->id,
						   	'Marca'=>$datos->marca['name'],
						   	'Modelo'=>$datos->model['name'],
						   	'Ciudad'=>$Ciudad->city['name'],
						   	'Concesionario'=>$datos->distributor['name'],
						   	'Nombre'=>$datos->person['name'],
						   	'Apellido'=>$datos->person['last_name'],
						   	'Correo'=>$datos->person['email'],
						   	'Telefono'=>$datos->person['phone'],
						   	'ModeloActual'=>$modeloFecha[0],
						   	'AgnoModeloActual'=>$modeloFecha[1],
						   	'Rut'=>$datos->person['rut'],
						   	'FechaCotizacion'=>(string)$lead->created_at,
						   	'ViaContacto'=>$lead->field2
						   	 )
					    );
		$datos_leads = array( "datos_leads" => $datosPrueba);
    	$enviar= $cliente->call('leads',$datos_leads);
    	var_dump($cliente->call('leads',$datos_leads));die();
    	if ($enviar['FlagRespuesta'] == 0) {
    		error_log('error no hay respuesta'.$enviar['FlagRespuesta']);
    	}else{
    		$datos->id_ws = $resultado['FlagRespuesta'];
			$datos->save();
			error_log('leads recibido por el webservices');
    	}
	}

	public static function WebserviceCron(){

		$datos = Lead::where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                      ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                      ->get();
      foreach ($datos as $data) {

      		$Ciudad = Distributor::find($data->distributor_id);

			if (empty($data->field1)) {
				$vacio = "NULL NULL";
				$modeloFecha=explode(' ', $vacio);
			}else{
				$ModeloActual = (string)$data->field1;
				$modeloFecha=explode(' ', $ModeloActual);
			}

			error_log('Incio envio del lead por Webservice');
			require 'vendor/nusoap/nusoap.php';
			$cliente = new \nusoap_client('http://www.porschecl.cl/BUC/awscotizador.aspx');
			$error = $cliente->getError();

			if ($error) {
				var_dump("error!!");
			}
			$fechaCotizacion =(string)$data->created_at;

			$datos_leads = array('datos_persona_lead' => 
						   array(
						   	'ID' =>  $data->id,
						   	'Marca'=>$data->marca['name'],
						   	'Modelo'=>$data->model['name'],
						   	'Ciudad'=>$Ciudad->city['name'],
						   	'Concesionario'=>$data->distributor['name'],
						   	'Nombre'=>$data->person['name'],
						   	'Apellido'=>$data->person['last_name'],
						   	'Correo'=>$data->person['email'],
						   	'Telfono'=>$data->person['phone'],
						   	'ModeloActual'=>$modeloFecha[0],
						   	'AgnoModeloActual'=>$modeloFecha[1],
						   	'Rut'=>$data->person['rut'],
						   	'FechaCotizacion'=>$fechaCotizacion,
						   	'ViaContacto'=>$data->field2
						   	 )
					    );
			
	    	$enviar= $cliente->call('SDTCotizacion.SDTCotizacionItem',$datos_leads);

	    	if ($enviar['FlagRespuesta'] == 0) {
	    		error_log('error no hay respuesta'.$enviar['FlagRespuesta']);
	    	}else{
	    		$datos->id_ws = $resultado['FlagRespuesta'];
				$datos->save();
				error_log('lead recibido por el webservices');
	    	}
   	 	}
   	 	die();
	}

	private function WSCallCenter($distribuidor = "", $lead = "", $person = ""){

		$concesionarios = [0];

		if (in_array($distribuidor, $concesionarios)) {
			error_log("Inicio WS envio del lead");	
			require 'vendor/nusoap/nusoap.php';
			$cliente = new \nusoap_client("e");
			$err = $cliente->getError();

			if ($err) {
				error_log('Error conexion webservice (Call Center): ' . $err);
			}

			//Hack siempre enviar datos en campaña para el WS
			if ($lead->source == "") {
				$lead->source = "Lead sin campana definida";
			}
			//Hack siempre enviar un dato valido
			if(strtolower($lead->origen) != 'cotizador' && strtolower($lead->origen) != "facebook" && strtolower($lead->origen) != "cotizador_tiguan"){
				$lead->origen = "Cotizador";
			}

			$datos_persona_entrada = array("datos_persona_entrada" => 
						array(
							'id' => strval($lead->id),
							'origen' => strtolower($lead->origen),
							'campania' => $lead->source,
							'modelo' => strval($lead->model_id),
							'concesionario' => strval($lead->distributor_id),
							'marca' => strval($lead->brand_id),
							'nombres' => $person->name,
							'apellidos' => $person->last_name,
							'telefono' => strval($person->phone),
							'correo' => $person->email,
							'user_id' => $lead->assessor_id,
							'link' => $lead->link,
							'reason_id' => 1,
							'created_at' => $lead->created_at,
						)
			);

			$resultado = $cliente->call('crear_lead',$datos_persona_entrada);

			if ($resultado['estado'] == 0 && isset($resultado['error'])) {
				error_log($resultado['error']['name']." ".$resultado['error']['description']);
			} else {

				$lead->id_ws = $resultado['id_tercero'];
				$lead->save();

				error_log("Webservice ( WSCallCenter ) consumido correctamente, id: ".$lead->id." id_tercero ".$lead->id_ws);
			}
			

			
		}

		return true;
	}


	private function sendContactAsesor($lead){
		
		$blade='newLead';
		$asesor = "";
		$emailConglomerate = Group_distributor::join('conglomerates', function($join){
								$join->on('conglomerates.id','=', 'brand_distributor.conglomerate_id');
												})
								->where('brand_distributor.distributor_id', '=',$lead->distributor_id)
								->get();
		
		//Verificamos si el lead tiene un asesor asignado

		if(is_null($lead->assessor_id) || empty($lead->assessor_id) ){
			//Enviar correo a root
			$blade='newLeadError';
			$data['correos'] = 'david@daab.co';

		}elseif (is_numeric($lead->person_id) && $lead->person_id > 0) {

			$asesor = User::find($lead->assessor_id);
			$data['correos'] = explode(",",$asesor->email);
			$data['nombre'] =  $asesor->name;
		}
		//Datos del correo
		$data['asunto'] = "Nueva Cotización #".$lead->id." ( ".$lead->marca->name." )";
		$data['link'] = "https://leads-management-cl.com/gestionar/".$lead->id."/gestion";
	

		//Envio a conglomerado
		if (!is_null($emailConglomerate[0]->email)) {
				$data['correosa'] = explode(",",$emailConglomerate[0]->email);
				$data['nombres'] =  $emailConglomerate[0]->name;

				Mail::send('emails.newLeadConglomerado', ['data' =>  $data], function ($message) use ($data) {
				$message->from('noreply@leadman.cl', 'Cotizador Porsche');
				$message->subject($data['asunto']);
				$message->to($data['correosa']);
				});
		}
		//Envia al asesor
		Mail::send('emails.'.$blade, ['data' =>  $data], function ($message) use ($data) {
			$message->from('noreply@leadman.cl', 'Cotizador Porsche');
			$message->subject($data['asunto']);
			$message->to($data['correos']);
		});
	}
	
	private function sendContactUser($lead){
		
		//Pruebas
		//$lead = Lead::find(6);
		
		//Buscamos los correos que hay registrados en el concesionario puede ser mas de uno separado por coma
		$concesionario  = Distributor::where('id','=',$lead->distributor_id)->first();
		

		$data['asunto'] = "Contacto VOLKSWAGEN";
		$data['correos'] = $lead->person->email;
		$data['concesionario'] = $concesionario->name;
		$data['nombre'] = $lead->person->name." ".$lead->person->las_name;

		/*Mail::send('emails.mailing-welcome', ['data' =>  $data], function ($message) use ($data) {
			$message->from('noreply@leadman.co', 'Lead Manager');
			$message->subject($data['asunto']);
			$message->to($data['correos']);
		});*/
	}


	private function isDistributor($id){

		$response = array('field2' => "", 'distributor_id' => $id, 'success' => false);
		$distribuidor = Conglomerate::where('distributor_id', '=', $id)->first();
	
		if (isset($distribuidor->id)) {
				
			//Existe un distribuidor, cambiamos el concesionario
			$conglomerado = Group_distributor::where('conglomerate_id', '=',$distribuidor->id)->get();
			$concesionarios = array();
			foreach ($conglomerado as $distr) {
				array_push($concesionarios, $distr->distributor_id);
			}

			//Buscamos el ultimo concesionario al que se le repartio
			$lastLead = Lead::where('field2', '=', $distribuidor->concesionario->name)->orderBy('id', 'desc')->first();
			
			$index = -1;
			if ($lastLead !== NULL) {
				$index = array_search($lastLead->distributor_id, $concesionarios);
			}
			    
			if($index == count($concesionarios) - 1){
			    //Volvemos al primero
			    $index = 0;
			}else{
			    //Tomamos el siguiente
			    $index++;
			}
			    
			//Marcamos el lead como del concesionario papa...
			$response['field2'] = $distribuidor->concesionario->name;
			//Lo enviamos al nuevo concesionario
			$response['distributor_id'] = $concesionarios[$index];
			$response['success'] = true;

		}


		return (object)$response;
	}

	public  function getAsesor($concesionario, $marca){
	    date_default_timezone_set('America/Santiago');
	    $asesor_id = null;
	    $concesionario = 35;
		$user= User_rol::join('users', function($join){
						$join->on('user_rol.user_id', '=', 'users.id');
						})
						->where('user_rol.distributor_id', '=', $concesionario )
						->where('users.capacidad', '>=', 0)
						->where('user_rol.rol_id', '=', 5)
						->get();
		$userU= User_rol::join('users', function($join){
						$join->on('user_rol.user_id', '=', 'users.id');
						})
						->where('user_rol.distributor_id', '=', $concesionario )
						->where('users.capacidad', '>', 0)
						->where('user_rol.rol_id', '=', 5)
						->get();

		$siguienteActivo=[];
		$primerCupo=null;
		$ningunTurno=0;
		$ningunCupo=0;
		$ningunCapacidad=0;
		$totalU=0;
		$dobleTurno= null;
		$unicoUser= 0;


		//unico asesor
		$totalU=count($user);

			foreach ($user as $key) {
				switch ($key->turno) {
					case '0':

						//siguiente persona y tiene cupo disponible para recibir lead
						if ($key->capacidad != 0 && $key->cupo < $key->capacidad ) {

							if (!is_null($primerCupo) && $primerCupo == $key->cupo)  {
								$primerCupo = $key->cupo;	
								$siguienteActivo = ['siguienteC'=>$key->user_id];
							}

							if (!is_null($primerCupo) && $primerCupo >= $key->cupo)  {
								//se guarda el cupo"valor" de persona para decir si es el menor cupo
								$primerCupo = $key->cupo;	
								$siguienteActivo = ['siguienteC'=>$key->user_id];
							}

							if (!is_null($primerCupo) && $primerCupo < $key->cupo)  {
								$siguienteActivo = ['siguienteC'=>$key->user_id];
							}

							//primera vez
							if (is_null($primerCupo))  {
								$primerCupo = $key->cupo;
								$siguienteActivo = ['siguienteC'=>$key->user_id];
							}
						}

						if ($key->capacidad > 0 && $key->cupo >= $key->capacidad) {
							$ningunCupo++;
						}
						//unico user
						if ($key->capacidad > 0 && $key->cupo < $key->capacidad && count($userU) == 1) {
							$unicoUser++;
						}
						if ($key->capacidad > 0 && $key->cupo >= $key->capacidad && count($userU) == 1) {
							$unicoUser++;
						}

						if ($key->capacidad == 0 ) {
							$ningunCapacidad++;
						}

						if ($key->turno == 0) {
							$siguienteActivo = ['siguienteC'=>$key->user_id];
							$ningunTurno++; //si todos los usauarios son 0 se habilita el primero y se agrega datos
						}

					break;
					case '1':

						//cuando alguien tiene el turno
						if ($key->turno == 1 && $key->capacidad != 0 && $key->cupo < $key->capacidad) {

							
							if (is_null($dobleTurno)) {
								$asignarCupo = User::where('id', '=', $key->user_id)
											->first();
								$asignarCupo->cupo = $asignarCupo->cupo + 1;
								$asignarCupo->turno = 0;
								$asignarCupo->save();
								$asesor_id = $key->user_id;
								$dobleTurno = $key->user_id;
							}
								//unico asesor con capacidad	
							if (is_null($dobleTurno) && count($userU)== 1) {
								$asignarCupo = User::where('id', '=', $userU[0]->user_id)
											->first();
								$asignarCupo->cupo = $asignarCupo->cupo + 1;
								$asignarCupo->turno = 1;
								$asignarCupo->save();
								$asesor_id = $key->user_id;
								$dobleTurno = $key->user_id;
							}

							//por si hay mas de un asesro con turno activo
							if (!is_null($dobleTurno) &&  $key->turno == 1) {
								$asignarCupo = User::where('id', '=', $key->user_id)
													->first();
								$asignarCupo->turno = 0;
								$asignarCupo->save();
							}

							//$turnoActivo['activo']=$key->user_id;
							//array_push($turno, $turnoActivo);
						}

						if (is_null($dobleTurno) && $key->turno == 1 && $key->capacidad != 0 && $key->cupo >= $key->capacidad) {
							$asignarCupo = User::where('id', '=', $key->user_id)
												->first();
									$asignarCupo->turno = 0;
									$asignarCupo->save();

							if (is_null($dobleTurno) && count($userU)== 1) {
									$asignarCupo = User::where('id', '=', $userU[0]->user_id)
												->first();
									$asignarCupo->cupo =  1;
									$asignarCupo->turno = 1;
									$asignarCupo->save();
									$asesor_id = $key->user_id;
									$dobleTurno = $key->user_id;
							}
							$ningunCupo++;
						}

						if (is_null($dobleTurno) && $key->turno == 1 &&  $key->capacidad == 0) {
							$ningunCapacidad++;
						}
						//valida si hay varios turnos repetido
						if (!is_null($dobleTurno) && $key->turno == 1 && $key->capacidad != 0 && $key->cupo >= $key->capacidad) {
							$asignarCupo = User::where('id', '=', $key->user_id)
												->first();
									$asignarCupo->turno = 0;
									$asignarCupo->save();
							$ningunCupo++;
						}

						if (!is_null($dobleTurno) && $key->turno == 1 &&  $key->capacidad == 0) {
							$ningunCapacidad++;
						}

					break;

					default:
						# code...
					break;
				}
			}
			//unico user sin turno
			if ($unicoUser == 1 ) {

				$activarC = User::where('id', '=', $userU[0]->user_id)
								->first();

				$activarC->cupo = 1;
				$activarC->turno = 1;
				$activarC->save();
				$asesor_id = $activarC->id;
			}
			//si unico asesor no tiene capacidad
			if ($unicoUser == 1 && $userU[0]->capcidad == 0) {

				$activarC = User::where('id', '=', $userU[0]->user_id)
								->first();

				$activarC->cupo = 1;
				$activarC->capacidad = 4;
				$activarC->turno = 1;
				$activarC->save();
				$asesor_id = $activarC->id;
				//esto debe ir con el correo electronico
			}


			//si todos los cupos estan llenos
			if ($ningunCupo == $totalU) {
				foreach ($user as $users) {
					$resetearCupos = User::where('id', '=', $users->user_id)
									->first();
					$resetearCupos->cupo = 0;
					$resetearCupos->save();
				}

				$activarT = User::where('id', '=', $user[0]->user_id)
									->first();
				$activarT->cupo = $activarT->cupo +1;
				$activarT->turno = 1;
				$activarT->save();	
				$asesor_id = $user[0]->user_id;
			}

					//si no hay turnos activos
			if ($ningunCupo < $totalU && $ningunTurno == $totalU && !empty($siguienteActivo)) {
				
				$agregaCupo = User::where('id', '=', $siguienteActivo['siguienteC'])
									->first();
				$agregaCupo->cupo = $agregaCupo->cupo + 1;
				$agregaCupo->save();
				$asesor_id = $agregaCupo->id;
			}

			//si no hay capacidad 
			if ($ningunCapacidad == $totalU) {

				foreach ($user as $users) {
					$activarCapcaidad = User::where('id', '=', $users->user_id)
											->first();
					$activarCapcaidad->capacidad = 4;
					$activarCapcaidad->cupo = 0;
					$activarCapcaidad->save();
				}

				$Activar = User::where('id', '=', $user[0]->user_id)
									->first();
				$Activar->cupo = $Activar->cupo + 1;
				$Activar->turno =  1;
				$asesor_id = $Activar->id;
				$Activar->save();
				//datos para enviar correo
				/*$correo = Manager::where('brand_id','=', $concesionario)
								 ->get();

				$agregar = "";
				foreach ($correo as $correos) {

					$agregar.="". str_replace(",", ",", $corre->email).",";
				}

				$data['correos']  = trim($agregar, ',');
				$data['correo'] = explode(",", $data['correos']);
				$data['asunto']  = "Error en poderacion de asesores".date('Y-m-d');

				Mail::send(['text' => $data['correos']], function($message) use($data){
					$message->from('noreply@cotizadoronline.com.co', 'LM Chile');
	                $message->subject($data['asunto']);
	                $message->to($data['correo']);
				});*/

				error_log("No hay capacidad para ingresar nuevos cupos, se debe ingresar capacidad para asesores");
				var_dump("aqui va el errror log no hay capacidad!!");
			}

			//habilita el siguiente turno si total capacidad es menor al total de usuarios
			if ($ningunCapacidad < $totalU && !empty($siguienteActivo) && $totalU == 2) {

				$siguienteTurno= User::where('id', '=', $siguienteActivo['siguienteC'])
									->first();
				$siguienteTurno->turno = 1;
				$siguienteTurno->save();
				var_dump("Siguiente");
			}
			
			//cuando son mas de 2 asesores
			if ($ningunCapacidad < $totalU && !empty($siguienteActivo) && $totalU > 2){

				$siguienteTurno= User::where('id', '=', $siguienteActivo['siguienteC'])
									->first();
				$siguienteTurno->turno = 1;
				$siguienteTurno->save();
				$asesor_id = $siguienteTurno->id;
				var_dump("Siguiente mayor 2 asesores");
			} 
		//si no hay capacidad 

		
		
		/*//buscamos el ultimo lead
		$leads = Lead::where('distributor_id', '=', $concesionario)
						->where('brand_id', '=', $marca)
						->where('assessor_id', '<>', 0)
						->orderby('id', 'desc')->take(1)->get();
		
		if (count($leads) > 0) {
			$lead = $leads[0];
		}else{
			$lead = new Lead();
		}

		$aux = DB::select( DB::raw("SELECT user_rol.user_id as asesor, user_rol.brand_id as brand FROM rol, user_rol WHERE rol.name='Asesor' and user_rol.distributor_id=".$concesionario) );
			
		
		$asesores = array();
		foreach ($aux as $key) {
			if ($key->brand == $marca){
				array_push($asesores, $key->asesor);
			}
		}

		//No hay asesores de la marca pero si hay asesores en el concesionario
		if (count($asesores) == 0 && count($aux) > 0) {
			foreach ($aux as $key) {
				array_push($asesores, $key->asesor);
			}
		}

		$numAsesores = count($asesores);
		if ($lead->user_id > 0 && $numAsesores > 0 ) {
			//Buscamos el siguiente asesor...
			$encontrado = false;
			
			
			foreach ($asesores as $asesor) {
				if($encontrado){
					$asesor_id = $asesor;
					$encontrado = false;
					break;
				}

				if ($asesor == $lead->user_id) {
					$encontrado = true;
				}
			}
			
		
			//$asesor_id = "" El ultimo asesor ya no esta activo...
			if ($encontrado || $asesor_id == "") {
				//El asesor encontrado fue el ultimo, debemos seleccionar el primero
				$asesor_id = $asesores[0];
			}


		}elseif($numAsesores > 0 && $lead->user_id == ""){
			//NO tiene asesor...
			$asesor_id = $asesores[0];
		}else{
			//echo "Este concesionario no tiene asesores configurados ".$numAsesores;
			$mensaje = "El concesionario ".$concesionario." no tiene asesores configurados para la marca ".$marca;
			error_log($mensaje);
		}*/
     	
		var_dump($asesor_id);
     	return $asesor_id;
	}

	public function validarLead($email, $concesionario){
		
		$validacion = false;
		
		
		//Buscamos todos los leads con estado sin gestionar y estado revisar = 0
		$leads = DB::select( DB::raw("SELECT leads.created_at, leads.id FROM persons, leads WHERE persons.id=leads.person_id and email='".$email."' and distributor_id=".$concesionario) );
		
		if(count($leads) == 0){
			//Es la primera vez que el lead envia datos
		    $validacion = true;
		}else{
		    
		    date_default_timezone_set('America/Santiago'); 
		    $hoy = new \DateTime(date('Y-m-d'));
		    
		    $lead = $leads[0];
		    $aux = explode(' ', $lead->created_at);
			$fecha_lead = new \DateTime($aux[0]);
			$interval = date_diff($fecha_lead, $hoy);
			
			
			if ($interval->format('%a') > 90) {
				//El lead lleva mas de 3 meses sin pedir info
			    $validacion = true;
			}else{
			    error_log("El usuario pero no lleva mas de 3 meses desde que pidio cotizacion al mismo concesionario".$lead->id);
			 	$validacion = false;
			}
		}
		
        return $validacion;
		
	}

	private function getOriginDomain(){
		if (array_key_exists('HTTP_ORIGIN', $_SERVER)) {
			$origin = $_SERVER['HTTP_ORIGIN'];
		}
		else if (array_key_exists('HTTP_REFERER', $_SERVER)) {
		    $origin = $_SERVER['HTTP_REFERER'];
		} else {
		    $origin = $_SERVER['REMOTE_ADDR'];
		}



		return $origin;
		//return '127.0.0.1';

	}


}