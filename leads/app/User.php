<?php 

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Lead;
use App\Models\TimeZone;

use App\iLeads;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract, iLeads {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	// protected $fillable = ['nombres', 'apellidos', 'rol','email', 'marca', 'concesioanrio','password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function roles() {

		return $this->belongsToMany('App\Models\Rol', 'user_rol');
	}

	public function rol() {

		return $this->hasOne('App\Models\User_rol');
	}

    public function brand()
    {
        return $this->belongsToMany('App\Models\Brand', 'user_rol');
    }

    public function distributor()
    {
        return $this->belongsToMany('App\Models\Distributor', 'user_rol');
    }

    public function conglomerate()
    {
        return $this->belongsToMany('App\Models\Conglomerate', 'user_rol');
    }         

	public function toListLeads($case='today', $params) {			

		$sql = $this->toQuery($case, $params);
		$result = DB::select($sql);
		return Lead::hydrate($result);
	}

	private function toQuery($case='today', $params) {

		switch ($case) {

			case 'today':
				return $this->sql_leads($params);
			break;

			case 'status':
				return $this->sql_leads($params, 'status');
			break;

			case 'all':
				return $this->sql_leads($params, 'all');
			break;

			case 'calendar':
				return $this->sql_leads($params, 'calendar');
			break;						
		}
	}

	private function sql_leads($params, $case = 'today'){

		$sql = "";

		switch ($case) {			

			case 'today':			
				$sql .= "
						select * from 
						(
						select 
                                                        l.id,
							l.brand_id,
							l.commission,
							l.created_at,
							l.origen,
							l.field1,
							l.field2,
                                                        l.source,
							l.medio_contacto,
							p.name as nombres,
							p.last_name as apellidos,
							mo.name as modelo_actual,
							di.name as concesionario,
							di.name as nombre_concesionario,
							e.name as nombreEstado, 
							m.name as motivo, 
							u.name as asesor, 
							e.color as color,
							r.date as fechaAgenda, 
							r.time as horaAgenda					
						from leads as l 
							inner join persons p on p.id = l.person_id
							inner join models mo on mo.id = l.model_id
							inner join reasons m on m.id = l.reason_id
							inner join status e on e.id = m.status_id
							inner join distributors di on di.id = l.distributor_id and di.id = ".Auth::user()->distributor()->first()->id." 
							left join users u on u.id = l.assessor_id	
							left join reminders r on r.lead_id = l.id and r.date >= CURDATE()	
						where 
								l.assessor_id = '".Auth::user()->id."' and  
								month(l.created_at) = ".$params['month']." and 
								year(l.created_at) = ".$params['year']." and 
								day(l.created_at) = ".$params['day']."

						UNION

						select
                                                        l.id, 
						        l.brand_id,
							l.commission,
							l.created_at,
							l.origen,
							l.field1,
							l.field2,
							l.source,
							l.medio_contacto,
							p.name as nombres,
							p.last_name as apellidos,
							mo.name as modelo_actual,
							di.name as concesionario,
							di.name as nombre_concesionario,
							e.name as nombreEstado, 
							m.name as motivo, 
							u.name as asesor, 
							e.color as color,
							r.date as fechaAgenda, 
							r.time as horaAgenda									
						from leads as l 
							inner join persons p on p.id = l.person_id
							inner join models mo on mo.id = l.model_id
							inner join reasons m on m.id = l.reason_id
						        inner join status e on e.id = m.status_id
						        inner join distributors di on di.id = l.distributor_id	 and di.id = ".Auth::user()->distributor()->first()->id." 				        
						        left join users u on u.id = l.assessor_id	
						        inner join reminders r on r.lead_id = l.id and r.date >= CURDATE()	
						where 
						        l.assessor_id = '".Auth::user()->id."' and  
						        month(r.date) = ".$params['month']." and 
						        year(r.date) = ".$params['year']." and 
						        day(r.date) = ".$params['day']."
						        and lower(e.name) <> 'agendado'
						) a  
                                                  order by created_at desc     								
						";				
			break;

			case 'status':

				if (strtolower(trim($params['value'])) == 'agendado') {

                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    u.id = '".Auth::user()->id."' and
                    r.date >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id IN (8,11,12,13,14,15)
                    ) a order by created_at desc;
                    ";
				} else if (strtolower(trim($params['value'])) == 'en proceso'){
                	//echo "<pre>";print_r($params['value']);die();
                    
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    u.id = '".Auth::user()->id."' and
                    r.date >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id IN (11,15,16)
                    ) a order by created_at desc;
                    ";
                } else if (strtolower(trim($params['value'])) == 'sin gestionar'){
                	//echo "<pre>";print_r($params['value']);die();
                    //$sql.= "CALL getLeadsStatus_Asesor('".strtolower(trim($params['value']))."', ".Auth::user()->id.")";
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    u.id = '".Auth::user()->id."' and
                    l.created_at >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id = 1
                    ) a order by created_at desc;
                    ";
                }

               		
			break;

			case 'all':

				$sql .= "
						select 
                                                l.id,
						l.brand_id,
						l.commission,
						l.created_at,
						l.origen,
						l.field1,
						l.field2,
                                                l.source,
						l.medio_contacto,
						p.name as nombres,
						p.last_name as apellidos,
						mo.name as modelo_actual,
						mo.id as modelo_id,
						di.name as concesionario,
						di.id as concesionario_id,												
						e.name as nombreEstado, 
						m.name as motivo, 
						u.name as asesor, 
						e.color as color,
						r.date as fechaAgenda, 
						r.time as horaAgenda  					 
						from leads as l 
						inner join persons p on p.id = l.person_id
						inner join models mo on mo.id = l.model_id						
						inner join reasons m on m.id = l.reason_id 
						inner join status e on e.id = m.status_id
						inner join distributors di on di.id = l.distributor_id						
						left join users u on u.id = l.assessor_id
						left join reminders r on r.lead_id = l.id and r.date >= CURDATE()					
						where 
						l.assessor_id = '".Auth::user()->id."' and  
						l.created_at >= '".$params['value']."'
						order by l.created_at desc
					";
			break;

			case 'calendar':

				$sql .= "
						select 
                                                l.id,
						l.brand_id,
						l.commission,
						l.created_at,
						l.origen,
						l.field1,
						l.field2,
                                                l.source,
						l.medio_contacto,
						p.name as nombres,
						p.last_name as apellidos,
						mo.name as modelo_actual,
						mo.id as modelo_id,	
						di.name as concesionario,
						di.id as concesionario_id,											 
						e.name as nombreEstado, 
						m.name as motivo, 
						u.name as asesor, 
						e.color as color,
						r.date as fechaAgenda, 
						r.time as horaAgenda  					 
						from leads as l 
						inner join persons p on p.id = l.person_id
						inner join models mo on mo.id = l.model_id
						inner join reasons m on m.id = l.reason_id
						inner join status e on e.id = m.status_id
						inner join distributors di on di.id = l.distributor_id						
						left join users u on u.id = l.assessor_id
						left join reminders r on r.lead_id = l.id and r.date >= CURDATE()					
						where 
						l.assessor_id = '".Auth::user()->id."' and  
						month(l.created_at) = ".$params['month']." and 
						year(l.created_at) = ".$params['year']."
					";
			break;						
			
		}

         // echo "<pre>";
	 // echo $sql;
         // die();

		return $sql;
	}
}
