<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;


use App\Models\Distributor;
use App\Models\Conglomerate;
use App\Models\TimeZone;
use App\Models\Brand;
use App\Models\Lead;
use App\Models\Rol;

use App\User;
use App\Security;

class ActionsRol extends Model {

	private $object;
	private $params_date;

	public function __construct() {
		
		//Configuracion regional
		$time_zone = TimeZone::all()->first();

		date_default_timezone_set($time_zone->zone);
				
		$today = new \DateTime(date('Y-m-d H:i:s'));		
		
		$current_day = $today->format('Y-m-d');		
		
		$current_day = explode('-', $current_day);

		$this->params_date = ['day' => $current_day[2], 'month' => $current_day[1], 'year' => $current_day[0] ];
	}

	public function getHome($role) {

		if ($role->name == 'root')

			return 'root';

		return 'mobile.test.default';
	}

	public function setObject($role) {

		switch (strtolower($role->name)) {

			case 'marca':
				$this->object = new Brand();
			   break;

			case 'conglomerado': 
				$this->object = new Conglomerate();
			   break;

			case 'concesionario': 

					$this->object = new Distributor();
					// $this->object->asesores = User::where('concesionario', '=', Auth::user()->concesionario)->get();	
				break;

			case 'asesor': // Asesor
					$this->object = new User();					
				break;
		}		
	}

	public function toListLeads() {

		// return $this->object->toListLeads('today', $this->params_date);
                return $this->object->toListLeads('status', ['value' => 'agendado']);
	}

	public function generateUrlManageLeads($leads, $source="none") {

		$security = new Security();

		 foreach ($leads as $lead) {

			//$lead->apciones = url('/gestionar', [$security->encrypt($lead->id), $source]);
                       $lead->apciones = url('/gestionar', [$lead->id, $source]);
		}
	}

	public function filterLeads($options) {

		if (isset($options['case'])) {

			switch ($options['case']) {

				case 'today':				
						return $this->object->toListLeads('today', $this->params_date);
					break;

				case 'all':

						$numero_mes = 3;
						$numero_mes = '-'.$numero_mes.' month';
						$numero_mes_antes = date('Y-m-d', strtotime($numero_mes));

						$month = date('m');
						$year = date('Y');
						$numero_mes_antes = date('Y-m-d', mktime(0,0,0, $month, 1, $year));

						return $this->object->toListLeads('all', ['value' => $numero_mes_antes]);

					break;						
				
				case 'status':
						return $this->object->toListLeads('status', $options);
					break;

				case 'calendar':
						return $this->object->toListLeads('calendar', $options);
					break;					
			}	
		}
	}

	public function getObject() {

		return $this->object;
	}
}
