<?php 

namespace App;

class Security {

	public static function encrypt($string) {

		$secret_word = 'FiGuX_76740';

	    $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

	    $iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_DEV_URANDOM );

	    mcrypt_generic_init($td, $secret_word, $iv);

	    $encrypted_data_bin = mcrypt_generic($td, $string);

	    mcrypt_generic_deinit($td);

	    mcrypt_module_close($td);

	    $encrypted_data_hex = bin2hex($iv).bin2hex($encrypted_data_bin);

	    return $encrypted_data_hex;
	}


	public static function decrypt($string) {
		
		$secret_word = 'FiGuX_76740';

	    $td = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '', MCRYPT_MODE_CBC, '');

	    $iv_size_hex = mcrypt_enc_get_iv_size($td)*2;

	    $iv = pack("H*", substr($string, 0, $iv_size_hex));

	    $encrypted_data_bin = pack("H*", substr($string, $iv_size_hex));

	    mcrypt_generic_init($td, $secret_word, $iv);

	    $decrypted = mdecrypt_generic($td, $encrypted_data_bin);

	    mcrypt_generic_deinit($td);

	    mcrypt_module_close($td);

	    return $decrypted;
	}	
}