<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use App\Models\Status;
use App\Models\Reason;
use App\Models\Phase;
use App\User;

class StatusController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	
	{
		$status = Status::all();

		return view('root.estados.default')
				->with(compact('status'));
	}

	public function create($id = "") {

		$status = "";

		$phases = Phase::all();

		if (!empty($id)) {

			$status = Status::find($id);
		}

		return view('root.estados.form')
				->with(compact('phases'))
				->with(compact('status'));
	}

	public function save(Request $requets) {

		$params = $requets->all();

		$status = new Status;

		if (!empty($params['id'])) {

			$status = Status::find($params['id']);

			// Reason::where('status_id', $status->id )->delete();
		}

		$status->name = $params['name'];
		$status->color = $params['color'];
		$status->phase_id = $params['phase'];
		$status->title = $params['title'];		
		$status->is_initial = NULL;
		$status->is_final = 0;

		if (isset($params['es_inicial'])) {

			$status->is_initial = 1;
		}

		if (isset($params['es_final'])) {

			$status->is_final = 1;
		}

		$status->save();

		if ($status->id > 0) {

			$rate = "";
			$name = "";
			$sw_reminder = 0;
			$sw_commission = 0;
			$web_services = 0;
			$sw_option = "";

			for ($i = 1; $i < sizeof($params); $i++) {

				if (isset($params['reason_'.$i])) {

					$name = $params['reason_'.$i];

					if (isset($params['rate_'.$i])) {

						$rate = $params['rate_'.$i];
					}

					if (isset($params['sw_reminder_'.$i])) {

						$sw_reminder = 1;
					}

					if (isset($params['sw_commission_'.$i])) {

						$sw_commission = 1;
					}

					if (isset($params['web_services_'.$i])) {

						$web_services = 1;
					}

					if (isset($params['sw_option_'.$i])) {

						$sw_option = $params['sw_option_'.$i];
					}					


					$reason = new Reason;

					if (isset($params['reason_id_'.$i]) && $params['reason_id_'.$i] > 0) {

						$reason = Reason::find($params['reason_id_'.$i]);
					}

					

					// $reason->id = $params['reason_id_'.$i];
					$reason->status_id = $status->id;
					$reason->name = $name;
					$reason->rate = $rate;
					$reason->show_reminder = $sw_reminder;
					$reason->show_commission = $sw_commission;
					$reason->web_services = $web_services;
					$reason->sw_option = $sw_option;

					$reason->save();

					$rate = "";
					$name = "";
					$sw_reminder = 0;
					$sw_commission = 0;
					$web_services = 0;
					$sw_option = "";										
				} 
				
			}

		}		

		return redirect('/root/estados');
	}

	public function delete(Request $requets) {

		$params = $requets->all();

		if (isset($params['id'])) {

			$id = $params['id'];
			$reason = Reason::find($id);
			$reason->delete();
		}

		return redirect('/root/estados');
	}
}