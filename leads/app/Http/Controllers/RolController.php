<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use App\Models\Rol;
use App\Models\Module;
use App\User;

class RolController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	
	{
		$roles = Rol::all();

		return view('root.roles.default')
				->with(compact('roles'));
	}

	public function create($id = "") {

		$role = "";

		if (!empty($id)) {

			$role = Rol::find($id);
		}

		return view('root.roles.form')
				->with(compact('role'));
	}

	public function save(Request $requets) {

		$role = new Rol;

		$param = $requets->all();

		if (!empty($param['id'])) {

			$role = Rol::find($param['id']);			
		}

		$role->name = $param['name'];
		$role->description = $param['description'];

		$role->save();

		return redirect('/root/roles');
	}

	public function delete($id = "") {

		if (!empty($id)) {
			
			Rol::destroy($id);
		}

		return redirect('/root/roles');
	}

	public function saveAllow(Request $request) {

		$param = $request->all();

        $data = array('allow' => 'CRUD',

                 'created_at' => date('Y-m-d H:m:s'),

                 'updated_at' => date('Y-m-d H:m:s'));

        $role = Rol::find($param['role']);

        $module = Module::find($param['module']);

        if($param['action'] == 'toassing') {

        	$role->modules()->attach($module,$data);
        	
        } else {

        	$role->modules()->detach($module);
        }
	}

	public function allowIn($id = "") {

		$modules = Module::leftJoin('rol_module', function($join) use ($id){

                         $join->on('rol_module.module_id', '=', 'modules.id');

                         $join->on('rol_module.rol_id', '=', DB::raw($id));
                     })

		             ->select('modules.id as id', 

		             		  'modules.name as name', 

		             		  'modules.description as description', 

		             		  'rol_module.rol_id as assigned')->get();

		$role = Rol::find($id);             

 		return view('root.roles.permissions')
 				->with(compact('role'))
				->with(compact('modules'));
	}
}