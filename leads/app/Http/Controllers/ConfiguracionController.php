<?php 

namespace App\Http\Controllers;

use App\User;
use App\Models\Fase;
use App\Models\Estado;
use App\Models\Motivo;
use App\Models\TimeZone;

// use App\Models\Conglomerado;
use Illuminate\Http\Request;
use Input;


class ConfiguracionController extends Controller {

    private $path;

    public function __construct() {
      
        $this->middleware('auth');
        $this->path = public_path() . '/perfiles/'; // upload directory
    }

    public function index() {

        // \Config::get('app.app_max_day_recontact');

        $fases = Fase::orderBy('orden')->get();

        return view('config.fase.default')
                ->with('active', '')
                ->with(compact('fases'));
    }

    public function form($view, $id_nivel_superior) {

        $page = 'config.fase.form';
        $form = 'reason';
        $object = "";
        $phase = "";
        $state = "";
        $list_phases = "";
        $state_next = "";

        switch ($view) {

            case 'phase':
                    $form = 'phase';
                break;

            case 'state':
                    $form = 'state';
                    $phase = Fase::find($id_nivel_superior);
                break;

            case 'reason':
                    $state = Estado::find($id_nivel_superior);
                    $list_phases = Fase::all();
                break;
        }

        return view('config.fase.form')
                ->with('active', '')
                ->with('id', '')
                ->with(compact('phase'))
                ->with(compact('state'))
                ->with(compact('list_phases'))
                ->with(compact('state_next'))
                ->with('form', $form);            
    }

    public function save(Request $request) {

        $input = $request->all();

        if ($input['btn-action'] != 'cancel') {

            switch ($input['type']) {
                
                case 'phase':

                        $fase = new Fase();

                        if($input['id'] != "") {
                            $fase = Fase::find($input['id']);
                        }

                        $fase->nombre = $input['nombre'];
                        $fase->orden = $input['orden'];                        
                        $fase->save();                        
                    break;
                
                case 'state':

                        $state = new Estado();
                        $phase = Fase::find($input['phase']);

                        if($input['id'] != "") {

                            $state = Estado::find($input['id']);
                        }

                        $state->es_final = 0;

                        if(isset($input['es_final'])) {
                            $state->es_final = 1;
                        }

                        $state->titulo = $input['titulo'];
                        $state->nombre = $input['nombre'];
                        $state->color = ($input['color'] == "" ? "#ffffff" : $input['color']);
                                               
                        $phase->estados()->save($state);
                    break;

                case 'reason':

                        $reason = new Motivo();
                        $state = Estado::find($input['state']);

                        if($input['id'] != "") {

                            $reason = Motivo::find($input['id']);
                        }

                        $reason->nombre = $input['nombre'];
                        $reason->medio_contacto = $input['medio'];
                        $reason->mostrar_recordatorio = (isset($input['recordatorio']) ? 1 : 0);
                        $reason->mostrar_comision = (isset($input['comision']) ? 1 : 0);

                        if (isset($input['transicion'])) {

                            $reason->genera_transicion = 1;
                            $reason->fase_siguiente = $input['fase_siguiente'];

                            if (isset($input['estado_siguiente'])) {
                                $reason->estado_siguiente = $input['estado_siguiente'];
                            }                            
                        }                        
                                               
                        $state->motivos()->save($reason);
                    break;

                    case 'timezone':

                            $time_zone = new TimeZone();

                            if ($input['id'] != "") {
                                $time_zone = TimeZone::find($input['id']);
                            }
                            $time_zone->zone = $input['zone'];

                            $time_zone->save();

                        break;

            }
        }

        $fases = Fase::orderBy('orden')->get();

        return view('config.fase.default')
                ->with('active', '')
                ->with(compact('fases'));
    }


    public function edit($type, $id){

        $form = 'reason';
        $object = "";
        $exists = false;
        $phase = "";
        $state = "";
        $list = "";
        $list_phases = Fase::all();
        $state_next = "";

        if ($type == 'phase') {

            $object = Fase::find($id);

            if (is_object($object)) {

                $form = 'phase';
                $id = $object->id;
                $exists = true;
            }

        } elseif ($type == 'state') {

            $object = Estado::find($id);

            if (is_object($object)) {

                $form = 'state';
                $id = $object->id;
                $phase = Fase::find($object->fase_id);
                $exists = true;           
            }            
        } elseif ($type == 'reason') {

            $object = Motivo::find($id);

            if (is_object($object)) {

                $form = 'reason';
                $id = $object->id;
                $state = Estado::find($object->estado_id);
                $state_next = Estado::find($object->estado_siguiente);
                $exists = true;                
            }            
        } 


        if ($exists) {

            return view('config.fase.form')
                    ->with('active', '')
                    ->with('id', $id)
                    ->with('form', $form)
                    ->with(compact('phase'))
                    ->with(compact('state'))
                    ->with(compact('list_phases'))
                    ->with(compact('state_next'))                    
                    ->with(compact('object'));
        } else {

            $fases = Fase::orderBy('orden')->get();
            $list = Lista::all();

            return view('config.fase.default')
                    ->with('active', '')
                    ->with(compact('fases'));            
        }

    }


    public function delete($type, $id){

        $object = "";
        $exists = false;

        if ($type == 'phase') {

            $object = Fase::find($id);

            if (is_object($object)) {
                $id = $object->delete();
            }
        } elseif ($type == 'state') {

            $object = Estado::find($id);

            if (is_object($object)) {
                $id = $object->delete();
            }
        } elseif ($type == 'reason') {

            $object = Motivo::find($id);

            if (is_object($object)) {
                $id = $object->delete();
            }            
        }

        $fases = Fase::orderBy('orden')->get();

        return view('config.fase.default')
                ->with('active', '')
                ->with('edit', '0')
                ->with(compact('fases'));
    }

    public function toListState($id) {

        $phase = "";
        $type = isset($_GET['type']) ? 'select':'list';

        if(is_numeric($id)) {

            $phase = Fase::find($id);

            if (is_object($phase)) {

                return view('config.fase.state')
                        ->with('active', '')
                        ->with('type', $type)
                        ->with('state', $phase->estados)                        
                        ->with(compact('phase'));
            }
        }

        echo "<br/><p class='center'>No se han encontrado estados relacionados.</p>";
    }

    public function toListReason($id) {

        $state = "";

        $type = isset($_GET['type']) ? 'select':'list';

        if(is_numeric($id)) {

            $state = Estado::find($id);

            if (is_object($state)) {

                return view('config.fase.reasons')
                        ->with('active', '')
                        ->with('type', $type)
                        ->with('reasons', $state->motivos)
                        ->with(compact('state'));
            }
        }

        echo "<br/><p class='center'>No se han encontrado estados relacionados.</p>";        
    }

    public function timeZone() {

        $zone = TimeZone::all()->first();

        return view('config.fase.timezone')
                    ->with('object', $zone)
                    ->with('active', '');
    }  
}