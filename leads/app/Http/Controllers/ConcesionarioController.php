<?php 

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;


use App\Models\Group_distributor;
use App\Models\Distributor;
use App\Models\Brand;
use App\Models\City;
use App\Models\Rol;
use App\User;
 	
class ConcesionarioController extends Controller {


	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	public function listar(){
		$concesionarios = Distributor::all();

		return view('concesionario.listar')
				->with(compact('concesionarios'));
	}
	
	public function obtenerConcesionarioFacebook(Request $request){
		$input = $request->all();
		$name = $input['name'];
		
		$concesionario = Distributor::where('facebook', '=', $name)->first();
		if(isset($concesionario->id) && $concesionario->id > 0){
			return $concesionario->id;
		}else{
			return -1;
		}
	
	}
	

	public function nuevo($id=-1){

		$marcas = array();
		$concesionarios = array();

		$cities = City::all();

		if ($id < 0) {
			//nuevo
			$concesionario = new Distributor();
			$concesionario->id = -1;
			
		}else{
			
			$concesionario = Distributor::find($id);
			$cons = $concesionario->marcas;
			$marcas = Brand::all();

			$aux = array();
			foreach ($cons as $value) {
				$aux[] = $value->id;
			}
		
			$cons = $aux;
		}

		return view('concesionario.form')
				->with(compact('marcas'))
				->with(compact('cons'))
				->with(compact('cities'))
				->with(compact('concesionario'));

	}

	public function guardar(Request $request){
		$input = $request->all();
		if ($input['id'] < 0) {
			//nuevo
			$concesionario = new Distributor();
		}else{
			$concesionario = Distributor::find($input['id']);
		}

		$concesionario->name = $input['nombre'];
		$concesionario->address = $input['direccion'];
		$concesionario->email = $input['correo'];
		$concesionario->phone = $input['telefono'];
		$concesionario->facebook = $input['facebook'];
		$concesionario->city_id = $input['city'];
		$concesionario->save();

		return $this->listar();

	}

	function eliminar($id){
		Concesionario::destroy($id);

		return $this->listar();
	}



	/*
	*
	*	Gestion de usuarios para los concesionarios...
	*
	*/

	public function usuarios(){
		//Buscamos los usuarios del concesionario
		$users = User::where('concesionario', '=', Auth::user()->concesionario)->get();

		return view('concesionario.usuarios.listar')
				->with('active', 'usuarios')
				->with(compact('users'));
	}



    public function nuevousuario($id=-1) {

       	$concesionario = Concesionario::where('nombre', '=', Auth::user()->concesionario)->first();
        $marcas = $concesionario->marcas;
    	$um = array();

        if($id < 0){
            //Nuevo
            $user = new User();
            $user->id = -1;
        }else{
        	$user = User::find($id);
        	foreach ($user->marcas as $marca) {
        		array_push($um, $marca->id);
        	}
        }

        return view('concesionario.usuarios.form')
                    ->with('active', '')
                    ->with(compact('user'))
                    ->with(compact('um'))
                    ->with(compact('marcas'));
    }
    
    public function eliminarusuario($id){
	$user = User::find($id);
	$user->activo = 0;
	$user->save();

	return $this->usuarios();
    }


    public function guardarusuario(Request $request) {

        $input = $request->all();

        if ($input['id'] < 0) {
            //nuevo
            $user = new User();
        }else{
            //Editar
            $user = User::find($input['id']);

            //Borramos las marcas
        	$um = User_has_marca::where('user_id', '=', $user->id)->get();
        	foreach ($um as $key) {
        		$key->delete();
        	}
        }
        
        $user->nombres = $input['nombres'];
        $user->apellidos = $input['apellidos'];
        $user->email = $input['email']; 
        $user->telefono = $input['telefono']; 
        $user->activo = 1; 
        $user->rol = 5;
        $user->concesionario = Auth::user()->concesionario;
		
		if ($input['password'] != "") {
            $user->password = bcrypt($input['password']);
        }

        $user->save();


		if (isset($input['marcas'])) {
			$marcas = $input['marcas'];
        	foreach ($marcas as $marca) {
        		$obj = new User_has_marca();
        		$obj->user_id = $user->id;
        		$obj->marca_id = $marca;
        		$obj->save();
        	}
        }

        return redirect('/concesionario/usuarios');
    }

    public function assessors() {
    	
    	$distributor = Auth::user()->distributor()->first();

    	$id = $distributor->id;

    	$role = "'Asesor'";

		$assessors = User::join('user_rol', function($join) use ($id) {

		   $join->on('user_rol.user_id', '=', 'users.id');

		   $join->on('user_rol.distributor_id', '=', DB::raw($id));

		 })
		 ->join('rol', function($join) use ($role) {

		   $join->on('rol.id', '=', 'user_rol.rol_id');

		   $join->on('rol.name', '=', DB::raw(strtolower($role)));

		 })
		 ->leftJoin('brands', function($join) {

		   $join->on('brands.id', '=', 'user_rol.brand_id');

		 })		 
		 ->select('users.id as user_id', 
				  'users.name as user_name',
				  'users.email as user_email',
				  'users.capacidad as capacidad',
				  'users.cupo as cupo',
				  'rol.id as rol_id',
				  'rol.name as rol_name',
				  'brands.id as brand_id',
				  'brands.name as brand_name')->get();


		return view('concesionario.usuarios.listar')
				->with(compact('assessors'));

    }

    public function create_asessor($id = "") {

		$assessor = "";    	

 		$distributor = Auth::user()->distributor()->first();

 		$role = Rol::where('name', '=', 'asesor')->first();

 		if (!empty($id)) {

 			$assessor = User::find($id);
 		}

 		return view('concesionario.usuarios.form')			

				->with(compact('distributor'))

				->with(compact('assessor'))

				->with(compact('role'));
    }


    public function save_asessor(Request $request) {

    	$input = $request->all();

        $user = new User();        

        $param = $request->all();

        if (!empty($param['assessor'])) {
           
            $user = User::find($param['assessor']);
        }

        $user->name = $param['name'];

        $user->email = $param['email'];

        $user->capacidad = $param['capacidad'];

        $user->cupo = 0;

        $user->turno = 0;

        $brand = ($param['brand'] != "" ? $param['brand'] : NULL);
       
        $distributor = ($param['distributor'] != "" ? $param['distributor'] : NULL);      

        if (!empty($param['password'])) {

            $user->password = bcrypt($param['password']);
        }        

        $data = array('brand_id' => $brand,

                 'distributor_id' => $distributor,

                 'updated_at' => date('Y-m-d H:m:s'));


        if (empty($param['assessor'])) {

            $data['created_at'] = date('Y-m-d H:m:s');
        }

        $user->save();        

        $role = Rol::find($param['role']);

        $user->roles()->sync([$role->id => $data]);

        return redirect('/root/concesionarios/asesores');
    }

    public function delete_asessor($id) {

        User::destroy($id);
        
        return redirect('/root/concesionarios/asesores');
    }

}