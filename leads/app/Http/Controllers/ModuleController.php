<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use App\Models\Module;
use App\User;

class ModuleController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	
	{
		$modules = Module::all();

		return view('root.modules.default')
				->with(compact('modules'));
	}

	public function create($id = "") {

		$module = "";

		if (!empty($id)) {

			$module = Module::find($id);
		}

		return view('root.modules.form')
				->with(compact('module'));
	}

	public function save(Request $requets) {

		$module = new Module;

		$param = $requets->all();

		if (!empty($param['id'])) {

			$module = Module::find($param['id']);			
		}

		$module->name = $param['name'];
		$module->route = $param['route'];
		$module->icon = $param['icono'];
		$module->description = $param['description'];

		$module->save();

		return redirect('/root/modulos');
	}

	public function delete($id = "") {

		if (!empty($id)) {
			
			Module::destroy($id);
		}

		return redirect('/root/modulos');
	}
}