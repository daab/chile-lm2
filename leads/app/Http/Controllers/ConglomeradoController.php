<?php 

namespace App\Http\Controllers;

use App\Models\Conglomerate;
use App\Models\Brand;
use App\Models\Distributor;
use App\Models\Group_distributor;
use Illuminate\Http\Request;

class ConglomeradoController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	
	}
	

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function listar()
	{

		$objs = Conglomerate::all();


		return view('root.conglomerado.listar')
				->with('active', '')
				->with(compact('objs'));
	}

	public function eliminar($id){
		Conglomerado::destroy($id);

		return $this->listar();
	}

	public function nuevo($id){
		$concesionarios = array();
		$cons = array();

		$marcas = Brand::All();
		$distribuidores = Distributor::all();


		if ($id < 0) {
			//Nuevo
			$obj = new Conglomerate();
			$obj->id = -1;
		}else{
			//Editar
			$obj = Conglomerate::find($id);
			$marca = Brand::where('id', '=', $obj->brand_id)->first();
			$concesionarios = $marca->concesionarios;
			
			$cons = Group_distributor::where('conglomerate_id', '=', $obj->id)->get();
			$aux = array();
			foreach ($cons as $value) {
				$aux[] = $value->distributor_id;
			}
		
			$cons = $aux;
		}



		return view('root.conglomerado.form')
				->with('active', '')
				->with(compact('concesionarios'))
				->with(compact('cons'))
				->with(compact('marcas'))
				->with(compact('distribuidores'))
				->with(compact('obj'));
	}

	

	public function guardar(Request $request){

		//ToDo: Cuando se cambia de marca se deben borrar los checks en concesionario_marca

		$input = $request->all();

		if ($input['id'] > 0) {
			//Editar
			$obj = Conglomerate::find($input['id'] );
		}else{
			//Nuevo
			$obj = new Conglomerate();
		}

		$obj->name = $input['nombre'];
		$obj->brand_id = $input['marca'];
		
		$obj->distributor_id = null;
		if($input['distributor'] > 0)
			$obj->distributor_id = $input['distributor'];
		
		$obj->save();

		return $this->listar();
	}


	public function update(Request $request){
	
		$input = $request->all();

		$mhc = Group_distributor::where('brand_id', '=', $input['idMarca'])
											->where('distributor_id', '=', $input['idConcesionario'])
											->first();
		
		if ($input['guardar'] == 1) {
			$mhc->conglomerate_id = $input['idConglomerado'];
		}else{
			$mhc->conglomerate_id = null;
		}

		$mhc->save();

	}

	

}
