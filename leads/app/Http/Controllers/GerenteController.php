<?php 

namespace App\Http\Controllers;

use App\User;
use App\Models\Marca;
use App\Models\Lead;
use App\Models\Cron;
use App\Models\Concesionario;
use App\Models\Gerente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Datatables;

class GerenteController extends Controller {


    public function index() {
        $gerentes = Gerente::all();

        return view('root.gerente.listar')
                    ->with('active', '')
                    ->with(compact('gerentes'));
    }

    public function getgerente(Request $request){

        if($request->ajax()){ 
            $gerentes = Gerente::all();
            return Datatables::of($gerentes)
            ->editColumn('gerenteproducto', function($gerentes){ 
                if ($gerentes->rol == 1) {
                   return 'Gerente de producto';
                }elseif($gerentes->rol == 2){
                    return 'Gerente de concesionario';
                }else{
                    return 'Gerente de MIX';
                }
            })

            ->editColumn('gerenteproducto2', function($gerentes){ 
                if ($gerentes->rol == 1 || $gerentes->rol == 3) {
                   return $gerentes->text;
                }else{
                    return NULL;
                }
            })

            ->editColumn('gerenteproducto3', function($gerentes){ 
                if ($gerentes->rol == 1) {
                   return NULL;
                }elseif($gerentes->rol == 2){
                    return $gerentes->text;
                }else{
                    return $gerentes->text2;
                }
            })
            ->addColumn('apciones', function ($gerentes) {
                 return '<a href="'.url('root/nuevo/gerente', $gerentes->id).'">Editar</a> | <a href="'.url('root/eliminar/gerente', $gerentes->id).'">Eliminar</a>';
            })
            ->make(true);
        }     
    }

    public function nuevo($id=-1) {

        $marcas = Marca::All();
        $concesionarios = Concesionario::All();
    	
        if($id < 0){
            //Nuevo
            $gerente = new Gerente();
            $gerente->id = -1;
        }else{
            $gerente = Gerente::find($id);
        }

        return view('root.gerente.form')
                    ->with('active', '')
                    ->with(compact('gerente'))
                    ->with(compact('marcas'))
                    ->with(compact('concesionarios'));
    }

    public function guardar(Request $request) {
        $input = $request->all();

        if ($input['id'] < 0) {
            //nuevo
            $gerente = new Gerente();
        }else{
            //Editar
            $gerente = Gerente::find($input['id']);
        }
        
        $gerente->correo = $input['correo'];
        $gerente->rol = $input['rol'];

        switch ($gerente->rol) {
            case 1:
                $gerente->text = $input['marca'];
            break;
            case 2:
                $gerente->text = $input['concesionario'];
            break;
            case 3:
                $gerente->text = $input['marca'];
                $gerente->text2 = $input['concesionario'];
            break;
        }
       
        
        $gerente->save();
        return redirect('/root/gerente');
    }


    public function eliminar($id) {
        Gerente::destroy($id);
        
        return redirect('/root/gerente');
    }


    public function generarreportes(){
        
        date_default_timezone_set('America/Santiago'); 
        $hora = date('H');
        error_log("Si ejecuta el cron");

        if($hora == 20){
            
            //Verificamos que no se haya enviado el cron...
            $cronsDia = Cron::where('created_at', '>=', date('Y-m-d').' 00:00:00' )
                            ->where('created_at', '<=', date('Y-m-d').' 23:59:59' )
                            ->get();
            
            if(count($cronsDia) == 0){
            
                error_log("Inicio de reporte: Entro a las 8pm");
                
                $gerentes = Gerente::all();
                foreach ($gerentes as $gerente) {
                    switch ($gerente->rol) {
                        case 1:
                            //Marca --- Producto
                            $this->enviarCorreoMarca($gerente->correo, $gerente->text);
                        break;
                        case 2:
                            //Concesionario ---- Producto
                            $this->enviarCorreoConcesionario($gerente->correo, $gerente->text);
                        break;
                        case 3:
                            // MIX -----
                            $this->enviarCorreoMix($gerente->correo, $gerente->text, $gerente->text2);
                        break;
                        default:
                            error_log("No existe el rol para el gerente seleccionado...");
                        break;
                    }
                }
                
                
                $cron = new Cron();
                $cron->nombre = "Reporte";
                $cron->ejecuto = 1;
                $cron->save();
                error_log("Reporte enviado correctamente!");
            }else{
                error_log("El cron ya fue ejecutadooo...");
            }
        }else{
           error_log("La hora no es la correcta...".$hora);
        }

    }


    private function enviarCorreoMarca($correo, $marca){
        date_default_timezone_set('America/Santiago'); 
        //Buscamos los datos que necesitamos...
        $hoy = date('Y-m-d');
        $mes = date('m');
        $year = date('Y');


        //Leads del mes...
        $leadsMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

        //Interesados del mes...
        $interesadosMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->where('estado', '=', 'Interesado')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                         ->get();

        //Test Drive del mes...
        $testMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->where('estado2', '=', 'Test Drive')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

         //Agendo Cita del mes...
        $agendoMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->where('estado2', '=', 'Agendo Cita')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

        //Top Modelos mes
        $modelos = Lead::select('modelo', \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->groupBy('modelo')
                        ->orderby('num', 'desc')
                        ->get();

        //Top Concesionarios x Mes
        $concesionariosMes = Lead::select('concesionario', \DB::raw("count(id) as num"))
                        ->where('marca','=', $marca)
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->groupBy('concesionario')
                        ->orderby('num', 'desc')
                        ->get();

        $totalConcesionarios = array();
         foreach ($concesionariosMes as $value) {
            $totalConcesionarios[$value->concesionario]['nombre'] = $value->concesionario;
            $totalConcesionarios[$value->concesionario]['mes'] = $value->num;
            $totalConcesionarios[$value->concesionario]['dia'] = 0;
        }


        $marcaObj = Marca::where('nombre', '=', 'Volkswagen PKW')->first();

        //Validamos que la marca no sea Volkswagen PKW
        if ($marca == "Volkswagen PKW" && $marcaObj->leads > 0 && $marcaObj->concesionario > 0 && $marcaObj->testdrive > 0 ) {

            //Calculo de los indicadores...
            $conce = number_format((($testMes[0]->num + $agendoMes[0]->num) / $leadsMes[0]->num)*100);
            $test = number_format(($testMes[0]->num / ($testMes[0]->num + $agendoMes[0]->num))*100);

            //Formateamos el HTML
            $html = file_get_contents(public_path().'/mailing/reporte-leadmanager-pkw.html');
            $html = str_replace('{_totalLeadsMes}', $leadsMes[0]->num, $html);
            $html = str_replace('{_totalInteresadosMes}', $interesadosMes[0]->num, $html);
            $html = str_replace('{_totalVisitasMes}', $conce, $html);
            $html = str_replace('{_totalTestMes}', $test, $html);
            $html = str_replace('{_totalLeads}', $marcaObj->leads, $html);
            $html = str_replace('{_totalVisitas}', $marcaObj->concesionario." %", $html);
            $html = str_replace('{_totalTest}', $marcaObj->testdrive." %", $html);
            
            //Modelos
            $modelosHtml = "";
            for($i=0;$i<3;$i++){
                if (isset($modelos[$i])) {
                    $modelo = $modelos[$i];
                    $modelosHtml .=  '<tr><td style="background-color: #FFFFFF;  width: 50%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color: #444444; border: 1px solid #9dd1e3">'.$modelo->modelo.'</td><td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$modelo->num.'</td></tr>';
                }
            }
            $html = str_replace('{_modelos}', $modelosHtml, $html);

            $concesionarioHTML = "";
            foreach ($concesionariosMes as $value) {
                $concesionarioHTML .= '<tr><td style="background-color: #FFFFFF;  width: 50%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color: #444444; border: 1px solid #9dd1e3">'.$value->concesionario.'</td><td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$value->num.'</td></tr>';
            }
            
            $html = str_replace('{_concesionarios}', $modelosHtml, $html);

        }else{

            //Leads del dia...
            $leadsDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            
             //Interesados del dia...
            $interesadosDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado', '=', 'Interesado')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            

             //Test Drive del dia...
            $testDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado2', '=', 'Test Drive')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            

             //Agendo Cita del dia...
            $agendoDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado2', '=', 'Agendo Cita')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
           

            //No Contesto del mes...
            $noContesto = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado', '=', 'No contesto')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

            
                            
                            
               //Test Drive del dia...
            $sinGestionarDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado', '=', 'Sin Gestionar')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
                            
                            
                            
            //Test Drive del mes...
            $sinGestionarMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('estado', '=', 'Sin Gestionar')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

            //Top Concesionarios x Dia
            $concesionarios = Lead::select('concesionario', \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->groupBy('concesionario')
                            ->orderby('num', 'desc')
                            ->get();
                            
            

            foreach ($concesionarios as $value) {
               $totalConcesionarios[$value->concesionario]['dia'] = $value->num;
            }
            


            //Formateamos el HTML
            $html = file_get_contents(public_path().'/mailing/lead-manager-email.html');
            $html = str_replace('{_producto}', $marca, $html);
            $html = str_replace('{_leads_dia}', $leadsDia[0]->num, $html);
            $html = str_replace('{_leads_mes}', $leadsMes[0]->num, $html);
            $html = str_replace('{_contesto_dia}', $interesadosDia[0]->num, $html);
            $html = str_replace('{_contesto_mes}', $interesadosMes[0]->num, $html);
            $html = str_replace('{_test_dia}', $testDia[0]->num, $html);
            $html = str_replace('{_test_mes}', $testMes[0]->num, $html);
            $html = str_replace('{_sin_dia}', $sinGestionarDia[0]->num, $html);
            $html = str_replace('{_sin_mes}', $sinGestionarMes[0]->num, $html);
            $html = str_replace('{_agendo_dia}', $agendoDia[0]->num, $html);
            $html = str_replace('{_agendo_mes}', $agendoMes[0]->num, $html);
            $html = str_replace('{_nocontesto}', $noContesto[0]->num, $html);
            
            //Modelos
            $modelosHtml = "";
            for($i=0;$i<3;$i++){
                if (isset($modelos[$i])) {
                    $modelo = $modelos[$i];
                    $modelosHtml .= ($i+1)." ".$modelo->modelo." ( ".$modelo->num." )<br />";
                }
            }
            $html = str_replace('{_modelos}', $modelosHtml, $html);


            $concesionariosHtml = "";
            foreach($totalConcesionarios as $concesionario){
                if(isset($concesionario['nombre'])){
                
                
                	$aux_nombre = explode(",",$concesionario['nombre']);
                	if(count($aux_nombre) == 3){
                		$nombre_concesionario = $aux_nombre[0]." ".$aux_nombre[1];
                	}else{
                		$nombre_concesionario = $concesionario['nombre'];
                	}
            
            

                    $concesionariosHtml .= '<tr><td style="background-color: #FFFFFF;  width: 50%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color: #444444; border: 1px solid #9dd1e3">'.$nombre_concesionario.'</td>
                                                <td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$concesionario['dia'].'</td>
                                                <td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$concesionario['mes'].'</td>
                                            </tr>';
                                            
                }
                                            
                
                
            }
            
            $html = str_replace('{_concesionarios}', $concesionariosHtml, $html);

        }
        
        $this->enviarCorreo($correo,$html,$marca);

    }

    private function enviarCorreoConcesionario($correo, $concesionario){
        date_default_timezone_set('America/Santiago'); 

        //Buscamos los datos que necesitamos...
        $hoy = date('Y-m-d');
        $mes = date('m');
        $year = date('Y');

        //Leads del dia...
        $leadsDia = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('created_at', '>', $hoy." 00:00:00")
                        ->where('created_at', '<=', $hoy." 18:00:00")
                        ->get();
        //Leads del mes...
        $leadsMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();
         //Interesados del dia...
        $interesadosDia = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado', '=', 'Interesado')
                        ->where('created_at', '>', $hoy." 00:00:00")
                        ->where('created_at', '<=', $hoy." 18:00:00")
                        ->get();
        //Interesados del mes...
        $interesadosMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado', '=', 'Interesado')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

         //Test Drive del dia...
        $testDia = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado2', '=', 'Test Drive')
                        ->where('created_at', '>', $hoy." 00:00:00")
                        ->where('created_at', '<=', $hoy." 18:00:00")
                        ->get();
                        
                        
                        
        //Test Drive del mes...
        $testMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado2', '=', 'Test Drive')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();
                        
          //Test Drive del dia...
        $sinGestionarDia = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado', '=', 'Sin Gestionar')
                        ->where('created_at', '>', $hoy." 00:00:00")
                        ->where('created_at', '<=', $hoy." 18:00:00")
                        ->get();
                        
                        
                        
        //Test Drive del mes...
        $sinGestionarMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado', '=', 'Sin Gestionar')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

         //Agendo Cita del dia...
        $agendoDia = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado2', '=', 'Agendo Cita')
                        ->where('created_at', '>', $hoy." 00:00:00")
                        ->where('created_at', '<=', $hoy." 18:00:00")
                        ->get();
        //Agendo Cita del mes...
        $agendoMes = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado2', '=', 'Agendo Cita')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

        //No Contesto del mes...
        $noContesto = Lead::select( \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->where('estado', '=', 'No contesto')
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->get();

        //Top Modelos mes
        $modelos = Lead::select('modelo', \DB::raw("count(id) as num"))
                        ->where('concesionario','=', $concesionario)
                        ->whereRaw('MONTH(created_at) = ' . $mes)
                        ->whereRaw('YEAR(created_at) = ' . $year)
                        ->groupBy('modelo')
                        ->orderby('num', 'desc')
                        ->get();

       
        //Formateamos el HTML
        $html = file_get_contents(public_path().'/mailing/lead-manager-email2.html');
        $html = str_replace('{_producto}', $concesionario, $html);
        $html = str_replace('{_leads_dia}', $leadsDia[0]->num, $html);
        $html = str_replace('{_leads_mes}', $leadsMes[0]->num, $html);
        $html = str_replace('{_contesto_dia}', $interesadosDia[0]->num, $html);
        $html = str_replace('{_contesto_mes}', $interesadosMes[0]->num, $html);
        $html = str_replace('{_test_dia}', $testDia[0]->num, $html);
        $html = str_replace('{_test_mes}', $testMes[0]->num, $html);
        $html = str_replace('{_agendo_dia}', $agendoDia[0]->num, $html);
        $html = str_replace('{_agendo_mes}', $agendoMes[0]->num, $html);
        $html = str_replace('{_nocontesto}', $noContesto[0]->num, $html);
        $html = str_replace('{_sin_dia}', $sinGestionarDia[0]->num, $html);
        $html = str_replace('{_sin_mes}', $sinGestionarMes[0]->num, $html);
        
        //Modelos
        $modelosHtml = "";
        for($i=0;$i<3;$i++){
            if (isset($modelos[$i])) {
                $modelo = $modelos[$i];
                $modelosHtml .= ($i+1)." ".$modelo->modelo." ( ".$modelo->num." )<br />";
            }
        }
        $html = str_replace('{_modelos}', $modelosHtml, $html);

        $this->enviarCorreo($correo,$html,$concesionario);
    }


    private function enviarCorreoMix($correo, $marca, $concesionario){
        date_default_timezone_set('America/Santiago'); 
        //Buscamos los datos que necesitamos...
        $hoy = date('Y-m-d');
        $mes = date('m');
        $year = date('Y');
        
        $figux = $marca." ".$concesionario;



        $marcaObj = Concesionario::where('nombre', '=', $concesionario)->first();
        //Leads del mes...
        $leadsMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();


        //Interesados del mes...
        $interesadosMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado', '=', 'Interesado')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

        //Test Drive del mes...
        $testMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado2', '=', 'Test Drive')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

        //Agendo Cita del mes...
        $agendoMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado2', '=', 'Agendo Cita')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

        //Top Modelos mes
            $modelos = Lead::select('modelo', \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->groupBy('modelo')
                            ->orderby('num', 'desc')
                            ->get();



        //Validamos que la marca no sea Volkswagen PKW
        if ($marca == "Volkswagen PKW" && $concesionario != "" && $marcaObj->leads > 0 && $marcaObj->concesionario > 0 && $marcaObj->testdrive > 0 ) {
            
            //Calculo de los indicadores...
            $conce = number_format((($testMes[0]->num + $agendoMes[0]->num) / $leadsMes[0]->num)*100);
            $test = number_format(($testMes[0]->num / ($testMes[0]->num + $agendoMes[0]->num))*100);

            //Formateamos el HTML
            $html = file_get_contents(public_path().'/mailing/reporte-leadmanager-pkw-concesionario.html');
            $html = str_replace('{_totalLeadsMes}', $leadsMes[0]->num, $html);
            $html = str_replace('{_totalInteresadosMes}', $interesadosMes[0]->num, $html);
            $html = str_replace('{_totalVisitasMes}', $conce, $html);
            $html = str_replace('{_totalTestMes}', $test, $html);
            $html = str_replace('{_totalLeads}', $marcaObj->leads, $html);
            $html = str_replace('{_totalVisitas}', $marcaObj->concesionario." %", $html);
            $html = str_replace('{_totalTest}', $marcaObj->testdrive." %", $html);
            $html = str_replace('{_nombre_concesionario}', $marcaObj->nombre." %", $html);
            
            
            //Modelos
            $modelosHtml = "";
            for($i=0;$i<3;$i++){
                if (isset($modelos[$i])) {
                    $modelo = $modelos[$i];
                    $modelosHtml .=  '<tr><td style="background-color: #FFFFFF;  width: 50%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color: #444444; border: 1px solid #9dd1e3">'.$modelo->modelo.'</td><td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$modelo->num.'</td></tr>';
                }
            }
            $html = str_replace('{_modelos}', $modelosHtml, $html);

        }else{


            //Leads del dia...
            $leadsDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            
             //Interesados del dia...
            $interesadosDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado', '=', 'Interesado')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            
             //Test Drive del dia...
            $testDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado2', '=', 'Test Drive')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            

             //Agendo Cita del dia...
            $agendoDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado2', '=', 'Agendo Cita')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
            

            //No Contesto del mes...
            $noContesto = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado', '=', 'No contesto')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

            
                            
                            
               //Test Drive del dia...
            $sinGestionarDia = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado', '=', 'Sin Gestionar')
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->get();
                            
                            
                            
            //Test Drive del mes...
            $sinGestionarMes = Lead::select( \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('estado', '=', 'Sin Gestionar')
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->get();

            //Top Concesionarios x Dia
            $concesionarios = Lead::select('concesionario', \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->where('created_at', '>', $hoy." 00:00:00")
                            ->where('created_at', '<=', $hoy." 18:00:00")
                            ->groupBy('concesionario')
                            ->orderby('num', 'desc')
                            ->get();
                            
            //Top Concesionarios x Mes
            $concesionariosMes = Lead::select('concesionario', \DB::raw("count(id) as num"))
                            ->where('marca','=', $marca)
                            ->where('concesionario','=', $concesionario)
                            ->whereRaw('MONTH(created_at) = ' . $mes)
                            ->whereRaw('YEAR(created_at) = ' . $year)
                            ->groupBy('concesionario')
                            ->orderby('num', 'desc')
                            ->get();

            $totalConcesionarios = array();
            foreach ($concesionariosMes as $value) {
                $totalConcesionarios[$value->concesionario]['nombre'] = $value->concesionario;
                $totalConcesionarios[$value->concesionario]['mes'] = $value->num;
                $totalConcesionarios[$value->concesionario]['dia'] = 0;
            }

            foreach ($concesionarios as $value) {
               $totalConcesionarios[$value->concesionario]['dia'] = $value->num;
            }
            


            //Formateamos el HTML
            $html = file_get_contents(public_path().'/mailing/lead-manager-email.html');
            $html = str_replace('{_producto}', $marca, $html);
            $html = str_replace('{_leads_dia}', $leadsDia[0]->num, $html);
            $html = str_replace('{_leads_mes}', $leadsMes[0]->num, $html);
            $html = str_replace('{_contesto_dia}', $interesadosDia[0]->num, $html);
            $html = str_replace('{_contesto_mes}', $interesadosMes[0]->num, $html);
            $html = str_replace('{_test_dia}', $testDia[0]->num, $html);
            $html = str_replace('{_test_mes}', $testMes[0]->num, $html);
            $html = str_replace('{_sin_dia}', $sinGestionarDia[0]->num, $html);
            $html = str_replace('{_sin_mes}', $sinGestionarMes[0]->num, $html);
            $html = str_replace('{_agendo_dia}', $agendoDia[0]->num, $html);
            $html = str_replace('{_agendo_mes}', $agendoMes[0]->num, $html);
            $html = str_replace('{_nocontesto}', $noContesto[0]->num, $html);
            
            //Modelos
            $modelosHtml = "";
            for($i=0;$i<3;$i++){
                if (isset($modelos[$i])) {
                    $modelo = $modelos[$i];
                    $modelosHtml .= ($i+1)." ".$modelo->modelo." ( ".$modelo->num." )<br />";
                }
            }
            $html = str_replace('{_modelos}', $modelosHtml, $html);


            $concesionariosHtml = "";
            foreach($totalConcesionarios as $concesionario){
                    if(isset($concesionario['nombre'])){
                $aux_nombre = explode(",",$concesionario['nombre']);
                if(count($aux_nombre) == 3){
                    $nombre_concesionario = $aux_nombre[0]." ".$aux_nombre[1];
                }else{
                    $nombre_concesionario = $concesionario['nombre'];
                }
            
                
                 

                    $concesionariosHtml .= '<tr><td style="background-color: #FFFFFF;  width: 50%; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:left; color: #444444; border: 1px solid #9dd1e3">'.$nombre_concesionario.'</td>
                                                <td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$concesionario['dia'].'</td>
                                                <td style="background-color: #FFFFFF; font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:14px; text-align:center; color: #444444; border: 1px solid #9dd1e3">'.$concesionario['mes'].'</td>
                                            </tr>';
                }
                
            }
            
            $html = str_replace('{_concesionarios}', $concesionariosHtml, $html);

        }
        

        $this->enviarCorreo($correo,$html,$figux);

    }


    private function enviarCorreo($para,$html,$text){
        // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                
                //Buscamos el correo del concesionario
                $asunto = "Reporte Lead manager ".$text." ( ".date('Y-m-d')." )";

                require_once 'vendor/PHPMailer/PHPMailerAutoload.php';
                $mail = new \PHPMailer();
        
                $mail->IsSMTP();
                $mail->CharSet = 'UTF-8';
        
                $mail->Host       = "cloud1.hostingred.com"; // SMTP server example
                $mail->SMTPDebug  = false;                     // enables SMTP debug information (for testing)
                $mail->SMTPAuth   = true;                  // enable SMTP authentication
                $mail->SMTPSecure = 'ssl';
                $mail->Port       = 465;                    // set the SMTP port for the GMAIL server
                $mail->Username   = "noreply@cotizadoronline.com.co"; // SMTP account username example
                $mail->Password   = "t(wQ7k?9L2PW";        // SMTP account password example
        
                //Set who the message is to be sent from
                $mail->setFrom('noreply@cotizadoronline.com.co', 'Lead Manager');
                //Set who the message is to be sent to
                $correos = explode(",",$para);
                foreach($correos as $correo){
                    $mail->addAddress($correo);
                }
                
                //Set the subject line
                $mail->Subject = $asunto;
                //Read an HTML message body from an external file, convert referenced images to embedded,
                //convert HTML into a basic plain-text alternative body
                //$mail->msgHTML(file_get_contents('contents.html'), dirname(__FILE__));
                $mail->msgHTML($html);
             

                //send the message, check for errors
                if (!$mail->send()) {
                    echo "Mailer Error: " . $mail->ErrorInfo;
                } else {
                    echo "Message sent!";
                }
                
                //echo $html;
                
        
                

    }


}
