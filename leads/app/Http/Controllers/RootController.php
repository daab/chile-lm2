<?php 

namespace App\Http\Controllers;
use App\Models\Brand;
use App\Models\Distributor;
use App\Models\Group_distributor;
use App\Models\Domain;
use App\Models\Modelo;
use Illuminate\Http\Request;
use Datatables;
use Input;

use App\Security;


class RootController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Root Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $valid_exts_d; 
	private $pathd;

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		
		$this->valid_exts_d = array('pdf'); // valid extensions
		$this->pathd = public_path() . '/fichas/'; // upload directory
		$this->security = new Security();
	}
	
	public function obtenerModeloFacebook(Request $request){
		$input = $request->all();
		$name = $input['name'];
		
		$marca = Modelo::where('name', '=', $name)->first();
		if(isset($marca->id) && $marca->id > 0){
			return $marca->id;
		}else{
			return -1;
		}
	
	}


	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function nuevo($obj = 1, $id = -1)
	{	
		if ($obj == 1) {
			return $this->nuevaMarca($id);
		}else{
			return $this->nuevoDominio($id);
		}
	}

	public function guardar($obj, Request $request){
		$input = $request->all();

		if ($obj == 1) {
			$this->guardarMarca($input);
			return $this->marcas();
		}else{
			$this->guardarDominio($input);
			return $this->dominios();
		}

	}

	public function eliminar($obj = 1, $id)
	{	
		if ($obj == 1) {
			return $this->borrarMarca($id);
		}else{
			return $this->borrarDominio($id);
		}
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function marcas()
	{

		$marcas = Brand::all();

		return view('root.marcas.listar')
				->with('active', '')
				->with(compact('marcas'));
	}

	 public function getmarcas(Request $request){

	 	if($request->ajax()){ 
		 	$marcas = Marca::all();
			return Datatables::of($marcas)
	       	->addColumn('apciones', function ($marcas) {
				 return '<a href="'.url('root/marcas/nuevo/1', $marcas->id).'">Editar</a> | <a href="'.url('root/marcas/eliminar/1', $marcas->id).'">Eliminar</a>';
	        })
	        ->make(true);
	    }     
    }

	private function borrarMarca($id){
		Marca::destroy($id);

		return $this->marcas();

	}

	private function nuevaMarca($id){
		$concesionarios = array();
		$cons = array();

		if ($id < 0) {
			//Nuevo
			$marca = new Brand();
			$marca->id = -1;
		}else{
			//Editar
			$concesionarios = Distributor::all();
			$marca = Brand::find($id);
			$cons = $marca->concesionarios;

			$aux = array();
			foreach ($cons as $value) {
				$aux[] = $value->id;
			}
		
			$cons = $aux;
		}

		return view('root.marcas.form')
				->with('active', '')
				->with(compact('concesionarios'))
				->with(compact('cons'))
				->with(compact('marca'));
	}

	

	private function guardarMarca($input){

		if ($input['id'] > 0) {
			//Editar
			$marca = Brand::find($input['id'] );
		}else{
			//Nuevo
			$marca = new Brand();
		}

		$marca->name = $input['nombre'];
		$marca->logo = $input['logo'];
		$marca->template = $input['template'];
		$marca->save();
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function dominios()
	{
		$dominios = Domain::all();


		return view('root.dominios.listar')
				->with('active', '')
				->with(compact('dominios'));
	}


	private function borrarDominio($id){
		Domain::destroy($id);

		return $this->dominios();

	}

	private function nuevoDominio($id){
		

		if ($id < 0) {
			//Nuevo
			$dominio = new Domain();
			$dominio->id = -1;
		}else{
			//Editar
			$dominio = Domain::find($id);
		}
		return view('root.dominios.form')
				->with('active', '')
				->with(compact('dominio'));
				
	}

	

	private function guardarDominio($input){

		if ($input['id'] > 0) {
			//Editar
			$dominio = Domain::find($input['id'] );
		}else{
			//Nuevo
			$dominio = new Domain();
		}

		$dominio->ip = $input['ip'];
		$dominio->dominio = $input['dominio'];
		$dominio->token = $this->security->encrypt($dominio->dominio);
		$dominio->save();
	}



	public function update(Request $request){
	
		$input = $request->all();

		if ($input['guardar'] == 1) {
			$mhc = new Group_distributor();
			$mhc->brand_id = $input['idMarca'];
			$mhc->distributor_id = $input['idConcesionario'];
			$mhc->save();	
		}else{
			$mhc = Group_distributor::where('brand_id', '=', $input['idMarca'])
											->where('distributor_id', '=', $input['idConcesionario'])->first();


			Group_distributor::destroy($mhc->id);
		}
		

	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function modelos()
	{
		$modelos = Modelo::with('marcas')->get();

		return view('root.modelo.listar')
				->with('active', '')
				->with(compact('modelos'));
	}

	public function borrarmodelo($id){
		Modelo::destroy($id);

		return $this->modelos();

	}

	public function nuevomodelo($id=-1){
		$marcas = Brand::all();

		if ($id < 0) {
			//Nuevo
			$modelo = new Modelo();
			$modelo->id = -1;
		}else{
			//Editar
			$modelo = Modelo::find($id);
		}
		return view('root.modelo.form')
				->with('active', '')
				->with(compact('modelo'))
				->with(compact('marcas'));
	}

	

	public function guardarmodelo(Request $request){

		$input = $request->all();

		if ($input['id'] > 0) {
			//Editar
			$modelo = Modelo::find($input['id'] );
		}else{
			//Nuevo
			$modelo = new Modelo();
		}

		$modelo->brand_id = $input['marca'];
		$modelo->name = $input['nombre'];

	
		$uploadd = $this->uploadDoc( );
		if($uploadd['status'])
			$modelo->doc = $uploadd['name'];
		
		$modelo->save();

		return $this->modelos();
	}

	public function uploadDoc() {
		$file = Input::file('doc');

	
		$fileName = '';
    	$status = true;

	    if($file) {

	    	$ext = $file->guessClientExtension();
		    // get size
		    //$size = $file->getClientSize();
		    
		    $name = $file->getClientOriginalName();
		    $name = str_replace(" ", "_", $name);
		    
		    //Only PDF
		    if (in_array($ext, $this->valid_exts_d)) {

		    	if ($file->move($this->pathd, $name)) {
				    $fileName = $name;
			    } else {
			    	error_log("NO se pudo mover el archivo al path especificado");
			    	$status = false;
			    }

		    } else {
		    	error_log("No tiene la extension correcta");
		    	$status = false;
		    }

		} else {
			error_log("No es un archivo valido");
		  	$status = false;
		}

	    return array('status' => $status, 'name' => $fileName);

	}//End Function...

}
