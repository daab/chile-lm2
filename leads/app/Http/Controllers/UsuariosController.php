<?php 

namespace App\Http\Controllers;

use App\User;
use App\Models\Marca;
use App\Models\Concesionario;
use App\Models\Conglomerado;
use Illuminate\Http\Request;
use Input;
use Config;


class UsuariosController extends Controller {

    private $path;

    public function __construct() {
      
        $this->middleware('auth');
        $this->path = public_path() . '/perfiles/'; // upload directory
    }

    public function index() {
        $users = User::all();

       return view('root.usuarios.listar')
                    ->with('active', '')
                    ->with(compact('users'));
    }

    public function nuevo($id=-1) {

        $marcas = Marca::All();
        $concesionarios = Concesionario::All();
        $conglomerados = Conglomerado::All();
    	
        if($id < 0){
            //Nuevo
            $user = new User();
            $user->id = -1;
        }else{
            $user = User::find($id);
        }

        return view('root.usuarios.form')
                    ->with('active', '')
                    ->with(compact('user'))
                    ->with(compact('marcas'))
                    ->with(compact('conglomerados'))
                    ->with(compact('concesionarios'));
    }

    public function guardar(Request $request) {
        $input = $request->all();

        if ($input['id'] < 0) {
            //nuevo
            $user = new User();
        }else{
            //Editar
            $user = User::find($input['id']);
        }
        
        $user->nombres = $input['nombres'];
        $user->apellidos = $input['apellidos'];
        $user->rol = $input['rol'];
        $user->email = $input['email']; 

        $user->marca = $input['marca'];
        $user->concesionario = $input['concesionario'];
        $user->conglomerado = $input['conglomerado'];

        if ($input['password'] != "") {
            $user->password = bcrypt($input['password']);
        }
        


        $user->save();
        return redirect('/root/usuarios');
    }


    public function eliminar($id) {
        User::destroy($id);
        
        return redirect('/root/usuarios');
    }

    public function cambiarimagen(){
        
        $id = Input::get('id_user');
        $user = User::find($id);

        $upload =  $this->redimensionar(100, 100);
        $return = -1;
        if($upload['status']) {

            $user->image = $upload['name'];
            $user->save();
            $return = $upload['name'];
        }

        
        return redirect('/home');
    }

    function redimensionar($sWidth, $sHeight){
        
        $file = Input::file('img');
        $status = true;
        $fileName = '';



        if ($file !== NULL) {
            
            if($file)
                $ext = $file->guessClientExtension();
            
            // get size
            $size = $file->getClientSize();
            // looking for format and size validity
            $name = date('Y-m-d_His') . $file->getClientOriginalName();
            $name = str_replace(" ", "_", $name);

            $ruta_imagen = $this->path.$name;
            $miniatura_ancho_maximo = $sWidth;
            $miniatura_alto_maximo = $sHeight;

            if ($file->move($this->path, $name)) {

                $info_imagen = getimagesize($ruta_imagen);
                $imagen_ancho = $info_imagen[0];
                $imagen_alto = $info_imagen[1];
                $imagen_tipo = $info_imagen['mime'];


                $proporcion_imagen = $imagen_ancho / $imagen_alto;
                $proporcion_miniatura = $miniatura_ancho_maximo / $miniatura_alto_maximo;

                if ( $proporcion_imagen > $proporcion_miniatura ){
                    $miniatura_ancho = $miniatura_alto_maximo * $proporcion_imagen;
                    $miniatura_alto = $miniatura_alto_maximo;
                } else if ( $proporcion_imagen < $proporcion_miniatura ){
                    $miniatura_ancho = $miniatura_ancho_maximo;
                    $miniatura_alto = $miniatura_ancho_maximo / $proporcion_imagen;
                } else {
                    $miniatura_ancho = $miniatura_ancho_maximo;
                    $miniatura_alto = $miniatura_alto_maximo;
                }

                $x = ( $miniatura_ancho - $miniatura_ancho_maximo ) / 2;
                $y = ( $miniatura_alto - $miniatura_alto_maximo ) / 2;

                switch ( $imagen_tipo ){
                    case "image/jpg":
                    case "image/jpeg":
                    $imagen = imagecreatefromjpeg( $ruta_imagen );
                    break;
                    case "image/png":
                    $imagen = imagecreatefrompng( $ruta_imagen );
                    break;
                    case "image/gif":
                    $imagen = imagecreatefromgif( $ruta_imagen );
                    break;
                }

                $lienzo = imagecreatetruecolor( $miniatura_ancho_maximo, $miniatura_alto_maximo );
                $lienzo_temporal = imagecreatetruecolor( $miniatura_ancho, $miniatura_alto );

                imagecopyresampled($lienzo_temporal, $imagen, 0, 0, 0, 0, $miniatura_ancho, $miniatura_alto, $imagen_ancho, $imagen_alto);
                imagecopy($lienzo, $lienzo_temporal, 0,0, $x, $y, $miniatura_ancho_maximo, $miniatura_alto_maximo);


                unlink($ruta_imagen);
                imagejpeg($lienzo, $this->path.$name, 80);

                return array('status' => true, 'name' => $name);

            }
            
        }

        return array('status' => false, 'name' => $fileName);
    }


}
