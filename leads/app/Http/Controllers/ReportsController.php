<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;
use Mail;
use Config;

use Maatwebsite\Excel\Facades\Excel;

use App\User;
use App\models\Gerente;
use App\Models\City;
use App\Models\Lead;
use App\Models\Goal;
use App\Models\Brand;
use App\Models\Modelo;
use App\CalculateReports;
use App\Models\Distributor;
use App\Models\Conglomerate;
use App\Models\User_rol;

class ReportsController extends Controller {

    public function __construct() {
      
        $this->middleware('auth');
    }

    public function exportExcel($type) {

        $file_name = "Reporte_mensual";

        if (strtolower($type) == 'annual') {

          $file_name = "Reporte_anual";          
        }

        CalculateReports::init($type);        

        self::generateExcel(CalculateReports::$data_report, $file_name);
    }

    public function export_all_leads() {

        $rol = User_rol::where('user_id', '=', Auth::user()->id)
                        ->first();

        ini_set('memory_limit', -1);

        if ($rol->rol_id == 2) {
            $datofilter = $rol->brand_id;
            $filter = 'l.brand_id';
            # 2  = brand_id, 3 = disttributor_id,  4 = conglomerate_id, 5 = asesor_id
        }
        if ($rol->rol_id == 3) {
            $datofilter = $rol->distributor_id;
            $filter = 'l.distributor_id';
        }
        if ($rol->rol_id == 4) {
            $datofilter = $rol->conglomerate_id;
            $filter = 'bd.conglomerate_id';
        }
        if ($rol->rol_id == 5) {
            $datofilter = $rol->user_id;
            $filter = 'l.assessor_id';
        }

        $sql = "
                    select
                    l.id as id,
                    p.name as Nombre,
                    p.last_name as Apellido,
                    p.email as Email,
                    p.phone as Telefono,
                    l.origen as Origen,
                    p.rut as rut,
                    m.name as Modelo,
                    ma.name as Modelo2,
                    d.name as Concesionario,
                    b.name as Marca,
                    s.name as Estado,
                    r.name as Motivo,
                    l.created_at as Fecha_Registro,
                    l.managed as Fecha_primera_gestion,
                    a.date as Fecha_Agenda,
                    a.time as Hora_Agenda,
                    u.name as Asesor,
                    bd.conglomerate_id as Conglomerado
                    from leads l 
                    inner join persons p on p.id = l.person_id
                    inner join reasons r on r.id = l.reason_id
                    inner join status s on s.id = r.status_id
                    inner join models m on m.id = l.model_id
                    left join models ma on ma.id = l.model_alter
                    inner join distributors d on d.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    inner join brands b on b.id = l.brand_id
                    inner join users u on u.id = l.assessor_id
                    left join reminders a on a.lead_id = l.id

                    where
                    ".$filter." = '".$datofilter."'

                    union

                    select
                    l.id as id,
                    p.name as Nombre,
                    p.last_name as Apellido,
                    p.email as Email,
                    p.phone as Telefono,
                    l.origen as Origen,
                    p.rut as rut,
                    m.name as Modelo,
                    ma.name as Modelo2,
                    d.name as Concesionario,
                    b.name as Marca,
                    s.name as Estado,
                    r.name as Motivo,
                    l.created_at as Fecha_Registro,
                    l.managed as Fecha_primera_gestion,
                    a.date as Fecha_Agenda,
                    a.time as Hora_Agenda,
                    u.name as Asesor,
                    bd.conglomerate_id as Conglomerado
                    from leads l 
                    inner join persons p on p.id = l.person_id
                    inner join reasons r on r.id = l.reason_id
                    inner join status s on s.id = r.status_id
                    inner join models m on m.id = l.model_id
                    inner join models ma on ma.id = l.model_alter
                    inner join distributors d on d.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    inner join brands b on b.id = l.brand_id
                    inner join users u on u.id = l.assessor_id
                    inner join reminders a on a.lead_id = l.id

                    where
                    ".$filter." = '".$datofilter."'

                    order by Fecha_Registro DESC
                    limit 4000
                    
                           
               ";
        
        // var_dump($rol);
        //var_dump($sql);
        //die();
        $result = DB::select($sql); 
        
        
        $result = Lead::hydrate($result); 
        
        self::generateExcel($result, 'leads');
    }

    public static function generateExcel($data = array() , $file_name) {

        Excel::create($file_name, function($excel) use($data) {
            $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->fromArray($data);
            });
        })->export('xlsx');
    }

    public function filters(Request $request) {

      $inputs = $request->all();

      ini_set('memory_limit', -1);

        $marca = Brand::find(Auth::user()->rol->brand_id);
        $models = $marca->modelos;
        $distributors = $marca->concesionarios;
        $cities = City::all();
        $conglomerates = Conglomerate::all();
        $statistics = json_encode($this->getStatistics($inputs));
        unset($inputs['_token']);
        $filtros = json_encode($inputs);

        return view("mobile.test.statistics")
                ->with(compact('statistics'))
                ->with(compact('cities'))
                ->with(compact('distributors'))
                ->with(compact('conglomerates'))
                ->with(compact('filtros'))
                ->with(compact('models'));
    }

    public function statistics($other_filters = []) {

        ini_set('memory_limit', -1);


        $statistics = json_encode($this->getStatistics($other_filters));

        $marca = Brand::find(Auth::user()->rol->brand_id);
        $models = $marca->modelos;
        $distributors = $marca->concesionarios;
        //$models = Modelo::where('brand_id', '=', Auth::user()->rol->brand_id )->get();
        //$distributors = Distributor::all();

        $cities = City::all();
        $conglomerates = Conglomerate::all();
        //$statistics = json_encode($this->getStatistics($other_filters));
        $filtros = json_encode([]);

        return view("mobile.test.statistics")
                ->with(compact('statistics'))
                ->with(compact('cities'))
                ->with(compact('distributors'))
                ->with(compact('conglomerates'))
                ->with(compact('filtros'))
                ->with(compact('models'));
    } 


    public function getStatistics($other_filters = []) {

        ini_set('memory_limit', -1);


        $rol = User_rol::where('user_id', '=', Auth::user()->id)
                        ->first();
        switch ($rol['rol_id']) {
           case '2':
               $datofilter = $rol['brand_id'];
               $filter = 'leads.brand_id';
            break;
            case '3':
                $datofilter = $rol['distributor_id'];
                $filter = 'leads.distributor_id';
            break;
            case '4':
                $datofilter = $rol['conglomerate_id'];
                $filter = 'brand_distributor.conglomerate_id';
                break;
                case '5':
                    $datofilter = $rol['user_id'];
                    $filter = 'leads.assessor_id';
                break;
           
           default:
               # code...
               break;
        }

        $statistics = array();
      
        //leads
        $total_lead                 = self::total_leads('t', 't',  $filter, $datofilter, $other_filters);
        $total_leads_sin_gestionar  = self::total_leads(1, 0, $filter, $datofilter, $other_filters);
        $total_leads_no_gesitonable = self::total_leads(2, 0, $filter, $datofilter, $other_filters);
        $total_leads_gesitonados    = self::total_leads(3, 0, $filter, $datofilter, $other_filters);
        $total_leads_interesados    = self::total_leads(4, 0, $filter, $datofilter, $other_filters);
        $total_leads_no_interesados = self::total_leads(5, 0,  $filter, $datofilter, $other_filters);
        
        //tasa Citas
        $total_tasa_contactado      = self::total_leads(6, 1, $filter, $datofilter, $other_filters);
        $total_tasa_agendado        = self::total_leads(7, 1, $filter, $datofilter, $other_filters);
        $total_tasa_cita_efectiva   = self::total_leads(8, 1, $filter, $datofilter, $other_filters);
        $total_tasa_cierre          = self::total_leads(9, 1, $filter, $datofilter, $other_filters);
        //tiempos de respuesta
        $menos_dos_horas            = self::total_tiempo_respuesta(14,"",$filter, $datofilter, $other_filters);
        $menos_un_dia               = self::total_tiempo_respuesta(15,"",$filter, $datofilter, $other_filters);
        $mas_de_un_dia              = self::total_tiempo_respuesta(16, "", $filter, $datofilter, $other_filters);
        //tasa agendado
        $total_reason_agendado      = self::total_leads(10, 0, $filter, $datofilter, $other_filters);
        $total_reason_proceso       = self::total_leads(11, 0, $filter, $datofilter, $other_filters);
        $total_reason_cerrado_gestion = self::total_leads(12, 0, $filter, $datofilter, $other_filters);
        $total_reason_cierre_vitrina = self::total_leads(13, 0, $filter, $datofilter, $other_filters);


        //estadisticas
        $statistic['total_leads'] = $total_lead;
        $statistic['total_leads_sin_gestionar'] = $total_leads_sin_gestionar;
        $statistic['total_leads_no_gesitonable'] = $total_leads_no_gesitonable;
        $statistic['total_leads_gesitonados'] = $total_leads_gesitonados;
        $statistic['total_leads_interesados'] = $total_leads_interesados;
        $statistic['total_leads_no_interesados'] = $total_leads_no_interesados;
        

        $statistic['total_tasa_contactado'] = $total_tasa_contactado;
        $statistic['total_tasa_agendado'] = $total_tasa_agendado;
        $statistic['total_tasa_cita_efectiva'] = $total_tasa_cita_efectiva;
        $statistic['total_tasa_cierre'] = $total_tasa_cierre;

        $statistic['menos_dos_horas'] = $menos_dos_horas;
        $statistic['menos_un_dia'] = $menos_un_dia;
        $statistic['mas_de_un_dia'] = $mas_de_un_dia;


        $statistic['total_reason_agendado'] = $total_reason_agendado;
        $statistic['total_reason_proceso'] = $total_reason_proceso;
        $statistic['total_reason_cerrado_gestion'] = $total_reason_cerrado_gestion;
        $statistic['total_reason_cierre_vitrina'] = $total_reason_cierre_vitrina;
        
        return $statistic;
    }
    //codigo Brahian
    public static function total_leads($reason, $column, $filter, $datofilter, $other_filters) {
       
        $total = 0; 
        $busca ="";

        if ($reason == 't' && $column == 't') {
             $total = Lead::join('brand_distributor', function($join) {
                               $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                            })
                            ->join('reasons', function($join) {
                               $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where( $filter , '=', $datofilter )
                            ->where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                            ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                            ->select('leads.id')->count();
        }else if (is_numeric($reason) && is_numeric($column) ) {
        

            $colums  =[0 =>'leads.reason_id', 1 =>'reasons.funnel_id'];
            $estadistica=[0 => '',1 => '= 1', 2 => 'in(2,3,4)', 3 =>'>= 5', 
                    4 => 'in(7,8,11,12,13,14,15,16)', 5 =>'in(9,10)', 6 => '>= 2',
                    7 =>'>= 3', 8 => '>= 4' , 9 =>'= 5', 10 =>'in(8,7,14)',
                    11 =>'in(11,16)', 12 => 'in(2,3,4,9,10)', 13 => 'in(12,13,15)'];
            //datos a buscar por cada colum y valor de acuerdo  a la estadistica

            $colum=$colums[$column];
            $in=$estadistica[$reason];
            $busca .= " ".$colum." ".$in." ";
        
            $total = Lead::join('brand_distributor', function($join) {
                                   $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                                })
                                ->join('reasons', function($join) {
                                   $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where( $filter , '=', $datofilter )
                                ->whereRaw(' '.$busca.' ')
                                ->where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                                ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                                ->select('leads.id')->count();
         
        }
            
        if (count($other_filters)> 0) {     
           
          $total = self::filtros_leads($reason, $column, $filter, $datofilter, $other_filters);  
        }
        return $total;
    }
    //codigo Brahian
    public static function filtros_leads($reason, $column ="", $filter, $datofilter, $other_filters) {
    
        $busca ="";
        date_default_timezone_set('America/Santiago');
        $fechaMedia  = date('Y-m');

        if (is_numeric($reason) && is_numeric($column)) {

            $colums  =[0 =>'leads.reason_id', 1 =>'reasons.funnel_id'];
            $estadistica=[0 => '',1 => '= 1', 2 => 'in(2,3,4)', 3 =>'>= 5', 
                    4 => 'in(7,8,11,12,13,14,15,16)', 5 =>'in(9,10)', 6 => '>= 2',
                    7 =>'>= 3', 8 => '>= 4' , 9 =>'= 5', 10 =>'in(8,7,14)',
                    11 =>'in(11,16)', 12 => 'in(2,3,4,9,10)', 13 => 'in(12,13,15)'];
            //datos a buscar por cada colum y valor de acuerdo  a la estadistica
            $colum=$colums[$column];
            $in=$estadistica[$reason];
            
            $busca .= " ".$colum." ".$in." "; 
        }

        if (empty($column)) {

            if ($reason == 13 || $reason == 14 || $reason == 15) {
                switch ($reason) {
                    case '13':
                    $busca = "timediff(leads.managed, leads.created_at) <= '02:00:00' AND DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= '09:00:00' AND TIME(leads.created_at) <= '19:00:00' ";
                    break;
                    case '14':
                    $busca = "timediff(leads.managed, leads.created_at) > '02:00:00' AND 
                              timediff(leads.managed, leads.created_at) <= '24:00:00' AND 
                              DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND
                              TIME(leads.created_at) >= '09:00:00' AND TIME(leads.created_at) <= '19:00:00'";
                    break;
                    case '15':
                         $busca ="timediff(leads.managed, leads.created_at) > '24:00:00' AND DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) <= '09:00:00' AND TIME(leads.created_at) <= '19:00:00' ";
                    break;
                }
            }
        }
        
        
        foreach ($other_filters as $key => $value) {
            if (strtolower($key)=="modelo") {
              //completa sentencia para filtro por modelo
              $busca.=" and leads.model_id = '".trim(strtolower($value))."' ";
            }
            if (strtolower($key) == 'ciudad') {
              $busca.= " and leads.distributor_id = ( select distributors.city_id  from distributors where  city_id='".trim(strtolower($value)).")' group by(distributors.city_id)";
            }
            if (strtolower($key) == 'vitrina') {
              $busca.= " and leads.distributor_id = '".trim(strtolower($value))."' ";
            }
            if(strtolower($key)=="fecha_inicial"){
              $date_ini = $other_filters['fecha_inicial'].' 00:00:00';
              $date_end = $other_filters['fecha_final'].' 23:59:59';
                if ( $other_filters['fecha_inicial'] ==  $other_filters['fecha_final']) {
                    $busca.= " and leads.created_at = '".$value."'";  
                } else {
                    $busca.= " and leads.created_at >= '".$date_ini."' and leads.created_at <= '".$date_end."'";  
                }       
            }
        }

        $fecha=strpos($busca, 'leads.created_at');
            if ($fecha === false) {
                $busca.=' and leads.created_at like  "%'.$fechaMedia.'%"';
            }
        $buscar=trim($busca ,' and');

        //total de leads por filtro
        if ($reason == 't' && $column == 't') {
             $result = Lead::join('brand_distributor', function($join) {
                               $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                            })
                            ->join('reasons', function($join) {
                               $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->join('models', function($join) {
                                       $join->on('leads.model_id', '=', 'models.id');
                                    })
                            ->where( $filter , '=', $datofilter )
                            ->WhereRaw(''.$buscar.' ')
                            ->select('leads.id')->count();
        }else{

            $result = Lead::join('brand_distributor', function($join) {
                                       $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                                    })
                                    ->join('reasons', function($join) {
                                       $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                                    ->join('models', function($join) {
                                       $join->on('leads.model_id', '=', 'models.id');
                                    })
                                    ->where($filter, '=', $datofilter)
                                    ->WhereRaw(''.$buscar.' ')
                                    ->count('leads.id');
        }
        
        return $result;
        
    }
        //codigo Brahian
     public static function total_tiempo_respuesta($reason, $column, $filter, $datofilter, $other_filters) {

       $total = 0;

        if (count($other_filters) == 0) {
            switch ($reason) {
                case '14':
                    $total =Lead::join('brand_distributor', function($join) {
                                   $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                                })
                                ->join('reasons', function($join) {
                                   $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where( $filter , '=', $datofilter )
                                ->whereRaw('timediff(leads.managed, leads.created_at) <= "02:00:00" AND DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ')
                                ->where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                                ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                                ->select('leads.id')->count(); 
                break;
                case '15':
                    $total =Lead::join('brand_distributor', function($join) {
                                   $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                                })
                                ->join('reasons', function($join) {
                                   $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where( $filter , '=', $datofilter )
                               ->whereRaw('timediff(leads.managed, leads.created_at) > "02:00:00" AND 
                                        timediff(leads.managed, leads.created_at) <= "24:00:00" AND DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ')
                                ->where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                                ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                                ->select('leads.id')->count(); 
                break;
                case '16':
                    $total =Lead::join('brand_distributor', function($join) {
                                   $join->on('brand_distributor.distributor_id', '=', 'leads.distributor_id');
                                })
                                ->join('reasons', function($join) {
                                   $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where( $filter , '=', $datofilter )
                               ->whereRaw('timediff(leads.managed, leads.created_at) > "24:00:00" AND DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) <= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ')
                                ->where(DB::raw('MONTH(leads.created_at)'), '=', date('m'))
                                ->where(DB::raw('YEAR(leads.created_at)'), '=', date('Y'))
                                ->select('leads.id')->count(); 
                break;
            }
        }else{
            $total = self::filtros_leads($reason, $column, $filter, $datofilter, $other_filters); 
        }
       return $total;
    }
 
    /*public  function emaileverydays(){

        date_default_timezone_set('America/Santiago');
        $fechaActual = date('Y-m-d');
        $fechaMedia  = date('Y-m'); 
        //managers -brands- distributors
        $managers = DB::table('managers')
                        ->join('brands',function($join){
                        $join->on('brands.id', '=', 'managers.brand_id');            
                        })
                        ->groupby('managers.brand_id')
                        ->select('managers.id AS idManagers', 'managers.brand_id')
                        ->get();

        $allBrand = Brand::all();
        $allDistributors = Distributor::all();
        foreach ($managers as $key ) {
            $funnelTotal = Lead::where('brand_id', '=', $key->brand_id)
                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                ->count();
            $leadsContactado = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(2,3,4,5))
                                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();
            $leadsCita = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->join('reminders', function($join){
                                            $join->on('reminders.lead_id', '=', 'leads.id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(3,4,5))
                                        ->where('reminders.created_at', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();
            $leadsCitaEfectiva = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->join('reminders', function($join){
                                            $join->on('reminders.lead_id', '=', 'leads.id');
                                        })
                                        ->where('reminders.date', 'like', '%'.$fechaMedia.'%')
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(4,5))
                                        ->select('leads.id')
                                        ->count();
             $leadsVenta = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->where('reasons.funnel_id', '=', 5)
                                        ->where('leads.purchase_date', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();
            //porcentajes para funnel
            if ($funnelTotal == 0) {
                $funnelTotal= 1;
            }
            $datos['totalLeadsFunnel'] = number_format(($funnelTotal *100)/$funnelTotal, 2, '.', '');
            $datos['porcentaje_Leads_contactados'] = number_format(($leadsContactado *100)/$funnelTotal, 2, '.', '');
            $datos['porcentaje_Leads_cita'] = number_format(($leadsCita *100)/$funnelTotal, 2, '.', '');
            $datos['porcentaje_Leads_citaEfectiva'] = number_format(($leadsCitaEfectiva *100)/$funnelTotal, 2, '.', '');
            $datos['porcentaje_Leads_venta'] = number_format(($leadsVenta *100)/$funnelTotal, 2, '.', '');
            //leads por fecha 
            $totalLeads = Lead::join('reminders', function($join){
                            $join->on('leads.id', '=', 'reminders.lead_id');
                            })
                            ->join('reasons', function($join){
                            $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->Where('reminders.date', '=', $fechaActual)
                            ->where('reasons.funnel_id', '>', 2)
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->count();

            $leadsBrandsAgendasPendientes = Lead::join('reminders', function($join){
                            $join->on('leads.id', '=', 'reminders.lead_id');
                            })
                            ->join('reasons', function($join){
                            $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->Where('reminders.date', '=', $fechaActual)
                            ->where('leads.brand_id', '=', $key->idManagers)
                            ->whereIn('leads.reason_id', array(95,103,110))
                            ->count();
            $ficherovista=array();
            foreach ($allDistributors as $distributor) {

                $totalLeadsDistributor = Lead::join('reminders', function($join){
                            $join->on('leads.id', '=', 'reminders.lead_id');
                            })
                            ->join('reasons', function($join){
                            $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->Where('reminders.date', '=', $fechaActual)
                            ->where('reasons.funnel_id', '>', 2)
                            ->where('leads.distributor_id', '=', $distributor->id)
                            ->count();

                $leadsdistributorsPendientes = Lead::join('reminders', function($join){
                            $join->on('leads.id', '=', 'reminders.lead_id');
                            })
                            ->join('reasons', function($join){
                            $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->Where('reminders.date', '=', $fechaActual)
                            ->where('leads.distributor_id', '=', $distributor->id)
                            ->whereIn('leads.reason_id', array(95,103,110))
                            ->count();

                $leadsEnSeguimiento = Lead::join('reasons', function($join){
                                    $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                            ->join('reminders', function($join){
                            $join->on('leads.id', '=', 'reminders.lead_id');
                            })
                            ->where('leads.distributor_id', '=', $distributor->id)
                            ->where('reminders.date', 'like', '%'.$fechaMedia.'%')
                            ->whereIn('leads.reason_id', array(57,58,104,105,106,107,108))
                            ->count();

                $leadsEfectivos = Lead::join('reasons', function($join){
                                    $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                                    ->join('reminders', function($join){
                                    $join->on('leads.id', '=', 'reminders.lead_id');
                                    })
                            ->where('leads.distributor_id', '=', $distributor->id)
                            ->where('reminders.date', 'like', '%'.$fechaMedia.'%')
                            ->where('reasons.funnel_id', '>', 4)
                            ->count();
                $leadsVentas = Lead::join('reasons', function($join){
                                    $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                                    ->join('reminders', function($join){
                                    $join->on('leads.id', '=', 'reminders.lead_id');
                                    })
                            ->where('leads.distributor_id', '=', $distributor->id)
                            ->where('reminders.date', 'like', '%'.$fechaMedia.'%')
                            ->where('reasons.funnel_id', '=', 5)
                            ->count();

                //$datos['nameDistributor'.$distributor->id] = $distributor->name;
                $datos['totalDistributos'] = $totalLeadsDistributor;
                $datos['totalDistributosPendientes'] = $leadsdistributorsPendientes;

                /*
                $ficherovista.='<tr><td>'.$distributor->name.'</td>';
                $ficherovista.='<td>'. $totalLeadsDistributor.'<td>';
                $ficherovista.='<td>'. $leadsEnSeguimiento.'<td>';
                $ficherovista.='<td>'. $leadsEfectivos.'<td>';
                $ficherovista.='<td>'. $leadsVentas.'<td>';
                $ficherovista.='<td>'.$leadsdistributorsPendientes.'<td><tr>';
                
                $arrayName = array( 'nombreDistridor'.$distributor->id => $distributor->name , 'totalLeadsDistributor'.$distributor->id => $totalLeadsDistributor, 'leadsEnSeguimiento'.$distributor->id => $leadsEnSeguimiento, 'leadsEfectivos'.$distributor->id => $leadsEfectivos, 'leadsVentas'.$distributor->id => $leadsVentas, 'leadsDsitribuidorPendiente'.$distributor->id =>$leadsdistributorsPendientes); 
                array_push($ficherovista, $arrayName);
            }
            
            //$datos['correos'] = $key->email;
            $datos['totalLeads_brand'] = $totalLeads;
            $datos['totalLeadsAgendas_pendientes'] = $leadsBrandsAgendasPendientes;
            //$datos['fichero'] = $ficherovista;
            $datos['fecha'] = $fechaActual;
            $datos['fichero'] = $ficherovista;

            //lista de correos a enviar
            $correosEnviar = DB::Table('managers')
                                    ->select('email')
                                    ->where('brand_id', '=', $key->brand_id)
                                    ->get();
            $data=[];
            $agregar = "";
            foreach ($correosEnviar as $corre) {
                $agregar.= "". str_replace(",", ",", $corre->email).",";
            }
        
            $data['correos']=trim($agregar, ',');
            $data['correo'] =explode(",", $data['correos']);
            $data["Asunto"] = "Contacto Audi";
            $data['asunto'] = " Reports LM2 " .$fechaActual; 

            
            Mail::send('emails.audiReport',['data' => $datos], function ($message) use ($data) {
                $message->from('noreply@cotizadoronline.com.co', 'Leads Manager');
                $message->subject($data['asunto']);
                $message->to($data['correo']);
            }); 
        }
    }*/
}