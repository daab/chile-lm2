<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Input;

use App\User;
use App\Models\Rol;
use App\Models\Brand;
use App\Models\Distributor;
use App\Models\Conglomerate;
use App\Models\Group_distributor;
use App\Security;


class UserController extends Controller {

    private $path;

    public function __construct() {

        $this->middleware('auth');
    }

    public function index() {


        // $role = Auth::user()->roles()->first();

        $users = User::all();
        return view('root.usuarios.listar')
                    ->with(compact('users'));
    }

    public function nuevo($id="") {

        $brands = Brand::All();

        $distributors = Distributor::all();

        $conglomerates = Conglomerate::all();

        $roles = Rol::all();

        $user = "";
    	
        if(!empty($id)) {

            $user = User::Find($id);
        }

        return view('root.usuarios.form')
                    ->with(compact('user'))
                    ->with(compact('roles'))
                    ->with(compact('brands'))
                    ->with(compact('distributors'))
                    ->with(compact('conglomerates'));
    }

    public function guardar(Request $request) {
        
        $user = new User();        

        $param = $request->all();

        if (!empty($param['id'])) {
           
            $user = User::find($param['id']);
        }

        $user->name = $param['name'];

        $user->email = $param['email'];
  
        if (isset($param['brand']) && $param['brand'] != "" ) {
            $brand = ($param['brand'] != "" ? $param['brand'] : NULL);
        }else{

            if ( isset($param['conglomerate']) && $param['conglomerate'] != "" ) {
                $conglomerado = conglomerate::where('id', '=', $param['conglomerate'])->get();
                $brand = $conglomerado[0]->brand_id;
            }elseif(isset($param['distributor']) && $param['distributor'] > 0 ){
                $brand_id = Group_distributor::where('distributor_id', '=', $param['distributor'])->get();
                $brand = $brand_id[0]->brand_id;
            }
        }
         
       /* if (isset($param['brand_distributor']) && $param['brand_distributor'] != "") {
            
            $brand = ($param['brand_distributor'] != "" ? $param['brand_distributor'] : NULL);
        }  */      
        
        $distributor = ($param['distributor'] != "" ? $param['distributor'] : NULL);
        
        $conglomerate = ($param['conglomerate'] != "" ? $param['conglomerate'] : NULL);

        if (!empty($param['password'])) {

            $user->password = bcrypt($param['password']);
        }        

        $data = array('brand_id' => $brand,

                 'distributor_id' => $distributor,

                 'conglomerate_id' => $conglomerate,

                 'updated_at' => date('Y-m-d H:m:s'));

        if (empty($param['id'])) {

            $data['created_at'] = date('Y-m-d H:m:s');
        }
        
        $user->save();        

        $role = Rol::find($param['role']);

        $user->roles()->sync([$role->id => $data]);

        return redirect('/root/usuarios');
    }


    public function eliminar($id) {

        User::destroy($id);
        
        return redirect('/root/usuarios');
    }

    public function toList_brand_distributor(Request $request) {

        $param = $request->all();

        if ($param['distributor']!="") {

            $distributor = Distributor::find($param['distributor']);

            $brands = $distributor->marcas;

            return view('root.usuarios.list_brands')
                        ->with(compact('brands'));
        }    
    }
}
