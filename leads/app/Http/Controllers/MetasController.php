<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use Maatwebsite\Excel\Facades\Excel;

use App\User;
use App\helpers;
use App\Models\City;
use App\Models\Lead;
use App\Models\Goal;
use App\Models\Brand;
use App\Models\Modelo;
use App\Models\Distributor;
use App\Models\Conglomerate;


class MetasController extends Controller {

    public function __construct() {
      
        $this->middleware('auth');
    }

    public function index($consecionario) {

        $year = date('Y');

        $goals_0 = [ 
                      ["value" => 0, "month" => 'Enero', "year" => $year],
                      ["value" => 0, "month" => 'Febrero', "year" => $year],
                      ["value" => 0, "month" => 'Marzo', "year" => $year],
                      ["value" => 0, "month" => 'Abril', "year" => $year],
                      ["value" => 0, "month" => 'Mayo', "year" => $year],
                      ["value" => 0, "month" => 'Junio', "year" => $year],
                      ["value" => 0, "month" => 'Julio', "year" => $year],
                      ["value" => 0, "month" => 'Agosto', "year" => $year],
                      ["value" => 0, "month" => 'Septiembre', "year" => $year],
                      ["value" => 0, "month" => 'Octubre', "year" => $year],
                      ["value" => 0, "month" => 'Noviembre', "year" => $year],
                      ["value" => 0, "month" => 'Diciembre', "year" => $year]
                  ];

        $goals = Goal::where('year', '=', $year)
                     ->where('distributor_id', '=', $consecionario)->get();      

        $distributor = Distributor::find($consecionario);

        if (count($goals) == 0) {

          $goals = $goals_0;
        }

        return view('concesionario.metas')
               ->with(compact('distributor'))
               ->with(compact('goals'));
    }

    public function save(Request $request) {

        $inputs = $request->all();

        foreach ($inputs as $key => $value) {

            $month = helpers::getMonth($key);

            $goal = Goal::where('distributor_id', '=', $inputs['distributor_id'])
                          ->where('month', '=', $month)->first();

            if (count($goal) == 0) {

                $goal = new Goal();
            }
            
            if ($key != "_token" && $key != "distributor_id") {

              $goal->distributor_id = $inputs['distributor_id'];
              $goal->month = helpers::getMonth($key);
              $goal->value = $value;
              $goal->year = date('Y');
              $goal->save();
            }
        }

       return $this->index($inputs['distributor_id']);
    }
}