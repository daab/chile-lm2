<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\ActionsRol;

use App\Security;

use App\iLeads;

use App\User;

use Input;


class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $agente;
	private $vista;
	private $month;
	private $year;
	private $status;
	private $source = "agendado";

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {

		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index($ano="", $mes="")	
	{

		$role = Auth::user()->roles()->first();

		// Aciones segun rol
		$actionsRol = new ActionsRol();	

		$this->vista .= $actionsRol->getHome($role);
		
		$actionsRol->setObject($role);

		if ($this->vista == "root") {

			return redirect()->action('UserController@index');

		} else {

			$leads = $actionsRol->toListLeads();
		}
		
		//return $this->goToView($actionsRol, $leads);
		$this->status = 'agendado';
		return $this->goToView($actionsRol, $leads, 'status');
	}

	public function goToView($actionsRol, $leads, $view = 'default') {

		$asesores = "";
		$concesionarios = "";

		// Obtiene los asesores de un concesionario
		if (isset($actionsRol->getObject()->asesores)) {
			$asesores = $actionsRol->getObject()->asesores;
		}

		// Obtiene los concesionarios de una marca
		if (isset($actionsRol->getObject()->concesionarios)) {
			$concesionarios = $actionsRol->getObject()->concesionarios;
		}

		// Generar Url para la gestion
		$actionsRol->generateUrlManageLeads($leads, $this->source);

		// Obtiene la vista siguiente
		// $this->vista = $actionsRol->getHome();

		$this->vista =  "mobile.test.".$view;

		return view($this->vista)
				->with(compact('asesores'))
				->with(compact('concesionarios'))
				->with('month', $this->month)
				->with('year', $this->year)
				->with('status', $this->status)
				->with(compact('leads'));	
	}

	public function filtro(Request $request) {


		$role = Auth::user()->roles()->first();

		$actionsRol = new ActionsRol();
		$actionsRol->setObject($role);
		$leads = $actionsRol->filterLeads($request->all());

		$view = $request->all();
		$this->source = $view['value'];
		if ($view['case'] == 'calendar' || $view['case'] == 'all') {

			$this->month = date('m');
			$this->year = date('Y');

			if (isset($view['month'])) {

				$this->month = $view['month'];
			}

			if (isset($view['year'])) {
				
				$this->year = $view['year'];
			}

			$view['case'] = 'all';

		} else if ($view['case'] == 'status') {
			$this->status = $view['value'];
			$this->source = $view['value'];
		}

		return $this->goToView($actionsRol, $leads, $view['case']);
	}
}