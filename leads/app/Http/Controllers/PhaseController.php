<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use App\Models\Rol;
use App\Models\Module;
use App\Models\Phase;
use App\User;

class PhaseController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	
	{
		$phases = Phase::all();

		return view('root.fase.default')
				->with(compact('phases'));
	}

	public function create($id = "") {

		$phase = "";

		if (!empty($id)) {

			$phase = Phase::find($id);
		}

		return view('root.fase.form')
				->with(compact('phase'));
	}

	public function save(Request $requets) {

		$phase = new Phase;

		$param = $requets->all();

		if (!empty($param['id'])) {

			$phase = Phase::find($param['id']);			
		}

		$phase->name = $param['name'];
		$phase->sort = $param['sort'];

		$phase->save();

		return redirect('/root/fases');
	}

	public function delete($id = "") {

		if (!empty($id)) {
			
			Phase::destroy($id);
		}

		return redirect('/root/fases');
	}
}