<?php 

namespace App\Http\Controllers;
use App\Models\Lead;

use App\Models\Conglomerate;
use App\Models\Distributor;
use App\Models\TimeZone;
use App\Models\Reminder;
use App\Models\Status;
use App\Models\Reason;
use App\Models\Modelo;
use App\Models\Domain;
use App\Models\Brand;
use App\Models\Phase;
use App\Models\Note;
use App\Models\Funnel;
use App\Models\Person;


use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Collection;
use App\Librerias\Mobile_Detect;
use Illuminate\Http\Request;



use App\LeadEntity;
use App\Security;
use App\User;


use Mail;
use Config;


class LeadsController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	private $colores;
	private $time_zone;
	private $max_day_recontact;
	

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
	
		// date_default_timezone_set('America/Santiago'); 
		//Configuracion regional

		$this->max_day_recontact = \Config::get('app.app_max_day_recontact');
		$this->time_zone = TimeZone::all()->first();		
		$this->security = new Security();

		$this->active = "";
		$this->colores = ['En proceso' => '#000000','Lead Duplicado' => '#FF8000','Datos errados'=>'#FF00FF','Cerrado'=>'#FF0000','Vendido'=>'#00FFFF', 'Sin Gestionar' => '#ed1c24', 'Interesado' => '#00a651', 'No Interesado' => '#fff200','No contesto' => '#00aeef','sin revisar' => '#898989','Alerta' => '#000000', '<span>12h</span> sin gestionar' => '#ed1c24' ,'<span>24h</span> sin gestionar' => '#ed1c24' ,'<span>48h</span> sin gestionar' => '#ed1c24'];
		$this->agente = new Mobile_Detect();
	}
	
	
	public function getAsesor($distributor, $brand){
		$leadEntity = new LeadEntity();
		return($leadEntity->getAsesor($distributor, $brand));
	}
	

	public function sendSMS(){

		date_default_timezone_set($this->time_zone->zone);

		//Buscamos todos los leads con citas el dia de mañana
		$today = date_create(date('Y-m-d'));
		date_modify($today,"+1 day");
		$tomorrow =  date_format($today,"Y-m-d");

		$reminders = Reminder::where('date', '=', $tomorrow)->get();

//ToDO: Guardar direccion en la base de datos...		
		$address = [1 => 'https://goo.gl/maps/p7nMG9yyZyM2', 2 => 'https://goo.gl/maps/W6yrAvxxAcN2', 3 => 'https://goo.gl/maps/uE3Q87mHNnN2'];

		foreach ($reminders as $rem) {
			//Solo de los 3 concesionarios
//ToDo: Checkbox en concesionario para enviar SMS
			if ($rem->lead->distributor_id > 0 && $rem->lead->distributor_id < 4 && $rem->lead->person->phone != "") {
			
				//Solo el primer nombre
				$nombre = explode(" ", $rem->lead->person->name);

				$msg = "Hola ".$nombre[0].", recuerda tu cita con Volkswagen en: ".$rem->lead->distributor->name." el ".$rem->date." a las ".$rem->time.". ¿Como llegar? ".$address[$rem->lead->distributor_id];
				//Preparamos la URL
				$url = "https://platform.clickatell.com/messages/http/send?apiKey=FlYc88IoSjGur2AkskWgfQ==&to=57".$rem->lead->person->phone."&content=".urlencode($msg);

				//Enviar el SMS
				$ch3 = curl_init();
				curl_setopt($ch3, CURLOPT_URL, $url);
				curl_setopt($ch3, CURLOPT_RETURNTRANSFER, 1);
				$validacionLead = curl_exec($ch3);
				curl_close($ch3);
			}

			error_log("Todos los mensajes han sido enviados ");
		}

	}
	
	
	private function ws_obc_resultado_visita($opcion, $lead, $commission="", $reagendamiento=-1){
	
	
	        require 'vendor/nusoap/nusoap.php';
			$cliente = new \nusoap_client("http://crmporsche.grupoobc.com.co/custom/service/crearLead/soap.php");

			$err = $cliente->getError();

			if ($err) {
				error_log('Error conexion webservice (Call Center): ' . $err);
			}
			
			
			$datos_persona_entrada = array( "datos_persona_entrada" => array( 'id' => $lead,) );
			$wsName = "ws_obc_resultado_visita";
			
			switch ($opcion) {
				case 1:
					
					if($reagendamiento == -1){
						$datos_persona_entrada = array("datos_persona_entrada" => array(
							'id' => $lead,
							'tipo_agendamiento' => 1,
							'fecha_reagendamiento' => "",
						));
						# code...
						$wsName = "reagendamiento_lead: No asistio";

				   	}else{
				   		$datos_persona_entrada = array("datos_persona_entrada" => array(
							'id' => $lead,
							'tipo_agendamiento' => 2,
							'fecha_reagendamiento' => $reagendamiento,
						));
						# code...
						$wsName = "reagendamiento_lead: Reagendamiento leadman";
				   	}
				   	
				   	$resultado = $cliente->call('reagendamiento_lead',$datos_persona_entrada);
				break;
				case 2:
					# code...
					$wsName = "cerradoSinVenta_lead: No compro";
                	                $resultado = $cliente->call('cerradoSinVenta_lead',$datos_persona_entrada);
				break;
				case 3:
					# code...
					$wsName = "realizadaReunion_lead: Visito";
                	                $resultado = $cliente->call('realizadaReunion_lead',$datos_persona_entrada);
				break;
				case 4:
					# code...
					$wsName = "clienteConOtroAsesor_lead: Lead con otro asesor";
                	                $resultado = $cliente->call('clienteConOtroAsesor_lead',$datos_persona_entrada);
				break;
				case 5:
					# code...
					$wsName = "sincronizarVenta_lead: Vendido";
					$datos_persona_entrada = array( "datos_persona_entrada" => array( 'id' => $lead, 'fechaVenta' => date('Y-m-d'), 'numeComision' => $commission,) );
                	                $resultado = $cliente->call('sincronizarVenta_lead',$datos_persona_entrada);
				break;
				
				default:
					# code...
					$resultado['estado'] = 0;
					error_log("WS no implementado aún, opcion no valida = ".$opcion);
				break;
			}
			
			
			
			if ($resultado['estado'] == 0 && isset($resultado['error'])) {
				error_log($resultado['error']['name']." ".$resultado['error']['description']);
			} else {

				//error_log("Webservice (".$wsName.") consumido correctamente, id: ".$lead." id_tercero ".$resultado['id_tercero']);
				error_log("Webservice (".$wsName.") consumido correctamente, id: ".$lead);
				return true;
			}
	}
	 function wbs(){
		require 'vendor/nusoap/nusoap.php';
	$server = new \nusoap_server();
	 
	$server->configureWSDL('Mi Web Service', 'urn:webservice_leads');
	 
	// Parametros de entrada
	$server->wsdl->addComplexType(  'datos_leads', 
	                                'complexType', 
	                                'struct', 
	                                '', 
	                                '',
	                                array('ID'       => array('name' => 'ID','type' => 'xsd:long'),
	                                      'Marca'    => array('name' => 'Marca','type' => 'xsd:string'),
	                                      'Modelo'   => array('name' => 'Modelo','type' => 'xsd:string'),
	                                      'Ciudad'   => array('name' => 'Ciudad','type' => 'xsd:string'),
	                                      'Concesionario' => array('name' => 'Concesionario','type' => 'xsd:string'),
	                                      'Nombre'   => array('name' => 'Nombre','type' => 'xsd:string'),
	                                      'Apellido' => array('name' => 'Apellido','type' => 'xsd:string'),
	                                      'Correo'   => array('name' => 'Correo','type' => 'xsd:string'),
	                                      'Telefono' => array('name' => 'Telefono','type' => 'xsd:string'),
	                                      'ModeloActual' => array('name' => 'ModeloActual','type' => 'xsd:string'),
	                                      'AgnoModeloActual'  => array('name' => 'AgnoModeloActual','type' => 
	                                      	'xsd:string'),
	                                      'FechaCotizacion'  => array('name' => 'FechaCotizacion','type' => 'xsd:date'),
	                                      'ViaContacto'  => array('name' => 'ViaContacto','type' => 'xsd:string')
	                                    )
	                                
	);
	// Parametros de Salida
	$server->wsdl->addComplexType(  'salida', 
	                                'complexType', 
	                                'struct', 
	                                'all', 
	                                '',
	                                array('FlagRespuesta'   => array('name' => 'FlagRespuesta','type' => 'xsd:string'))
	);
	 
	 
	$server->register(   'leads', // nombre del metodo o funcion
	                    array('datos_leads' => 'tns:datos_leads'), // parametros de entrada
	                    array('return' => 'tns:salida'), // parametros de salida
	                    'urn:webservice_leads', // namespace
	                    'urn:recibedatos#leads', // soapaction debe ir asociado al nombre del metodo
	                    'rpc', // style
	                    'encoded', // use
	                    'La siguiente funcion recibe un arreglo multidimensional de leads y devuelve 1 si todo es exitoso' // documentation,
	                     //$encodingStyle
	);
	 
	 
	function leads($datos) {
	 
	    $msg = '';  
	         
	        if (!is_null($datos)) {
	        	 $msg .= '1'; 
	        	 var_dump($datos);
	        }else {
	        	$msg .= '0'; 
	        	var_dump($datos);
	        }
	       
	    
	     
	    return $msg;
	}
		 
		$HTTP_RAW_POST_DATA = isset($HTTP_RAW_POST_DATA) ? $HTTP_RAW_POST_DATA : '';
		$server->service($HTTP_RAW_POST_DATA);


	}

	public function saveLead(Request $request){

		//Permitimos cualquier lugar
		header('Access-Control-Allow-Origin: *');
		header('Access-Control-Allow-Methods: POST'); 
		
		// date_default_timezone_set('America/Santiago'); 
		date_default_timezone_set($this->time_zone->zone);
		
		error_log("Se recibe un lead");

		$leadEntity = new LeadEntity();
		$result = $leadEntity->save($request);
		echo $result;

	}

	/****
	*
	*
	*   Old Code Depurado...
	*
	********/
	
	public function exportar($type){
		
		$vista = "";
		if($this->agente->isMobile()){
			$vista = "mobile.";
		}

		$vista .= 'dashboard.exportar';
		return view($vista)
				->with('active', 'export')
				->with('type', $type);
	}

	public function getCSVFile(Request $request){
        ini_set('memory_limit', -1);
        $input = $request->all();

    	//buscamos el lead
    	$type = $input['type'];
    	$ffin = $input['ffin']." 23:59:59";
    	$finicio = $input['finicio']." 00:00:00";
    	$normalrol = true;

        // fetch the data
        switch ($type) {
        	case 'marca':
        		$typex = 'marca';
        		$criteria = Auth::user()->marca;
        		$columns = ['Id','Estado','Estado2','Fecha','Ultima Gestion','Gestionado','Origen', 'Source', 'Modelo','Nombres','Apellidos','Telefono','Email','Concesionario','Acepto','Revisado', 'Comision', 'field1', 'field2'];
        	break;
        	case 'concesionario':
        		$typex = 'concesionario';
        		$criteria = Auth::user()->concesionario;
        		$columns = ['Estado','Estado2','Fecha','Ultima Gestion','Gestionado','Origen', 'Source','Modelo','Nombres','Apellidos','Telefono','Email','Marca','Acepto', 'Comentarios', 'Comision', 'field1', 'field2'];
        	break;
        	case 'conglomerado':
        		$columns = ['Estado','Estado2','Fecha','Ultima Gestion','Gestionado','Origen', 'Source','Modelo','Nombres','Apellidos','Telefono','Email','Concesionario','Acepto','Revisado', 'Comision', 'field1', 'field2'];
				$conglo = Conglomerado::where('nombre', '=', Auth::user()
									->conglomerado)
									->first();
				$normalrol = false;

				if ($input['ffin'] == "" || $input['finicio'] == "") {
					$rows = DB::select("SELECT l.* FROM leads as l, concesionarios as c, concesionario_marca as cm WHERE l.concesionario=c.nombre and c.id=cm.concesionario_id and cm.conglomerado_id=".$conglo->id." ORDER BY created_at DESC");
				}else{
					$rows = DB::select("SELECT l.* FROM leads as l, concesionarios as c, concesionario_marca as cm WHERE l.concesionario=c.nombre and c.id=cm.concesionario_id and l.created_at >= '".$finicio."' and l.created_at <= '".$ffin."' and cm.conglomerado_id=".$conglo->id." ORDER BY created_at DESC");
				}
        	break;
        	case 'asesor':
        		$columns = ['Estado','Estado2','Fecha','Ultima Gestion','Gestionado','Origen', 'Source','Modelo','Nombres','Apellidos','Telefono','Email','Marca','Acepto', 'Comentarios','Comision', 'field1', 'field2'];
        		$criteria = Auth::user()->id;
        		$typex = 'user_id'; 

        	break;
        	default:
        		//ToDo
	        	//Verificamos permisos root y dejamos descargar todo...
	        	echo "Ha ocurrido un error generando el archivo CSV";
	        	die();
        	break;
        }
       

        if($normalrol){
	        if ($input['ffin'] == "" || $input['finicio'] == "") {
	        	$rows = Lead::where($typex, '=', $criteria)
	        		->with('anotacion')
	        		->with('user')
	        		->orderby('created_at', 'desc')
	        		->get();

	        }else{
	        	$rows = Lead::where($typex, '=', $criteria)
	        		->where('created_at', '>=', $finicio)
	        		->where('created_at', '<=', $ffin)
	        		->with('anotacion')
	        		->with('user')
	        		->orderby('created_at', 'desc')
	        		->get();
			}
		}

        
        // create a file pointer connected to the output stream
        $output = fopen(public_path().'/reportes/'.Auth::user()->id.'.csv', 'w+');
        
        $columns2 = $columns;
        if($type == 'marca' || $type == 'concesionario'){
            $columns2[] = "Asesor";    
        }
        
		// output the column headings
        fputcsv($output, $columns2,";");

        // loop over the rows, outputting them
        foreach ($rows as $row) {

        	foreach ($columns as $col) {
        		$field = strtolower(trim($col));
			
			if ($field == 'fecha') {
        			$field = "created_at";
        		}elseif($field == "email"){
        			$field = "correo";
        		}elseif ($field == 'comentarios') {
        			$row['comentarios'] = "";
        			foreach ($row['anotacion'] as $value) {
        				$row['comentarios'] .= $value['anotacion']."\n";
        			}
        		}elseif($field == 'ultima gestion'){
        			$field = "updated_at";
        		}
        		

        		$fila[] = utf8_decode($row[$field]);	
        	}
        	
        	//Asesor, solo si es marca o concesionario.
        	if($type == 'marca' || $type == 'concesionario'){
        	    $fila[] = utf8_decode($row['user']['nombres'])." ".utf8_decode($row['user']['apellidos']);	
        	}

            fputcsv($output, $fila,";");   
            unset($fila);
        }
        

        fclose($output);

      	echo Auth::user()->id.".csv";
         
    }

    public function cambioconcesionario(Request $request){
    	$input = $request->all();

    	//buscamos el lead
    	$lead = Lead::find($input['id']);
    	//Actualizamos el concesionario
    	$lead->concesionario = $input['concesionario'];
    	$lead->save();
    }

    public function editar($id){
    	// $id = $this->desencriptar($id);
    	$id = $this->security->decrypt($id);
		$lead = Lead::where('id', '=', $id)->first();

		$vista = "";
		if($this->agente->isMobile()){
			$vista = "mobile.";
		}

		$anotaciones = $lead->anotacion->sortByDesc('created_at');

		if ($lead->id > 0) {
			//$concesionarios = Concesionario::all();
			if(Auth::user()->rol == 2){
				$marca = Marca::where('nombre','=', Auth::user()->marca)->first();	
				$concesionarios = $marca->concesionarios;

				$hoy = new \DateTime(date('Y-m-d H:i:s'));
				$interval = date_diff($hoy, new \DateTime($lead->updated_at));
				$dias = $interval->format('%d');
				$horas = $interval->format('%h');

				if ($dias == 0 && $horas >= 12 && $horas < 24) {
					$lead->estado = "<span>12h</span> sin gestionar";
				}elseif($dias >= 1 && $dias < 2){
					$lead->estado = "<span>24h</span> sin gestionar";
				}elseif($horas >= 48){
					$lead->estado = "<span>48h</span> sin gestionar";
				}
				$concesionarios = $marca->concesionarios;
			}else{
				$conglo = Conglomerado::where('nombre', '=', Auth::user()->conglomerado)->first();
				$concesionarios = DB::select("SELECT c.* FROM concesionarios as c, concesionario_marca as cm WHERE c.id=cm.concesionario_id  and cm.conglomerado_id=".$conglo->id." ORDER BY created_at DESC");
			}
			
			$vista .= "dashboard.editar";

			return view($vista)
				->with('colores', $this->colores)
				->with('active', 'listar')
				->with(compact('anotaciones'))
				->with('concesionarios', $concesionarios)
				->with('lead', $lead);
		}else{
			echo "Error el lead no existe";
		}
    }


	public function gestionar($id, $source) {

		$vista = "";

		$security = new Security();

		//$id = $security->decrypt($id);

		$lead = Lead::find($id);

		
		$marca = Brand::where('id', '=', $lead->brand_id)->first();

		$modelos = Modelo::where('brand_id', '=', $marca->id)->select('id', 'name as nombre')->get();

		$agenda = Reminder::whereRaw('lead_id = ? and date >= CURDATE()', array($lead->id))->get();

		$anotaciones = $lead->anotacion->sortByDesc('created_at');		

		$concesionario = Distributor::where('id', '=', $lead->distributor_id)->first();

		if ($lead->id > 0) {	
			//20180408:ajuste nuevo leer concesionarios para enviar a la vista como objeto JSON
			//usa el modelo (clase realmente) declarado en la linea 7: App\Models\Distributor; 
			//para generar una arreglo sql convertido a JSON, usando el método DB:select("<mi sql>")
			//y lo vuelca sobre la variable $concesionarios
			$concesionarios = json_encode(DB::select("SELECT * FROM distributors order by name"));
			
			//echo "<pre>";
		        //print_r("Eber: " . $concesionarios);
		        //die();

			$lead->save();


			$vista .= 'mobile.test.gestion';

			// Lista de estados de la fase inicio
			$fase = self::getPhase_Status(1);
			
			// Lista de estados de la fase de prospectacion
			$prospectacion = self::getPhase_Status(2);

			// Lista de estados de la fase de agendados
			$agendados = self::getPhase_Status(3);

			// Lista de estados de la fase resultado visita
			$agenda_visita = self::getPhase_Status(3);

			// Lista de estados de la fase resultado proceso
			$resultado_proceso = self::getPhase_Status(4);

			// Lista de estados de la fase resultado visita
			$resultado_visita = self::getPhase_Status(6);

			$lead->estados = $lead->reason->status;

			$lead->modelo = $lead->model;

			$lead->concesionario = $lead->distributor;
			
			return view($vista)
				->with('concesionarios', $concesionarios)//20180508:ajuste envia modelo como objeto JSON a la vista
				->with('modelos', $modelos)			
				->with('fase', $fase)
				->with('estados_fase', $fase->status)
				->with('prospectacion', $prospectacion)
				->with('agendados', $agendados)
				->with('agenda_visita', $agenda_visita)
				->with('resultado_visita', $resultado_visita)				
				->with('resultado_proceso', $resultado_proceso)
				->with('motivo', $lead->motivo) // motivo actual del lead
				->with('persona', $lead->person) // motivo actual del lead
				->with('agenda', $agenda)
				->with('source',trim($source))
				->with('lead', $lead);

		}else{
			
			echo "Error el lead no existe";
		}

	}

	private function getPhase_Status($order) {

		$phase = Phase::where('sort', '=', $order)->first();

		foreach ($phase->status as $e) {

			$e->reasons = $e->reasons;
		}

		return $phase;
	}

	public function anotacion(Request $request){

		//ToDo: Verificar el rol solo pueden ingresar los concesionarios
		$input = $request->all();

		$anotacion = new Anotacion();
		$anotacion->lead_id = $input['id'];
		$anotacion->anotacion = $input['anotacion'];
		$anotacion->save();

		$anotaciones = Anotacion::where('lead_id','=', $input['id'])->orderby('id','desc')->get();
		
		return view('dashboard.anotaciones')
				->with(compact('anotaciones'));
				
	}

	public function actualizarLead(Request $request){
		date_default_timezone_set('America/Santiago');
		$input = $request->all();	
		$lead = Lead::find($input['lead']);
		if (isset($input['motivo'])) {
			//valida cuando el estado es pedido o entrega
			if ($input['motivo'] == 16) {
				$lead->purchase_date=date('Y-m-d H:i:s');
			}
			//-----
			if(is_null($lead->managed ) || $lead->managed  == ""){
				date_default_timezone_set('America/Santiago');
				$FechaManaged = date('Y-m-d H:i:s');
			    $lead->managed = $FechaManaged;
			    
			}
			
			$nuevo_motivo = Reason::find($input['motivo']);

			$estado_nuevo = Status::find($nuevo_motivo->status_id);

			$estado_anterior = $lead->reason->status;

			if ($lead->reason_id != $nuevo_motivo->id) {
				//verifica si hay un rate que agregar a los leads
				if (!empty($nuevo_motivo->rate)) {
					$lead->rate = $nuevo_motivo->rate;
				}
				if($nuevo_motivo->id != 103){
					$note =  "Cambio estado: " . $estado_anterior->name . " a " . $estado_nuevo->name;
				}else{
					$note = "Reagendado";
				}
				
				self::saveNote($lead->id, $note);

				// Valida si el nuevo estado consume webservices
				if ($nuevo_motivo->web_services == 1 && in_array($lead->distributor_id, [1, 2, 3, 4, 5, 6, 7, 8])) {
					if(isset($input['fecha']) && isset($input['hora'])){
						$reagendamiento = $input['fecha']." ".$input['hora'];
					}else{
						$reagendamiento = -1;
					}
					self::ws_obc_resultado_visita($nuevo_motivo->sw_option, $lead->id, $lead->commission, $reagendamiento);
				}

			}

			if ($lead->count_contact < $this->max_day_recontact) {

				if ($estado_nuevo->id == 2 || $nuevo_motivo->id == 35) { //Estado: Cerrado, Motivo: No atiende
						
					$lead->count_contact = $lead->count_contact + 1;

					//Se crea un recordatorio para el día siguiente
					$today = date('Y-m-j');

					$date_next = strtotime ( '+1 day' , strtotime ( $today ) ) ;

					$date_next = date ('Y-m-j' , $date_next);
					Reminder::where('lead_id', $lead->id )->delete();
					self::saveReminder($lead->id, $date_next, '08:00');
					
					$note = "Intento número ".$lead->count_contact." de contacto. Se contactará nuevamente el ".$date_next;

					if ($lead->count_contact == 3) {
						
						$note = "Intento número ".$lead->count_contact." de contacto. Lead cerrado definitivamente.";

						Reminder::where('lead_id', $lead->id )->delete();
					}
					
					self::saveNote($lead->id, $note);
				}
			}


			

			if (isset($input['medio'])) {

				$lead->medio_contacto = $input['medio'];	
			}

			$lead->reason_id = $nuevo_motivo->id;
		}

		if (isset($input['modelo'])) {

			$modelo = Modelo::find($input['modelo']);
			$lead->model_id = $modelo->id;
		}

		if(isset($input['fecha']) && isset($input['hora'])){			
			Reminder::where('lead_id', $lead->id )->delete();
			self::saveReminder($lead->id, $input['fecha'], $input['hora']);

			$note = "Nuevo recordatorio: Llamar el " . $input['fecha'] . " a las ".$input['hora'];

			self::saveNote($lead->id, $note);
		}

		if(isset($input['nota']) && $input['nota'] != ""){

			self::saveNote($lead->id, $input['nota']);
		}		
			
		if (isset($input['comision'])) {

			$lead->commission = $input['comision'];
		}
		
		// Estado de reagendamiento, necesario para calcular estadisticas
		if (isset($input['motivo']) && $nuevo_motivo->id == 103) {
			$lead->reason_id = 95;
		}
		//si trae vitrina o consecionario
		if (!empty($input['vitrina'])) {
			$lead->distributor_id = $input['vitrina'];
		}
                

                /*$distributor = $input['vitrina'];
                
		$assessor = User::Join('user_rol', function($join) use ($distributor){

                         $join->on('user_rol.user_id', '=', 'users.id');

                         $join->on('user_rol.distributor_id', '=', DB::raw($distributor));

                         $join->on('user_rol.rol_id', '=', DB::raw(5));

                     })->select('users.id as id')->get();

                $lead->assessor_id = $assessor;
		*/
		//valida si quotation se esta enviando
        if (isset($input['quotation'])) {
        	$lead->quotation = $input['quotation'];
        }
        //valida si purchase viene en el formulario
 		if (isset($input['purchase'])) {
        	$lead->purchase_time = $input['purchase'];
        }
        if (isset($input['test'])) {
        	$lead->test_drive = $input['test'];
        }
		$lead->save();
		echo 1;
	}

	private function saveNote($lead, $message) {

		$note = new Note();
		$note->lead_id = $lead;
		$note->note = $message;
		$note->save();
	}

	private function saveReminder($lead, $date, $time) {

		$reminder = new Reminder();			
		$reminder->lead_id = $lead;
		$reminder->date = $date;
		$reminder->time = $time;
		$reminder->save();
	}

	private function getData($type, $value){
		echo "Llega aqui";
		die();
		$result['leads'] = Lead::select('estado', \DB::raw("count(id) as num"))
						->where($type,'=', $value)
						->groupBy('estado')
						->get();

		$result['leads2'] = Lead::select('estado2', \DB::raw("count(id) as num"))
						->where($type,'=', $value)
						->groupBy('estado2')
						->get();

		$result['modelos'] = Lead::select('modelo', \DB::raw("count(id) as num"))
						->where($type,'=', $value)
						->groupBy('modelo')
						->orderby('num', 'desc')
						->get();
						
		$result['sinRevisar'] = Lead::select('revisado', \DB::raw("count(id) as num"))
						->where($type,'=', $value)
						->groupBy('revisado')
						->get();

		/* Info para graficas... */
		$result['aux'] = Lead::select('origen', \DB::raw("count(id) as num"))
						->where($type,'=', $value)
						->groupBy('origen')
						->get();

		return $result;
	}


	

	public function cron(){
	
		//Buscamos todos los leads con estado sin gestionar y estado revisar = 0
		$leads = Lead::where('estado', '=', 'Sin Gestionar')
						->where('revisado', '=', 0)
						->get();

		//miramos si la fecha de creacion es mayor a dos dias
		$hoy = new \DateTime(date('Y-m-d'));
		foreach ($leads as $lead) {
			$aux = explode(' ', $lead->created_at);
			$fecha_lead = new \DateTime($aux[0]);

			$interval = date_diff($fecha_lead, $hoy);
			
			
			if ($interval->format('%a') >= 2) {
				$lead->revisado = -1;
				$lead->save();
			}
		}
	}

	public function bdbackup(){
		//creamos los archivos por marca...
		$marcas = Marca::all();
		$columnas = ['id', 'estado', 'estado2', 'origen', 'modelo','concesionario','marca','nombres','apellidos','telefono','correo','acepto','revisado','created_at','updated_at'];
		foreach ($marcas as $marca) {
			$leads = Lead::where('marca', '=', $marca->nombre)
					->orderby('created_at', 'desc')
					->get();

			$postdata = "";
			foreach ($leads as $lead) {
				foreach ($columnas as $col) {
					$postdata .= $lead->$col.";";
				}
				$postdata .= "\n";
			}

			$filename = $marca->nombre.".csv";
			Storage::disk('local')->put($filename, $postdata);
		}
		
	}

	public function enviarcorreo(){
		
		//$lead = Lead::find(Input::get('idLead'));
		/*$lead = Lead::find(528);
		
		//Buscamos los correos que hay registrados en el concesionario puede ser mas de uno separado por coma
		$concesionario  = Distributor::where('id','=',$lead->distributor_id)->first();
		//Verificamos si el lead tiene un asesor asignado
		if(is_numeric($lead->user_id) && $lead->user_id > 0){
					
			$asesor = User::find($lead->user_id);
			$concesionario->correo = $asesor->email;

			//Si el concesionario no tiene asesores se lo enviamos al correo del concesionario
		}else{
			//Si no hay correos registrados en el concesionario enviamos el correo a...
			if( $concesionario->correo == ""){
				//Enviar siempre al root
				$concesionario->correo = 'saufigo@gmail.com';
			}
		}

		$data['asunto'] = "Nuevo contacto #".$lead->id." ( ".$lead->marca->name." )";
		$data['correos'] = explode(",",$concesionario->correo);
		$data['link'] = Config::get('app.domain')."gestionar/".trim( $this->security->encrypt($lead->id) );

		Mail::send('emails.newLead', ['data' =>  $data], function ($message) use ($data) {
			$message->from('noreply@leadman.co', 'Lead Manager');
			$message->subject($data['asunto']);
			$message->to($data['correos']);
		});

		return 1;*/
		
		$leadEntity = new LeadEntity();
		$leadEntity->sendContactUser(6);
		
		echo "todo buen correo enviado";
	}
	
	public  function emaileveryday(){

        date_default_timezone_set('America/Santiago');
        $fechaActual = date('Y-m-d');
        $fechaMedia  = date('Y-m'); 
        //managers -brands- distributors
        $managers = DB::table('managers')
                        ->join('brands',function($join){
                        $join->on('brands.id', '=', 'managers.brand_id');            
                        })
                        ->groupby('managers.brand_id')
                        ->select('managers.id AS idManagers', 'managers.brand_id')
                        ->get();

        $allBrand = Brand::all();
        $allDistributors = Distributor::all();
		
        foreach ($managers as $key ) {
        	//consulas para tasa gestion
        	//consulas para tasa gestion
	    	$total_leads = self::getLeadsEmail('reasons.id', 'total', $key->brand_id, $fechaMedia);
	    	$Lead_sin_gestionar = self::getLeadsEmail('reasons.id', '=1', $key->brand_id, $fechaMedia);
	    	$Lead_gestionados = self::getLeadsEmail('reasons.id', '>=5', $key->brand_id, $fechaMedia);
	    	$Lead_no_gestionados = self::getLeadsEmail('reasons.id', 'in(2,3,4)', $key->brand_id, $fechaMedia);

	    	//interesados/no interesados
	    	$interesado = self::getLeadsEmail('reasons.id', 'in(7,8,11,12,13,14,15,16)', $key->brand_id, $fechaMedia);
	    	$no_interesado = self::getLeadsEmail('reasons.id', 'in(9,10)', $key->brand_id, $fechaMedia);
	  
	    	//consulta resultado cita
	    	$venta_primer_cita = self::getLeadsEmailFlag('reasons.id', '=16', 'leads.flag_reagendamiento', '=0', $key->brand_id, $fechaMedia);
	    	$venta_cita = self::getLeadsEmailFlag('reasons.id', '=16', 'leads.flag_reagendamiento', '>=1', $key->brand_id, $fechaMedia);
	    	$sin_asistencia = self::getLeadsEmailFlag('reasons.id', '=13', '', '', $key->brand_id, $fechaMedia);
	    	$sin_venta = self::getLeadsEmailFlag('reasons.id', 'in(12,15)', '', '', $key->brand_id, $fechaMedia);

	    	//tiempos de gestion
	    	$todos_tiempos = self::getLeadsTiempoGestion(0, $key->brand_id, $fechaMedia);
	    	$menos_dos_horas = self::getLeadsTiempoGestion(1, $key->brand_id, $fechaMedia);
	    	$menos_un_dia = self::getLeadsTiempoGestion(2, $key->brand_id, $fechaMedia);
	    	$mas_un_dia = self::getLeadsTiempoGestion(3, $key->brand_id, $fechaMedia);

	    	//Cerrados por etapas
	    	$cerrados_en_gestion = self::getLeadsEmail('reasons.id', 'in(2,3,4)', $key->brand_id, $fechaMedia);
	    	$cerrados_en_cita = self::getLeadsEmail('reasons.id', 'in(9,10,12,13,15)', $key->brand_id, $fechaMedia);

	    	//funnel
	    	$leads_contactado = self::getLeadsEmailFunnel('reasons.funnel_id', 'in(2,3,4,5)', 'leads.created_at', $key->brand_id, $fechaMedia);
	    	$leads_cita = self::getLeadsEmailFunnel('reasons.funnel_id', 'in(3,4,5)', 'reminders.created_at', $key->brand_id, $fechaMedia);
	    	$leads_cita_efectiva = self::getLeadsEmailFunnel('reasons.funnel_id', 'in(4,5)', 'reminders.date', $key->brand_id, $fechaMedia);
	    	$leads_venta = self::getLeadsEmailFunnel('reasons.funnel_id', '=5', 'leads.purchase_date', $key->brand_id, $fechaMedia);

	    	if ($total_leads == 0) {$total_leads= 1;}
	        //porcentajes de tasa de gistionados 
	      	$datos['pLeadSinGestionar'] = number_format(($Lead_sin_gestionar *100)/$total_leads, 2, '.', '');
	      	$datos['pLeadGestionados']  = number_format(($Lead_gestionados *100)/$total_leads, 2, '.', '');
	      	$datos['pLeadNoGestionados']= number_format(($Lead_no_gestionados *100)/$total_leads, 2, '.', '');

	      	//datos normales de tasa de gestion
	      	$datos['totalLeads'] = $total_leads;
	      	$datos['LeadSinGestionar'] = $Lead_sin_gestionar;
	      	$datos['LeadGestionados'] = $Lead_gestionados;
	      	$datos['LeadNoGestionados'] = $Lead_no_gestionados;

	      	//interesados / no interesados
	      	$datos['interesado'] = $interesado;
	        $datos['noInteresado'] = $no_interesado;

	        //se suman sinAsistencia + sin venta para dar asistencia sin venta
	        $datos['ventaReagendad'] = $venta_cita;
	        $datos['ventaPriCita'] = $venta_primer_cita;
	        $datos['asistenciaSinVenta'] = $sin_venta;
	        $datos['agendadosSinAsistencia'] = $sin_asistencia;

	        //Tiempos gestion
	        $datos['horarioLaborable'] = $todos_tiempos;
	        $datos['menosDosH'] = $menos_dos_horas;
	        $datos['menosUnDia'] = $menos_un_dia;
	        $datos['masUnDia'] = $mas_un_dia;
	        //porcentajes
            if ($todos_tiempos == 0) {$todos_tiempos= 1;}
	        $datos['pMenosDosH']  = number_format(($menos_dos_horas * 100)/$todos_tiempos ,'2', '.', '');
	        $datos['pMenosUnDia'] = number_format(($menos_un_dia * 100)/$todos_tiempos ,'2', '.', '');
	        $datos['pMasUnDia'] = number_format(($mas_un_dia * 100)/$todos_tiempos ,'2', '.', '');

	        //cerrados en proceso
			$datos['cerradosEnGestion'] = $cerrados_en_gestion;
			$datos['cerradosdosEnCita'] = $cerrados_en_cita;

			 //funnel a vista
	        $datos['leadsContactado'] = $leads_contactado;
	        $datos['leadsCita'] = $leads_cita;
	        $datos['leadsCitaEfectiva'] = $leads_cita_efectiva;
	        $datos['leadsVenta'] = $leads_venta;

	        //funnel porcentajes
	      	$datos['pleadsContactado'] = number_format(($leads_contactado *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsCita'] = number_format(($leads_cita *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsCitaEfectiva']  = number_format(($leads_cita_efectiva *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsVenta']= number_format(($leads_venta *100)/$total_leads, 2, '.', '');
        	/*$totalLeads = Lead::where('brand_id', '=', $key->brand_id)
                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                ->count();

        	$LeadSinGestionar = Lead::join('reasons', function($join){
                                      $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                                    ->where('leads.brand_id', '=', $key->brand_id)
                                    ->where('reasons.id', '=', 1)
                                    ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                    ->select('leads.id')
                                    ->count();

            $LeadGestionados = Lead::join('reasons', function($join){
                                      $join->on('reasons.id', '=', 'leads.reason_id');
                                    })
                                    ->where('leads.brand_id', '=', $key->brand_id)
                                    ->where('reasons.id', '>=', 5 )
                                    ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                    ->select('leads.id')
                                    ->count();

            $LeadNoGestionados = Lead::join('reasons', function($join){
	                                  $join->on('reasons.id', '=', 'leads.reason_id');
	                                })
	                                ->where('leads.brand_id', '=', $key->brand_id)
	                                ->whereIn('reasons.id', array(2,3,4))
	                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                                ->select('leads.id')
	                                ->count();
	        if ($totalLeads == 0) {$totalLeads= 1;}
            //porcentajes de tasa de gistionados 
          	$datos['pLeadSinGestionar'] = number_format(($LeadSinGestionar *100)/$totalLeads, 2, '.', '');
          	$datos['pLeadGestionados']  = number_format(($LeadGestionados *100)/$totalLeads, 2, '.', '');
          	$datos['pLeadNoGestionados']= number_format(($LeadNoGestionados *100)/$totalLeads, 2, '.', '');

          	//datos normales de tasa de gestion
          	$datos['totalLeads'] = $totalLeads;
          	$datos['LeadSinGestionar'] = $LeadSinGestionar;
          	$datos['LeadGestionados'] = $LeadGestionados;
          	$datos['LeadNoGestionados'] = $LeadNoGestionados;

          	//consulta para interesado/no interesado
          	$interesado = Lead::join('reasons', function($join){
                                  $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where('leads.brand_id', '=', $key->brand_id)
                                ->whereIn('reasons.id', array(7,8,11,12,13,14,15,16))
                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                ->select('leads.id')
                                ->count();

            $noInteresado = Lead::join('reasons', function($join){
                                  $join->on('reasons.id', '=', 'leads.reason_id');
                                })
                                ->where('leads.brand_id', '=', $key->brand_id)
                                ->whereIn('reasons.id', array(9,10))
                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                ->select('leads.id')
                                ->count(); 
            //datos para el blade
            $datos['interesado'] = $interesado;
            $datos['noInteresado'] = $noInteresado;
            
            //consulta resultado cita
        	$ventaPriCita = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereIn('reasons.id', array(16))
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->where('leads.flag_reagendamiento', '=', '0')
                            ->select('leads.id')
                                ->count();

            $ventaCita = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereIn('reasons.id', array(16))
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->where('leads.flag_reagendamiento', '>=', '1')
                            ->select('leads.id')
                                ->count();

            $sinAsistencia = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->where('reasons.id', '=', 13 )
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            $sinVenta = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->wherein('reasons.id', array(12,15))
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            //se suman sinAsistencia + sin venta para dar asistencia sin venta
            $datos['ventaReagendad'] = $ventaCita;
            $datos['ventaPriCita'] = $ventaPriCita;
            $datos['asistenciaSinVenta'] = $sinVenta;
            $datos['agendadosSinAsistencia'] = $sinAsistencia;

            //tiempo de respuesta
            $todosTiempos = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereRaw('DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ')
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            $menosDosH = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereRaw('DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) <= "02:00:00"')
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            $menosUnDia = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereRaw('DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) > "02:00:00" AND 
                            			timediff(leads.managed, leads.created_at) <= "24:00:00"')
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            $masUnDia = Lead::join('reasons', function($join){
                              $join->on('reasons.id', '=', 'leads.reason_id');
                            })
                            ->where('leads.brand_id', '=', $key->brand_id)
                            ->whereRaw('DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) > "24:00:00"')
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                            ->select('leads.id')
                            ->count();

            //datos al blade                
            $datos['horarioLaborable'] = $todosTiempos;
            $datos['menosDosH'] = $menosDosH;
            $datos['menosUnDia'] = $menosUnDia;
            $datos['masUnDia'] = $masUnDia;
            //porcentajes
            if ($todosTiempos == 0) {$todosTiempos= 1;}
            $datos['pMenosDosH']  = number_format(($menosDosH * 100)/$todosTiempos ,'2', '.', '');
            $datos['pMenosUnDia'] = number_format(($menosUnDia * 100)/$todosTiempos ,'2', '.', '');
            $datos['pMasUnDia'] = number_format(($masUnDia * 100)/$todosTiempos ,'2', '.', '');

            //creados por etapas
		    $cerradosEnGestion = Lead::join('reasons', function($join){
			                      $join->on('reasons.id', '=', 'leads.reason_id');
			                    })
			                    ->where('leads.brand_id', '=', $key->brand_id)
			                    ->wherein('reasons.id', array(2,3,4))
			                    ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
			                    ->select('leads.id')
			                    ->count();

            $cerradosdosEnCita = Lead::join('reasons', function($join){
				                      $join->on('reasons.id', '=', 'leads.reason_id');
				                    })
				                    ->where('leads.brand_id', '=', $key->brand_id)
				                    ->wherein('reasons.id', array(9,10,12,13,15))
				                    ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
				                    ->select('leads.id')
				                    ->count();
            //funnel
            $leadsContactado = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(2,3,4,5))
                                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();
            $leadsCita = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->join('reminders', function($join){
                                            $join->on('reminders.lead_id', '=', 'leads.id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(3,4,5))
                                        ->where('reminders.created_at', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();
            $leadsCitaEfectiva = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->join('reminders', function($join){
                                            $join->on('reminders.lead_id', '=', 'leads.id');
                                        })
                                        ->where('reminders.date', 'like', '%'.$fechaMedia.'%')
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->whereIn('reasons.funnel_id', array(4,5))
                                        ->select('leads.id')
                                        ->count();
             $leadsVenta = Lead::join('reasons', function($join){
                                          $join->on('reasons.id', '=', 'leads.reason_id');
                                        })
                                        ->where('leads.brand_id', '=', $key->brand_id)
                                        ->where('reasons.funnel_id', '=', 5)
                                        ->where('leads.purchase_date', 'like', '%'.$fechaMedia.'%')
                                        ->select('leads.id')
                                        ->count();

            //funnel a vista
            $datos['leadsContactado'] = $leadsContactado;
            $datos['leadsCita'] = $leadsCita;
            $datos['leadsCitaEfectiva'] = $leadsCitaEfectiva;
            $datos['leadsVenta'] = $leadsVenta;
            //funnel porcentajes
          	$datos['pleadsContactado'] = number_format(($leadsContactado *100)/$totalLeads, 2, '.', '');
          	$datos['pleadsCita'] = number_format(($leadsCita *100)/$totalLeads, 2, '.', '');
          	$datos['pleadsCitaEfectiva']  = number_format(($leadsCitaEfectiva *100)/$totalLeads, 2, '.', '');
          	$datos['pleadsVenta']= number_format(($leadsVenta *100)/$totalLeads, 2, '.', '');

			//datos cerrados 
			$datos['cerradosEnGestion'] = $cerradosEnGestion;
			$datos['cerradosEnCita'] = $cerradosdosEnCita;
			*/
			//array de cotenido de tabla
			$tabla=[];
			foreach ($allDistributors as $distributor) {

				$totalLeadsAyer = Lead::where('leads.distributor_id', '=', $distributor->id)
										->where('leads.brand_id', '=', $key->brand_id)
			                            ->where('leads.created_at', '>=', DB::raw('DATE_SUB("'.$fechaActual.'", INTERVAL 1 DAY)'))
			                            ->where('leads.created_at', '<', $fechaActual)
			                            ->count(); 

				$leadsAyer = Lead::join('reasons', function($join){
				                      $join->on('reasons.id', '=', 'leads.reason_id');
				                    })
								->where('leads.distributor_id', '=', $distributor->id)
								->where('leads.brand_id', '=', $key->brand_id)
								->where('reasons.id', '<>', 1 )
	                            ->where('leads.created_at', '>=', DB::raw('DATE_SUB("'.$fechaActual.'", INTERVAL 1 DAY)'))
			                            ->where('leads.created_at', '<', $fechaActual)
	                            ->count();

               $cotizacionMes= Lead::join('reasons', function($join){
				                      $join->on('reasons.id', '=', 'leads.reason_id');
				                    })
								->where('leads.distributor_id', '=', $distributor->id)
								->where('leads.brand_id', '=', $key->brand_id)
								->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                            ->count();

	            $sinGestionMes = Lead::join('reasons', function($join){
					                      $join->on('reasons.id', '=', 'leads.reason_id');
					                    })
									->where('leads.distributor_id', '=', $distributor->id)
									->where('leads.brand_id', '=', $key->brand_id)
									->where('reasons.id', '<>', 1 )
		                   			->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
		                            ->count();

		        if ($totalLeadsAyer == 0 ) {
	            	$totalLeadsAyer2 = 1;
	            }else{
	            	$totalLeadsAyer2 = $totalLeadsAyer;
	            }

	            if ($cotizacionMes == 0 ) {
	            	$cotizacionMes2 = 1;
	            }else{
	            	$cotizacionMes2 = $cotizacionMes;
	            }

	            $regla=number_format(($leadsAyer * 100)/ $totalLeadsAyer2, '2', '.', '');
	            $regla2=number_format(($sinGestionMes * 100)/ $cotizacionMes2, '2', '.', '');

				$datostabla = array('nombre'.$distributor->key => $distributor->name, 
									'totaleadsAyer'.$distributor->key => $totalLeadsAyer, 
									'leadsAyer'.$distributor->key => $regla."%", 
									'cotizacionMes'.$distributor->key => $cotizacionMes,
									'sinGestionMes'.$distributor->key => $regla2."%");
				array_push($tabla, $datostabla);
			}
			
			$datos['tabla'] = $tabla;
            
            $datos['fecha'] = $fechaActual;

            //lista de correos a enviar
            $correosEnviar = DB::Table('managers')
                                    ->select('email')
                                    ->where('brand_id', '=', $key->brand_id)
                                    ->get();
            $data=[];
            $agregar = "";
            foreach ($correosEnviar as $corre) {
                $agregar.= "". str_replace(",", ",", $corre->email).",";
            }
        
            $data['correos']=trim($agregar, ',');
            $data['correo'] =explode(",", $data['correos']);
            $data["asunto"] = "Lead Manager Chile ".$fechaActual;

            Mail::send('emails.reportechile',['data' => $datos], function ($message) use ($data) {
                $message->from('noreply@cotizadoronline.com.co', 'LM Chile');
                $message->subject($data['asunto']);
                $message->to($data['correo']);
            }); 
        }
    }

    public static function getLeadsEmail($columna, $datoColumna, $brand_id, $fechaMedia){
		$total = 0;

		
		if ($datoColumna == 'total') {

			$total = Lead::where('brand_id', '=', $brand_id)
	                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                            ->count();
		}else{
		

			$total = Lead::join('reasons', function($join){
	                                  $join->on('reasons.id', '=', 'leads.reason_id');
	                                })
	                                ->where('leads.brand_id', '=', $brand_id)
	                                ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                                ->select('leads.id')
	                                ->count();
		}
		//var_dump($columna, $datoColumna, $otherColumna="", $otherDataColumna="", $brand_id, $fechaMedia);
		return $total;
	}	

	public static function getLeadsEmailFlag($columna, $datoColumna, $otherColumna, $otherDataColumna, $brand_id, $fechaMedia){
		$total = 0;
		
		if (!empty($otherColumna) && !empty($otherDataColumna)) {

			$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.brand_id', '=', $brand_id)
	                        ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                        ->whereRAW(' '.$otherColumna.' '. $otherDataColumna.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();

		}else{

			$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.brand_id', '=', $brand_id)
	                        ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();
		}
		return $total;
	}	

	public static function getLeadsTiempoGestion($query, $brand_id, $fechaMedia){

		$consulta = ['DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) <= "02:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) > "02:00:00" AND 
			timediff(leads.managed, leads.created_at) <= "24:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6)AND 
			TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND 
			timediff(leads.managed, leads.created_at) > "24:00:00" '];

		$in=$consulta[$query];

		$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.brand_id', '=', $brand_id)
	                        ->whereRaw(''.$in.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();
		
	return $total;
	}

	public static function getLeadsEmailFunnel($columna, $datoColumna, $otherColumna, $brand_id, $fechaMedia){


			
			$total = Lead::join('reasons', function($join){
                          $join->on('reasons.id', '=', 'leads.reason_id');
                        })
                        ->join('reminders', function($join){
                            $join->on('reminders.lead_id', '=', 'leads.id');
                        })
                        ->where('leads.brand_id', '=', $brand_id)
                        ->whereRAW(' '.$columna.' '.$datoColumna.' ')
                        ->whereRAW(' '.$otherColumna.' like "%'.$fechaMedia.'%" ')
                       // ->whereRAW('"'.$otherColumna.'" "%'.$fechaMedia.'%" ')
                        ->select('leads.id')
                        ->count();
		
		return $total;
	}

    #Envio de e-mail por marca --------------------------------------------------------------------

	public  function emailEverydayDistributor(){

	    date_default_timezone_set('America/Santiago');
	    $fechaActual = date('Y-m-d');
	    $fechaMedia  = date('Y-m'); 

	    //managers -brands- distributors
	    $managers = DB::table('managers')
	                    ->join('distributors',function($join){
	                    $join->on('distributors.id', '=', 'managers.distributor_id');            
	                    })
	                    ->groupby('managers.distributor_id')
	                    ->select('managers.id AS idManagers', 'managers.distributor_id')
	                    ->get();

	    $allBrand = Brand::all();
	    $allDistributors = Distributor::all();
		
	    foreach ($managers as $key ) {

	    	//consulas para tasa gestion
	    	$total_leads = self::getLeadsEmailB('reasons.id', 'total', $key->distributor_id, $fechaMedia);
	    	$Lead_sin_gestionar = self::getLeadsEmailB('reasons.id', '=1', $key->distributor_id, $fechaMedia);
	    	$Lead_gestionados = self::getLeadsEmailB('reasons.id', '>=5', $key->distributor_id, $fechaMedia);
	    	$Lead_no_gestionados = self::getLeadsEmailB('reasons.id', 'in(2,3,4)', $key->distributor_id, $fechaMedia);

	    	//interesados/no interesados
	    	$interesado = self::getLeadsEmailB('reasons.id', 'in(7,8,11,12,13,14,15,16)', $key->distributor_id, $fechaMedia);
	    	$no_interesado = self::getLeadsEmailB('reasons.id', 'in(9,10)', $key->distributor_id, $fechaMedia);
	  
	    	//consulta resultado cita
	    	$venta_primer_cita = self::getLeadsEmailFlagB('reasons.id', '=16', 'leads.flag_reagendamiento', '=0', $key->distributor_id, $fechaMedia);
	    	$venta_cita = self::getLeadsEmailFlagB('reasons.id', '=16', 'leads.flag_reagendamiento', '>=1', $key->distributor_id, $fechaMedia);
	    	$sin_asistencia = self::getLeadsEmailFlagB('reasons.id', '=13', '', '', $key->distributor_id, $fechaMedia);
	    	$sin_venta = self::getLeadsEmailFlagB('reasons.id', 'in(12,15)', '', '', $key->distributor_id, $fechaMedia);

	    	//tiempos de gestion
	    	$todos_tiempos = self::getLeadsTiempoGestionB(0, $key->distributor_id, $fechaMedia);
	    	$menos_dos_horas = self::getLeadsTiempoGestionB(1, $key->distributor_id, $fechaMedia);
	    	$menos_un_dia = self::getLeadsTiempoGestionB(2, $key->distributor_id, $fechaMedia);
	    	$mas_un_dia = self::getLeadsTiempoGestionB(3, $key->distributor_id, $fechaMedia);

	    	//Cerrados por etapas
	    	$cerrados_en_gestion = self::getLeadsEmailB('reasons.id', 'in(2,3,4)', $key->distributor_id, $fechaMedia);
	    	$cerrados_en_cita = self::getLeadsEmailB('reasons.id', 'in(9,10,12,13,15)', $key->distributor_id, $fechaMedia);

	    	//funnel
	    	$leads_contactado = self::getLeadsEmailFunnelB('reasons.funnel_id', 'in(2,3,4,5)', 'leads.created_at', $key->distributor_id, $fechaMedia);
	    	$leads_cita = self::getLeadsEmailFunnelB('reasons.funnel_id', 'in(3,4,5)', 'reminders.created_at', $key->distributor_id, $fechaMedia);
	    	$leads_cita_efectiva = self::getLeadsEmailFunnelB('reasons.funnel_id', 'in(4,5)', 'reminders.date', $key->distributor_id, $fechaMedia);
	    	$leads_venta = self::getLeadsEmailFunnelB('reasons.funnel_id', '=5', 'leads.purchase_date', $key->distributor_id, $fechaMedia);

	    	if ($total_leads == 0) {$total_leads= 1;}
	        //porcentajes de tasa de gistionados 
	      	$datos['pLeadSinGestionar'] = number_format(($Lead_sin_gestionar *100)/$total_leads, 2, '.', '');
	      	$datos['pLeadGestionados']  = number_format(($Lead_gestionados *100)/$total_leads, 2, '.', '');
	      	$datos['pLeadNoGestionados']= number_format(($Lead_no_gestionados *100)/$total_leads, 2, '.', '');

	      	//datos normales de tasa de gestion
	      	$datos['totalLeads'] = $total_leads;
	      	$datos['LeadSinGestionar'] = $Lead_sin_gestionar;
	      	$datos['LeadGestionados'] = $Lead_gestionados;
	      	$datos['LeadNoGestionados'] = $Lead_no_gestionados;

	      	//interesados / no interesados
	      	$datos['interesado'] = $interesado;
	        $datos['noInteresado'] = $no_interesado;

	        //se suman sinAsistencia + sin venta para dar asistencia sin venta
	        $datos['ventaReagendad'] = $venta_cita;
	        $datos['ventaPriCita'] = $venta_primer_cita;
	        $datos['asistenciaSinVenta'] = $sin_venta;
	        $datos['agendadosSinAsistencia'] = $sin_asistencia;

	        //Tiempos gestion
	        $datos['horarioLaborable'] = $todos_tiempos;
	        $datos['menosDosH'] = $menos_dos_horas;
	        $datos['menosUnDia'] = $menos_un_dia;
	        $datos['masUnDia'] = $mas_un_dia;
	        //porcentajes
            if ($todos_tiempos == 0) {$todos_tiempos= 1;}
	        $datos['pMenosDosH']  = number_format(($menos_dos_horas * 100)/$todos_tiempos ,'2', '.', '');
	        $datos['pMenosUnDia'] = number_format(($menos_un_dia * 100)/$todos_tiempos ,'2', '.', '');
	        $datos['pMasUnDia'] = number_format(($mas_un_dia * 100)/$todos_tiempos ,'2', '.', '');

	        //cerrados en proceso
			$datos['cerradosEnGestion'] = $cerrados_en_gestion;
			$datos['cerradosdosEnCita'] = $cerrados_en_cita;

			 //funnel a vista
	        $datos['leadsContactado'] = $leads_contactado;
	        $datos['leadsCita'] = $leads_cita;
	        $datos['leadsCitaEfectiva'] = $leads_cita_efectiva;
	        $datos['leadsVenta'] = $leads_venta;

	        //funnel porcentajes
	      	$datos['pleadsContactado'] = number_format(($leads_contactado *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsCita'] = number_format(($leads_cita *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsCitaEfectiva']  = number_format(($leads_cita_efectiva *100)/$total_leads, 2, '.', '');
	      	$datos['pleadsVenta']= number_format(($leads_venta *100)/$total_leads, 2, '.', '');
	        
	        $datos['fecha'] = $fechaActual;

	        //lista de correos a enviar
	        $correosEnviar = DB::Table('managers')
	                                ->select('email')
	                                ->where('distributor_id', '=', $key->distributor_id)
	                                ->get();
	        $data=[];
	        $agregar = "";
	        foreach ($correosEnviar as $corre) {
	            $agregar.= "". str_replace(",", ",", $corre->email).",";
	        }
	    	
	        $data['correos']=trim($agregar, ',');
	        $data['correo'] =explode(",", $data['correos']);
	        $data["asunto"] = "Lead Manager Chile Reporte Consecionario ".$fechaActual;
	        
	        Mail::send('emails.reporteChileDealer',['data' => $datos], function ($message) use ($data) {
	            $message->from('noreply@cotizadoronline.com.co', 'LM Chile');
	            $message->subject($data['asunto']);
	            $message->to($data['correo']);
	        }); 
	    }
	}
	
	public static function getLeadsEmailB($columna, $datoColumna, $distributor_id, $fechaMedia){
		$total = 0;

		
		if ($datoColumna == 'total') {

			$total = Lead::where('distributor_id', '=', $distributor_id)
	                            ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                            ->count();
		}else{
		

			$total = Lead::join('reasons', function($join){
	                                  $join->on('reasons.id', '=', 'leads.reason_id');
	                                })
	                                ->where('leads.distributor_id', '=', $distributor_id)
	                                ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                                ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                                ->select('leads.id')
	                                ->count();
		}
		//var_dump($columna, $datoColumna, $otherColumna="", $otherDataColumna="", $distributor_id, $fechaMedia);
		return $total;
	}	

	public static function getLeadsEmailFlagB($columna, $datoColumna, $otherColumna, $otherDataColumna, $distributor_id, $fechaMedia){
		$total = 0;
		
		if (!empty($otherColumna) && !empty($otherDataColumna)) {

			$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.distributor_id', '=', $distributor_id)
	                        ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                        ->whereRAW(' '.$otherColumna.' '. $otherDataColumna.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();

		}else{

			$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.distributor_id', '=', $distributor_id)
	                        ->whereRAW(' '.$columna.' '. $datoColumna.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();
		}
		return $total;
	}	

	public static function getLeadsTiempoGestionB($query, $distributor_id, $fechaMedia){

		$consulta = ['DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) <= "02:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6) AND TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND timediff(leads.managed, leads.created_at) > "02:00:00" AND 
			timediff(leads.managed, leads.created_at) <= "24:00:00" ', 'DAYOFWEEK(leads.created_at) in(2,3,4,5,6)AND 
			TIME(leads.created_at) >= "09:00:00" AND TIME(leads.created_at) <= "19:00:00" AND 
			timediff(leads.managed, leads.created_at) > "24:00:00" '];

		$in=$consulta[$query];

		$total = Lead::join('reasons', function($join){
	                          $join->on('reasons.id', '=', 'leads.reason_id');
	                        })
	                        ->where('leads.distributor_id', '=', $distributor_id)
	                        ->whereRaw(''.$in.' ')
	                        ->where('leads.created_at', 'like', '%'.$fechaMedia.'%')
	                        ->select('leads.id')
	                        ->count();
		
	return $total;
	}

	public static function getLeadsEmailFunnelB($columna, $datoColumna, $otherColumna, $distributor_id, $fechaMedia){


			
			$total = Lead::join('reasons', function($join){
                          $join->on('reasons.id', '=', 'leads.reason_id');
                        })
                        ->join('reminders', function($join){
                            $join->on('reminders.lead_id', '=', 'leads.id');
                        })
                        ->where('leads.distributor_id', '=', $distributor_id)
                        ->whereRAW(' '.$columna.' '.$datoColumna.' ')
                        ->whereRAW(' '.$otherColumna.' like "%'.$fechaMedia.'%" ')
                       // ->whereRAW('"'.$otherColumna.'" "%'.$fechaMedia.'%" ')
                        ->select('leads.id')
                        ->count();
		
		return $total;
	}
	
}