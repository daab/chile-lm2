<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Input;

use App\Models\Rol;
use App\Models\Module;
use App\Models\City;
use App\User;

class CityController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	public function index()	
	{
		$cities = City::all();

		return view('root.ciudad.default')
				->with(compact('cities'));
	}

	public function create($id = "") {

		$city = "";

		if (!empty($id)) {

			$city = City::find($id);
		}

		return view('root.ciudad.form')
				->with(compact('city'));
	}

	public function save(Request $requets) {

		$city = new City;

		$param = $requets->all();

		if (!empty($param['id'])) {

			$city = City::find($param['id']);			
		}

		$city->name = $param['name'];

		$city->save();

		return redirect('/root/ciudad');
	}

	public function delete($id = "") {

		if (!empty($id)) {
			
			City::destroy($id);
		}

		return redirect('/root/ciudad');
	}
}