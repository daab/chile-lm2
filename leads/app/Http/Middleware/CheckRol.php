<?php namespace App\Http\Middleware;

use Closure;

class CheckRol {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$user = $request->user();

        if ( $user->rol != 1) {
            return redirect('/');
        }


		return $next($request);
	}

}
