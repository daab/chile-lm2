<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Servicio que guarda los leads de los otros formularios
Route::post('/saveLead', 'LeadsController@saveLead');
//Pruebas unitarias
Route::get('/asesor/{concesionario}/{marca}', 'LeadsController@getAsesor');
//Prueba validacion 3 meses no repetir lead
Route::get('/validarlead/{correo}/{concesionario}', 'ServicesController@validarLead');


Route::get('/sendsms', 'LeadsController@sendSMS');


Route::get('/web', 'LeadsController@wbs');// Prueba webservices
Route::get('/12fkgjse/reportesmarca', 'LeadsController@emaileveryday');// para enviar correos
Route::get('/$dfxgvds/reportesConcesionario', 'LeadsController@emailEverydayDistributor');// para enviar correos
Route::get('/', 'HomeController@index');
Route::get('/cron', 'LeadsController@cron');
Route::get('/bdbackup', 'LeadsController@bdbackup');
Route::get('/reportes', 'GerenteController@generarreportes');
Route::get('/concesionariox/{id}', 'LeadsController@hackConcesionarios');

Route::get('/configuracion/', 'ConfiguracionController@index');
Route::get('/configuracion/edit/{type}/{id}', 'ConfiguracionController@edit');
Route::get('/configuracion/delete/{type}/{id}', 'ConfiguracionController@delete');
Route::get('/configuracion/form/{form}/{fase}', 'ConfiguracionController@form');
Route::post('/configuracion/save', 'ConfiguracionController@save');
Route::get('/configuracion/state/{id}', 'ConfiguracionController@toListState');
Route::get('/configuracion/reason/{id}', 'ConfiguracionController@toListReason');
Route::get('/configuracion/timezone', 'ConfiguracionController@timeZone');

Route::post('/concesionariofacebook', 'ConcesionarioController@obtenerConcesionarioFacebook');
Route::post('/modelofacebook', 'RootController@obtenerModeloFacebook');

// Route::get('usuarios', 'UserController@index'); 

Route::get('/guide', function()
{
    return view("mobile.test.guide");
});

Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::group(['middleware' => 'auth'], function () {

    // Route::get('/listarLeads/{tipo}/{estado}', 'HomeController@listarLeads');

    Route::get('/home/{ano?}/{mes?}', 'HomeController@index');
    Route::post('/home/filtro', 'HomeController@filtro');
    // Route::get('/gestionar/{id}', 'LeadsController@gestionar');
    Route::get('/gestionar/{id}/{source}', 'LeadsController@gestionar');
    Route::get('/editar/{id}', 'LeadsController@editar');
    Route::post('/anotacion', 'LeadsController@anotacion');
    Route::post('/actualizar', 'LeadsController@actualizarLead');
    Route::post('/cambioconcesionario', 'LeadsController@cambioconcesionario');
    
    Route::get('/estados/{type}/{concesionario?}', 'LeadsController@estados');
    Route::post('/estados', 'LeadsController@estadosFiltro');

    Route::get('/exportar/{type}', 'LeadsController@exportar');
    Route::post('/exportar', 'LeadsController@getCSVFile');

    Route::post('/verificar', 'HomeController@verificar');
    Route::post('/imagenperfil', 'UserController@cambiarimagen');

    Route::post('/fichatecnica', 'LeadsController@enviarcorreo');
    

    Route::get('/concesionario/usuarios', 'ConcesionarioController@usuarios');
    Route::get('/concesionario/nuevo/usuario/{id?}', 'ConcesionarioController@nuevousuario');
    Route::post('/concesionario/guardar/usuario', 'ConcesionarioController@guardarusuario');
    Route::get('/concesionario/eliminar/usuario/{id}', 'ConcesionarioController@eliminarusuario');

    route::get('concesionario/usuarios/','ConcesionarioController@getUsuarios');
    // route::get('concesionario/getUsuarios/', [
    // 'uses'  =>  'ConcesionarioController@getUsuarios',
    // 'as'    =>  'obtener-usuarios-concesionario'
    // ]);

    Route::get('concesionario/getconcesionarios', [
        'uses'  =>  'HomeController@getConcesionarios',
        'as'    =>  'obtener-concesionarios-home'
    ]);

    Route::get('/homemarca', 'HomeController@getmarca');

});



Route::group(['prefix' => 'root', 'middleware' => 'auth'], function () {
    Route::get('marcas', 'RootController@marcas');

    route::get('marcas/getmarcas/', [
        'uses'  =>  'RootController@getmarcas',
        'as'    =>  'obtener-marcas'
    ]);

    Route::get('marcas/nuevo/{obj}/{id?}', 'RootController@nuevo');
    Route::get('marcas/eliminar/{obj}/{id}', 'RootController@eliminar');
    Route::post('marcas/guardar/{obj}', 'RootController@guardar');

    Route::get('dominios', 'RootController@dominios');

    route::get('dominios/getdominios/', [
    'uses'  =>  'RootController@getdominios',
    'as'    =>  'obtener-dominios'
    ]);

    Route::get('dominios/nuevo/{obj}/{id?}', 'RootController@nuevo');
    Route::get('dominios/eliminar/{obj}/{id}', 'RootController@eliminar');
    Route::post('dominios/guardar/{obj}', 'RootController@guardar');

    Route::get('modelos', 'RootController@modelos');
    Route::get('modelo/nuevo/{id?}', 'RootController@nuevomodelo');
    Route::get('modelo/eliminar/{id}', 'RootController@borrarmodelo');
    Route::post('modelo/guardar', 'RootController@guardarmodelo');

    Route::get('concesionarios', 'ConcesionarioController@listar');

     route::get('concesionario/getconcesionarios/', [
    'uses'  =>  'ConcesionarioController@getconcesionarios',
    'as'    =>  'obtener-concesionarios'
    ]);

    Route::get('concesionario/nuevo/{id?}', 'ConcesionarioController@nuevo');
    Route::get('concesionario/eliminar/{id}', 'ConcesionarioController@eliminar');
    Route::post('concesionario/guardar', 'ConcesionarioController@guardar');

    Route::get('conglomerado', 'ConglomeradoController@listar');

    route::get('conglomerado/getconglomerado/', [
    'uses'  =>  'ConglomeradoController@getconglomerado',
    'as'    =>  'obtener-conglomerado'
    ]);

    Route::get('conglomerado/nuevo/{id?}', 'ConglomeradoController@nuevo');
    Route::get('conglomerado/eliminar/{id}', 'ConglomeradoController@eliminar');
    Route::post('conglomerado/guardar', 'ConglomeradoController@guardar');
    Route::post('conglomerado/update', 'ConglomeradoController@update');

    Route::get('usuarios', 'UserController@index'); 
    route::get('usuarios/getusuarios/', [
    'uses'  =>  'UserController@getusuarios',
    'as'    =>  'obtener-usuarios'
    ]);

    Route::get('usuario/nuevo/{id?}', 'UserController@nuevo');
    Route::get('usuario/eliminar/{id}', 'UserController@eliminar');
    Route::post('usuario/guardar', 'UserController@guardar');
    Route::post('usuario/concesionario/marca', 'UserController@toList_brand_distributor');    


    Route::get('gerente', 'GerenteController@index');
     route::get('gerente/getgerente/', [
    'uses'  =>  'GerenteController@getgerente',
    'as'    =>  'obtener-gerentes'
    ]);
    Route::get('nuevo/gerente/{id?}', 'GerenteController@nuevo');
    Route::get('eliminar/gerente/{id}', 'GerenteController@eliminar');
    Route::post('guardar/gerente', 'GerenteController@guardar');

    Route::post('/update', 'RootController@update');

    Route::get('/modulos', 'ModuleController@index');
    Route::get('modulo/eliminar/{id}', 'ModuleController@delete');
    Route::get('modulo/nuevo/{id?}', 'ModuleController@create');
    Route::post('modulo/guardar', 'ModuleController@save');

    Route::get('/roles', 'RolController@index');
    Route::get('roles/eliminar/{id}', 'RolController@delete');
    Route::get('roles/nuevo/{id?}', 'RolController@create');    
    Route::get('roles/permisos/{id}', 'RolController@allowIn');
    Route::post('roles/permisos/save', 'RolController@saveAllow');
    Route::post('roles/guardar', 'RolController@save');

    Route::get('/estados', 'StatusController@index');
    Route::post('/estados/eliminar', 'StatusController@delete');
    Route::get('/estados/nuevo/{id?}', 'StatusController@create');
    Route::post('estados/guardar', 'StatusController@save');

    Route::get('/ciudad', 'CityController@index');
    Route::get('ciudad/eliminar/{id}', 'CityController@delete');
    Route::get('ciudad/nuevo/{id?}', 'CityController@create');
    Route::post('ciudad/guardar', 'CityController@save');

    Route::get('concesionarios/asesores', 'ConcesionarioController@assessors');
    Route::get('concesionarios/asesores/nuevo/{id?}', 'ConcesionarioController@create_asessor');
    Route::get('concesionarios/asesores/eliminar/{id}', 'ConcesionarioController@delete_asessor');
    Route::post('concesionarios/asesores/guardar', 'ConcesionarioController@save_asessor');

    Route::get('concesionario/metas/{id}', 'MetasController@index');
    Route::post('concesionario/metas/guardar', 'MetasController@save');

    Route::get('/fases', 'PhaseController@index');
    Route::get('fases/eliminar/{id}', 'PhaseController@delete');
    Route::get('fases/nuevo/{id?}', 'PhaseController@create');
    Route::post('fases/guardar', 'PhaseController@save');
});

Route::get('/exportExcel/{type}', 'ReportsController@exportExcel');
 
Route::get('/statistics', 'ReportsController@statistics');
Route::post('/statistics/filters', 'ReportsController@filters');
Route::get('/exportLeads', 'ReportsController@export_all_leads');
