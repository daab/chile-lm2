<?php

namespace App;

Class helpers {


    public static function getMonth($key) {

	        switch ($key) {

	          case 'value_1':
	          case 'value_01':
	                return 'Enero';
	            break;
	          
	          case 'value_2':
	          case 'value_02':
	                return 'Febrero';
	            break;

	          case 'value_3':
	          case 'value_03':
	                return 'Marzo';
	            break;

	          case 'value_4':
	          case 'value_04':
	                return 'Abril';
	            break;

	          case 'value_5':
	          case 'value_05':
	                return 'Mayo';
	            break;

	          case 'value_6':
	          case 'value_06':
	                return 'Junio';
	            break;           

	          case 'value_7':
	          case 'value_07':
	                return 'Julio';
	            break;

	          case 'value_8':
	          case 'value_08':
	                return 'Agosto';
	            break;

	          case 'value_9':
	          case 'value_09':
	                return 'Septiembre';
	            break;              

	          case 'value_10':
	                return 'Octubre';
	            break;

	          case 'value_11':
	                return 'Noviembre';
	            break;

	          case 'value_12':
	                return 'Diciembre';
	            break;            
        }
    } 	
}