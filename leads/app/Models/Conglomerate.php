<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Models\Conglomerate;
use App\iLeads;


class Conglomerate extends Model implements iLeads {

	//
	protected $table = 'conglomerates';

    public function marca()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }


    public function concesionario()
    {
        return $this->belongsTo('App\Models\Distributor', 'distributor_id');
    }


    public function users()
    {
        return $this->belongsToMany('App\User');
    }     
    public function toListLeads($case='today', $params) {           

        $sql = $this->toQuery($case, $params);
        $result = DB::select($sql);
        return Lead::hydrate($result);
    }

    public function getConglomerates() {

        $sql = "SELECT d.id, d.name, br_d.brand_id FROM brand_distributor br_d 
                inner join distributors d on d.id = br_d.distributor_id
                and br_d.conglomerate_id = (select id from conglomerates where lower(name) = '".Auth::user()->conglomerate."')";

        $result = DB::select($sql);

        return Distributor::hydrate($result);
    }

    private function toQuery($case='today', $params) {

        switch ($case) {

            case 'today':
                return $this->sql_leads($params);
            break;

            case 'status':
                return $this->sql_leads($params, 'status');
            break;

            case 'all':
                return $this->sql_leads($params, 'all');
            break;

            case 'calendar':
                return $this->sql_leads($params, 'calendar');
            break;                      
        }
    }

    private function sql_leads($params, $case = 'today'){

        $sql = "";
        $conglomerate = Auth::user()->conglomerate()->first();
        $brand_id = $conglomerate->brand_id;
        switch ($case) {            

            case 'today':           
                $sql .= "
                        select * from (

                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id,
                    di.name as concesionario,
                    di.id as concesionario_id,
                    di.name as concesionario_name,
                    e.name as nombreEstado,
                    m.name as motivo,
                    u.name as asesor,
                    e.color as color,
                    r.date as fechaAgenda,
                    r.time as horaAgenda
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id and r.date >= CURDATE()
                    where
                    bd.conglomerate_id = '".$conglomerate->id."' and
                    month(l.created_at) = ".$params['month']." and
                    year(l.created_at) = ".$params['year']." and
                    day(l.created_at) = ".$params['day']."
                    ) a  order by created_at desc
                    ";    

            break;

            case 'status':
                
               if (strtolower(trim($params['value'])) == 'agendado') {
                    //echo "<pre>";print_r($params['value']);die();
                    
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    bd.conglomerate_id = '".$conglomerate->id."' and
                    r.date >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id IN (8,11,12,13,14,15)
                    ) a order by created_at desc;
                    ";


                } else if (strtolower(trim($params['value'])) == 'en proceso'){
                    //echo "<pre>";print_r($params['value']);die();
                    //$sql.= "CALL getLeadsStatus_Asesor('".strtolower(trim($params['value']))."', ".Auth::user()->id.")";
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    bd.conglomerate_id = '".$conglomerate->id."' and
                    m.id IN (11,15,16)
                    ) a order by created_at desc;
                    ";
                } else if (strtolower(trim($params['value'])) == 'sin gestionar'){
                    //$sql.= "CALL getLeadsStatus_Asesor('".strtolower(trim($params['value']))."', ".Auth::user()->id.")";
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    bd.conglomerate_id = '".$conglomerate->id."' and
                    m.id IN (1) and
                    l.created_at >= date_sub(CURDATE(), INTERVAL 80 DAY)
                    ) a order by created_at desc;
                    ";
                }


            break;

            case 'all':
                $sql .= "
                select * from (
                        select
                        l.id,
                        l.brand_id,
                        l.commission,
                        l.created_at,
                        l.origen,
                        l.field1,
                        l.field2,
                        l.source,
                        l.medio_contacto,
                        p.name as nombres,
                        p.last_name as apellidos,
                        mo.name as modelo_actual,
                        mo.id as modelo_id, 
                        di.name as concesionario,
                        di.name as nombre_concesionario,
                        di.id as concesionario_id,          
                        e.name as nombreEstado, 
                        e.id as estado_id, 
                        m.name as motivo,
                        m.id as motivo_id, 
                        u.name as asesor,
                        u.id as asesor_id, 
                        e.color as color,
                        r.date as fechaAgenda, 
                        r.time as horaAgenda,
                        year(l.created_at) as year,
                        month(l.created_at) as month,
                        day(l.created_at) as day                      
                        from leads as l 
                        inner join persons p on p.id = l.person_id
                        inner join models mo on mo.id = l.model_id
                        inner join reasons m on m.id = l.reason_id
                        inner join status e on e.id = m.status_id
                        inner join distributors di on di.id = l.distributor_id
                        inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                        left join users u on u.id = l.assessor_id
                        left join reminders r on r.lead_id = l.id                   
                        where 
                        bd.conglomerate_id = '".$conglomerate->id."' and
                        l.created_at >= '".$params['value']."'
                       ) a order by created_at desc;
                    ";
            break;

            case 'calendar':
                
                $sql .= "
                        select * from ( 
                        select
                        l.id,
                        l.brand_id,
                        l.commission,
                        l.created_at,
                        l.origen,
                        l.field1,
                        l.field2,
                        l.source,
                        l.medio_contacto,
                        p.name as nombres,
                        p.last_name as apellidos,
                        mo.name as modelo_actual,
                        mo.id as modelo_id, 
                        di.name as concesionario,
                        di.name as nombre_concesionario,
                        di.id as concesionario_id,          
                        e.name as nombreEstado, 
                        e.id as estado_id, 
                        m.name as motivo,
                        m.id as motivo_id, 
                        u.name as asesor,
                        u.id as asesor_id, 
                        e.color as color,
                        r.date as fechaAgenda, 
                        r.time as horaAgenda,
                        year(l.created_at) as year,
                        month(l.created_at) as month,
                        day(l.created_at) as day                      
                        from leads as l 
                        inner join persons p on p.id = l.person_id
                        inner join models mo on mo.id = l.model_id
                        inner join reasons m on m.id = l.reason_id
                        inner join status e on e.id = m.status_id
                        inner join distributors di on di.id = l.distributor_id
                        inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                        left join users u on u.id = l.assessor_id
                        left join reminders r on r.lead_id = l.id                  
                        where 
                        bd.conglomerate_id = '".$conglomerate->id."' and
                        month(l.created_at) = ".$params['month']." and 
                        year(l.created_at) = ".$params['year']."
                        ) a order by created_at desc;
                    ";
                    

            break;                      
            
        }
    
        // echo "<pre>";
        // echo $sql;
        // die();
        return $sql;
    }
}
