<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group_distributor extends Model {

	//
	protected $table = 'brand_distributor';

	public function concesionarios()
    {
        return $this->belongsToMany('App\Models\Distributor', 'brand_distributor', 'distributor_id');
    }

    public function marcas()
    {
        return $this->belongsToMany('App\Models\Brand', 'brand_distributor', 'brand_id');
    }

}