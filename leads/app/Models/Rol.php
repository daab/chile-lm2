<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Rol extends Model {

	protected $table = "rol";

    public function users() {

        return $this->belongsToMany('App\User','user_rol');
    }

	public function modules()
    {
        return $this->belongsToMany('App\Models\Module', 'rol_module');
    }
}
