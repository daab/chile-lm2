<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model {

	protected $table = 'listas';

	public function valores()
    {
        return $this->hasMany('App\Models\Lista');
    }

}