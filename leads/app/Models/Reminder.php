<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as Model;

class Reminder extends Model {

	protected $table = 'reminders';
	
	public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }
}