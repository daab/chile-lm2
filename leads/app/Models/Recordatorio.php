<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Recordatorio extends Model {

	//
	protected $table = 'recordatorios';

	public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }
    
}
