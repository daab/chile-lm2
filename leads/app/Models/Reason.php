<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reason extends Model {

	protected $table = 'reasons';

	public function status() {

		return $this->belongsTo('App\Models\Status');
	}

	public function funnel(){
		return $this->belongsTo('App\Models\Funnel', 'funnel_id');
	}
}