<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Anotacion extends Model {

	//
	protected $table = 'anotaciones';

	public function lead()
    {
        return $this->belongsTo('App\Models\Lead');
    }
}
