<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model {

	protected $table = 'status';

	protected $casts = [
        'motivos' => 'array',
    ];

	public function reasons()
    {
        return $this->hasMany('App\Models\Reason');
    }

	public function phase() {

		return $this->belongsTo('App\Models\Phase');
	}    

}