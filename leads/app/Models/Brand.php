<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\iLeads;

class Brand extends Model implements iLeads {

	protected $table = 'brands';

    protected $casts = [

        'concesionarios' => 'array',
    ];

	public function dominios()
    {
        return $this->hasMany('App\Models\Domain');
    }

    public function concesionarios()
    {
        return $this->belongsToMany('App\Models\Distributor');
    }

    public function modelos()
    {
        return $this->hasMany('App\Models\Modelo');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    } 

    public function toListLeads($case='today', $params) {           

        $sql = $this->toQuery($case, $params);
        $result = DB::select($sql);
        return Lead::hydrate($result);
    }

    private function toQuery($case='today', $params) {

        switch ($case) {

            case 'today':
                return $this->sql_leads($params);
            break;

            case 'status':
                return $this->sql_leads($params, 'status');
            break;

            case 'all':
                return $this->sql_leads($params, 'all');
            break;

            case 'calendar':
                return $this->sql_leads($params, 'calendar');
            break;                      
        }
    }

    private function sql_leads($params, $case = 'today'){

        $sql = "";

        // $distributor = Auth::user()->distributor()->first();
        $brand = Auth::user()->brand()->first();

        switch ($case) {            

            case 'today':
                $sql .= "
                        select * from (

                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id,
                    di.name as concesionario,
                    di.id as concesionario_id,
                    di.name as nombre_concesionario,
                    e.name as nombreEstado,
                    m.name as motivo,
                    u.name as asesor,
                    e.color as color,
                    r.date as fechaAgenda,
                    r.time as horaAgenda
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id and r.date >= CURDATE()
                    where
                    l.brand_id = '".$brand->id."' and
                    month(l.created_at) = ".$params['month']." and
                    year(l.created_at) = ".$params['year']." and
                    day(l.created_at) = ".$params['day']."
                    ) a  order by created_at desc
                    ";              
            break;

            case 'status':

                if (strtolower(trim($params['value'])) == 'agendado') {

                    //$sql.= "CALL getLeadsStatus_Agendado(".$brand->id.")";
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    l.brand_id = '".$brand->id."' and
                    r.date >= date_sub(CURDATE(), INTERVAL 30 DAY) and
                    m.id IN (8,11,12,13,14,15)
                    ) a order by created_at desc;
                    ";

                } else if (strtolower(trim($params['value'])) == 'en proceso'){
                   
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    l.brand_id = '".$brand->id."' and
                    r.date >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id IN (11,15,16)
                    ) a order by created_at desc;
                    ";
                    //$sql.= "CALL getLeadsStatus('".strtolower(trim($params['value']))."', ".$brand->id.")";
                } else if (strtolower(trim($params['value'])) == 'sin gestionar'){
                    $sql .= "
                    select * from (
                    select
                    l.id,
                    l.brand_id,
                    l.commission,
                    l.created_at,
                    l.origen,
                    l.field1,
                    l.field2,
                    l.source,
                    l.medio_contacto,
                    p.name as nombres,
                    p.last_name as apellidos,
                    mo.name as modelo_actual,
                    mo.id as modelo_id, 
                    di.name as concesionario,
                    di.name as nombre_concesionario,
                    di.id as concesionario_id,          
                    e.name as nombreEstado, 
                    e.id as estado_id, 
                    m.name as motivo,
                    m.id as motivo_id, 
                    u.name as asesor,
                    u.id as asesor_id, 
                    e.color as color,
                    r.date as fechaAgenda, 
                    r.time as horaAgenda,
                    year(l.created_at) as year,
                    month(l.created_at) as month,
                    day(l.created_at) as day 
                    from leads as l
                    inner join persons p on p.id = l.person_id
                    inner join models mo on mo.id = l.model_id
                    inner join reasons m on m.id = l.reason_id
                    inner join status e on e.id = m.status_id
                    inner join distributors di on di.id = l.distributor_id
                    inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                    left join users u on u.id = l.assessor_id
                    left join reminders r on r.lead_id = l.id
                    where
                    l.brand_id = '".$brand->id."' and
                    l.created_at >= date_sub(CURDATE(), INTERVAL 60 DAY) and
                    m.id = 1
                    ) a order by created_at desc;
                    ";
                }else{
                    echo "<pre>";print_r($params['value']);die();
                }
     
            break;

            case 'all':

                     $sql .= "
                select * from (
                        select
                        l.id,
                        l.brand_id,
                        l.commission,
                        l.created_at,
                        l.origen,
                        l.field1,
                        l.field2,
                        l.source,
                        l.medio_contacto,
                        p.name as nombres,
                        p.last_name as apellidos,
                        mo.name as modelo_actual,
                        mo.id as modelo_id, 
                        di.name as concesionario,
                        di.name as nombre_concesionario,
                        di.id as concesionario_id,          
                        e.name as nombreEstado, 
                        e.id as estado_id, 
                        m.name as motivo,
                        m.id as motivo_id, 
                        u.name as asesor,
                        u.id as asesor_id, 
                        e.color as color,
                        r.date as fechaAgenda, 
                        r.time as horaAgenda,
                        year(l.created_at) as year,
                        month(l.created_at) as month,
                        day(l.created_at) as day                      
                        from leads as l 
                        inner join persons p on p.id = l.person_id
                        inner join models mo on mo.id = l.model_id
                        inner join reasons m on m.id = l.reason_id
                        inner join status e on e.id = m.status_id
                        inner join distributors di on di.id = l.distributor_id
                        inner join brand_distributor bd on bd.distributor_id = l.distributor_id
                        left join users u on u.id = l.assessor_id
                        left join reminders r on r.lead_id = l.id                   
                        where 
                        l.brand_id = '".$brand->id."' and 
                        l.created_at >= '".$params['value']."'
                       ) a order by created_at desc;
                    ";
            break;

            case 'calendar':

                $sql .= "
                        select 
                        l.id,
		        l.brand_id,
		        l.commission,
		        l.created_at,
	                l.origen,
	                l.field1,
		        l.field2,
                        l.source,
	                l.medio_contacto,
                        p.name as nombres,
                        p.last_name as apellidos,
                        mo.name as modelo_actual,
                        mo.id as modelo_id, 
                        di.name as concesionario,
                        di.name as nombre_concesionario,
                        di.id as concesionario_id,                                           
                        e.name as nombreEstado, 
                        m.name as motivo, 
                        u.name as asesor, 
                        e.color as color,
                        r.date as fechaAgenda, 
                        r.time as horaAgenda                     
                        from leads as l 
                        inner join persons p on p.id = l.person_id
                        inner join models mo on mo.id = l.model_id
                        inner join reasons m on m.id = l.reason_id
                        inner join status e on e.id = m.status_id
                        inner join distributors di on di.id = l.distributor_id                      
                        left join users u on u.id = l.assessor_id
                        left join reminders r on r.lead_id = l.id and r.date >= CURDATE()                   
                        where 
                        l.brand_id = '".$brand->id."' and 
			month(l.created_at) = ".$params['month']." and 
			year(l.created_at) = ".$params['year']."
                    ";
                    

            break;                      
            
        }
	
	//echo $sql;die("pruebas");
        return $sql;
    }   
}