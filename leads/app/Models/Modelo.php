<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modelo extends Model {

	//
	protected $table = 'models';

	public function marcas()
    {
        return $this->belongsTo('App\Models\Brand' ,'brand_id');
    }
}