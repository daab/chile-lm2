<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\helpers;
Use App\Models\Lead;

class Goal extends Model {

	protected $table = "goal";


	public function distributor() {

		return $this->belongsTo('App\Models\Distributor');
	}

	public static function get_goals_objective($filter = []) {

		$sql = "";
		$ids = "";

		$role = Auth::user()->roles()->first();

		if ($role->name == 'Marca') {

			$brand = Auth::user()->brand()->first();

			foreach ($brand->concesionarios as $d) {
				
				$ids.= $d->id . ", ";
			}

			$ids = substr($ids, 0, -2);

				if (isset($filter['Vitrina']) && $filter['Vitrina'] != "") {

					$ids = $filter['Vitrina'];
				} 
			
			$sql = self::get_sql_calculate_goal_distributors($ids, $filter);

		} else {			

			if ($role->name == 'Conglomerado') {

				$conglomerate = Auth::user()->conglomerate()->first();

				if (count($conglomerate->concesionario()->first())>0) {

					$ids = $conglomerate->concesionario()->first()->id;
				}

			} else {

				$ids = Auth::user()->distributor()->first()->id;
			}

			$sql = self::get_sql_calculate_goal_general($ids, $filter);
		}

		$result = DB::select($sql);

		$result = Goal::hydrate($result);

		return $result;
	}

	public static function get_sql_calculate_goal_general($ids, $filter=[]) {

		$month = helpers::getMonth("value_".date('m'));

			$sql = "
				select 
					CONVERT(goal.value,UNSIGNED INTEGER)  as BN, 
				    CONVERT((goal.value * 0.09),UNSIGNED INTEGER) as ventas_objetivo,
				    CONVERT(round(((goal.value * 0.09)/0.6),2),UNSIGNED INTEGER) as agendados_efectivos_objetivo,
				    CONVERT(round((((goal.value * 0.09)/0.6)/0.1),2),UNSIGNED INTEGER) as leads_contactados_objetivo,
				    CONVERT(round(((((goal.value * 0.09)/0.6)/0.1)/0.7),2),UNSIGNED INTEGER) as leads_logrados_objetivo    
				from goal 
				inner join distributors on 
					  distributors.id = goal.distributor_id 

					  and 2 > 1 
				";

				if (count($filter) > 0) {

					if (isset($filter['fecha_inicial']) && $filter['fecha_inicial'] != "" &&
						isset($filter['fecha_final']) && $filter['fecha_final'] != "") {

						$initial = explode('-', $filter['fecha_inicial']);
						$final = explode('-', $filter['fecha_final']);

						$month_initial = $initial[1];
						$year_initial = $initial[0];

						$month_final = $final[1];
						$year_final = $final[0];

						for($i = $month_initial; $i <= $month_final ; $i++){

							$month = helpers::getMonth("value_".$i);

							if ($i == $month_initial) {

								$sql.= " and ( ";
							}

							$sql.=" goal.month = '".$month."' ";

							if ($i == $month_final) {

								$sql.= ") ";

							} else {

								$sql.= " or ";
							}
						}

						$sql.= "and year = '".$year_final."'";


					} else {

						// En caso que no halla datos de fechas se prioriza la informaci??n del mes vigente
						$sql.="	  
							  and goal.month = '".$month."' and 
							  goal.year = YEAR(CURRENT_DATE())
						";
					}

				} else {

					$sql.="	  
						  and goal.month = '".$month."' and 
						  goal.year = YEAR(CURRENT_DATE())
					";
				}	

				$sql.="	  
					where 
					distributors.id = ".$ids."
				";		

		return $sql;
	}

	public static function get_sql_calculate_goal_distributors($ids, $filter=[]) {

		$month = helpers::getMonth("value_".date('m'));

		$sql = "
				select 
				       CONVERT(sum(t.BN),UNSIGNED INTEGER) as BN,
				       CONVERT(sum(t.ventas_objetivo),UNSIGNED INTEGER) as ventas_objetivo,
				       CONVERT(sum(t.agendados_efectivos_objetivo),UNSIGNED INTEGER) as agendados_efectivos_objetivo,
				       CONVERT(sum(t.leads_contactados_objetivo),UNSIGNED INTEGER) as leads_contactados_objetivo,
				       CONVERT(sum(t.leads_logrados_objetivo),UNSIGNED INTEGER) as leads_logrados_objetivo
				from (
				select 
				    goal.value as BN, 
				    (goal.value * 0.09) as ventas_objetivo,
				    round(((goal.value * 0.09)/0.6),2) as agendados_efectivos_objetivo,
				    round((((goal.value * 0.09)/0.6)/0.1),2) as leads_contactados_objetivo,
				    round(((((goal.value * 0.09)/0.6)/0.1)/0.7),0) as leads_logrados_objetivo    
				from goal 
				inner join distributors on 
					  distributors.id = goal.distributor_id and 2 > 1
				";

				if (count($filter) > 0) {

					if (isset($filter['fecha_inicial']) && $filter['fecha_inicial'] != "" &&
						isset($filter['fecha_final']) && $filter['fecha_final'] != "") {

						$initial = explode('-', $filter['fecha_inicial']);
						$final = explode('-', $filter['fecha_final']);

						$month_initial = $initial[1];
						$year_initial = $initial[0];

						$month_final = $final[1];
						$year_final = $final[0];

						for($i = $month_initial; $i <= $month_final ; $i++){

							$month = helpers::getMonth("value_".$i);

							if ($i == $month_initial) {

								$sql.= " and ( ";
							}

							$sql.=" goal.month = '".$month."' ";

							if ($i == $month_final) {

								$sql.= ") ";

							} else {

								$sql.= " or ";
							}
						}

						$sql.= "and year = '".$year_final."'";


					} else {

						// En caso que no halla datos de fechas se prioriza la informaci??n del mes vigente
						$sql.="	  
							 and goal.month = '".$month."' and 
							  goal.year = YEAR(CURRENT_DATE())
						";
					}

				} else {

					$sql.="	  
						 and  goal.month = '".$month."' and 
						  goal.year = YEAR(CURRENT_DATE())
					";
				}	

				$sql.="	  
				where 
						distributors.id in(".$ids.")
					) as t		
				";	

		return $sql;	
	}

	public static function get_total_leads($filter = []) {

		$flag_date = 0;

		$sql = " MONTH(created_at) = MONTH(CURRENT_DATE()) and YEAR(created_at) = YEAR(CURRENT_DATE()) and ";

		if (count($filter) > 0) {

			if (isset($filter['fecha_inicial']) && $filter['fecha_inicial'] != "" && 
				isset($filter['fecha_final']) && $filter['fecha_final'] != "" && $flag_date == 0) {

				if ($filter['fecha_inicial'] == $filter['fecha_final']) {

					$sql = " created_at >= '".$filter['fecha_inicial']."' and ";	

				} else {

					$sql = " created_at >= '".$filter['fecha_inicial']."' and created_at <= '".$filter['fecha_final']."' and ";	
				}

				$flag_date = 1;
			}			

			foreach ($filter as $key => $value) {

				if (strtolower($key) == 'modelo') {

					$sql.= "leads.model_id =  ".trim(strtolower($value))." and ";
				}

	            if (strtolower($key) == 'vitrina') {

	                $sql.= "leads.distributor_id = ".trim(strtolower($value))." and ";
	            }								
			}
		}
		

		$sql.= self::get_filter_by_role();

		// echo "<pre>";
		// echo $sql;
		// die();

        $total = Lead::WhereRaw($sql)->count();

        return $total;
	}

	public static function get_total_leads_contacted() {

		$sql = " MONTH(created_at) >= MONTH(CURRENT_DATE()) and 
				 YEAR(created_at) = YEAR(CURRENT_DATE()) and 
				 medio_contacto <> ''";

        $total = Lead::WhereRaw($sql)->count();

        return $total;
	}	

	public static function get_filter_by_role() {

		$role = Auth::user()->roles()->first();

		switch ($role->name) {

			case 'Concesionario': 

					return "leads.distributor_id = " . Auth::user()->distributor()->first()->id; 
				break;

			case 'Conglomerado': 

					$conglomerate = Auth::user()->conglomerate()->first();					
					return "leads.brand_id = " . $conglomerate->brand_id;
				break;

			case 'Asesor':

					return "leads.assessor_id = " . Auth::user()->id; 
				break;

			case 'Marca':

					return "leads.brand_id = " . Auth::user()->brand()->first()->id;
				break;
		}

	}

	public static function get_filter_by_role_array() {

        $filter = explode('=', self::get_filter_by_role());        
        $filter['field'] = trim($filter[0]);
        $filter['value'] = trim($filter[1]);
        return $filter;
	}

	public static function get_top_source_media($filter = []) {

		$sql = "select t.title, t.val from (
					select source as title, count(id) as val from leads 
					where 
				";

		if (isset($filter['fecha_inicial']) && $filter['fecha_inicial'] != "" &&
			isset($filter['fecha_final']) && $filter['fecha_final'] != "") {

			// $sql.= "
			// 		created_at >= '".$filter['fecha_inicial']."' and  created_at <= '".$filter['fecha_final']."' 
			// ";
                if ($filter['fecha_inicial'] == $filter['fecha_final']) {

                    $sql.= " leads.created_at >= '".$filter['fecha_inicial']." 00:00:00' and 
                             leads.created_at <= '".$filter['fecha_final']." 23:59:59'  ";
                } else {

                    $sql.= " leads.created_at >= '".$filter['fecha_inicial']."' and 
                             leads.created_at <= '".$filter['fecha_final']."'  ";
                }			

		} else {

			$sql.= "
					MONTH(created_at) = MONTH(CURRENT_DATE()) and 
				 	YEAR(created_at) = YEAR(CURRENT_DATE())				
			";
		}

		if (isset($filter['Vitrina']) && $filter['Vitrina'] != "") {

			$sql.= " and leads.distributor_id = ".$filter['Vitrina'];
		}

		if (isset($filter['Modelo']) && $filter['Modelo'] != "") {

			$sql.= " and leads.model_id =  ".trim(strtolower($filter['Modelo']));
		}


		$sql.= " group by source
			) as t
			order by t.val desc
			limit 4
		";

		$result = DB::select($sql);
		return Goal::hydrate($result)->toArray();
	}
}