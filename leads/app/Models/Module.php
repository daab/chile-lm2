<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model {

	protected $table = 'modules';

	public function roles()
    {
        return $this->belongsToMany('App\Models\Roles', 'rol_module');
    }

    public function submodules() {

    	return $this->hasMany('App\Module', 'parent_id');
    }    
}