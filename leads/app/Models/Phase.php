<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model {

	protected $table = 'phase';

	public function status()
    {
        return $this->hasMany('App\Models\Status');
    }

    

}