<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Funnel extends Model {

	protected $table = 'funnel';

	public function reasons() {
		return $this->hasMany('App\Models\Reasons', 'id');
	}
}
