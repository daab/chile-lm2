<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lead extends Model {

    protected $casts = [
        'estados' => 'array',
        'modelo' => 'array',
        'concesionario' => 'array',
        'nombre_concesionario' => ''
    ];

	//
	public function anotacion()
    {
        return $this->hasMany('App\Models\Note');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function marca()
    {
        return $this->belongsTo('App\Models\Brand', 'brand_id');
    }

    // public function fase()
    // {
    //     return $this->belongsTo('App\Models\Fase');
    // }

    public function estados()
    {
        return $this->belongsTo('App\Models\Status', 'estado_id', 'id');
    }

    public function motivo()
    {
        return $this->belongsTo('App\Models\Reason', 'reason_id', 'id');        
    }

    public function reason() {

        return $this->belongsTo('App\Models\Reason');
    }

    public function person() {

        return $this->belongsTo('App\Models\Person');
    }

    public function model() {

        return $this->belongsTo('App\Models\Modelo');
    }

    public function distributor() {

        return $this->belongsTo('App\Models\Distributor');
    }            
}
