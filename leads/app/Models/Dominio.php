<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dominio extends Model {

	//
	protected $table = 'dominios';


	public function marcas()
    {
        return $this->belongsTo('App\Models\Marca');
    }

}
