var json_estadistica = {
	"leads_objetivo": 15, // meta total por marca o por concesionario segun el roll, anual o mensual /  anula sumando todas los leads objetivo de año.
	"leads_total": 20, //total de leads
	"total_ventas": 2, // total de ventas (pedidos + entregados)
	"total_efectivas": 10, // total asistencias, que son los leads que quedaron en proceso + los no compro + sin venta + ventas
	"total_contactados": 9, // total lead que hayan sido contactados (se sabe porque se guardo el medio)
	"total_enproceso": 1, // total lead queo sea sin gestiona o cerrados y que no sean CAll center //editado/nuevo
	"total_enproceso_callcenter": 1, // total lead con estado Callcenter //editado/nuevo
	"total_cerrados": 1, // total lead con estado de cerrado


	"source_media":[ // en la base de dayos hay una columa llamada "source" que documenta el medio de la campaña, seria hacer un top 4 con la cantidades sumadas, funiona como un array
		{"title":"GNP", "color": "#47D24E", "val": 1000},
		{"title":"facebook", "color": "#1F7A1F", "val": 200},
		{"title":"eltiempo", "color": "#850101", "val": 100},
		{"title":"otro", "color": "#DD0101", "val": 100}
	]
}

var json_modelos = [
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 34,
	"marca_id": 1,
	"nombre": "beetle1"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 33,
	"marca_id": 1,
	"nombre": "beetle2"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 32,
	"marca_id": 1,
	"nombre": "a3"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 31,
	"marca_id": 1,
	"nombre": "beetle4"
	}
]

var json_ciudades = [
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 34,
	"marca_id": 1,
	"nombre": "Bogota"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 33,
	"marca_id": 1,
	"nombre": "Valledupar"
	}
]

var json_concesionarios = [
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 1,
	"marca_id": 1,
	"nombre": "Casa toro"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 2,
	"marca_id": 1,
	"nombre": "Autoblitz"
	}
]

var json_vitrinas = [
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 34,
	"marca_id": 1,
	"nombre": "Casa toro 170"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 33,
	"marca_id": 1,
	"nombre": "Autoblitz 109"
	}
]

var json_asesores = [
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 34,
	"marca_id": 1,
	"nombre": "David Bautista"
	},
	{"created_at": "-0001-11-30 00:00:00",
	"doc": null,
	"id": 33,
	"marca_id": 1,
	"nombre": "Saul Figueroa"
	}
]
