var json_set = {
  "animacion": [{
      "imagenes": [
        "images/guia/lm_1.jpg",
        "images/guia/login01.png"
      ],
      "images": [
        "login",
        "login1"
      ],
      "tempo": "400",
      "typo": "bounce",
      "titulo": "Ingreso",
      "texto": "1. Ingrese usuario y contraseña. <br><br>2. Dar clic en el botón “inicio de sesión”"
    },
    {
      "imagenes": [
        "images/guia/lm_2_1.jpg",
        "images/guia/lm_2_2.jpg"
      ],
      "images": [
        "lm_2_1",
        "lm_2_2"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "Calendario",
      "texto": "2. Visualizará en la pestaña de “agendados” un calendario en donde se resaltaran en amarillo los días en que se ha agendado un lead a la vitrina. <br><br>Para revisar el detalle de las citas agendadas y ver un breve resumen de las necesidades del “lead”, deberá dar clic en uno de los días."
    },
    {
      "imagenes": [
        "images/guia/lm_3_1.jpg",
        "images/guia/lm_3_2.jpg"
      ],
      "images": [
        "lm_3_1",
        "lm_3_2"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "Leads agendados",
      "texto": "3. En esta pantalla podrá visualizar todas las citas agendadas pendientes."
    },
    {
      "imagenes": [
        "images/guia/lm_4_1.jpg"
      ],
      "images": [
        "lm_4_1"
      ],
      "typo": "gif",
      "titulo": "Filtros",
      "texto": "4. En la parte izquierda de la pantalla encontrará los filtros principales en donde podrá revisar todos los leads activos y sus estadísticas."
    },
    {
      "imagenes": [
        "images/guia/lm_5_1.jpg"
      ],
      "images": [
        "lm_5_1"
      ],
      "typo": "gif",
      "titulo": "Gestión",
      "texto": "5. Para hacer seguimiento del cliente agendado, deberá dar clic en el botón “Gestión”."
    },
    {
      "imagenes": [
        "images/guia/lm_6_1.jpg",
        "images/guia/lm_6_2.jpg"
      ],
      "images": [
        "lm_6_1",
        "lm_6_2"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "Detalles del Lead",
      "texto": "6. En esta pantalla encontrará la información suministrada por el cliente en su registro, incluyendo el modelo del carro de su interés y sus datos de contacto. <br><br>Al dar clic en la pestaña “Agenda de la visita”, podrás escribir comentarios sobre el resultado de la visita del cliente."
    },
    {
      "imagenes": [
        "images/guia/lm_7_1.jpg",
        "images/guia/lm_7_2.jpg"
      ],
      "images": [
        "lm_7_1",
        "lm_7_2"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "¿No asistió?",
      "texto": "7. Si el prospecto NO asistió a la vitrina en la fecha y hora acordada o es un Lead duplicado, da clic en “no asistió” o “Lead duplicado” según sea el caso.<br><br> Se visualizará nuevamente la información completa del cliente. <br><br>Para guardar, es necesario dar clic en “Guardar cambios”."
    },
    {
      "imagenes": [
        "images/guia/lm_8_1.jpg"
      ],
      "images": [
        "lm_8_1"
      ],
      "typo": "gif",
      "titulo": "¡Si asistió!",
      "texto": "8. Si el cliente cumplio con su cita y visito la vitrina, selecciona entre las opciones “Visitó” o “Visitó e hizo test drive” según sea el caso."
    },
    {
      "imagenes": [
        "images/guia/lm_9_1.jpg",
        "images/guia/lm_9_2.jpg"
      ],
      "images": [
        "lm_9_1",
        "lm_9_2"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "No compro",
      "texto": "9. Si el cliente NO compro, se visualizará nuevamente la información completa del mismo. <br><br>Para guardar, es necesario dar clic en “Guardar cambios”"
    },
    {
      "imagenes": [
        "images/guia/lm_10_1.jpg",
        "images/guia/lm_10_2.jpg",
        "images/guia/lm_10_3.jpg"
      ],
      "images": [
        "lm_10_1",
        "lm_10_2",
        "lm_10_3"
      ],
      "tempo": "2000",
      "typo": "gif",
      "titulo": "Proceso de venta",
      "texto": "10. Si el cliente “Separo” o está en “Proceso de crédito”, deberás ingresar el “# de comisión” del vehículo. Al ingresar el “# de comisión”, se visualizará nuevamente la información completa del cliente.<br><br> Para guardar, es necesario dar clic en “Guardar cambios”"
    }]
}

