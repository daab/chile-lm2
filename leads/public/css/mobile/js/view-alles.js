// Global variable that validates if other animation or actions are taking place
var active = false;
var columnrows;

//Data saved
var s = {
	"page":1,
	"show":10,
	"leadsinfo":{},
	"filters":{},
	"search": false,
	"fecha": 0,
	"array": [],
	"dias":[],
	"calendario": false,
	"filter_date": false,
	"filters_on": false
}

// grind data structure and Print
function launchPage(){

	columnrows =[
		{
			"type": "status",
			"name": "estado",
			"maindata": ["nombreEstado"],
			"subdata": "motivo",
			"thirddata": "fechaAgenda",
			"forthdata": "horaAgenda",
			"rol": [2,3,4,5,6],
			"css": "lead-item-e",
			"title": false,
			"filter": "filter"
		},
		{	
			"type": "default",
			"name": "name",
			"maindata": ["nombres", "apellidos"],
			"subdata": false,
			"rol": [2,3,4,5,6],
			"css": "lead-item-n",
			"title": "Nombre:",
			"filter": "search"
		},
		{	
			"type": "default",
			"name": "fecha",
			"maindata": ["created_at"],
			"subdata": false,
			"rol": [2,3,4,5,6],
			"css": "lead-item-d",
			"title": "Fecha creado: ",
			"filter": "fecha"
		},
		{	
			"type": "default",
			"name": "concesionario",
			"maindata": ["nombre_concesionario"],
			"subdata": false,
			"rol": [2,3,4,5,6],
			"css": "lead-item-a",
			"title": "Dealer: ",
			"filter": "filter"
		},
		{	
			"type": "default",
			"name": "asesor",
			"maindata": ["asesor"],
			"subdata": false,
			"rol": [2,3,4],
			"css": "lead-item-a",
			"title": "Asesor: ",
			"filter": "filter"
		},
		{	
			"type": "default",
			"name": "modelo",
			"maindata": ["modelo_actual"],
			"subdata": false,
			"rol": [2,3,4,5,6],
			"css": "lead-item-a",
			"title": "Modelo: ",
			"filter": "filter"
		},
		{	
			"type": "default",
			"name": "origen",
			"maindata": ["origen"],
			"subdata": "source",
			"rol": [2,3,4,5,6],
			"css": "lead-item-a",
			"title": "Origen: ",
			"filter": "filter"
		},
		{	
			"type": "edit",
			"name": "cta",
			"maindata": ["apcion"],
			"subdata": false,
			"rol": [2,3,4,5,6],
			"css": "lead-item-g",
			"title": "Origen: ",
			"filter": "filter"
		}
	]

	// Index for filters
	for (var i in columnrows){
		s.leadsinfo[columnrows[i]["name"]] = []
		s.filters[columnrows[i]["name"]] = false;
	}

	for (var i = 0; i<leads.length; i++) {

		for(var j in columnrows){
			if(columnrows[j]["filter"] == "filter"){
				s.leadsinfo[columnrows[j]["name"]].indexOf(leads[i][columnrows[j]["maindata"][0]]) === -1 ? s.leadsinfo[columnrows[j]["name"]].push(leads[i][columnrows[j]["maindata"][0]]) : true;
			}
		}

		if(leads[i].fechaAgenda !== null){
			var key = leads[i].fechaAgenda;
			if ( s.dias.hasOwnProperty(key) ) {
				leads[i].nombreEstado == "Agendado" ? s.dias[key]["number"] += 1 : true;
			}else{
				leads[i].nombreEstado == "Agendado" ? s.dias[key] = {"number": 1} : s.dias[key] = {"number": 0};
			}	

		}
	}

	for (var i in columnrows){
		if(columnrows[i]["filter"] == "filter"){
			s.leadsinfo[columnrows[i]["name"]].sort().push("Todos")
			for (var j = 0; j<s.leadsinfo[columnrows[i]["name"]].length; j++){
				var name = columnrows[i]["name"]
				var data = s.leadsinfo[name][j]
				var chain = "<div class='btta-"+name+" btta-filter btta-menu' data-fuu='"+data+"' data-filter='filter' data-row='"+name+"'>"+data+"</div>"
				$(".menu-board").append(chain)
			}
		}
	}

	//hacks:
	$(".header-drop").data("fee", 1).addClass("header-drop-dsktp");
	$(".menu-board").append("<div class='clearfix'></div>")

	console.log(s)
	launchView();
}


function daabgrid(columnrows, array, pag, show, rol) {
	this.data_colrow = columnrows;
	this.pag = pag; this.show = show; this.rol = rol;
	this.a = array;
	
	// FILTER ARRAY
	var filters_boxes = ""
	var title = false
	for (var i in this.data_colrow){
		var current_name = this.data_colrow[i]["name"]

		// HACK Todos a False
		if (s.filters[ current_name ] == "Todos") s.filters[ current_name ] = false;

		// Ejecutar filtros guardados
		if(s.filters[ current_name ] !== false){
			if(title == false){
				title = true;
				filters_boxes += "<h2>Filtros:</h2><br>"
			}
			var current_data = this.data_colrow[i]["maindata"][0]
			this.a = this.a.filter(function(itm){
			return itm[current_data] === s.filters[current_name]
			})
			filters_boxes += "<div class='box-result print-search clear-filter' data-filter='"+current_name+"'>"+current_name +": "+ s.filters[current_name] +"</div>"
		}
	}

	// Ejecutar filtro Search
	if(s.search !== false){
		if(title == false){
				title = true;
				filters_boxes += "<h2>Filtros:</h2><br>"
			}
		this.a = this.a.filter( function(itm){
			return itm.nombres.toLowerCase().includes(s.search.toLowerCase()) || itm.apellidos.toLowerCase().includes(s.search.toLowerCase())
		})
		filters_boxes += "<div class='box-result print-search clear-search' data-search='"+s.search+"'>Nombre: "+ s.search+"</div>"
	}

	// Ejecuta filtro Dia
	if(s.filter_date !== false){
		this.a = this.a.filter( function(itm){
			return itm.fechaAgenda === s.filter_date
		})
		if(title == false){
				title = true;
				filters_boxes += "<h2>Filtros:</h2><br>"
			}
		filters_boxes += "<div class='box-result print-search clear-dia' data-dia='"+s.filter_date+"'>Fecha: "+ s.filter_date+"</div>"
	}

	s.array = this.a;
	// Ejecutar cambio de orden
	if(s.fecha) this.a = this.a.slice().reverse()

	// Contador superior:
	//$(".item-num").html(" ("+this.a.length+")")


	this.grid_data = this.daab_printleads()
	this.content = filters_boxes + this.grid_data;
	
}

daabgrid.prototype.daab_printleads = function(){
	var full_data =""
	for (var i = (this.pag*this.show)-this.show; i < this.show*this.pag && i<this.a.length; i++) {

		// Data correction:
		if (this.a[i].estado2 == null){this.a[i].estado2 = "--"}
		if (this.a[i].fechaAgenda == null){this.a[i].fechaAgenda = "--"}
		if (this.a[i].horaAgenda == null){this.a[i].horaAgenda = "--"}
		var item = "";


		for (var r in this.data_colrow ){

			// Check if rol aplies
			var rolcheck = false;
			console.log(".----", this.data_colrow );
			console.log(this.rol);
			for (var x in this.data_colrow[r]["rol"]){
				if(this.rol == this.data_colrow[r]["rol"][x]) rolcheck = true;
				console.log(this.data_colrow[r]["rol"][x]);
			}
			if(rolcheck){

				// Sets data rows
				switch (this.data_colrow[r]["type"]) {
				case "status":
					var icon = "<div class='eicon' style='background-color:"+this.a[i].color+";'></div>";
					var css =  this.data_colrow[r]["css"];
					var thirddata = "<br><span>"+ this.a[i][this.data_colrow[r]["thirddata"]]+": "+ this.a[i][this.data_colrow[r]["forthdata"]] + "</span>"
					var data = this.a[i][this.data_colrow[r]["maindata"][0]] +" <span>"+this.a[i][this.data_colrow[r]["subdata"]]+"</span>"+thirddata;
					var data_icon = this.data_colrow[r]["name"]
					var chain = "<div class='lead-item "+css+"'>"+icon+data+"<div class='lead-ico-i' data-fuu='"+data_icon+"'></div></div>"
					item+= chain;
				break;

				case "edit":
					var css =  this.data_colrow[r]["css"]
					var chain = "<div class='lead-item "+css+"' data-url='"+this.a[i]["apciones"]+"'>Gestion</div>"
					item+= chain;
				break;
				
				default:
					var data = "";
					if (this.data_colrow[r]["title"] !== false) data += "<span>"+ this.data_colrow[r]["title"] + "</span><br>"
					for (var d in this.data_colrow[r]["maindata"]) {
						data += this.a[i][this.data_colrow[r]["maindata"][d]];
						data += " "
					}
					var data_icon = this.data_colrow[r]["name"]
					var css =  this.data_colrow[r]["css"]
					var subdata = ""; if (this.data_colrow[r]["subdata"] !== false) subdata = "<br><span>"+ this.a[i][this.data_colrow[r]["subdata"]]+"</span>";
					var chain = "<div class='lead-item "+css+"'>"+data+""+subdata+"<div class='lead-ico-i' data-fuu='"+data_icon+"'></div></div>"
					item+= chain;
				break;
				}
			}
		}
		full_data += "<div class='leads-box'>"+item+"</div>"
	}

	// Line break fix
	full_data += "<div class='clearfix'></div>"

	// Sets pager
	var num = this.a.length % this.show
	if(this.a.length > this.show){
		var n = ((this.a.length - num) / this.show)
		if (num > 0) n += 1
		var counter = 3;
		var item = ""
		if(this.pag == 1){
			for (var i = 1; i <= n; i++) {
			if(i == this.pag) item += "<div class='pags nuller pag-"+i+"' data-pag='"+i+"''>Pag "+this.pag+"/"+n+"</div>"
			if(i == this.pag+1) item +="<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			if(i == this.pag+2) item +="<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			}
		}else if(this.pag == n){
			for (var i = 1; i <= n; i++) {
			if(i == this.pag) item +="<div class='pags nuller pag-"+i+"' data-pag='"+i+"''>Pag "+this.pag+"/"+n+"</div>"
			if(i == this.pag-1) item += "<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			if(i == this.pag-2) item += "<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			}
		}else{
			for (var i = 1; i <= n; i++) {
			if(i == this.pag) item +="<div class='pags nuller pag-"+i+"' data-pag='"+i+"''>Pag "+this.pag+"/"+n+"</div>"
			if(i == this.pag-1) item +="<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			if(i == this.pag+1) item +="<div class='pags pag-"+i+"' data-pag='"+i+"''>"+i+"</div>"
			}
		}
		item += "<br><br><div class='conter'>Total: "+this.a.length+"</div>"
		full_data += "<div class='box-result box-pages'>"+item+"</div>"
	}else{
		full_data += "<div class='box-result box-pages'><br><br><div class='conter'>Total: "+this.a.length+"</div></div>"
	}

	return full_data ;
}

///FUNCIONES MENU
var menu;
function setMenu(){
	$(".header-drop, .header-title, .btta-menu, .lead-ico-i, .btta-modal").click(function(){
		if(active || $(this).hasClass("btta-current") || $(this).hasClass("header-title-null") || $(this).hasClass("nuller") || $(this).hasClass("title-nuller")  ) return;
		active = true;
		$(".header-drop").addClass("bttn-while")
		if( $(this).hasClass("btta-menu") ) $(".header-drop").data("fee", 0)
		if($(".header-drop").data("fee")){
			menu = $(this).data("fuu")
			if( $(this).hasClass("header-title") || $(this).hasClass("lead-ico-i") ) $(".btta-homemenu").addClass("nuller")
			$(".header-bg, #ico-exit").css({"display": "block"})
			$(".btta-"+menu).addClass("btta-fade")
			$(".header-title").addClass("title-nuller")
			$("#ico-menu").css({"display": "none"})
			$(".menu-board").addClass("menu-board-active")
			$(".header-drop").data("fee", 0).removeClass("header-drop-dsktp")
			$(".wrapper").addClass("wrapper-null")
			$(".btta-modal").css({"display": "block"})
			}else{
				s.page = 1;
				if( $(this).hasClass("btta-filter")){
					var filter = $(this).data("filter")
					switch (filter) {
						case "filter":
							var row = $(this).data("row")
							var data = $(this).data("fuu")
							s.filters[row] = data;
						break;
					}
					launchView();

				}else if($(this).hasClass("btta-name") ){
					s.search = $("#searcher").val();
					if (s.search.replace(/^\s+|\s+$/g, "").length = 0){
						s.search = false;
					}
					launchView();

				}else if( $(this).hasClass("btta-fecha") ){
					s.fecha = Number( $(this).data("fuu") )
					$(".btta-fecha").removeClass("btta-current")
					$(this).addClass("btta-current")
					launchView();

				}else if($(this).hasClass("btta-homemenu")){
					url = $(this).data("url");
					window.open(url,"_self")

				}else if($(this).hasClass("btta-mes")){
					var mes = $(this).data("mes")
					var anho = $(this).data("anho")
					botonMes(mes,anho)
				}
				
				$(".btta-homemenu").removeClass("nuller")
				$(".header-title").removeClass("title-nuller")
				$(".header-bg, #ico-exit").css({"display": "none"})
				$(".btta-"+menu).removeClass("btta-fade")
				$("#ico-menu").css({"display": "block"})
				$(".menu-board").removeClass("menu-board-active")
				$(".header-drop").data("fee", 1).addClass("header-drop-dsktp")
				$(".wrapper").removeClass("wrapper-null")
				$(".btta-modal").css({"display": "none"})
			}
			setTimeout(function(){
				headerDropEnd($(".header-drop"))
				
			}, 500)

	})
}

var output_day, output_month;

function launchView(){
	s.calendario = calendar;
	s.filters_on = false;
	for (var i in columnrows){
		if(s.filters[columnrows[i]["name"]] !== false) s.filters_on = true
	}
	if(s.search !== false  || s.filter_date !== false) s.filters_on = true

	if (s.calendario == true && s.filters_on !== true ){
		printCalendar()
		return;
	}

	var blade = es_all_blade;
	if(blade){
		var d = new Date(),mes_n = d.getMonth(),anho_y = d.getFullYear();
	    var array_meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre','Noviembre', 'Diciembre']
	    
	    $(".header-title").html("<div class='drop-title'>"+array_meses[(current_date[0]-1)] + " - "+ current_date[1])+"</div>"
	  
	    var rate_mes = 12
	    mes_n ++
	    $(".btta-mes").remove();
	    for (var i = 1; i<=rate_mes; i++) {
			mes_n -= 1
			if(mes_n < 0) {mes_n = 11; anho_y --}
			$(".menu-board").append("<div class='btta-menu btta-mes id-fecha-"+i+"'>"+array_meses[mes_n]+" - "+anho_y+"</div>")
			$(".id-fecha-"+i).data("mes", (mes_n +1))
			$(".id-fecha-"+i).data("anho", anho_y)
		}
	}

	var d = new Date();
	d.setDate(d.getDate() + 1);
	var month = d.getMonth()+1;
	var day = d.getDate();
	output_day = day;
	output_month = month;
	console.log(leads)
	var newgrid = new daabgrid(columnrows,leads, s.page, s.show, rol)
	$(".print-leads").html(newgrid.content)
	if(rol == 5) $(".lead-item").addClass("lead-dsktp")

	//New page action
	$(".pags").click(function(){
		$t = $(this)
		if($t.hasClass("nuller")) return;
		s.page = Number( $t.data("pag") )
		$('html, body').animate({scrollTop:0}, 0);
		launchView()
	})

	$(".clear-filter").click(function(){
		s.pag = 1
		var filter = $(this).data("filter")
		s.filters[filter] = false;
		launchView()	
	})

	$(".clear-search").click(function(){
		s.pag = 1
		s.search = false;
		launchView()	
	})

	$(".clear-dia").click(function(){
		s.pag = 1
		s.filter_date = false;
		launchView()
	})

	$(".lead-item-g").click(function(){
        if( $(this).hasClass("nuller") ) return;
		url = $(this).data("url") 
		window.open(url,"_self")
	})

	setMenu();
}

function headerDropEnd(a){
	a.removeClass("bttn-while")
	active=false;
}

function modal(a){
	if(!a){
		$(".modal").css({"display": "block"})
		}else{
		$(".modal").css({"display": "none"})
	}
}

$( document ).ajaxStart(function() {
	 modal(0)
})


















var ref_mes = 0;
var selected_date;

function Calendario($div, array){
	this.$div = $div;
	this.arrayLeads = array;
	this.$date = new Date();
	this.mes = ( this.$date.getMonth() ) + ref_mes;
	if (this.mes < 0) this.mes = this.mes + 12
	this.current_mes = ( this.$date.getMonth() ) 
	this.year = ( this.$date.getFullYear() );
	this.curryear = this.CurrYear();
	this.dias = new Date(this.year, this.mes+1, 0).getDate();
	this.primero = new Date(this.year, this.mes, 1).getDay();
	this.meses = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre','Noviembre', 'Diciembre', 'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre','Noviembre', 'Diciembre'];
	this.dnombre = ['D', 'L', 'M', 'M', 'J', 'V', 'S']
	

	this.currday = this.$date.getDate();
	this.nextFunction = function(){ if(ref_mes < 2) {return "<div class='arrow_cal id_next'></div>"}else{ return ""} };
	this.backFunction = function(){ if(ref_mes > -2) {return "<div class='arrow_cal id_back'></div>"}else{ return ""} };
	this.titulo = "<div class='cal-header'>"+this.backFunction()+"<div class='title_cal'><h3>" + this.meses[this.mes] + " - " + this.curryear + "</h3></div>"+this.nextFunction()+"</div>";
        this.dock = "<div class='cal-view'>Lista</div>";
	
	this.list = this.printDays()
}

Calendario.prototype.printDays = function () {
	var ds = "";
	for (var i = 0; i <= 6; i++) {
		ds += "<div class='day_cal day_title'>"+this.dnombre[i]+"</div>"
	}
	for (var i = 0; i < this.primero; i++) {
		ds += "<div class='day_cal'>&nbsp;</div>"
	}
	for (var i = 1; i <= this.dias; i++) {
		var day = i; if(i < 10) day = "0"+ i;
		var mm = this.mes +1; if((this.mes +1) < 10) mm = "0"+ mm;
		var agendas = 0;
		var hasData = false;

		var key = (this.curryear+"-"+mm+"-"+day)
		if ( s.dias.hasOwnProperty(key) ){
			hasData = true
			agendas = s.dias[key]["number"]
		}

		var n = ""; if (hasData) n = "day_cerrado"; if (agendas > 0) n = "day_agenda"; if (i < this.currday && agendas !== 0 && this.mes == this.current_mes || agendas !== 0 && this.mes < this.current_mes) n = "day_agenda_lost"
		ds += "<div class='day_cal day_id-"+i+" "+n+"' data-d='"+i+"' data-m='"+this.mes+"' data-y='"+this.curryear+"'>" + day +"</div>";
	}
	console.log(this.mes, this.current_mes)
	return ds;
}

Calendario.prototype.CurrYear = function () {
	if(ref_mes >= 11) this.year++
	if( (( this.$date.getMonth() ) + ref_mes) < 0 ) this.year --
	return this.year;
}

Calendario.prototype.setCbuttoms = function () {
	$(".id_next").click( function(){
		ref_mes++;
		printCalendar()
	})

	$(".id_back").click( function(){
		ref_mes--;
		printCalendar()
	})

	$(".day_agenda, .day_agenda_lost, .day_cerrado").click(function(){
		if( $(this).hasClass("day_title") || $(this).hasClass("nuller") ) return;
		$(".day_agenda").removeClass("day_select")
		$(this).addClass("day_select")
		var month_fix = $(this).data("m") +1
		var day_fix = $(this).data("d")
		if(month_fix > 12) month_fix = month_fix - 12
		if(month_fix < 10) month_fix = "0" + month_fix
		if(day_fix < 10) day_fix = "0" + day_fix
		var selected_date = $(this).data("y") + "-" + month_fix + "-" + day_fix
		s.filter_date = selected_date
		launchView();
	})

    $(".cal-view").click(function(){
		if( $(this).hasClass("nuller") ) return;
		s.filters.estado = "Agendado"
		launchView();
	})

}

function printCalendar(){
	var mes_view = new Calendario($(".print-leads"), s.dias)
	$(mes_view.$div).html("")
    $(mes_view.$div).append(mes_view.dock)
	$(mes_view.$div).append(mes_view.titulo)
	$(mes_view.$div).append(mes_view.list)
	$(mes_view.$div).append(mes_view.next)
	$(mes_view.$div).append(mes_view.back)
    //$(".item-num").html(" ("+leads.length+")")
	
	mes_view.setCbuttoms()
	setMenu();	
}
