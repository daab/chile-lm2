function barra_a(id, array, title){
	this.id = id;
	this.array = array;
	this.title = title;
	this.range = 100;
	this.x_length = this.array.length;
	
	this.total = 0;
	this.sum = 0;
	for (var i in this.array) {
		var num = Number( this.array[i]["val"] )
		if(this.total < num )this.total = num
		this.sum += num
	}
	
	this.print = "<div class='es-item es-item-t col1'>"+this.title+"</div>";
	this.items = "";
	
	for (var i in this.array) {
		var w = this.range / this.array.length;
		var l = (this.range / this.array.length) * (i);
		var y = Math.floor( 100 - ( (this.array[i]["val"] * 100) / this.total ) )
		var c = this.array[i]["color"]
		this.items += "<div class='barra_a' style='background-color:"+c+";top:"+y+"%; width:"+w+"%; left:"+l+"%'></div>";
	}
	this.print += "<div class='es-item es-item-p es-item-p2 col1'> <div class='barras_box'>"+this.items+"</div> </div>";

	for (var i in this.array) {
		var c = this.array[i]["color"]
		var t = this.array[i]["title"]
		var p =  (this.array[i]["val"] * 100) / this.sum 
		p = Math.round( p * 100) / 100
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+c+";'></div> "+p+"% "+t+"</div>";
	}

	this.print += "<div class='es-item es-item-x col1'></div>";
	this.print  = "<div class='es-item-box col1'>"+this.print+"</div>"

}


function ponque_c(id, titulo1, titulo2, titulo3, val1, val2, val3, color1, color2, color3, titlo4, titulo5){
		this.val1 = Number(val1);
		this.val2 = Number(val2);
		this.val3 = Number(val3);
		this.title = titlo4
		this.title5 = titulo5;
		this.base = val1 + val2 + val3;
		this.titulo1 = titulo1;
		this.titulo2 = titulo2;
		this.titulo3 = titulo3;
		this.porcentage1 = Math.floor( ( 100 * (this.val1) ) / this.base);
		this.porcentage2 = Math.floor( ( 100 * (this.val2) ) / this.base);
		this.porcentage3 = Math.floor( ( 100 * (this.val3) ) / this.base);
		this.porcentage_1_2 = this.porcentage2 + this.porcentage3;
		this.grados1 = this.calculate1();
		this.grados2 = this.calculate2();
		this.id = "pie_id_" + id;
		this.color1 = color1;
		this.color2 = color2;
		this.color3 = color3;
		this.print = "<div class='es-item es-item-t col1'>"+this.title+"</div>";
		if(!this.title) this.print = "<div class='es-item es-item-t col1'>"+this.titulo1+" ("+this.val1+") / "+this.titulo2+" ("+this.val2+") / "+this.titulo3+" ("+this.val3+")</div>";
		console.log(this.title5)
		if(this.title5 !== false) this.print = "<div class='es-item es-item-t col1'>"+this.title5+":  "+ Number(this.base) +"</div>";
		this.items = "<li class='c1_r c1_id_"+this.id+"'><p><span class='pie_left'></span></p></li>"
		this.items += "<li class='c1_l c1_id_"+this.id+"'><p><span class='pie_right'></span></p></li>"
		this.items += "<li class='c2_r c2_id_"+this.id+"'><p><span class='pie_left'></span></p></li>"
		this.items += "<li class='c2_l c2_id_"+this.id+"'><p><span class='pie_right'></span></p></li>"
		this.items += "<li class='c3_r c3_id_"+this.id+"'><p><span class='pie_left'></span></p></li>"
		this.items += "<li class='c3_l c3_id_"+this.id+"'><p><span class='pie_right'></span></p></li>"
		this.print += "<div class='es-item es-item-p es-item-p2 col1'><div class='pie_chart pie_id_"+this.id+"'><ul>"+this.items+"</ul></div></div>";
		
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+this.color1+";'></div>"+this.val1+" "+this.titulo1+" ("+(this.porcentage1)+"%) </div>";
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+this.color2+";'></div>"+this.val2+" "+this.titulo2+" ("+(this.porcentage2)+"%) </div>";
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+this.color3+";'></div>"+this.val3+" "+this.titulo3+" ("+(this.porcentage3)+"%) </div>";
		this.print += "<div class='es-item es-item-x col1'></div>";
		this.print  = "<div class='es-item-box col1'>"+this.print+"</div>"
	}

	ponque_c.prototype.calculate1 = function () {
		var g = 0;
		if (this.porcentage1 >= 100){g = 360}else{g = (this.porcentage3* 360)/100}
		g = 360 - (Math.floor(g))
		if(g < 180){g = {"right": 180, "left": 180 - g}}else{
			g = {"right": 180- (g-180), "left": 0}	
		}
		return g
	}

	ponque_c.prototype.calculate2 = function () {
		var g = 0;
		if (this.porcentage_1_2 > 100){g = 360}else{g = (this.porcentage_1_2* 360)/100}
		g = 360 - (Math.floor(g))
		console.log(g)
		if(g < 180){g = {"right": 180, "left": 180 - g}}else{
			g = {"right": 180- (g-180), "left": 0}	
		}
		return g
	}

	ponque_c.prototype.set = function () {
		$(".pie_id_"+this.id+" ul :nth-child(3) p, ."+this.id+" ul :nth-child(3) p").css({
				"-webkit-transform":"rotate("+this.grados2["right"]+"deg)",
			    "-moz-transform": "rotate("+this.grados2["right"]+"deg)",
			    "-o-transform": "rotate("+this.grados2["right"]+"deg)",
			    "-ms-transform": "rotate("+this.grados2["right"]+"deg)"
		})	

		$(".pie_id_"+this.id+" ul :nth-child(4) p, ."+this.id+" ul :nth-child(4) p").css({
				"-webkit-transform":"rotate("+this.grados2["left"]+"deg)",
			    "-moz-transform": "rotate("+this.grados2["left"]+"deg)",
			    "-o-transform": "rotate("+this.grados2["left"]+"deg)",
			    "-ms-transform": "rotate("+this.grados2["left"]+"deg)"
		})

		$(".pie_id_"+this.id+" ul :nth-child(5) p, ."+this.id+" ul :nth-child(5) p").css({
				"-webkit-transform":"rotate("+this.grados1["right"]+"deg)",
			    "-moz-transform": "rotate("+this.grados1["right"]+"deg)",
			    "-o-transform": "rotate("+this.grados1["right"]+"deg)",
			    "-ms-transform": "rotate("+this.grados1["right"]+"deg)"
		})	

		$(".pie_id_"+this.id+" ul :nth-child(6) p, ."+this.id+" ul :nth-child(6) p").css({
				"-webkit-transform":"rotate("+this.grados1["left"]+"deg)",
			    "-moz-transform": "rotate("+this.grados1["left"]+"deg)",
			    "-o-transform": "rotate("+this.grados1["left"]+"deg)",
			    "-ms-transform": "rotate("+this.grados1["left"]+"deg)"
		})

		$(".c1_id_"+this.id+" span").css({"background-color": this.color1})	
		$(".c2_id_"+this.id+" span").css({"background-color": this.color2})	
		$(".c3_id_"+this.id+" span").css({"background-color": this.color3})	
		
	}

function ponque_d(id, titulo1,  titulo3, val1,  val3, color1,  color3){
		this.val1 = Number(val1);
		this.val3 = Number(val3);
		this.base = val1 + val3;
		this.titulo1 = titulo1;
		this.titulo3 = titulo3;
		this.porcentage1 = Math.floor( ( 100 * (this.val1) ) / this.base);
		this.porcentage3 = Math.floor( ( 100 * (this.val3) ) / this.base);
		this.grados1 = this.calculate1();
		this.id = "pie_id_" + id;
		this.color1 = color1;
		this.color3 = color3;
		this.print = "<div class='es-item es-item-t col1'>"+this.titulo1+" ("+this.val1+") / "+this.titulo3+" ("+this.val3+")</div>";
		this.items = "<li class='c1_r c1_id_"+this.id+"'><p><span class='pie_left'></span></p></li>"
		this.items += "<li class='c1_l c1_id_"+this.id+"'><p><span class='pie_right'></span></p></li>"
		this.items += "<li class='c3_r c3_id_"+this.id+"'><p><span class='pie_left'></span></p></li>"
		this.items += "<li class='c3_l c3_id_"+this.id+"'><p><span class='pie_right'></span></p></li>"
		this.print += "<div class='es-item es-item-p es-item-p2 col1'><div class='pie_chart pie_id_"+this.id+"'><ul>"+this.items+"</ul></div></div>";
		
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+this.color3+";'></div> "+(this.porcentage3)+"% "+this.titulo3+"</div>";
		this.print += "<div class='es-item es-item-p col1'><div class='eicon' style='background-color:"+this.color1+";'></div> "+(this.porcentage1)+"% "+this.titulo1+"</div>";
		
		this.print += "<div class='es-item es-item-x col1'></div>";
		this.print  = "<div class='es-item-box col1'>"+this.print+"</div>"
	}

	ponque_d.prototype.calculate1 = function () {
		var g = 0;
		if (this.porcentage1 >= 100){g = 360}else{g = (this.porcentage1* 360)/100}
		g = 360 - (Math.floor(g))
		console.log(g)
		if(g < 180){g = {"right": 180, "left": 180 - g}}else{
			g = {"right": 180- (g-180), "left": 0}	
		}
		return g
	}

	ponque_d.prototype.calculate2 = function () {
		var g = 0;
		if (this.porcentage3 > 100){g = 360}else{g = (this.porcentage3* 360)/100}
		g = 360 - (Math.floor(g))
		console.log(g)
		if(g < 180){g = {"right": 180, "left": 180 - g}}else{
			g = {"right": 180- (g-180), "left": 0}	
		}
		return g
	}

	ponque_d.prototype.set = function () {
		$(".pie_id_"+this.id+" ul :nth-child(3) p, ."+this.id+" ul :nth-child(3) p").css({
				"-webkit-transform":"rotate("+this.grados1["right"]+"deg)",
			    "-moz-transform": "rotate("+this.grados1["right"]+"deg)",
			    "-o-transform": "rotate("+this.grados1["right"]+"deg)",
			    "-ms-transform": "rotate("+this.grados1["right"]+"deg)"
		})	

		$(".pie_id_"+this.id+" ul :nth-child(4) p, ."+this.id+" ul :nth-child(4) p").css({
				"-webkit-transform":"rotate("+this.grados1["left"]+"deg)",
			    "-moz-transform": "rotate("+this.grados1["left"]+"deg)",
			    "-o-transform": "rotate("+this.grados1["left"]+"deg)",
			    "-ms-transform": "rotate("+this.grados1["left"]+"deg)"
		})


		$(".c1_id_"+this.id+" span").css({"background-color": this.color3})	
		$(".c3_id_"+this.id+" span").css({"background-color": this.color1})	
		
	}

function funnel_c(id, array, title){
	this.id = id;
	this.array = array;
	this.title = title;
	this.range = 100;
	this.x_length = this.array.length;
	this.base = Number( this.array[0]["val"] )
	this.total = 0;
	this.sum = 0;
	for (var i in this.array) {
		var num = Number( this.array[i]["val"] )
		if(this.total < num )this.total = num
		this.sum += num
	}
	
	this.print = "<div class='es-item es-item-t col1'>"+this.title+"</div>";
	this.items = "";
	
	for (var i in this.array) {
		var w = this.range / this.array.length;
		var l = (this.range / this.array.length) * (i);
		var y = Math.floor( 100 - ( (this.array[i]["val"] * 100) / this.total ) ); /* if (y > 99) y = 98;*/
		var c = this.array[i]["color"]
		this.items += "<div class='barra_f' style='background-color:"+c+";top:"+y+"%; bottom:0; width:"+w+"%; left:"+l+"%'></div>";
	}
	this.print += "<div class='es-item es-item-p es-item-p2 col1'> <div class='barras_box'>"+this.items+"</div> </div>";

	this.holder = ""

	for (var i in this.array) {
		var w = (this.range - 1) / this.array.length;
		var c = this.array[i]["color"]
		var t = this.array[i]["title"]
		var p =  (this.array[i]["val"] * 100) / this.sum 
		p = Math.round( p * 100) / 100
		this.holder +="<div class='es-item-pp' style='width:"+w+"%;'>"+t+" </br>("+this.array[i]["val"]+")</div>"
	}

	this.holder2 = ""

	for (var i in this.array) {
		var w = (this.range - 1) / this.array.length;
		var c = this.array[i]["color"]
		var t = this.array[i]["title"]
		var p =  (this.array[i]["val"] * 100) / this.sum 
		p = Math.round( p * 100) / 100
		var endValue =  (this.array[i]["val"] * 100) / Number( this.array[this.array[i]["sobre"]]["val"] )
		var endValue =  Math.round( endValue * 100) / 100
		if (i == 0) endValue = "Base "
		this.holder2 +="<div class='es-item-pp' style='width:"+w+"%;'>"+endValue+"% </br>"+this.array[i]["titulobase"]+"</div>"
	}
	this.print += "<div class='es-item es-item-p col1'>"+this.holder+"</div>";
	this.print += "<div class='es-item es-item-p col1'>"+this.holder2+"</div>";
	this.print += "<div class='es-item es-item-x col1'></div>";
	this.print  = "<div class='es-item-box col1'>"+this.print+"</div>"

}